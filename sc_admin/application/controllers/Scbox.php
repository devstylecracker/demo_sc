<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Scbox extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Pa_model');
		$this->load->model('Scbox_model');
		$this->load->library('user_agent');
		$this->load->library('email');
		$this->load->library('form_validation');
	}

	public function scbox_booking(){
		$data = array();
		if($this->input->post()){
			// echo "<pre>";print_r($_POST);exit;
			$SCBOX_PACKAGE = unserialize(SCBOX_PACKAGE);
			
			$box_data = array();$data = array();
			$data['scbox_package'] = $this->input->post('scbox_package');
			$data['box_price'] = $this->input->post('box_price');
			$data['scb_gender'] = $this->input->post('scb_gender');
			$data['f_name'] = ucwords(strtolower($this->input->post('f_name')));
			$data['l_name'] = ucwords(strtolower($this->input->post('l_name')));
			$data['mobile_no'] = $this->input->post('mobile_no');
			$data['email'] = $this->input->post('email');
			
			
			
			$box_data['user_name'] = $data['f_name'].' '.$data['l_name'];
			$box_data['user_gender'] = $data['scb_gender'];
			$box_data['user_emailid'] = $data['email'];
			$box_data['user_dob'] = $this->input->post('date_from');
			$box_data['user_mobile'] = $data['mobile_no'];
			$box_data['user_shipaddress'] = $this->input->post('address');
			$box_data['user_pincode'] = $this->input->post('pincode');
			$getpincodedata = $this->Scbox_model->getpincodedata($box_data['user_pincode']);
			$box_data['user_state'] = @$getpincodedata['id'];
			$box_data['user_city'] = @$getpincodedata['city'];
			$box_data['user_likes'] = $this->input->post('user_like');
			$box_data['user_dislikes'] = $this->input->post('user_dislike');
			foreach($SCBOX_PACKAGE as $val){
				if($val->id == $data['scbox_package'] && $data['scbox_package'] != '4'){
					$box_data['scbox_pack'] = $val->name;
					$box_data['scbox_price'] = $val->price;
					$box_data['scbox_productid'] = $val->productid;
					$box_data['scbox_objectid'] = $val->id;
				}else if($val->id == $data['scbox_package'] && $data['scbox_package'] == '4'){
					$box_data['scbox_pack'] = $val->name;
					$box_data['scbox_price'] = $data['box_price'];
					$box_data['scbox_productid'] = $val->productid;
					$box_data['scbox_objectid'] = $val->id;
				}
			}
			if($data['scb_gender'] == '1'){
				
				$box_data['user_body_shape'] = $this->input->post('user_body_shape_women');
				$box_data['user_style_p1'] = $this->input->post('user_style_p1_women');
				$box_data['user_style_p2'] = $this->input->post('user_style_p2_women');
				$box_data['user_style_p3'] = $this->input->post('user_style_p3_women');
				$box_data['user_skintone'] = $this->input->post('user_skintone_women');
				$box_data['user_colors'] = $this->input->post('user_colors_women');
				$box_data['user_prints'] = $this->input->post('user_prints_women');
				$box_data['user_sizetop'] = $this->input->post('user_sizetop_women');
				$box_data['user_sizebottom'] = $this->input->post('user_sizebottom_women');
				$box_data['user_sizefoot'] = $this->input->post('user_sizefoot_women');
				$box_data['user_sizeband'] = $this->input->post('user_sizeband_women');
				$box_data['user_sizecup'] = $this->input->post('user_sizecup_women');
				
			}else if($data['scb_gender'] == '2'){
				
				$box_data['user_body_shape'] = $this->input->post('user_body_shape_men');
				$box_data['user_style_p1'] = $this->input->post('user_style_p1_men');
				$box_data['user_style_p2'] = $this->input->post('user_style_p2_men');
				$box_data['user_style_p3'] = $this->input->post('user_style_p3_men');
				$box_data['user_skintone'] = $this->input->post('user_skintone_men');
				$box_data['user_colors'] = $this->input->post('user_colors_men');
				$box_data['user_prints'] = $this->input->post('user_prints_men');
				$box_data['user_sizetop'] = $this->input->post('user_sizetop_men');
				$box_data['user_sizebottom'] = $this->input->post('user_sizebottom_men');
				$box_data['user_sizefoot'] = $this->input->post('user_sizefoot_men');
				
			}
			
			$box_data['user_colors'] = implode(',',$box_data['user_colors']);
			$box_data['user_prints'] = implode(',',$box_data['user_prints']);
			$box_data['user_profession'] = $this->input->post('user_profession');
			$box_data['user_occassion'] = $this->input->post('user_occassion');
			$box_data['user_occassion_text'] = $this->input->post('user_occassion_text');
			$box_data['user_wardrobe'] = $this->input->post('user_wardrobe');
			$box_data['user_willtoexpmnt'] = $this->input->post('user_willtoexpmnt');
			$box_data['scbox_brand_list'] = $this->input->post('scbox_brand');
			// echo "<pre>";print_r($box_data);exit;
			$birthdate = $box_data['user_dob'];
			$sc_gender = $data['scb_gender'];
			$scusername = $data['f_name'].$data['l_name'];
			$scemail_id = $data['email'];
			$scpassword = 'scbox-'.$data['f_name'];
			$ip = $this->input->ip_address();
			$json_array['firstname'] = $data['f_name'];
			$json_array['lastname'] = $data['l_name'];
			$json_array['age_range'] = $this->Scbox_model->get_userage_range($birthdate,$sc_gender);
			$json_array['scmobile'] = $data['mobile_no'];
			$json_array['question_id'] = $sc_gender==1 ? 8:17;
			$user_id = $this->signup('website',$scusername,$scemail_id,$scpassword,$ip,2,$sc_gender,$json_array);
			$box_data['user_id'] = $user_id;
			foreach($box_data as $key=>$val){
				$this->Scbox_model->save_scboxuser_answer($key,$val,$user_id,$box_data['scbox_objectid'],$key);
			}
			redirect('websiteusers/scbox');
		}else{
			//common data
			$data['SCBOX_SKINTONE_LIST'] = unserialize(SCBOX_SKINTONE_LIST);
			$data['SCBOX_COLOR_LIST'] = unserialize(SCBOX_COLOR_LIST);
			$data['SCBOX_PRINT_LIST'] = unserialize(SCBOX_PRINT_LIST);
			
			// men data
			$data['men_body_shape'] = $this->Pa_model->load_questAns(24);
			$data['men_style'] = $this->Pa_model->load_questAns(25);
			$data['SCBOX_MENTOP_LIST'] = $this->Pa_model->get_size_data('mentopsize');
			$data['SCBOX_MENBOTTOM_LIST'] = $this->Pa_model->get_size_data('menbottomsize');
			$data['SCBOX_MENFOOT_LIST'] = $this->Pa_model->get_size_data('menfootsize');
			
			//women data 
			$data['women_body_shape'] = $this->Pa_model->load_questAns(1);
			$data['women_style'] = $this->Pa_model->load_questAns(2);
			$data['SCBOX_WOMENTOP_LIST'] = $this->Pa_model->get_size_data('womentopsize');
			$data['SCBOX_WOMENBOTTOM_LIST'] = $this->Pa_model->get_size_data('womenbottomsize');
			$data['SCBOX_WOMENFOOT_LIST'] = $this->Pa_model->get_size_data('womenfootsize');
			$data['SCBOX_BANDSIZE_LIST'] = $this->Pa_model->get_size_data('womenbandsize');
			$data['SCBOX_CUPSIZE_LIST'] = $this->Pa_model->get_size_data('womencupsize');
			$data['form_type'] = 'add';
			// echo "<pre>";print_r($data);exit;
			$this->load->view('common/header');
			$this->load->view('scbox/scbox_booking',$data);
			$this->load->view('common/footer');
		}
		
	}
	
	public function signup($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender, $json_array = array()){
		
      $scusername = $scusername.date('Y-m-d h:i:s');
      $res = $this->Scbox_model->registerUser($medium,$scusername,$scemail_id,$scpassword,$ip,$role,$sc_gender,$json_array);
     
      if($scemail_id!='')
      {
        $this->Scbox_model->update_couponcode($scemail_id,'Welcome10');
      }
      if($res){ return $res; }else{ return '0'; }
	  
	}
	
	public function scbox_edit(){
		$data = array();
		$user_id = $this->uri->segment(3);
		$SCBOX_PACKAGE = unserialize(SCBOX_PACKAGE);
		if($this->input->post()){
			// echo "<pre>";print_r($_POST);exit;
			$box_data = array();$data = array();
			
			$data['scbox_package'] = $this->input->post('scbox_package');
			$data['box_price'] = $this->input->post('box_price');
			$data['scb_gender'] = $this->input->post('scb_gender');
			$data['f_name'] = ucwords(strtolower($this->input->post('f_name')));
			$data['l_name'] = ucwords(strtolower($this->input->post('l_name')));
			$data['mobile_no'] = $this->input->post('mobile_no');
			$data['email'] = $this->input->post('email');
			
			$box_data['user_name'] = $data['f_name'].' '.$data['l_name'];
			$box_data['user_gender'] = $data['scb_gender'];
			$box_data['user_emailid'] = $data['email'];
			$box_data['user_dob'] = $this->input->post('date_from');
			$box_data['user_mobile'] = $data['mobile_no'];
			$box_data['user_shipaddress'] = $this->input->post('address');
			$box_data['user_pincode'] = $this->input->post('pincode');
			$getpincodedata = $this->Scbox_model->getpincodedata($box_data['user_pincode']);
			$box_data['user_state'] = @$getpincodedata['id'];
			$box_data['user_city'] = @$getpincodedata['city'];
			$box_data['user_likes'] = $this->input->post('user_like');
			$box_data['user_dislikes'] = $this->input->post('user_dislike');
			foreach($SCBOX_PACKAGE as $val){
				if($val->id == $data['scbox_package'] && $data['scbox_package'] != '4'){
					$box_data['scbox_pack'] = $val->name;
					$box_data['scbox_price'] = $val->price;
					$box_data['scbox_productid'] = $val->productid;
					$box_data['scbox_objectid'] = $val->id;
				}else if($val->id == $data['scbox_package'] && $data['scbox_package'] == '4'){
					$box_data['scbox_pack'] = $val->name;
					$box_data['scbox_price'] = $data['box_price'];
					$box_data['scbox_productid'] = $val->productid;
					$box_data['scbox_objectid'] = $val->id;
				}
			}
			if($data['scb_gender'] == '1'){
				
				$box_data['user_body_shape'] = $this->input->post('user_body_shape_women');
				$box_data['user_style_p1'] = $this->input->post('user_style_p1_women');
				$box_data['user_style_p2'] = $this->input->post('user_style_p2_women');
				$box_data['user_style_p3'] = $this->input->post('user_style_p3_women');
				$box_data['user_skintone'] = $this->input->post('user_skintone_women');
				$box_data['user_colors'] = $this->input->post('user_colors_women');
				$box_data['user_prints'] = $this->input->post('user_prints_women');
				$box_data['user_sizetop'] = $this->input->post('user_sizetop_women');
				$box_data['user_sizebottom'] = $this->input->post('user_sizebottom_women');
				$box_data['user_sizefoot'] = $this->input->post('user_sizefoot_women');
				$box_data['user_sizeband'] = $this->input->post('user_sizeband_women');
				$box_data['user_sizecup'] = $this->input->post('user_sizecup_women');
				
			}else if($data['scb_gender'] == '2'){
				
				$box_data['user_body_shape'] = $this->input->post('user_body_shape_men');
				$box_data['user_style_p1'] = $this->input->post('user_style_p1_men');
				$box_data['user_style_p2'] = $this->input->post('user_style_p2_men');
				$box_data['user_style_p3'] = $this->input->post('user_style_p3_men');
				$box_data['user_skintone'] = $this->input->post('user_skintone_men');
				$box_data['user_colors'] = $this->input->post('user_colors_men');
				$box_data['user_prints'] = $this->input->post('user_prints_men');
				$box_data['user_sizetop'] = $this->input->post('user_sizetop_men');
				$box_data['user_sizebottom'] = $this->input->post('user_sizebottom_men');
				$box_data['user_sizefoot'] = $this->input->post('user_sizefoot_men');
				
			}
			$box_data['user_colors'] = implode(',',$box_data['user_colors']);
			$box_data['user_prints'] = implode(',',$box_data['user_prints']);
			$box_data['user_profession'] = $this->input->post('user_profession');
			$box_data['user_occassion'] = $this->input->post('user_occassion');
			$box_data['user_occassion_text'] = $this->input->post('user_occassion_text');
			$box_data['user_wardrobe'] = $this->input->post('user_wardrobe');
			$box_data['user_willtoexpmnt'] = $this->input->post('user_willtoexpmnt');
			$box_data['scbox_brand_list'] = $this->input->post('scbox_brand');
			$box_data['user_id'] = $user_id;
			foreach($box_data as $key=>$val){
				$this->Scbox_model->save_scboxuser_answer($key,$val,$user_id,$box_data['scbox_objectid'],$key);
			}
			$this->Scbox_model->update_user_info($data['f_name'],$data['l_name'],$user_id);
			// redirect('scbox/scbox_edit/'.$user_id);
			redirect('stylist/user_view/'.$user_id);
		}else if($user_id > 0){
			$data['user_info'] = $this->Scbox_model->get_user_info($user_id);
			$data['user_data'] = $this->Scbox_model->get_scbox_userdata($user_id);
			//common data
			$data['SCBOX_SKINTONE_LIST'] = unserialize(SCBOX_SKINTONE_LIST);
			$data['SCBOX_COLOR_LIST'] = unserialize(SCBOX_COLOR_LIST);
			$data['SCBOX_PRINT_LIST'] = unserialize(SCBOX_PRINT_LIST);

			// men data
			$data['men_body_shape'] = $this->Pa_model->load_questAns(24);
			$data['men_style'] = $this->Pa_model->load_questAns(25);
			$data['SCBOX_MENTOP_LIST'] = $this->Pa_model->get_size_data('mentopsize');
			$data['SCBOX_MENBOTTOM_LIST'] = $this->Pa_model->get_size_data('menbottomsize');
			$data['SCBOX_MENFOOT_LIST'] = $this->Pa_model->get_size_data('menfootsize');

			//women data 
			$data['women_body_shape'] = $this->Pa_model->load_questAns(1);
			$data['women_style'] = $this->Pa_model->load_questAns(2);
			$data['SCBOX_WOMENTOP_LIST'] = $this->Pa_model->get_size_data('womentopsize');
			$data['SCBOX_WOMENBOTTOM_LIST'] = $this->Pa_model->get_size_data('womenbottomsize');
			$data['SCBOX_WOMENFOOT_LIST'] = $this->Pa_model->get_size_data('womenfootsize');
			$data['SCBOX_BANDSIZE_LIST'] = $this->Pa_model->get_size_data('womenbandsize');
			$data['SCBOX_CUPSIZE_LIST'] = $this->Pa_model->get_size_data('womencupsize');
			
			$data['form_type'] = 'edit';
			// echo "<pre>";print_r($data);exit;
			$this->load->view('common/header');
			$this->load->view('scbox/scbox_booking',$data);
			$this->load->view('common/footer');
		}else{
				$this->load->view('common/header',$data_post);
				$this->load->view('not_permission');
				$this->load->view('common/footer');
		}
		
	}
	
	public function scbox_manual(){
		$order_id = $this->uri->segment(3);
		$boxuser_id = $this->uri->segment(4);
		if($this->input->post()){
			// echo "<pre>";print_r($_POST);exit;
			$box_data = array();$user_box_prints = array();
			if(trim($this->uri->segment(3))!='' && trim($this->uri->segment(4))!='')
			{
				$box_data['order_id'] = trim($this->uri->segment(3));
				$box_data['user_id'] = trim($this->uri->segment(4));
			}else
			{
				$data['scbox_package'] = $this->input->post('scbox_package');
				$data['box_price'] = $this->input->post('box_price');
			}
			
			$box_data['personal_style_desc'] = trim($this->input->post('personal_style_desc'));
			$box_data['user_box_color_pref'] = trim($this->input->post('user_box_color_pref'));
			$box_data['user_box_brand_pref'] = trim($this->input->post('user_box_brand_pref'));
			$box_data['user_box_bottom_pref'] = trim($this->input->post('user_box_bottom_pref'));
			$box_data['user_box_footewear_pref'] = trim($this->input->post('user_box_footewear_pref'));
			$box_data['user_box_women_top_pref'] = trim($this->input->post('user_box_women_top_pref'));
			$box_data['user_box_men_top_pref'] = trim($this->input->post('user_box_men_top_pref'));
			$box_data['user_box_prints_pref'] = trim($this->input->post('user_box_prints_pref'));
			if(!empty($this->input->post('user_box_prints'))){ $user_box_prints = implode(',',$this->input->post('user_box_prints')); }
			$box_data['user_box_prints'] = $user_box_prints;
			$box_data['is_experimenting'] = trim($this->input->post('is_experimenting'));
			$box_data['user_dislike'] = trim($this->input->post('user_dislike'));
			$box_data['other_specifications'] = trim($this->input->post('other_specifications'));
			$box_data['height'] = trim($this->input->post('height'));
			$box_data['weight'] = trim($this->input->post('weight'));
			$user_manual_data = serialize($box_data);
			$key = 'user_manual_data';
			$result = $this->Scbox_model->save_scboxuser_answer($user_manual_data,$box_data['user_id'],$box_data['order_id'],$key);
			if($result==1)
			{
				$this->session->set_flashdata('feedback_scbox', '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record Added Successfully</div>');
				$data['response'] = 'success';
				$data['user_manual_data'] = $this->Scbox_model->get_scbox_data($order_id,$key);							
				$this->sendMail($data['user_manual_data']);
				redirect('scbox/scbox_manual/'.$box_data['order_id'].'/'.$box_data['user_id']);				
			}else
			{
				$this->session->set_flashdata('feedback_scbox', '<div class="alert alert-error alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5><i class="fa fa-times-circle sign"></i><strong>Error!</strong></h5>Something went wrong Please try again</div>');
				$data['response'] = 'error';
				redirect('scbox/scbox_manual/'.$box_data['order_id'].'/'.$box_data['user_id']);				
			}
			//redirect('manage_orders');
		}else{
			//$order_id = $this->uri->segment(3);
			$key = 'user_manual_data';
			$data['user_manual_data'] = $this->Scbox_model->get_scbox_data($boxuser_id,$key,$order_id);
			$this->load->view('common/header');
			$this->load->view('scbox/scbox_booking_manual',$data);
			$this->load->view('common/footer');
		}
	}

	public function sendMail($data)
	{		
		$message = '';	
		$order_no = '';
		if(!empty($data))
		{	
		foreach($data as $key=>$val)
		{
			if($key == 'order_id')
			{
				$message = $message.'<b>Order Id</b> : '.$val.'<br/>';
				$order_no = $val;	
				
			}else if($key == 'user_box_bottom_pref')
			{
				if($val==1)
				{
					$message = $message.'<b>Bottom</b> : Dress<br/>';
				}else if($val==2)
				{
					$message = $message.'<b>Bottom</b> : Pants<br/>';
				}
				
			}else if($key == 'user_box_footewear_pref')
			{
				if($val==1)
				{
					$message = $message.'<b>Footwear</b> : Flats<br/>';
				}else if($val==2)
				{
					$message = $message.'<b>Footwear</b> : Heels<br/>';
				}
			}else if($key == 'user_box_women_top_pref')
			{
				if($val==1)
				{
					$message = $message.'<b>Women Top</b> : Blouse <br/>';
				}else if($val==2)
				{
					$message = $message.'<b>Women Top</b> : Shirt <br/>';
				}
			}else if($key == 'user_box_men_top_pref')
			{				
				if($val==1)
				{
					$message = $message.'<b>Bottom</b> : Blazer <br/>';
				}else if($val==2)
				{
					$message = $message.'<b>Bottom</b> : No Blazer <br/>';
				}
			}else if($key == 'user_box_prints_pref')
			{
				if($val==1)
				{
					$message = $message.'<b>Prints</b> : Prints <br/>';
				}else if($val==2)
				{
					$message = $message.'<b>Prints</b> : Solids <br/>';
				}

			}else if($key == 'user_box_prints')
			{
				//print_r($val);exit;
				if(!empty(@$val))
				{
					$user_box_prints = @$val;
				}else
				{
					$user_box_prints = '';
				}
				
				$message = $message.'<b>If yes to prints, which of the following would you prefer?</b> : '.$user_box_prints.' <br/>';
			}else if($key == 'is_experimenting')
			{
				if($val==1)
				{
					$message = $message.'<b>Would you be open to experimenting with your clothing?</b>  : Yes <br/>';
				}else if($val==2)
				{
					$message = $message.'<b>Would you be open to experimenting with your clothing?</b>  : No <br/>';
				}
			}else{
				$message = $message.'<b>'.$key.'</b> : '.$val.'<br/>';
			}
			
		}	

		 $logged_userid = $this->session->userdata('user_id');

		 $config['protocol'] = 'smtp';
		//      $config['mailpath'] = '/usr/sbin/sendmail';
		//     $config['charset'] = 'iso-8859-1';
		  $config['mailtype'] = 'html';
		  $config['wordwrap'] = TRUE;
		  $config['smtp_host'] = $this->config->item('smtp_host');
		  $config['smtp_user'] = $this->config->item('smtp_user');
		  $config['smtp_pass'] = $this->config->item('smtp_pass');
		  $config['smtp_port'] = $this->config->item('smtp_port');

			$this->email->initialize($config);	
			$email_id = "shalini@stylecracker.com";		
		    $email_id1 = "lavin@stylecracker.com";
			 
			$this->email->from($this->config->item('from_email'), 'StyleCracker');
		    $this->email->to($email_id);
			$this->email->cc($email_id1);
			 
		  $this->email->subject('Scbox Order Specific Data-'.$order_no.'- By -'.$logged_userid); 
			
	      $this->email->message($message);


		  if($this->email->send())
		  {
			  return "Email Sent";
		  }else
		  {
			  return "Email Error";
		  }
		}

	}
	
}