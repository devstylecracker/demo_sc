<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AffiliateUsers extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Manage_orders_model');
   	}

	public function index()
	{

		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else
		{
			 $data_post = array();

			 if($this->has_rights(81) == 1)
			{

				$search_by = 0;
				$table_search = 0;

        $affiliate_id = trim(strtolower($this->session->userdata('first_name'))).'-'.$this->session->userdata('user_id');

				$data_post['orderStatusList'] = array('1'=>'Processing', '2'=>'Confirmed', '3'=>'Dispatched', '4'=>'Delivered','5'=>'Cancelled','6'=>'Return','7'=>'Fake');
				
        $search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
        $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
		    $search_by2 = $this->input->post('search_by2')!='' ? $this->input->post('search_by2') : $this->uri->segment(5);
		
        if(($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!='') || ($search_by2!='0' && $search_by2!='') ){
          	$paginationUrl = 'AffiliateUsers/index/'.$search_by.'/'.$table_search.'/'.$search_by2;
          	$uri_segment = 6;
      	}else{
      		$paginationUrl = 'AffiliateUsers/index/0/0';
      		$uri_segment = 6;
      	}

       	$config['per_page'] = 20;
        $offset = $this->uri->segment(6)!='' ? $this->uri->segment(6) : 0;
        $config['total_rows'] = count($this->Manage_orders_model->getOrders_affiliate($search_by,$table_search,'','',$search_by2,'affiliate',$affiliate_id));

        $get_data_new = array();
      
        $order_data = $this->Manage_orders_model->getOrders_affiliate();
       
        $total_rows = $config['total_rows'];
        $get_data = $this->Manage_orders_model->getOrders_affiliate($search_by,$table_search,$offset,$config['per_page'],$search_by2,'affiliate',$affiliate_id);
       //echo '<pre>';print_r($get_data);exit;
        $count = $config['total_rows'];
        $data_post['orders'] = $get_data;
        $data_post['search_by'] = $search_by;
        $data_post['table_search'] = $table_search;
		    $data_post['search_by2'] = $search_by2;
        $data_post['sr_offset'] = $offset;       

				$config['base_url'] = base_url().$paginationUrl;
				$config['total_rows'] = $count;

				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li class="active" ><a href="" >';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				$this->pagination->initialize($config);
				$data_post['total_rows'] = $total_rows;


				$this->load->view('common/header');
				$this->load->view('orders/affiliate_manage_orders',$data_post);
				$this->load->view('common/footer');
			}else
			{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');

			}
		}
	}

	
public function exportExcel($param,$search_by=NULL,$table_search=NULL,$is_download=NULL,$search_by2=NULL)
  { $is_download = $this->uri->segment(6);
    $affiliate_id = trim(strtolower($this->session->userdata('first_name'))).'-'.$this->session->userdata('user_id');
    if($search_by == 6 )
    {
      $table_search_encode = base64_decode($table_search);
    }else if($search_by == 8)
    {
      $table_search_encode = base64_decode($table_search);
    }else
    {
      $table_search_encode = $table_search;
    }
    //$is_download = $this->input->post('param');
        $fieldName = array('Date','Order Unique No','Product Name','Original Product Price','Product Price','Payment Type','Order Status','Order Total','Username','City','State','Pincode','Platform','utm source ','utm medium','utm campaign');
    /*$fieldName = array('Date','Order ID','Product Name','Brand Name','Payment Type','Unique Reference Number','Transaction Id','Quantity','Product Amount','Brand Tax Amount','Shipping Charges','COD Charges','Grand Total','Rate Of Commission(%)','Commission Amount','PG cost @3.35%','Service Tax Amount(14.0%)','Swach Bharat Abhiyan(0.5%)','Total Commission Invoice','TDS deduction by Brand on commission','Net Income receivable from Brand','Brand Income','Brand Payout','Days Lapsed','Payment Status','Username','Phone Number','Email','Shipping Address','City','State','Pincode');*/
    $get_data = $this->Manage_orders_model->getViewOrdersExcel($search_by,$table_search_encode,'','',$search_by2,'affiliate',$affiliate_id);
    /*echo '<pre>';print_r($get_data);*/
    $result_data = array();
    $i =0;

    foreach($get_data as $val) {

           $referral_point = 0;$referalCode = '';
           $product_percent =  (($val['product_price']*100)/$val['OrderProductTotal']);
           $referral_point = $this->Manage_orders_model->get_refre_amt($val['order_unique_no']); 
           if($referral_point>0)
           {
              /*$refdiscount = round((($percent*$referral_point)/100),2);*/
              $referalCode = REDEEM_POINTS_CART_TEXT.'-'.$referral_point;
           }    
           
       
            $result_data[$i][] = $val['created_datetime'];
            $result_data[$i][] = $val['order_display_no']; 
            $result_data[$i][] = $val['product_name'];           
             //$result_data[$i][] = $val['actual_product_price'];
            $result_data[$i][] = ($val['id']==SCBOX_CUSTOM_ID) ? $val['product_price'] : $val['actual_product_price'];
            $result_data[$i][] = $val['product_price'];
            $brand_code ='';
            $product_price = 0;
            $commission_rate = 0;
            if($val['pay_mode']==1)
            {
              $result_data[$i][] = 'COD';
            }else
            {
              $result_data[$i][] = 'Online';
            }
            $date_ref_no = explode('-',$val['created_datetime']);

            if($val['brand_code']!='')
            {
              $brand_code = $val['brand_code'];
            }

            //$result_data[$i][] = $val['daysLapsed'];
            $order_status = "";
            if($val['order_status'] == 1)
            {
                $order_status = 'Processing';
            }else if($val['order_status'] == 2)
            {
                $order_status = 'Confirmed';
            }else if($val['order_status'] == 3)
            {
                $order_status = 'Dispatched';
            }else if($val['order_status'] == 4)
            {
                $order_status = 'Delivered';
            }else if($val['order_status'] == 5)
            {
                $order_status = 'Cancelled';
            }else if($val['order_status'] == 6)
            {
                $order_status = 'Return';
            }else if($val['order_status'] == 7)
            {
                $order_status = 'Fake';
            }

            $result_data[$i][] = $order_status;            

            //$result_data[$i][] = $val['product_price'];
            //$result_data[$i][] = $val['order_tax_amount'];
            //$result_data[$i][] = $val['shipping_amount'];
            //$result_data[$i][] = $val['cod_amount'];
            /* Coupon Setting Page */

              if(isset($val['coupon_code']) && $val['coupon_code']!='')
              {
                $coupon_info = $this->Manage_orders_model->getUserCoupon($val['coupon_code'],$val['created_datetime'],$val['brand_id']);
                /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
                $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
                $coupon_brand = $coupon_info['brand_id'];
                $coupon_products = $coupon_info['coupon_products'];
                if($coupon_products!='' || $coupon_products!=0)
                {
                  $coupon_products_arr =explode(',',$coupon_products);
                }
                $coupon_min_spend = $coupon_info['coupon_min_spend'];
                $coupon_max_spend = $coupon_info['coupon_max_spend'];
                $coupon_discount_type = $coupon_info['coupon_discount_type'];
                $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
                $individual_use_only =  $coupon_info['individual_use_only'];
                $data['coupon_code'] = $val['coupon_code'];
                $coupon_product_price = 0;
                $data['coupon_discount'] = 0;
                $price = $val['product_price'];

                 if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
                  {
                    if($coupon_products!=''&& $coupon_products!=0)
                    {
                       //coupon_discount_type =3 (Product discount)
                      if($coupon_discount_type==3)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $data['coupon_discount'] = $coupon_info['coupon_amount'];
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      }
                       //coupon_discount_type =4 (Product % discount)
                      if($coupon_discount_type==4)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            $data['coupon_discount'] = $price*($coupon_percent/100);
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      }

                    }else
                    {
                       //coupon_discount_type =1 (cart discount)
                      if($coupon_discount_type==1)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $data['coupon_discount'] = $coupon_info['coupon_amount'];
                          $data['coupon_code'] = $val['coupon_code'];
                        }
                      }
                       //coupon_discount_type =2 (cart % discount)
                      if($coupon_discount_type==2)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $coupon_percent = $coupon_info['coupon_amount'];
                          $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                          $data['coupon_code'] = $val['coupon_code'];
                        }
                      }
                    }
                  }

                if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
                {
                    //coupon_discount_type =1 (cart discount)
                        if($coupon_discount_type==1)
                        {
                          //$coupon_product_price = $coupon_product_price+$price;
                          $coupon_product_price = $val['OrderProductTotal'];
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $data['coupon_discount'] = $coupon_info['coupon_amount'];
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                         //coupon_discount_type =2 (cart % discount)
                        if($coupon_discount_type==2)
                        {
                          $coupon_product_price = $coupon_product_price+$price;
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            //$data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                             $data['coupon_discount'] = $val['OrderProductTotal']*($coupon_percent/100);
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                }


              }else
              {
                $data['coupon_discount']=0;
                $data['coupon_code'] = '';
              }

/* Coupon Setting Page End */   
            if($this->session->userdata('role_id') != 6){
              $result_data[$i][] = $val['order_total']-($data['coupon_discount']+$referral_point);
            }else{
              
              /*$pper = ($val['product_price']*100)/$val['OrderProductTotal'];
              $stot = $this->Manage_orders_model->calculate_product_discountAmt($val['product_price'],$val['OrderProductTotal'],$coupon_discount_type,($data['coupon_discount']));
              $ref_amt = ($pper*$referral_point)/100;*/

              $result_data[$i][] =$val['product_price'];/*$stot - $ref_amt;*/
            }
            //$result_data[$i][] = $val['commission_rate'];
         
           
            if(@$val['commission_rate']!='')
            {
              $commission_rate = $val['commission_rate'];
            }
            $comm_amt =  $product_price*($commission_rate/100);
            //$result_data[$i][] = $comm_amt;
            $pgCost=0;
            if($val['pay_mode']!=1)
            {
              $pgCost = $comm_amt * (3.35/100);
            }

            //$result_data[$i][] = round($pgCost,2);

            $service_tax = $comm_amt+$pgCost*(14.00/100);
            //$result_data[$i][] = round($service_tax,2);

            $swachba = ($comm_amt+$pgCost)*(0.50/100);
            //$result_data[$i][] = round($swachba,2);

            $total_comm_invoice = $comm_amt+$service_tax+$swachba;
            //$result_data[$i][] = round($total_comm_invoice,2);

            $tds_deduction = ($comm_amt+$pgCost)*(10/100);
            //$result_data[$i][] =  round($tds_deduction,2);

            $netIncome_rec_brand =  $total_comm_invoice-$tds_deduction;
            //$result_data[$i][] = round($netIncome_rec_brand,2);

            $brand_income = $val['order_total']-$total_comm_invoice;
            //$result_data[$i][] = round($brand_income,2);

            $brand_payout =  $tds_deduction+$brand_income;
            //$result_data[$i][] = round($brand_payout,2);

            //$result_data[$i][] = $val['daysLapsed'];
            if($val['payment_status'] == 1)
            {
              $pay_status = 'Success';
            }else if($val['payment_status'] == 2)
            {
              $pay_status = 'Cleared';
            }else
            {
              $pay_status = 'Pending';
            }
            //$result_data[$i][] = $pay_status;


            $result_data[$i][] = $val['first_name']." ".$val['last_name'];
           // $result_data[$i][] = $val['mobile_no'];
           // $result_data[$i][] = $val['email_id'];
            //$result_data[$i][] = trim($val['shipping_address']);
            $result_data[$i][] = trim($val['city_name']);
            $result_data[$i][] = trim($val['state_name']);
            $result_data[$i][] = $val['pincode'];
            //$result_data[$i][] = $val['look_id'];
            $result_data[$i][] = $val['platform_info'];
           // $result_data[$i][] = $val['product_cat_id'];
           // $result_data[$i][] = $val['product_sub_cat_id'];
           // $result_data[$i][] = $this->product_variations($val['id']);
           // $result_data[$i][] = $this->product_variations_value($val['id']);
      			$result_data[$i][] = $val['sc_source'];
      			$result_data[$i][] = $val['sc_medium'];
      			$result_data[$i][] = $val['sc_campaign'];
      			if($val['is_paymentdone'] == '1' || $val['pay_mode'] == '2'){ $payment_status = 'Paid'; }else{ $payment_status = 'Pending'; }
      			//$result_data[$i][] = $payment_status;
      			//$result_data[$i][] = $val['user_age'];
			
            $i++;
        }
        //echo "<pre>";print_r($result_data);exit;
       // echo $param.' '.$is_download;
        if($param == 'downloadExcel' && $is_download == 1)
        {
        //========================Excel Download==============================
          //$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
         $this->writeDataintoCSV($fieldName,$result_data,'view_order_payment_affilate');
         //========================End Excel Download==========================


        }

  }
  
	function calculateCouponAmount($cartproduct=array(),$totalProductPrice=null)
  {
      if(!empty($cartproduct))
      {
        $total_product_price = 0; $coupon_product_price = 0;
        foreach($cartproduct as $val)
        {
            if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
            {
               $price = $val['discount_price'];
            }else
            {
               $price = $val['product_price'];
            }
            $total_product_price = $total_product_price+$price;
        }
        foreach($cartproduct as $val)
        {

          if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];
          }else
          {
             $price = $val['product_price'];
          }

           $coupon_info = $this->Manage_orders_model->getUserCoupon($val['coupon_code'],$val['brand_id']);
            $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type'];
            $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only'];

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {

            /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
            $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/

            $data['coupon_code'] = $val['coupon_code'];
            $coupon_product_price = 0;

              if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
              {
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];
                        $data['coupon_code'] = $val['coupon_code'];
                      }
                    }
                  }
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100);
                        $data['coupon_code'] = $val['coupon_code'];
                      }
                    }
                  }

                }else
                {
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                      $data['coupon_discount'] = $coupon_info['coupon_amount'];
                      $data['coupon_code'] = $val['coupon_code'];
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                      $coupon_percent = $coupon_info['coupon_amount'];
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];
                    }
                  }
                }
              }

       if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
            {
              $coupon_product_price = $totalProductPrice;

                   //coupon_discount_type =1 (cart discount)
                if($coupon_discount_type==1)
                {

                  if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                  {
                    $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                    $data['coupon_discount'] = $coupon_info['coupon_amount'];
                    $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                    $data['coupon_code'] = $val['coupon_code'];
                  }
                }
                 //coupon_discount_type =2 (cart % discount)
                if($coupon_discount_type==2)
                {
                  if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                  {
                    $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                    $coupon_percent = $coupon_info['coupon_amount'];
                    $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                    $data['coupon_discount'] = $totalProductPrice*($coupon_percent/100);
                    $data['coupon_code'] = $val['coupon_code'];
                  }
                }
            }

          }else
          {
            $data['coupon_discount']=0;
            $data['coupon_code'] = '';
          }
        }
      }
      return $data;
  }
	
}

?>
