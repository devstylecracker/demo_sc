<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lookcreator_v2 extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
       $this->load->model('users_model');  
       $this->load->model('category_model');    
       $this->load->model('lookcreator_model');    
       $this->load->model('product_model');  
       $this->load->library('curl');
   } 
   
	public function index($offset = null)
	{ 
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			$data_post = array();
			$data_post['parent_category_list'] = $this->lookcreator_model->parent_catgory_list(); 
			$data_post['category_list'] = $this->lookcreator_model->catgory_list(); 
			$data_post['colors_list'] = $this->lookcreator_model->get_tags_by_tag_type(1); 
			$data_post['brand_list'] = $this->lookcreator_model->get_brands_list(); 
			$data_post['sticker_list'] = $this->lookcreator_model->get_sticker_list();
			$data_post['global_attributes'] = $this->lookcreator_model->all_attributes();
	
			if($this->has_rights(32) == 1){
			
				$this->load->view('common/header',$data_post);
				$this->load->view('look/lookcreator_v2');
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
		}
	}
	

	public function get_sub_cat(){
		/*if($this->has_rights(32) == 1){*/
			$sub_cat_id= $this->input->post('sub_cat_id');
			$data_post['subcategory_list'] = $this->category_model->get_sub_categories($sub_cat_id); 
			$subcat_combo_html = '';
			if(!empty($data_post['subcategory_list'])){ 
				foreach($data_post['subcategory_list'] as $val){ 
					$subcat_combo_html = $subcat_combo_html.'<li id="'.$val['id'].'" class="cat">'.$val['name'].'</li>';
				}
			}else{
					$subcat_combo_html = $subcat_combo_html.'<li class="cat">No record found </li>';
				}
			echo $subcat_combo_html; 
		/*}
		else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}*/
		
	}
	
	public function sc_variations_value_type(){
		if($this->has_rights(32) == 1){
			$sub_cat_id= $this->input->post('id');
			$data_post['subcategory_list'] = $this->lookcreator_model->get_all_variationtype($sub_cat_id); 
			$subcat_combo_html = '';
			if(!empty($data_post['subcategory_list'])){ 
				$subcat_combo_html = $subcat_combo_html.'<option id="" value="">Select</option>';
				//$subcat_combo_html = $subcat_combo_html;
				foreach($data_post['subcategory_list'] as $val){ 
					$subcat_combo_html = $subcat_combo_html.'<option id="'.$val['id'].'" value="'.$val['id'].'">'.$val['name'].'</option>';
				}
			}else{
					$subcat_combo_html = $subcat_combo_html.'<option></option>';
				}
			echo $subcat_combo_html; 
		}
		else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
	}

		public function get_variation_type(){
		if($this->has_rights(32) == 1){
			$variationtype_id= $this->input->post('id');
			$variations_type= $this->input->post('variations_type');
			$data_post['variationtype_list'] = $this->lookcreator_model->get_variationvalue($variationtype_id,$variations_type); 
			$subcat_combo_html = '';
			if(!empty($data_post['variationtype_list'])){ 
				//$subcat_combo_html = $subcat_combo_html.'<option id="" value="">Select</option>';
				$subcat_combo_html = $subcat_combo_html;
				foreach($data_post['variationtype_list'] as $val){ 
					//$subcat_combo_html = $subcat_combo_html.'<li id="'.$val['id'].'" class="cat">'.$val['name'].'</li>';
					$subcat_combo_html = $subcat_combo_html.'<option id="'.$val['id'].'" value="'.$val['id'].'">'.$val['name'].'</option>';
				}
			}else{
					$subcat_combo_html = $subcat_combo_html.'<option></option>';
				}
			echo $subcat_combo_html; 
		
		}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
	}

	public function get_product(){
		
			/*if($this->has_rights(32) == 1){*/
			$variationtype_id= $this->input->post('id');
			$color= $this->input->post('color');
			$brand= $this->input->post('brand');
			$tag= $this->input->post('tags');
			$sc_variations_value_type = $this->input->post('sc_variations_value_type');
			$variations_type = $this->input->post('variations_type'); 
			$lookoffset = $this->input->post('lookoffset');
			if(!empty($variations_type)){
			$variations_type = implode(",",$variations_type);
			}else{
				$variations_type = '';
			}
			$data_post['product_list'] = $this->lookcreator_model->get_product_list($variationtype_id,$color,$brand,$tag,$sc_variations_value_type,$variations_type,$lookoffset); 
			$subcat_combo_html = '';
			if($lookoffset == 0){
				$subcat_combo_html = $subcat_combo_html.'<input type="hidden" id="lookoffset" name="lookoffset" value="1">';
			}
			if(!empty($data_post['product_list'])){ 
				foreach($data_post['product_list'] as $val){ 
					$stock_count =$this->lookcreator_model->product_stock($val['product_id']);
					if($stock_count>0){
					$subcat_combo_html = $subcat_combo_html.'<li><img data-img-link="'.LIVE_SITE_URL.'assets/products/'.$val['image'].'" class="lazy" id="'.$val['product_id'].'" src="'.LIVE_SITE_URL.'assets/products/thumb_160/'.$val['image'].'" title="Url : '.$val['url'].', Used_count:'.$val['product_used_count'].', Product Id : '.$val['product_id'].',stock count: '.$stock_count.', Price : '.$val['price'].'"   alt="'.$val['name'].'" width="80px" height="100px">
						 <a class="product-link" href="'.$val['url'].'" target="_blank"><span class="fa fa-link"></span></a>
					</li>';
					}
					
				}
				$subcat_combo_html = $subcat_combo_html.'<li class="btn-load-more" style="border: medium none; cursor: default; padding-top: 30px;"><div class="btn btn-sm btn-primary load_more" id="load_more"> LOAD MORE <i class="fa fa-arrow-down"></i></div></li>';
			}else{
					$subcat_combo_html = $subcat_combo_html.'<li class="cat">No record found </li>';
				}
			echo $subcat_combo_html; 
		
		/*}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}*/
		}

			public function get_product_v2(){
		
			/*if($this->has_rights(32) == 1){*/
			/*$variationtype_id= $this->input->post('id');
			$color= $this->input->post('color');
			$brand= $this->input->post('brand');
			$tag= $this->input->post('tags');
			$sc_variations_value_type = $this->input->post('sc_variations_value_type');
			$variations_type = $this->input->post('variations_type'); */

			$brand = $this->input->post('brand');
			$cat = $this->input->post('cat');
			$attributes_chosen = $this->input->post('attributes_chosen');
			$category_specific_attr = $this->input->post('category_specific_attr');
			$attributes_chosen_ = ''; $category_specific_attr_ = '';

			if(!empty($attributes_chosen)){ $i = 0;
				foreach($attributes_chosen as $val) {
					if($i!=0) { $attributes_chosen_ = $attributes_chosen_.','; }
					$attributes_chosen_ = $attributes_chosen_.$val;
					$i++;
				}
			}

			

			$lookoffset = $this->input->post('lookoffset');
			/*if(!empty($variations_type)){
			$variations_type = implode(",",$variations_type);
			}else{
				$variations_type = '';
			}*/
			$data_post['product_list'] = $this->lookcreator_model->get_product_list_v2($brand,$cat,$attributes_chosen_,$category_specific_attr,$lookoffset); 
			$subcat_combo_html = '';
			if($lookoffset == 0){
				$subcat_combo_html = $subcat_combo_html.'<input type="hidden" id="lookoffset" name="lookoffset" value="1">';
			}
			if(!empty($data_post['product_list'])){ 
				foreach($data_post['product_list'] as $val){ 
					$stock_count =$this->lookcreator_model->product_stock($val['product_id']);
					if($stock_count>0){
					$subcat_combo_html = $subcat_combo_html.'<li><img data-img-link="'.LIVE_SITE_URL.'assets/products/'.$val['image'].'" class="lazy" id="'.$val['product_id'].'" src="'.LIVE_SITE_URL.'assets/products/thumb_160/'.$val['image'].'" title="Url : '.$val['url'].', Used_count:'.$val['product_used_count'].', Product Id : '.$val['product_id'].',stock count: '.$stock_count.', Price : '.$val['price'].'"   alt="'.$val['name'].'" width="80px" height="100px">
						 <a class="product-link" href="'.$val['url'].'" target="_blank"><span class="fa fa-link"></span></a>
					</li>';
					}
					
				}
				$subcat_combo_html = $subcat_combo_html.'<li class="btn-load-more" style="border: medium none; cursor: default; padding-top: 30px;"><div class="btn btn-sm btn-primary load_more" id="load_more"> LOAD MORE <i class="fa fa-arrow-down"></i></div></li>';
			}else{
					$subcat_combo_html = $subcat_combo_html.'<li class="cat">No record found </li>';
				}
			echo $subcat_combo_html; 
		
		/*}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}*/
		}

	public function save_look(){
			$look_desc =$this->input->post('look_desc');
	        $look_name =$this->input->post('look_name');
	        $street_canvas_img_id = $this->input->post('street_canvas_img_id');
	        $look_products = $this->input->post('look_products');
	        $look_type_new = $this->input->post('look_type');
	        $look_tags=$_POST['look_tags'];
		$look_from=$this->input->post('type');
		if($look_from == 'save_streeet_look'){ $look_type = $look_type_new; }
		else if($look_from == 'save_promotional_look'){  $look_type = 3; }
		else { $look_type = 1; }


                $prod_look = array();
                if(!empty($look_products)){
                        foreach($look_products as $val){
                                $prod_look[]  = str_replace("recommended_","",$val);
                        }
                }

                if($this->lookcreator_model->save_look($look_name,$look_desc,$street_canvas_img_id,$prod_look,$look_type,$look_tags)){
                        return true;
                }
		
	}
	
	public function save_stylist_look(){ 
		$imageData=$_POST['img'];
		$look_desc=$_POST['look_desc'];
		$look_name=$_POST['look_name'];
		$product_id=$_POST['product_id'];
		$look_tags=$_POST['look_tags'];

		// Remove the headers (data:,) part.
		// A real application should use them according to needs such as to check image type
		$filteredData=substr($imageData, strpos($imageData, ",")+1);

		// Need to decode before saving since the data we received is already base64 encoded
		$unencodedData=base64_decode($filteredData);

		//echo "unencodedData".$unencodedData;

		// Save file. This example uses a hard coded filename for testing,
		// but a real application can specify filename in POST variable
		$look_img = time().mt_rand(1,1000).'.png';
		$fp = fopen('assets/looks/'.$look_img, 'wb' );
		fwrite( $fp, $unencodedData);
		fclose( $fp );

		$this->compress_image('assets/looks/'.$look_img, 'assets/looks/'.$look_img, '70');
		
		 $prod_look = array();
                if(!empty($product_id)){
                        foreach($product_id as $val){
							if(is_numeric($val)){
                                $prod_look[]  = $val;
							}
                        }
                }
		$look_type = 1;
                if($this->lookcreator_model->save_look($look_name,$look_desc,$look_img,$prod_look,$look_type,$look_tags)){
                        return true;
                }
		
		}
/* commented by rajesh on 1 feb 2016 for pagination 
	public function manage_look($offset = null){
		 if($this->has_rights(52) == 1){
			    $search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');
 
			 $paginationUrl = 'lookcreator/manage_look/';
	                 $uri_segment = 3;
                         $config['per_page'] = 20;
			
			 $config['total_rows'] = count($this->lookcreator_model->get_all_look($search_by,$table_search,'',''));
			 $total_rows = $config['total_rows'];
			 $data['look_list'] = $this->lookcreator_model->get_all_look($search_by,$table_search,$offset,$config['per_page']);

				
			 $this->customPagination($paginationUrl,$total_rows,$uri_segment,$data['look_list'],$total_rows);
	
			 $this->load->view('common/header',$data);
                         $this->load->view('look/manage_look');
                         $this->load->view('common/footer');

			}else{
			 $this->load->view('common/header');
                         $this->load->view('not_permission');
                         $this->load->view('common/footer');

		}	
	}
*/
public function manage_look($offset = null){
		 if($this->has_rights(52) == 1){

			    /*$search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');*/
 				 $search_by = 0;
				$table_search = 0;
				$search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(4);
                $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(5);
                //echo "<pre>"; print_r($this->uri); 
                if($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!=''){ 
                	$paginationUrl = 'lookcreator/manage_look/index/'.$search_by.'/'.$table_search;
                	$uri_segment = 6;
            	}else{
            		$paginationUrl = 'lookcreator/manage_look/index/0/0';
            		$uri_segment = 6;
            	}

			 /*$paginationUrl = 'lookcreator/manage_look/';
	                 $uri_segment = 3;*/
               $config['per_page'] = 20;
			  $offset = $this->uri->segment(6)!='' ? $this->uri->segment(6) : 0;
			 $config['total_rows'] = count($this->lookcreator_model->get_all_look($search_by,$table_search,'',''));
			 
			$total_rows = $config['total_rows'];
			  $get_data = $this->lookcreator_model->get_all_look($search_by,$table_search,$offset,$config['per_page']);
			   $count = $config['total_rows'];
  				
				 $data_post['look_list'] =  $get_data ;
                $data_post['search_by'] = $search_by;
                $data_post['table_search'] = $table_search;
			 //$this->customPagination($paginationUrl,$total_rows,$uri_segment,$data['look_list'],$total_rows);
               
                $config['base_url'] = base_url().$paginationUrl;        
                $config['total_rows'] = $count;
                $config['per_page'] = 20;
                $config['uri_segment'] = $uri_segment;
                $config['use_page_numbers'] = TRUE;
                $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
                $config['full_tag_close'] = '</ul></div>';

                $config['first_link'] = '&laquo; First';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';

                $config['last_link'] = 'Last &raquo;';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';

                $config['next_link'] = 'Next &rarr;';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['prev_link'] = '&larr; Previous';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
                $config['cur_tag_close'] = '</a></li>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';

                
                $this->pagination->initialize($config); 
                $this->data['total_rows'] = $total_rows;
	
			 			$this->load->view('common/header');
                         $this->load->view('look/manage_look',$data_post);
                         $this->load->view('common/footer');

			}else{
			 			$this->load->view('common/header');
                         $this->load->view('not_permission');
                         $this->load->view('common/footer');

		}	
	}
	public function look_edit(){
	 	if($this->has_rights(53) == 1){
			$id = $this->uri->segment(3);
			 $data['look_data'] = $this->lookcreator_model->get_look_info($id);
				if($this->input->post()){
				$this->form_validation->set_rules('look_name', 'Look Name', 'required');
				$this->form_validation->set_rules('look_desc', 'Description', 'required');
	
				if ($this->form_validation->run() == FALSE){
						$this->load->view('common/header',$data);
						$this->load->view('look/edit_look');
						$this->load->view('common/footer');
					}else{
						$look_name = $this->input->post('look_name');
						$look_desc = $this->input->post('look_desc');
						if($this->lookcreator_model->update_look($id,$look_name,$look_desc)){
						redirect('lookcreator/manage_look');
						}
					}
				}
			 $this->load->view('common/header',$data);
                         $this->load->view('look/edit_look');
                         $this->load->view('common/footer');
	
		}else{
		 	$this->load->view('common/header');
                 	$this->load->view('not_permission');
                 	$this->load->view('common/footer');

	
		}
	}

	public function look_view(){
		if($this->has_rights(52) == 1){
			 $id = $this->uri->segment(3);	
			 $data['look_data'] =  $this->lookcreator_model->get_look_info($id);  
			 $data['product_data'] = $this->lookcreator_model->get_looks_product($id); 
			 $this->load->view('common/header',$data);
                         $this->load->view('look/view_look');
                         $this->load->view('common/footer');

		}else{
			$this->load->view('common/header');
                        $this->load->view('not_permission');
                        $this->load->view('common/footer');
		}
	}
	
	
	public function look_broadcast(){
		if($this->has_rights(53) == 1){
			 $id = $this->uri->segment(3);	
				$data['look_data'] =  $this->lookcreator_model->get_look_info($id);  
			    $data['category_list'] = $this->category_model->get_sub_categories(); 
				$data['body_shape'] = $this->product_model->load_questAns(1);
				$data['age'] = $this->product_model->load_questAns(8);
				$data['style'] = $this->product_model->load_questAns(2);
				$data['size'] = $this->product_model->load_questAns(9);
				$data['budget'] = $this->product_model->load_questAns(7);
				
				$data['mens_body_shape'] = $this->product_model->load_questAns(13);
				$data['mens_age'] = $this->product_model->load_questAns(17);
				$data['mens_style'] = $this->product_model->load_questAns(14);
				$data['mens_work_style'] = $this->product_model->load_questAns(15);
				$data['mens_personal_style'] = $this->product_model->load_questAns(16);
				$data['mens_size'] = $this->product_model->load_questAns(9);
				$data['mens_budget'] = $this->product_model->load_questAns(18);
				/*$i=37;
				echo "INSERT INTO `stylecra_stylecracker_v1`.`bucket`(`id`,`body_shape`,`style`,`work_style`,`personal_style`,`is_active`,`created_by`,`modified_by`,`created_datetime`,`modified_datetime`)VALUES";
				foreach($data['mens_body_shape'] as $body_shape_val_key=>$body_shape_val){
					foreach($data['mens_style'] as $body_style_val_key=>$body_style_val){
						foreach($data['mens_work_style'] as $mens_work_style_val_key => $mens_work_style_val){
							foreach($data['mens_personal_style'] as $mens_personal_style_val_key=>$mens_personal_style_val){
								if($body_shape_val_key > 0 && $body_style_val_key> 0 && $mens_work_style_val_key>0 && $mens_personal_style_val_key >0){
									echo "(".$i++.",$body_shape_val_key,$body_style_val_key,$mens_work_style_val_key,$mens_personal_style_val_key,1,1,1,now(),now()),";
									echo "<br/>";
								}
							}
						}
						}
				}
exit;*/
				$data['product_data'] = $this->lookcreator_model->get_looks_product($id); 
				$data['look_sub_cat'] = $this->lookcreator_model->get_look_sub_categories();
				$data['stylist_data'] = $this->lookcreator_model->get_stylist_data();
			 $this->load->view('common/header',$data);
                         $this->load->view('look/look_broadcast');
                         $this->load->view('common/footer');

		}else{
			$this->load->view('common/header');
                        $this->load->view('not_permission');
                        $this->load->view('common/footer');
		}
	}
	
	public function reject_look(){
		if($this->has_rights(56) == 1){
			   $id = $this->input->post('id');   
			   if($this->lookcreator_model->reject_look($id)){
					return true;
				}
			    
		}
	}
	
	public function delete_look(){
		if($this->has_rights(55) == 1){
			   $id = $this->input->post('id');   
			   if($this->lookcreator_model->delete_look($id)){
					return true;
				}
			    
		}
	}
	
	public function look_broadcast_save(){
		if($this->has_rights(54) == 1){
			$gender = $this->input->post('gender');  
			$look_id = $this->input->post('look_id');  
			$body_shape = $this->input->post('body_shape');  
			$body_style = $this->input->post('body_style');  
			@$mens_work_style = $this->input->post('mens_work_style');  
			@$mens_personal_style = $this->input->post('mens_personal_style');  
			$look_cat = $this->input->post('look_cat');  
			$stylist_id = $this->input->post('stylist_name');  
			$budget = $this->input->post('budget');  
			$size = $this->input->post('size');  
			$age = $this->input->post('age'); 

			if(!is_array($mens_work_style))
				$mens_work_style = array(0);

			if(!is_array($mens_personal_style))
				$mens_personal_style = array(0);
			
			
			if(!empty($body_shape)){
				foreach($body_shape as $body_shape_val){
					foreach($body_style as $body_style_val){
						foreach($mens_work_style as $mens_work_style_val){
							foreach($mens_personal_style as $mens_personal_style_val){
								if($body_shape_val > 0 && $body_style_val> 0){
									$this->lookcreator_model->broadcastlook($look_id,$body_shape_val,$body_style_val,$look_cat,$stylist_id,$budget,$size,$age,$gender,$mens_work_style_val,$mens_personal_style_val);
								}
							}
						}
						}
				}
			}
			if($gender == 1){
				$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_look_women', false, array(CURLOPT_USERAGENT => true));
			}else{
				$result = $this->curl->simple_post(LIVE_SITE.'Sccache/clear_look_men', false, array(CURLOPT_USERAGENT => true));
			}
			echo 'Ok';
			//$product_data = $this->lookcreator_model->get_looks_productdetail($look_id); 
			/*if($this->lookcreator_model->setproduct_usedcount($product_data))
			{
				if($this->lookcreator_model->broadcastlook($look_id,$body_shape,$body_style,$look_cat,$stylist_id,$budget,$size,$age))
				{
					echo 'Ok';
				}else
				{
					echo "Error";
				}	

			}else
			{
				if($this->lookcreator_model->broadcastlook_reject($look_id,$stylist_id,$product_data))
				{
					echo 'No';
				}else
				{
					echo 'Error';
				}
				
			}*/
			
			
		}else
		{
			echo 'Error';
		}
	}

	public function look_broadcast_edit(){
		if($this->has_rights(54) == 1){
			$look_id = $this->input->post('look_id');			
			$budget = $this->input->post('budget'); 	
			$look_cost = $this->input->post('lookCost'); 
			$product_ids = $this->input->post('productIds');			

			$product_data = $this->lookcreator_model->get_looks_productdetail($look_id); 
			
			if($this->lookcreator_model->broadcastlookEdit($look_id,$budget,$look_cost,$product_ids))
			{
				echo 'Ok';
			}else
			{
				echo "Error";
			}

		}else
		{
			echo 'Error';
		}
	}

	public function get_attributes(){
		$id = $this->input->post('cat_id');
		$global_attributes = $this->lookcreator_model->all_attributes_depends_on_cat($id);
		$option = $option."<option value=''>Select</option>";
		if(!empty($global_attributes)){
			foreach ($global_attributes as $key => $value) {
			
			$option =$option."<option disabled>".$value['attribute_name']."</option>";
			$attributes_value = $this->lookcreator_model->attributes_values($value['id']); 
				foreach ($attributes_value as $ke => $va) { 
					$option =$option."<option value=".$va['id'].">".$va['attr_value_name']."</option>";
			 }						
			}
		}
		echo $option;
	}
	public function get_child_cat(){
		$id = $this->input->post('gender');
		$global_attributes = $this->lookcreator_model->get_all_child($id);
		$option = '';
		$option = $option."<option value=''>Select</option>";
		if(!empty($global_attributes)){
			foreach ($global_attributes as $key => $value) {
				$option =$option."<option value=".$value['id'].">".$value['category_name']."</option>";					
			}
		}
		echo $option;
	}
	
	
}
