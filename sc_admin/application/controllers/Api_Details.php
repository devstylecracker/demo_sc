<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Details extends MY_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
       parent::__construct();
       $this->load->model('api_details_model');    
   	} 
   
	public function index()
	{  
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 
			if($this->session->userdata('role_id') == '6') {

				$api_details = $this->api_details_model->get_api_details();
				$data['api_key'] = $this->encryptOrDecrypt($api_details[0]['email'],'encrypt');
				$data['api_secret_key'] = $this->encryptOrDecrypt($api_details[0]['password'],'encrypt');

				$this->load->view('common/header');
				$this->load->view('api_details',$data);
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}
		}
	}

	


	
}	