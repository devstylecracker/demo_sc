<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends MY_Controller {    

    function __construct(){
        parent::__construct();
        $this->load->model('Brand_model');       

    }  
 /*---------Category submodule functionalities----------*/

    public function index($offset = null)
    {   
      
        if(!$this->session->userdata('user_id')){

            $this->load->view('login');                    

        }else{ 
			
			/*All Data Show*/			
			$data_post = array();                       
           if($this->has_rights(45) == 1){	 
			    $search_by = $this->input->post('search_by');
                $table_search = $this->input->post('table_search');                 
			    $paginationUrl = 'Tag/index/';
                $uri_segment = 3;
                $config['per_page'] = 20;
                $config['total_rows'] = count($this->Brand_model->get_all_tag($search_by,$table_search,'',''));                
                $total_rows = $config['total_rows'];                                                    
                $get_users = $this->Brand_model->get_all_tag($search_by,$table_search,$offset,$config['per_page']);
                //echo '<pre>'; print_r($get_users); echo '</pre>';
                $count = $config['total_rows']; 
                $data_post['categories'] = $get_users;
                
                $this->customPagination($paginationUrl,$count,$uri_segment,$get_users,$total_rows);    
			    $this->load->view('common/header');
				$this->load->view('brand_view',$data_post);
				$this->load->view('common/footer');
			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}        
		}
	}
	
	/*Add and edit tag function here*/
	public function brand_addedit($id=NULL)
    {
		$data_post = array();        
        $loggedInUser=$this->session->userdata('user_id');
        if($id==NULL){	
			if($this->has_rights(47) == 1)
			{	
				if($this->input->post()){
					
					$this->form_validation->set_rules('tag_name', 'Tag Name', 'required|is_unique[tag.name]'); 					
					$this->form_validation->set_rules('tag_slug', 'Tag Slug', 'required|is_unique[tag.slug]');
										
					if ($this->form_validation->run() == FALSE)
                    {
										
						$this->load->view('common/header');
                        $this->load->view('add_brand');
                        $this->load->view('common/footer');						
					}else
					{
						$tag_name = $this->input->post('tag_name');
						$tag_slug = $this->input->post('tag_slug');
						$tag_slug_space = str_replace(' ', '-', $tag_slug);												
						$active = $this->input->post('active');	
						$catCount = $this->Brand_model->checkMultilpetag($this->input->post('tag_name'));	
						if($catCount->catCount > 0){
							$this->session->set_flashdata('feedback', '<i class="fa fa-check sign"></i><strong>Error!</strong> Tag already exits!');
						}else{						
							$data_post = array(
							   'name'=>$tag_name,
							   'slug'=>$tag_slug_space,                           
							   'status'=> $active,                                
							   'created_by'=> $loggedInUser,
							   'created_datetime'=> date("Y-m-d H:i:s"),                                
							   'modified_by'=> $loggedInUser,
							   'modified_datetime'=> date("Y-m-d H:i:s")                                
							);
							$created = $this->Brand_model->new_tag($data_post);                
							   if($created){                    
								  $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5> Brand added successfully!</div>');
								  redirect('Brand'); 
								}else{                                          
								  $this->session->set_flashdata('feedback', '<i class="fa fa-times-circle sign"></i><strong>Error!</strong> The server is not responding, try again later.');
								  redirect('Brand'); 
								} 
                         }                          				
					}					                   
				}else{					
					$this->load->view('common/header');
					$this->load->view('add_brand');
					$this->load->view('common/footer');
				}					
			}else{
			$this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
			}
			
		}else{
			/*Edit Code Here tag*/
			if($this->has_rights(48) == 1){	
			     $data_post['tag'] = $this->Brand_model->get_tag_by_ID($id);
			     
			     if($this->input->post()){
					 $this->form_validation->set_rules('tag_name', 'Tag Name', 'required');
					 if ($this->form_validation->run() == FALSE)
                        {
                           
                           $this->load->view('common/header');
                           $this->load->view('add_brand');
                           $this->load->view('common/footer');
                        
                        }else{
							
							$tag_name = $this->input->post('tag_name');
							$tag_slug = $this->input->post('tag_slug');
							$tag_slug_space = str_replace(' ', '-', $tag_slug);
							$active = $this->input->post('active');
							
							$catCount = $this->Brand_model->checkMultilpetagEdit($id,$tag_name);
							
							if($catCount->catCount > 0){
								
								$this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> Brand already exits!</div>');
							
							}else{
								$data_post = array(
												'name'=>$tag_name,  
												'slug'=>$tag_slug_space,                                               
                                                'status'=> $active, 
                                                'modified_by'=> $loggedInUser,
                                                'modified_datetime'=> date("Y-m-d H:i:s")                                
                                                );
                                 $update = $this->Brand_model->update_tag($id,$data_post); 
                                 
                                 if($update){
									 
									 $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Changes has been Updated successfully</div>');
                                     redirect('/Brand');
									 
								 }else{
									 
									 $this->session->set_flashdata('feedback', '<div class="callout callout-warning"><h5><i class="icon fa fa-warning"></i><strong>Error!</strong></h5> The server is not responding, try again later.</div>');
                                     redirect('/Brand');
									 
								 }
                                                                  
							}							
						}
				 }
			     $this->load->view('common/header');
                 $this->load->view('add_brand',$data_post);
                 $this->load->view('common/footer');
			    
			}else{
				$this->load->view('common/header');
				$this->load->view('not_permission');
				$this->load->view('common/footer');
			}			
		}        
	}
	
	/*delete Code Here tag*/
	function delete_brand()
    {   		
        $catID = $this->input->post('catID');                
        $deleted = $this->Brand_model->delete_tag($catID);
        
        if($deleted)
        {
            $this->session->set_flashdata('feedback', '<div class="callout callout-success"><h5><i class="icon fa fa-check"></i><strong>Success!</strong> </h5>Record has been deleted successfully</div>');
            redirect('/Brand');
        }
        else
        {           
            $this->session->set_flashdata('feedback', '<div class="callout callout-danger"><h5><i class="icon fa fa-ban"></i><strong>Error!</strong></h5>The server is not responding, try again later.</div>');  
            redirect('/Brand');    
        }
    }
	
	
	    
/*----------End of Common functions------------------------------------*/	
}
