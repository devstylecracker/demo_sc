<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_orders extends MY_Controller {

	function __construct(){
       parent::__construct();
       $this->load->model('Manage_orders_model');
	   $this->load->model('Scbox_model');
	   $this->load->model('Pa_model');
	   $this->load->model('Stylist_model');
	   $this->load->model('Delivery_partner_mapping_model','dpm_model');
	   $this->load->library('email');  
   	}

	public function index()
	{
		$type = $this->input->post('type');
		if(!$this->session->userdata('user_id'))
		{
			$this->load->view('login');

		}else
		{
			 $data_post = array();

			 if($this->has_rights(62) == 1)
			{

				$search_by = 0;
				$table_search = 0;
				$search_by2 = 0;
				
				$data_post['orderStatusList'] = array('1'=>'Processing', '2'=>'Confirmed', '8'=>'Curated','3'=>'Dispatched', '4'=>'Delivered','5'=>'Cancelled','6'=>'Return','7'=>'Fake');
				
		        $search_by = $this->input->post('search_by')!='' ? $this->input->post('search_by') : $this->uri->segment(3);
		        $table_search = $this->input->post('table_search')!='' ? $this->input->post('table_search') : $this->uri->segment(4);
				$search_by2 = $this->input->post('search_by2')!='' ? $this->input->post('search_by2') : $this->uri->segment(5);
				if($search_by==10)
				{
					$table_search = $this->input->post('table_search_box')!='' ? $this->input->post('table_search_box') : $this->uri->segment(4);
				}else if($search_by==5)
				{
					$table_search = $this->input->post('table_search_status')!='' ? $this->input->post('table_search_status') : $this->uri->segment(4);
				}
				
				else if($search_by==8)
				{
					if(!empty($this->uri->segment(4)))
					{
						$table_search = $this->uri->segment(4);
					}
					else
					{
						$date_from = $this->input->post('date_from');
						$date_to = $this->input->post('date_to');
						$date = $date_from.','.$date_to;
						$table_search = base64_encode($date);
					} 
				}


				if(($search_by!='0' && $table_search!='0' && $search_by!='' && $table_search!='') || ($search_by2!='0' && $search_by2!='') ){
		          	$paginationUrl = 'manage_orders/index/'.$search_by.'/'.$table_search.'/'.$search_by2;
		          	$uri_segment = 6;
		      	}else{
		      		$paginationUrl = 'manage_orders/index/0/0/0';
		      		$uri_segment = 6;
		      	}

		       	$config['per_page'] = 20;
		        $offset = $this->uri->segment(6)!='' ? $this->uri->segment(6) : 0;
		        $config['total_rows'] = count($this->Manage_orders_model->getOrders($search_by,$table_search,'','',$search_by2));

		        $get_data_new = array();
		      
		        $order_data = $this->Manage_orders_model->getOrders();
		       
		        $total_rows = $config['total_rows'];
		        $get_data = $this->Manage_orders_model->getOrders($search_by,$table_search,$offset,$config['per_page'],$search_by2);
		       
		        $count = $config['total_rows'];
		        $data_post['orders'] = $get_data;
		        $data_post['search_by'] = $search_by;
		        $data_post['table_search'] = $table_search;
				$data_post['search_by2'] = $search_by2;
		        $data_post['sr_offset'] = $offset;       

				$config['base_url'] = base_url().$paginationUrl;
				$config['total_rows'] = $count;

				$config['uri_segment'] = $uri_segment;
				$config['use_page_numbers'] = TRUE;
				$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
				$config['full_tag_close'] = '</ul></div>';

				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';

				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';

				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';

				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';

				$config['cur_tag_open'] = '<li class="active" ><a href="" >';
				$config['cur_tag_close'] = '</a></li>';

				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';

				$this->pagination->initialize($config);
				$data_post['total_rows'] = $total_rows;
				$data_post['getAllTag']  = $this->Stylist_model->get_all_tag();
				$data_post['getAllState'] = $this->Manage_orders_model->get_all_state();
				$dpartners = $this->dpm_model->getDelPartners();        
				$data_post['dpartners'] = $dpartners;
				/*echo "<pre>";
				print_r($data['getAllTag']);
				echo "</pre>";*/
				
				 // if ($type=='stylist_manageorders') {   
		   //        $this->load->view('common/header_manage');      
		   //        $this->load->view('orders/manage_orders',$data_post);  
		   //        $this->load->view('common/footer_manage');    
		   //      }
		   //      else{
		          	$this->load->view('common/header2');
			        $this->load->view('orders/manage_orders',$data_post);
			        $this->load->view('common/footer2');
		        //}

				// $this->load->view('common/header');
				// $this->load->view('orders/manage_orders',$data_post);
				// $this->load->view('common/footer');
			}else
			{
				if($type=='stylist_manageorders'){
		          $this->load->view('not_permission');

		        }else
		        {
		            $this->load->view('common/header2');
		            $this->load->view('not_permission');
		            $this->load->view('common/footer2');
		        }
				// $this->load->view('common/header');
    //     		$this->load->view('not_permission');
    //     		$this->load->view('common/footer');

			}
		}
	}

	public function updateOrder()
	{
		if(!$this->input->is_ajax_request())
		{
			exit("No direct script access allowed");
		}else
		{
			$status_data = array();
			$orderPrdId = $this->input->post('id');
			$orderStatus = $this->input->post('orderStatus');
			$orderUniqueId 	= $this->input->post('orderUniqueId');
			$product_count = 0;
			$orderproduct_data = $this->Manage_orders_model->get_orderproductinfo_data($orderPrdId);
			$order_display_no = $orderproduct_data['order_display_no'];
			if($orderStatus==8)
			{				
				$product_id = $orderproduct_data['product_id'];				
				$objectId = $this->Scbox_model->get_object('box_order_products')[0]['object_id'];
				$metaValue = $this->Scbox_model->getSereializeProductDetail($order_display_no,'','',$objectId);
				$product_list  = unserialize($metaValue['object_meta_value']);	
				if(!empty($product_list))
				{
					$filtered_product = array_map(function (array $arr) {
				    // work on each array in the list of arrays
				    if($arr['is_delete'] == 0)
				    {
				    	// return the extended array
				    	return $arr;
				    }else
				    {
				    	return false;
				    }				    
					}, $product_list);

					$filtered_product = array_filter($filtered_product);
					$product_count = sizeof($filtered_product);
				}	
								
				$box_numofproducts = $this->Scbox_model->get_scboxdata_byproductid($product_id)->numofproducts;
				if($product_count==$box_numofproducts && $product_id!='190484')
				{
					$status_data['response'] = true;
					$status_data['message'] = 'Order Status Updated Successfully for Order Id : '.$order_display_no;
					$result = $this->Manage_orders_model->updateOrder($orderPrdId,$orderStatus);
					$this->sendwarehouse_email($orderPrdId,$orderUniqueId);
				}else
				{
					$status_data['response'] = false;
					$status_data['message'] = 'Please check the product count for this order.';
				}
				
			}else
			{
				$result = $this->Manage_orders_model->updateOrder($orderPrdId,$orderStatus);
				$status_data['response'] = true;
				$status_data['message'] = 'Order Status Updated Successfully for Order Id : '.$order_display_no;
			}
			
			$status = json_encode($status_data);
			echo $status;			
		}

	}
	
	public function sendwarehouse_email($orderPrdId = '',$orderUniqueId='')
	{
		// Send Email to warehouese

		$id 					= $orderPrdId; //$this->input->post('id');
		//$orderUniqueId 		= $this->input->post('orderUniqueId');
		//$cartId 				= $this->input->post('cartId');
		$objectkey 				= $this->Scbox_model->get_object('box_order_products')[0]['object_id'];
		$disPlayNo				= $this->Stylist_model->get_order_display_number($orderUniqueId);
		$product_data			= $this->Stylist_model->get_curated_data($objectkey,$disPlayNo['order_display_no']);
		$curatedProduct			= unserialize($product_data['object_meta_value']);
		
		if(is_array($curatedProduct) && count($curatedProduct))
		{
			foreach($curatedProduct as $key => $curatedData)
				{
					$productDataList[] = $this->Stylist_model->get_products($key);
					
				}			
		}
		$data_mail['selorderid']  	= $disPlayNo['order_display_no'];
		$data_mail['pdata'] 		= $curatedProduct;
		$data_mail['product_data'] 	= $productDataList;	
			
		 $config['protocol'] = 'smtp';         
        //$config['mailpath'] = '/usr/sbin/sendmail';
          $config['charset'] = 'iso-8859-1';
          $config['mailtype'] = 'html';
          $config['wordwrap'] = TRUE;
          $config['smtp_host'] = $this->config->item('smtp_host');
          $config['smtp_user'] = $this->config->item('smtp_user');
          $config['smtp_pass'] = $this->config->item('smtp_pass');
          $config['smtp_port'] = $this->config->item('smtp_port');

    	 $this->email->initialize($config);

		  $this->email->from($this->config->item('from_email'), 'StyleCracker');
		  	if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
		         $email = $this->config->item('sc_test_emaild');
		         $this->email->to($email); 
		    }else{
				  $this->email->to('warehouse@stylecracker.com');
				  $this->email->cc(array('arun@stylecracker.com','lavin@stylecracker.com'));
			}
		  // $this->email->to('sudha@stylecracker.com');
		  // $this->email->cc(array('arun@stylecracker.com','prajapati@stylecracker.com'));
		  $this->email->subject('Stylecracker : SCBox Order Warehouse- Curated Products: ('.$disPlayNo['order_display_no'].')');
		  $message = $this->load->view('emails/scbox_order_warehouse',$data_mail,true);
		  $this->email->message($message);
		  /* $this->email->print_debugger();
		  exit(); */
		  $this->email->send();
		   /*  if($this->email->send())
		  	{ echo "mail sent"; }
			else{ echo "mail not sent";	 }		 */			
					 
	}
	
	
	
	 

	public function orderstatus($orderStatus)
	{	
		$data['status'] = $orderStatus;
		$data['result'] = $this->Manage_orders_model->order_status($orderStatus);
		$this->load->view('common/header');
		$this->load->view('orders/order_status_view',$data);
		$this->load->view('common/footer');
	}
	
	function change_final_order_status(){
		
		$order_id = $this->input->post('order_id');
		$order_status = $this->input->post('order_status');
		$user_id = $this->input->post('user_id');
		
		$this->Manage_orders_model->change_final_order_status($order_id,$order_status,$user_id);
	}

	public function send_mail(){
		if(!$this->input->is_ajax_request())
		{
			exit("No direct script access allowed");
		}else
		{
			$order_id   = $this->input->post('order_id');
			$product_id = $this->input->post('product_id');
			$order_status = $this->input->post('order_status');
			$brand_id = $this->input->post('brand_id');
			$data = $this->Manage_orders_model->update_email($order_id,$product_id,$order_status);
			$data = $this->Manage_orders_model->get_order_unique_no($order_id);
			$product_info = $this->Manage_orders_model->get_product_info($product_id);
			
			if(!empty($data)){			
			if($order_status == '5'){
				
				/* added for notification */
				$notification_message = 'Your package for "'.$product_info['name'].' - '.$product_info['company_name'].'" has been cancelled.';
				$notification_data = $this->Manage_orders_model->update_notification_data($order_id,$product_id,$notification_message);
				$notification = $this->send_notification($notification_message,$data,$product_info);
				/* added for notification */
				$message = 'Your+package+for+"'.str_replace(' ', '+', $product_info['name']).'+-+'.str_replace(' ', '+', $product_info['company_name']).'"+has+been+cancelled.';
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=91'.$data[0]['mobile_no'].'&message='.$message);
				$this->ordercancelMessage($data[0]['order_unique_no'],$data[0]['user_id'],$data[0]['first_name'],$data[0]['last_name'],$order_id,$product_id,$order_status,$brand_id);

			}else if($order_status == '4'){
				
				/* added for notification */
				$notification_message = 'Your package for "'.$product_info['name'].' - '.$product_info['company_name'].'" has been delivered.';
				$notification_data = $this->Manage_orders_model->update_notification_data($order_id,$product_id,$notification_message);
				$notification = $this->send_notification($notification_message,$data,$product_info);
				/* added for notification */
			
				$message = 'Awesome!+Your+order+number+'.$data[0]['order_display_no'].'+has+been+delivered.+Enjoy.+We+hope+to+serve+you+soon.';
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=91'.$data[0]['mobile_no'].'&message='.$message);

				$this->order_Delivered_Message($data[0]['order_unique_no'],$data[0]['user_id'],$data[0]['first_name'],$data[0]['last_name'],$order_id,$product_id,$order_status,$brand_id);

			}else if($order_status == '3'){

				$email_data = array();

				$cartAwbNumber = $this->Manage_orders_model->get_cartid_for_awbnumber($order_id);
				
				$cartMetaData = $this->Manage_orders_model->get_cart_meta_data($cartAwbNumber['id']);
				//echo '<pre>';print_r($cartMetaData);exit();
				$delpartner = $this->Manage_orders_model->get_del_partner($cartMetaData[0]['cart_meta_value']);				
				//echo '<pre>';print_r($delpartner);exit();
				/* added for notification */
				if($delpartner['delivery_partner_name']!='' && $cartMetaData[1]['cart_meta_value']!='')
				{
					$email_data['awb_tracking_code'] = $cartMetaData[1]['cart_meta_value'];
					$email_data['dp_name'] = $delpartner['delivery_partner_name'];
					$email_data['tracking_link'] = $delpartner['tracking_link'];
					$notification_message = 'Your package for "'.$product_info['name'].' - '.$product_info['company_name'].'" has been dispatched. The '.$delpartner['delivery_partner_name'].' tracking code is '.$cartMetaData[1]['cart_meta_value'];
				}else
				{
					$notification_message = 'Your package for "'.$product_info['name'].' - '.$product_info['company_name'].'" has been dispatched.';
				}
			
				// $notification_data = $this->Manage_orders_model->update_notification_data($order_id,$product_id,$notification_message);	
				// $notification = $this->send_notification($notification_message,$data,$product_info);
				/* added for notification */
			
				//$message = 'Guess+what?+Your+StyleCracker+order+number+'.$data[0]['order_display_no'].'+has+already+been+dispatched!+It\'ll+be+on+your+doorstep+soon!';
				$message = 'Guess+what?+Your+StyleCracker+order+number+'.$data[0]['order_display_no'].'+has+already+been+dispatched!The+'.$email_data['dp_name'].'+tracking+code+is+'.$email_data['awb_tracking_code'].'+It\'ll+be+on+your+doorstep+soon!';
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=91'.$data[0]['mobile_no'].'&message='.$message);
			
				
				$this->order_Dispatched_Message($data[0]['order_unique_no'],$data[0]['user_id'],$data[0]['first_name'],$data[0]['last_name'],$order_id,$product_id,$order_status,$brand_id,$email_data);
			}else if($order_status == '2'){
				
				/* added for notification */
				$notification_message = 'We have great news! Your SCBox order number '.$data[0]['order_display_no'].' has been confirmed.';
				//$notification_message = 'Your package for "'.$product_info['name'].' - '.$product_info['company_name'].'" has been confirmed.';
				$notification_data = $this->Manage_orders_model->update_notification_data($order_id,$product_id,$notification_message);
				$notification = $this->send_notification($notification_message,$data,$product_info);
				
				/* added for notification */
				//$message = 'Your+package+for+"'.str_replace(' ', '+', $product_info['name']).'+-+'.str_replace(' ', '+', $product_info['company_name']).'"+has+been+confirmed.';
				
				$message = 'We+have+great+news!+Your+SCBox+order+number+'.$data[0]['order_display_no'].'+has+been+confirmed.';
				
				file_get_contents('http://txtguru.in/imobile/api.php?username='.SMS_USERNAME.'&password='.SMS_PASSWORD.'&source=STYCKR&dmobile=91'.$data[0]['mobile_no'].'&message='.$message);
				
				$this->order_Confirmed_Message($data[0]['order_unique_no'],$data[0]['user_id'],$data[0]['first_name'],$data[0]['last_name'],$order_id,$product_id,$order_status,$brand_id);
			}

			}
		}
	}

	function getEmailAddress($user_id){
      $res = $this->Manage_orders_model->get_user_info($user_id);
      if(!empty($res)){
        return $res[0]['email'];
      }else{ return ''; }
	}

    function getBrands_info($user_id){
      $res = $this->Manage_orders_model->getBrands_info($user_id);
      if(!empty($res)){
        return $res[0]['company_name'];
      }else{ return ''; }
  }
	
public function exportExcel($param,$search_by=NULL,$table_search=NULL,$is_download=NULL,$search_by2=NULL)
  { $is_download = $this->uri->segment(6);

    if($search_by == 6 )
    {
      $table_search_encode = base64_decode($table_search);
    }else if($search_by == 8)
    {
      $table_search_encode = base64_decode($table_search);
    }else
    {
      $table_search_encode = $table_search;
    }
    //$is_download = $this->input->post('param');
        $fieldName = array('Date','Order Unique No','Order Display No','Order ID','Product SKU','ProductId','Product Name','Company Name','Original Product Price','Product Price','Payment Type','Order Status','Size','Quantity', 'Order Total','Coupon Code-Referal Code','Username','Phone Number','Email','Shipping Address','City','State','Pincode','Look id','Platform','Category','Sub-Category','Variations','Variations Value','utm source ','utm medium','utm campaign','Payment Status','Age Range','Gender','Birth Date');
    /*$fieldName = array('Date','Order ID','Product Name','Brand Name','Payment Type','Unique Reference Number','Transaction Id','Quantity','Product Amount','Brand Tax Amount','Shipping Charges','COD Charges','Grand Total','Rate Of Commission(%)','Commission Amount','PG cost @3.35%','Service Tax Amount(14.0%)','Swach Bharat Abhiyan(0.5%)','Total Commission Invoice','TDS deduction by Brand on commission','Net Income receivable from Brand','Brand Income','Brand Payout','Days Lapsed','Payment Status','Username','Phone Number','Email','Shipping Address','City','State','Pincode');*/
    $get_data = $this->Manage_orders_model->getViewOrdersExcel($search_by,$table_search_encode,'','',$search_by2);
   // echo '<pre>';print_r($get_data);
    $result_data = array();
    $i =0;

    foreach($get_data as $val) {

           $referral_point = 0;$referalCode = '';
           if($val['OrderProductTotal']!='' && $val['OrderProductTotal']!=0)
           {
           	 $product_percent =  (($val['product_price']*100)/$val['OrderProductTotal']);
           }
          
           $referral_point = $this->Manage_orders_model->get_refre_amt($val['order_unique_no']); 
           if($referral_point>0)
           {
              /*$refdiscount = round((($percent*$referral_point)/100),2);*/
              $referalCode = REDEEM_POINTS_CART_TEXT.'-'.$referral_point;
           }    
           
       
            $result_data[$i][] = $val['created_datetime'];
            $result_data[$i][] = $val['order_unique_no'];
            $result_data[$i][] = $val['order_display_no'];      
            $result_data[$i][] = $val['order_id'];
            $result_data[$i][] = $val['product_sku'];
            $result_data[$i][] = $val['id'];
            $result_data[$i][] = $val['product_name'];
            $result_data[$i][] = $val['company_name'];
            //$result_data[$i][] = $val['actual_product_price'];
            $result_data[$i][] = ($val['id']=='190484') ? $val['product_price'] : $val['actual_product_price'];
            $result_data[$i][] = $val['product_price'];
            $brand_code ='';
            $product_price = 0;
            $commission_rate = 0;
            if($val['pay_mode']==1)
            {
              $result_data[$i][] = 'COD';
            }else
            {
              $result_data[$i][] = 'Online';
            }
            $date_ref_no = explode('-',$val['created_datetime']);

            if($val['brand_code']!='')
            {
              $brand_code = $val['brand_code'];
            }

            //$result_data[$i][] = $val['daysLapsed'];
            $order_status = "";
            if($val['order_status'] == 1)
            {
                $order_status = 'Processing';
            }else if($val['order_status'] == 2)
            {
                $order_status = 'Confirmed';
            }else if($val['order_status'] == 3)
            {
                $order_status = 'Dispatched';
            }else if($val['order_status'] == 4)
            {
                $order_status = 'Delivered';
            }else if($val['order_status'] == 5)
            {
                $order_status = 'Cancelled';
            }else if($val['order_status'] == 6)
            {
                $order_status = 'Return';
            }else if($val['order_status'] == 7)
            {
                $order_status = 'Fake';
            }

            $result_data[$i][] = $order_status;

            $result_data[$i][] = $val['size_text'];
            //$result_data[$i][] = $brand_code.'/'.$date_ref_no[1].'/'.$date_ref_no[0];
            if(isset($val['tranid']))
            {
              //$result_data[$i][] = $val['tranid'];
            }

            @$result_data[$i][] = $val['product_qty'];

            //$result_data[$i][] = $val['product_price'];
            //$result_data[$i][] = $val['order_tax_amount'];
            //$result_data[$i][] = $val['shipping_amount'];
            //$result_data[$i][] = $val['cod_amount'];
            /* Coupon Setting Page */

              if(isset($val['coupon_code']) && $val['coupon_code']!='')
              {
                $coupon_info = $this->Manage_orders_model->getUserCoupon($val['coupon_code'],$val['created_datetime'],$val['brand_id']);
                /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
                $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/
                $coupon_brand = $coupon_info['brand_id'];
                $coupon_products = $coupon_info['coupon_products'];
                if($coupon_products!='' || $coupon_products!=0)
                {
                  $coupon_products_arr =explode(',',$coupon_products);
                }
                $coupon_min_spend = $coupon_info['coupon_min_spend'];
                $coupon_max_spend = $coupon_info['coupon_max_spend'];
                $coupon_discount_type = $coupon_info['coupon_discount_type'];
                $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
                $individual_use_only =  $coupon_info['individual_use_only'];
                $data['coupon_code'] = $val['coupon_code'];
                $coupon_product_price = 0;
                $data['coupon_discount'] = 0;
                $price = $val['product_price'];

                 if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
                  {
                    if($coupon_products!=''&& $coupon_products!=0)
                    {
                       //coupon_discount_type =3 (Product discount)
                      if($coupon_discount_type==3)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $data['coupon_discount'] = $coupon_info['coupon_amount'];
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      }
                       //coupon_discount_type =4 (Product % discount)
                      if($coupon_discount_type==4)
                      {
                        if(in_array($val['product_id'], $coupon_products_arr))
                        {
                          if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            $data['coupon_discount'] = $price*($coupon_percent/100);
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                      }

                    }else
                    {
                       //coupon_discount_type =1 (cart discount)
                      if($coupon_discount_type==1)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $data['coupon_discount'] = $coupon_info['coupon_amount'];
                          $data['coupon_code'] = $val['coupon_code'];
                        }
                      }
                       //coupon_discount_type =2 (cart % discount)
                      if($coupon_discount_type==2)
                      {
                        $coupon_product_price = $coupon_product_price+$price;
                        if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                        {
                          $coupon_percent = $coupon_info['coupon_amount'];
                          $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                          $data['coupon_code'] = $val['coupon_code'];
                        }
                      }
                    }
                  }

                if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
                {
                    //coupon_discount_type =1 (cart discount)
                        if($coupon_discount_type==1)
                        {
                          //$coupon_product_price = $coupon_product_price+$price;
                          $coupon_product_price = $val['OrderProductTotal'];
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $data['coupon_discount'] = $coupon_info['coupon_amount'];
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                         //coupon_discount_type =2 (cart % discount)
                        if($coupon_discount_type==2)
                        {
                          $coupon_product_price = $coupon_product_price+$price;
                          if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                          {
                            $coupon_percent = $coupon_info['coupon_amount'];
                            //$data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                             $data['coupon_discount'] = $val['OrderProductTotal']*($coupon_percent/100);
                            $data['coupon_code'] = $val['coupon_code'];
                          }
                        }
                }


              }else
              {
                $data['coupon_discount']=0;
                $data['coupon_code'] = '';
              }

/* Coupon Setting Page End */   
            if($this->session->userdata('role_id') != 6){
              $result_data[$i][] = $val['order_total']-($data['coupon_discount']+$referral_point);
            }else{
              
              /*$pper = ($val['product_price']*100)/$val['OrderProductTotal'];
              $stot = $this->Manage_orders_model->calculate_product_discountAmt($val['product_price'],$val['OrderProductTotal'],$coupon_discount_type,($data['coupon_discount']));
              $ref_amt = ($pper*$referral_point)/100;*/

              $result_data[$i][] =$val['product_price'];/*$stot - $ref_amt;*/
            }
            //$result_data[$i][] = $val['commission_rate'];
            $result_data[$i][] = $val['coupon_code'].' '.$referalCode;
            if(@$val['product_price']!='')
            {
              //$product_price = $val['product_price'];
            }

            if(@$val['commission_rate']!='')
            {
              $commission_rate = $val['commission_rate'];
            }
            $comm_amt =  $product_price*($commission_rate/100);
            //$result_data[$i][] = $comm_amt;
            $pgCost=0;
            if($val['pay_mode']!=1)
            {
              $pgCost = $comm_amt * (3.35/100);
            }

            //$result_data[$i][] = round($pgCost,2);

            $service_tax = $comm_amt+$pgCost*(14.00/100);
            //$result_data[$i][] = round($service_tax,2);

            $swachba = ($comm_amt+$pgCost)*(0.50/100);
            //$result_data[$i][] = round($swachba,2);

            $total_comm_invoice = $comm_amt+$service_tax+$swachba;
            //$result_data[$i][] = round($total_comm_invoice,2);

            $tds_deduction = ($comm_amt+$pgCost)*(10/100);
            //$result_data[$i][] =  round($tds_deduction,2);

            $netIncome_rec_brand =  $total_comm_invoice-$tds_deduction;
            //$result_data[$i][] = round($netIncome_rec_brand,2);

            $brand_income = $val['order_total']-$total_comm_invoice;
            //$result_data[$i][] = round($brand_income,2);

            $brand_payout =  $tds_deduction+$brand_income;
            //$result_data[$i][] = round($brand_payout,2);

            //$result_data[$i][] = $val['daysLapsed'];
            if($val['payment_status'] == 1)
            {
              $pay_status = 'Success';
            }else if($val['payment_status'] == 2)
            {
              $pay_status = 'Cleared';
            }else
            {
              $pay_status = 'Pending';
            }
            //$result_data[$i][] = $pay_status;


            $result_data[$i][] = $val['first_name']." ".$val['last_name'];
            $result_data[$i][] = $val['mobile_no'];
            $result_data[$i][] = $val['email_id'];
            $result_data[$i][] = trim($val['shipping_address']);
            $result_data[$i][] = trim($val['city_name']);
            $result_data[$i][] = trim($val['state_name']);
            $result_data[$i][] = $val['pincode'];
            $result_data[$i][] = $val['look_id'];
            $result_data[$i][] = $val['platform_info'];
           // $result_data[$i][] = $val['product_cat_id'];
            //$result_data[$i][] = $val['product_sub_cat_id'];
            //$result_data[$i][] = $this->product_variations($val['id']);
            //$result_data[$i][] = $this->product_variations_value($val['id']);
            $result_data[$i][] = '';
            $result_data[$i][] = '';
            $result_data[$i][] = '';
            $result_data[$i][] = '';
			$result_data[$i][] = $val['sc_source'];
			$result_data[$i][] = $val['sc_medium'];
			$result_data[$i][] = $val['sc_campaign'];
			if($val['is_paymentdone'] == '1' || $val['pay_mode'] == '2'){ $payment_status = 'Paid'; }else{ $payment_status = 'Pending'; }
			$result_data[$i][] = $payment_status;
			$result_data[$i][] = @$val['user_age'];
			$result_data[$i][] = $val['gender'];
			$result_data[$i][] = $val['birth_date'];
            $i++;
        }
        //echo "<pre>";print_r($result_data);exit;
       // echo $param.' '.$is_download;
    	if($param == 'downloadExcel' && $is_download == 1)
        {
        //========================Excel Download==============================
          //$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
         $this->writeDataintoCSV($fieldName,$result_data,'view_order_payment');
         //========================End Excel Download==========================


        }

  }

  public function product_variations($product_id){
      $product_variations = $this->Manage_orders_model->product_variations($product_id);
      $product_variations_html = ''; $i = 0;
      if(!empty($product_variations)){
        foreach($product_variations as $val){
          if($i != 0) { $product_variations_html = $product_variations_html.','; }
          $product_variations_html = $product_variations_html.$val['name'];
        $i++;
        }
      }
      return $product_variations_html;
  }

    public function product_variations_value($product_id){
      $product_variations = $this->Manage_orders_model->product_variations_value($product_id);
      $product_variations_html = ''; $i = 0;
      if(!empty($product_variations)){
        foreach($product_variations as $val){
          if($i != 0) { $product_variations_html = $product_variations_html.','; }
          $product_variations_html = $product_variations_html.$val['name'];
        $i++;
        }
      }
      return $product_variations_html;
  }	

	function calculateCouponAmount($cartproduct=array(),$totalProductPrice=null)
  {
      if(!empty($cartproduct))
      {
        $total_product_price = 0; $coupon_product_price = 0;
        foreach($cartproduct as $val)
        {
            if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
            {
               $price = $val['discount_price'];
            }else
            {
               $price = $val['product_price'];
            }
            $total_product_price = $total_product_price+$price;
        }
        foreach($cartproduct as $val)
        {

          if(@$val['discount_percent']!='' || @$val['discount_percent']!=0)
          {
             $price = $val['discount_price'];
          }else
          {
             $price = $val['product_price'];
          }

           $coupon_info = $this->Manage_orders_model->getUserCoupon($val['coupon_code'],$val['brand_id']);
            $coupon_brand = $coupon_info['brand_id'];
            $coupon_products = $coupon_info['coupon_products'];
            if($coupon_products!='' || $coupon_products!=0)
            {
              $coupon_products_arr =explode(',',$coupon_products);
            }
            $coupon_min_spend = $coupon_info['coupon_min_spend'];
            $coupon_max_spend = $coupon_info['coupon_max_spend'];
            $coupon_discount_type = $coupon_info['coupon_discount_type'];
            $coupon_stylecracker =  $coupon_info['stylecrackerDiscount'];
            $individual_use_only =  $coupon_info['individual_use_only'];

          if(isset($val['coupon_code']) && $val['coupon_code']!='')
          {

            /*$data['coupon_discount'] = $coupon_info['coupon_amount'];
            $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];*/

            $data['coupon_code'] = $val['coupon_code'];
            $coupon_product_price = 0;

              if($coupon_brand==$val['brand_id'] && $coupon_brand!=0)
              {
                if($coupon_products!=''&& $coupon_products!=0)
                {
                   //coupon_discount_type =3 (Product discount)
                  if($coupon_discount_type==3)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $coupon_info['coupon_amount'];
                        $data['coupon_code'] = $val['coupon_code'];
                      }
                    }
                  }
                   //coupon_discount_type =4 (Product % discount)
                  if($coupon_discount_type==4)
                  {
                    if(in_array($val['product_id'], $coupon_products_arr))
                    {
                      if($coupon_min_spend<=$price && $price<=$coupon_max_spend)
                      {
                        $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                        $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                        $coupon_percent = $coupon_info['coupon_amount'];
                        $data['coupon_discount'] = $price*($coupon_percent/100);
                        $data['coupon_code'] = $val['coupon_code'];
                      }
                    }
                  }

                }else
                {
                   //coupon_discount_type =1 (cart discount)
                  if($coupon_discount_type==1)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                      $data['coupon_discount'] = $coupon_info['coupon_amount'];
                      $data['coupon_code'] = $val['coupon_code'];
                    }
                  }
                   //coupon_discount_type =2 (cart % discount)
                  if($coupon_discount_type==2)
                  {
                    $coupon_product_price = $coupon_product_price+$price;
                    if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                    {
                      $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                      $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                      $coupon_percent = $coupon_info['coupon_amount'];
                      $data['coupon_discount'] = $coupon_product_price*($coupon_percent/100);
                      $data['coupon_code'] = $val['coupon_code'];
                    }
                  }
                }
              }

       if(($coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only!=1  )|| ( $coupon_brand==0  && $coupon_stylecracker==1 && $individual_use_only==1 ))
            {
              $coupon_product_price = $totalProductPrice;

                   //coupon_discount_type =1 (cart discount)
                if($coupon_discount_type==1)
                {

                  if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                  {
                    $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                    $data['coupon_discount'] = $coupon_info['coupon_amount'];
                    $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                    $data['coupon_code'] = $val['coupon_code'];
                  }
                }
                 //coupon_discount_type =2 (cart % discount)
                if($coupon_discount_type==2)
                {
                  if($coupon_min_spend<=$coupon_product_price && $coupon_product_price<=$coupon_max_spend)
                  {
                    $data['coupon_amount'] =  $coupon_info['coupon_amount'];
                    $coupon_percent = $coupon_info['coupon_amount'];
                    $data['coupon_discount_type'] = $coupon_info['coupon_discount_type'];
                    $data['coupon_discount'] = $totalProductPrice*($coupon_percent/100);
                    $data['coupon_code'] = $val['coupon_code'];
                  }
                }
            }

          }else
          {
            $data['coupon_discount']=0;
            $data['coupon_code'] = '';
          }
        }
      }
      return $data;
  }

  function orderDetailsView()
  {
      if (!$this->input->is_ajax_request()) {
          exit('No direct script access allowed');
      }else{
		$imagify_brandid = unserialize(IMAGIFY_BRANDID); 
        $i=0;
        $orderStatusList = array('1'=>'Processing', '2'=>'Confirmed', '8'=>'Curated', '3'=>'Dispatched', '4'=>'Delivered','5'=>'Cancelled','6'=>'Return','7'=>'Fake');

        $order_unique_id = $this->input->post('order_unique_id');
        if($order_unique_id!="")
        {
          $result = $this->Manage_orders_model->getOrderProductInfo($order_unique_id);
        }  

        $order_pro_array = array(); $couponDiscountDiff = array();
        $coupDiscountPrdPrice = 0;
        $totalProductPrice = 0;$referral_point = 0;$orderDispalyNo = '';
        $referral_point = $this->Manage_orders_model->get_refre_amt($order_unique_id);
        $count_row = 0;$dpButton = ''; 
         if(!empty($result)){
          foreach($result as $value)
          { $count_row++;
            if($value['coupon_code']!='' || $referral_point>0)
            {
             //$totalProductPrice = $totalProductPrice+$value['product_price'];
              $totalProductPrice = $this->Manage_orders_model->get_order_price_ref($value['order_unique_no']);
            }
          }
        }
       
        /*Coupon Discount */
        $coupon_result = $this->Manage_orders_model->calculateCouponAmount($result,$totalProductPrice);

        $coupon_discount_type= @$coupon_result['coupon_discount_type'];
        $coupon_discount= @$coupon_result['coupon_discount'];
        $coupon_code = strtoupper(@$coupon_result['coupon_code']);
        $sc_coupon_discount = @$coupon_result['coupon_amount'];
        $coupon_stylecracker = @$coupon_result['coupon_stylecracker'];
        $coupon_brand = @$coupon_result['coupon_brand'];
        /*Coupon Discount End */
  
        if(!empty($result)){
        foreach($result as $value){
        $coupDiscountPrdPrice = 0; $ordTotal = 0; $prdDiscountAmtDiff = 0;

        if($value['coupon_code']!='' && $coupon_code!='' )//&& $coupon_stylecracker == 1 && $coupon_brand == 0
        {
          if($coupon_discount_type==1)
          {
            $coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt_order($value['product_price'],$totalProductPrice,$coupon_discount_type,($coupon_discount+$referral_point));


          }else if($coupon_discount_type==2)
          {
            $coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt_order($totalProductPrice,$totalProductPrice,$coupon_discount_type,$sc_coupon_discount);
            $coupDiscountPrdPrice = $totalProductPrice-$coupDiscountPrdPrice;
              $coupDiscountPrdPrice = $value['product_price'] - ($referral_point/$count_row);   

          }else if($coupon_discount_type==3 )
          {
            if($coupon_brand == $value['brand_id'])
            {
             $coupDiscountPrdPrice = $value['product_price']-$coupon_discount;
            }
          }else if($coupon_discount_type==4)
          {
            if($coupon_brand == $value['brand_id'])
            { 
             $coupDiscountPrdPrice = $value['product_price']-$sc_coupon_discount;
            }
          }
        }

        /* if($value['coupon_code']==''  && $referral_point >0)
        {    
            $coupon_discount_type = 1;  
            $coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt_order($totalProductPrice,$totalProductPrice,$coupon_discount_type,$referral_point);
            $coupDiscountPrdPrice = $totalProductPrice-$coupDiscountPrdPrice;
            $coupDiscountPrdPrice = $value['product_price'] - ($referral_point/$count_row);           
           
        }*/

         if($coupDiscountPrdPrice>0)
        {
          $grandPrice = $coupDiscountPrdPrice;
        }else
        {
          $grandPrice = $value['product_price'];
        }

        $order_pro_array[$value['brand_id']][] = array(
            'product_id' => $value['product_id'],
            'look_id' =>  $value['look_id'],
            'product_name' => $value['product_name'],
            'size_text' => $value['size_text'],
            'image' => $value['image'],
            'product_qty' => $value['product_qty'],
            'product_price' => $value['product_price'],
            'company_name' => $value['company_name'],
            'product_sku' => $value['product_sku'],
            'order_tax_amount' => $value['order_tax_amount'],
            'cod_amount' => $value['cod_amount'],
            'shipping_amount' => $value['shipping_amount'],
            'order_total' => $value['order_total'],
            'discount_price' => @$value['discount_price'],
            'product_size' => @$value['product_size'],
            'brand_id' => $value['brand_id'],
            'order_id' => $value['order_id'],
            'user_id' => $value['user_id'],
            'coupon_code' => $value['coupon_code'],
            'coupon_discount_price' => $coupDiscountPrdPrice,
            'grand_price' => $grandPrice,
            'OrderProductTotal' => $value['OrderProductTotal'],
            'OrderGrandTotal' => $value['OrderGrandTotal'],
            'OrderIdProductSum' => $value['OrderIdProductSum'],
            'order_prd_id' => $value['order_prd_id'],
            'user_id' => $value['user_id'],
            'order_sub_total' => $value['order_sub_total'],
            'send_email' => $value['send_email'],
            'pay_mode' => $value['pay_mode'],
            'created_datetime' => $value['created_datetime'],
            'modified_datetime' => $value['modified_datetime'],
            'order_status' => $value['order_status'],
            'company_name' => $value['company_name'],
            'order_unique_no' => $value['order_unique_no'],
            'discount_percent' => @$value['discount_percent'],
            'remark' => $value['remark'],
            'product_count' =>$count_row,
            'actual_product_price' => ($value['product_id']=='190484') ? $value['product_price'] : $value['actual_product_price'],
            'slug' =>@$value['slug'],
            'dp_active' => $value['dp_active'],
            'dpartner' => $this->Manage_orders_model->getOrderDp($value['brand_id']),
            'bms_discount' => @$value['bms_discount'],
			'order_display_no' => $value['order_display_no'],
			'txnid' =>@$value['txnid'],
            'mihpayid' => $value['mihpayid'],
            'online_pay_discount' => $value['online_pay_discount'],
            'order_coupon_amount' => $value['coupon_amount'],
          );

           /* if($coupDiscountPrdPrice>0)
            {
              $prdDiscountAmtDiff = $value['product_price'] - $coupDiscountPrdPrice;
            }else
            {
              $couponDiscountDiff[$value['brand_id']]['OrderSubTotal'] = $value['order_total'];
            }

            if(isset($couponDiscountDiff[$value['brand_id']]['OrderSubTotal']))
            {
              $couponDiscountDiff[$value['brand_id']]['OrderSubTotal'] = $couponDiscountDiff[$value['brand_id']]['OrderSubTotal']-$prdDiscountAmtDiff ;
            }else
            {
              $couponDiscountDiff[$value['brand_id']]['OrderSubTotal'] = $value['order_total']-$prdDiscountAmtDiff ;
            }*/
        }
      }

    //echo '<pre>'; print_r($order_pro_array);echo '</pre>'; //exit();
    	$gift_html = '';
      $scbox_gift_object = $this->Scbox_model->get_object('scbox_gift')[0]['object_id']; 
      if(@$value['order_display_no']!='')
      {
      	$gift_data = $this->Scbox_model->get_gift_data($scbox_gift_object,$value['order_display_no']);
      }          
     
      if(!empty($gift_data))
      {
        $gift_html = '<div class="col-md-6" style="text-align:right;">  
                          <h4><b>RECEPIENT\'S INFO</b></h4>
                          <i style="color:red" font-size:12px;>GIFT</i>
                          <label>Name:</label> '.@$gift_data['name'].'<br>
              <label>Email:</label> '.@$gift_data['email'].'<br>
                          <label><strong>Mobile:</strong>
                          </label>'.@$gift_data['mobileno'].'<br>
                            <div><label>Address:</label> '.@$gift_data['address'].' '.@$gift_data['pincode'].' '.@$gift_data['city'].'</div>
                         </div>';
      }else
      {
        $gift_html = '<div class="col-md-6" style="text-align:right;">  
                          <h4><b>RECEPIENT\'S INFO</b></h4>
                          <label>Name:</label> '.@$result[0]['first_name'].' '.@$result[0]['last_name'].'<br>  
              <label>Email:</label> '.@$result[0]['email_id'].'<br>
                          <label><strong>Mobile:</strong>
                          </label>'.@$result[0]['mobile_no'].'<br>
                            <div><label>Address:</label> '.@$result[0]['shipping_address'].' '.@$result[0]['pincode'].' '.@$result[0]['city_name'].'</div>
                         </div>';
      }

        $orderDetailhtml = '<div class="box-quick-view">
                  <div class="box box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Order Details</h3>
                        <div class="row">	                     
	                      <div class="col-md-6" >    
	                        <h4><b>SENDER\'S INFO</b></h4>
	                        <label>Name:</label> '.@$result[0]['first_name'].' '.@$result[0]['last_name'].'<br>  
	                        <label>Email:</label> '.@$result[0]['email_id'].'<br>
	                        <label>Mobile:</label> '.@$result[0]['mobile_no'].'<br>
	                          <div><label>Address:</label> '.@$result[0]['shipping_address'].' '.@$result[0]['pincode'].' '.@$result[0]['city_name'].'</div>
                      </div>
                       '.$gift_html.'
                      </div>  
                    </div><!-- /.box-header -->
                      <div class="box-body">
                         <div class="table-responsive">
                            <table class="table table-bordered table-striped dataTable table-order-details " style="width:100%">
                              <thead>
                                <tr>
                                  <!--<th width="20">Sr No.</th>-->
                                  <th>ADP</th>
                                  <th>Order ID</th>
                                  <th>Product SKU</th>                                  
                                  <th width="50">Produc Image</th>
                                  <th>Brand Name</th>
                                  <th>Product Name(Look Id)</th>
                                  <th width="20">Qty</th>
                                  <th>Size</th>
                                  <th width="90">Original Product Price</th>
                                  <th width="90">Product Price</th>
                                  <th width="90">Discounted Price</th>
                                  <th width="90">Order Total</th>
                                  <th width="110">Status</th>
                                  <th>Remark</th>
                                  <th>Send Email</th>
                                </tr>
                              </thead>';


        /*foreach($result as $val)
        { */

        if(!empty($order_pro_array)) {
        foreach($order_pro_array as $b_id => $value){
          if(!empty($order_pro_array[$b_id])){
            foreach ($order_pro_array[$b_id] as $val) {

          $i++;$taxCodShipDiv = '';$statusDiv=''; $coupDiscountPrdPrice = 0;
          $ordTotal = 0; $prdDiscountAmtDiff = 0;
          $referral_point = 0;$show_mail=0;$show_mail_text='';$show_email_button = '';$dpDiv ='';
          $referral_point = $this->Manage_orders_model->get_refre_amt($val['order_unique_no']);

          //$totalProductPrice=$val['OrderProductTotal'];
          
          /*Coupon Discount */
          $coupon_result = $this->Manage_orders_model->calculateCouponAmount($result,$totalProductPrice);
          $coupon_discount_type= @$coupon_result['coupon_discount_type'];
          $coupon_discount= @$coupon_result['coupon_discount'];
          $coupon_discount= $val['order_coupon_amount']; 
          $coupon_code = strtoupper(@$coupon_result['coupon_code']);
          $sc_coupon_discount = @$coupon_result['coupon_amount'];
          $coupon_stylecracker = @$coupon_result['coupon_stylecracker'];
          $coupon_brand = @$coupon_result['coupon_brand'];
          /*Coupon Discount End */
         /* echo '<pre>';print_r($coupon_result);  */


          if($val['coupon_code']!='' && $coupon_code!='' )//&& $coupon_stylecracker == 1 && $coupon_brand == 0
          {
            if($coupon_discount_type==1)
            {
              $coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt_order($val['product_price'],$totalProductPrice,$coupon_discount_type,($coupon_discount+$referral_point));                           

            }else if($coupon_discount_type==2)
            {
              /*$coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt($totalProductPrice,$totalProductPrice,$coupon_discount_type,$sc_coupon_discount);
              
              //$coupDiscountPrdPrice = $totalProductPrice-$coupDiscountPrdPrice;
              $coupDiscountPrdPrice = $val['product_price'] - (($coupDiscountPrdPrice/$val['product_count'])+($referral_point/$val['product_count']));*/

              $coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt($totalProductPrice,$totalProductPrice,$coupon_discount_type,$sc_coupon_discount);
              if($totalProductPrice>0)
              {
                $percent =  (($val['product_price']*100)/$totalProductPrice);                           
              } 
              //$coupDiscountPrdPrice = $val['product_price'] - ((($percent*$coupDiscountPrdPrice)/100) + (($percent*$referral_point)/100));
              $coupDiscountPrdPrice = $val['product_price'] - $val['order_coupon_amount'];   
             

            }else if($coupon_discount_type==3 || $coupon_discount_type==4)
            {
              if($coupon_brand == $val['brand_id'])
              {
               $coupDiscountPrdPrice = $val['product_price']-($coupon_discount+$referral_point);              
              }
               
            }
          }
          
        if($val['coupon_code']==''  && $referral_point >0)
        {    
            $coupon_discount_type = 1;  
            $coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt_order($totalProductPrice,$totalProductPrice,$coupon_discount_type,$referral_point);
            $coupDiscountPrdPrice = $totalProductPrice-$coupDiscountPrdPrice;
            //$coupDiscountPrdPrice = $val['order_coupon_amount'];   
            if($totalProductPrice>0)
            {
              $percent =  (($val['product_price']*100)/$totalProductPrice);
              $refdiscount = round((($percent*$referral_point)/100),2);              
            }            
            //$coupDiscountPrdPrice = $val['product_price'] - ($referral_point/$val['product_count']);
            $coupDiscountPrdPrice = $val['product_price'] - $refdiscount;                   
           
        }

          /*if($val['coupon_code']==''  && $referral_point >0)
          {
              $coupon_discount_type = 1;
              $coupDiscountPrdPrice = $this->Manage_orders_model->calculate_product_discountAmt($val['product_price'],$totalProductPrice,$coupon_discount_type,$referral_point);

          }
*/

           if($coupDiscountPrdPrice>0)
          {
           $prdDiscountAmtDiff = $val['product_price'] - $coupDiscountPrdPrice;
          }else
          {
            $couponDiscountDiff[$val['brand_id']]['OrderSubTotal'] = $val['order_total'];
          }

          if(isset($couponDiscountDiff[$val['brand_id']]['OrderSubTotal']))
          {
            $couponDiscountDiff[$val['brand_id']]['OrderSubTotal'] = $couponDiscountDiff[$val['brand_id']]['OrderSubTotal']-$prdDiscountAmtDiff ;
          }else
          {
            $couponDiscountDiff[$val['brand_id']]['OrderSubTotal'] = $val['order_total']-$prdDiscountAmtDiff ;
          }

          if($val['bms_discount']>0)
          {
            $couponDiscountDiff[$val['brand_id']]['OrderSubTotal'] = $val['order_total']-$val['bms_discount'] ;
          }

          if($coupDiscountPrdPrice>0)
          {
            $grandPrice = $coupDiscountPrdPrice;
          }else
          {
            $grandPrice = $val['product_price'];
          }

            if($val['order_tax_amount']>0)
            {
              $taxCodShipDiv = $taxCodShipDiv.'<div><label>Tax</label> <span class="price"><i class="fa fa-inr"></i> '.$val['order_tax_amount'].'</span></div>';
            }

            if($val['cod_amount']>0)
            {
              $taxCodShipDiv = $taxCodShipDiv.'<div><label>COD</label> <span class="price"> <i class="fa fa-inr"></i> '.$val['cod_amount'].'</span></div>';
            }

            if($val['shipping_amount']>0)
            {
              $taxCodShipDiv = $taxCodShipDiv.' <div><label>Shipping</label> <span class="price"> <i class="fa fa-inr"></i> '.$val['shipping_amount'].'</span></div>';
            }

             if($referral_point>0)
            {
              $taxCodShipDiv = $taxCodShipDiv.' <div><label>(Referral Discount </label> <i class="fa fa-inr"> '.$referral_point.')</i>  <span class="price"></span></div>';
            }

            if($val['online_pay_discount']>0 && $val['pay_mode']==2)
            {
            	$online_pay_discount_amt  = ($val['product_price']-$prdDiscountAmtDiff)*$val['online_pay_discount']/100;
              $taxCodShipDiv = $taxCodShipDiv.' <div><label>(Online Payment Discount ) -'.$val['online_pay_discount'].' %</label>   <span class="price"> <i class="fa fa-inr"></i>'.$online_pay_discount_amt.'</span></div>';
            }else
            {
            	$online_pay_discount_amt  = 0;
            }

            if($val['bms_discount']>0)
            {
              $taxCodShipDiv = $taxCodShipDiv.' <div><label>(BMS Discount </label> <i class="fa fa-inr"> '.$val['bms_discount'].')</i>  <span class="price"></span></div>';
            }

            if($val['dp_active']==1){ $dpstatus='checked';}else{ $dpstatus=''; } 
			
			$cartAwbNumber = $this->Manage_orders_model->get_cartid_for_awbnumber($val['order_id']);
			
           // $statusDiv = ' <td>
           //  <select name="orderStatus" id="orderStatus_'.$val['order_prd_id'].'" class="orderStatus form-control" order="'.$val['order_prd_id'].'" onchange="SetOrderStatus(\''.$val['order_prd_id'].'\',\''.$val['order_unique_no'].'\',\''.$cartAwbNumber['id'].'\')" >';
           //  foreach($orderStatusList as $key=>$value)
           //  {
           //    if($val['order_status'] == $key)
           //    {
           //      $statusDiv=$statusDiv.'<option  value="'.$key.'" selected >'.$value.'</option>';
           //    }else if(($val['order_status'] == 1 && ($key==2 || $key==5 ||  $key==7 ))){ /*'Processing' Status*/
           //      $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
           //    }else if(($val['order_status'] == 2 && ($key==3 || $key==4 || $key==5 || $key==7 || $key==8 ))){ /*'Confirmed' Status*/
           //      $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
           //    }else if(($val['order_status'] == 8 && ($key==3 || $key==4 || $key==5 || $key==7 || $key==8 ))){ /*'Curated' Status*/
           //      $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
           //    }else if(($val['order_status'] == 3 && ($key==3 || $key==4 || $key==5 || $key==6 ))){ /*'Dispatch' Status*/
           //      $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
           //    }else if(($val['order_status'] == 4 && ($key==4 || $key==6 ))){ /*'Delivered' Status*/
           //      $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
           //    }
           //    //  else if(($val['order_status'] <= $key) || ($val['order_status'] == 8 && ($key==3 || $key==4 || $key==5 || $key==6 ))){
           //    //   $statusDiv=$statusDiv.'<option  value="'.$key.'" >'.$value.'</option>';
           //    // }
           // }
           //  $statusDiv=$statusDiv.'</select></td> ';

			 $statusDiv = ' <td>';
            foreach($orderStatusList as $key=>$value)
            {
              if($val['order_status'] == $key)
              {
                $statusDiv=$statusDiv.$value;
              }         
            }
            $statusDiv=$statusDiv.'</select></td> ';

          if(($val['order_status'] == 5 && $val['send_email'] == 1) || ($val['order_status'] == 2 && $val['send_email'] == 2) || ($val['order_status'] == 3 && $val['send_email'] == 3) || ($val['order_status'] == 4 && $val['send_email'] == 4)){ $show_mail = 1; }
          else if($val['order_status'] != 5 && $val['send_email'] == 0){$show_mail = 0; }
          else { $show_mail = 0; }

          if($val['order_status'] < 7 && $val['order_status'] != 1) {
             if( $show_mail==1 )
             {
                //$show_mail_text = "disabled";
               $show_email_button = '<p>Email Sent</p> ';
             }else
             {
                //$show_mail_text = "";
                $show_email_button = '<a class="btn btn-xs btn-primary" data-toggle="modal" '.$show_mail_text.' onclick="send_email(\''.$val['order_id'].'\',\''.$val['product_id'].'\',\''.$val['order_status'].'\',\''.$val['order_unique_no'].'\',\''.$val['brand_id'].'\');" >Send Email</a> ';
             }

             //$show_email_button = '<a class="btn btn-xs btn-primary" data-toggle="modal" '.$show_mail_text.' onclick="send_email(\''.$val['order_id'].'\',\''.$val['product_id'].'\',\''.$val['order_status'].'\',\''.$val['order_unique_no'].'\',\''.$val['brand_id'].'\');" >Send Email</a> ';
          }else
          {
            $show_email_button = '';
          }

          if($val['dpartner']!='')
          {
            $dpDiv = $val['dpartner'];
            /*$dpButton = '';*/
          }else
          {
            //onchange="updateStatus(\''.$val['order_prd_id'].'\');"
            if($val['order_status']!=5 && $val['order_status']!=7 && $val['order_status']!=1 && $this->session->userdata('role_id')!=6)
            {
              $dpDiv = '<input type="checkbox"  name="dop_'.$val['order_unique_no'].'" value="'.$val['order_prd_id'].'" id="dop_'.$val['order_prd_id'].'" '.$dpstatus.' >';
              $dpButton = '<button class="btn btn-primary btn-primary btn-sm" onclick="assignOrderDp(\''.$val['order_unique_no'].'\')" >Assign Delivery Partner</button>';
            }
          }
			
          $productUrl = '';
          if(in_array($val['brand_id'],$imagify_brandid))
          {
            $productUrl = $this->config->item('products_tiny').$val['image'];
            $defaultImage = $this->config->item('default_image_path');
          }else
          {
           $productUrl = base_url().'assets/products/thumb_124/'.$val['image'];
           $defaultImage = $this->config->item('default_image_path');
          }

            $orderDetailhtml = $orderDetailhtml.'
                              <tbody>
                                <tr>
                                  <!--<td>'.$i.'</td>-->
                                  <td>'.$dpDiv.'</td>
                                  <td>'.$val['order_id'].'</td>
                                  <td>'.$val['product_sku'].'<br/><a target="_blank" href="'.LIVE_SITE.'product/details/'.$val['slug'].'-'.$val['product_id'].'"> '.$val['product_id'].'</a>
                                    <br/>'.$val['order_prd_id'].'
                                  </td>                                 
                                  <td>
                                      <div class="img-wrp">
                                    <a target="_blank" href="'.$productUrl.'">  <img src="'.$productUrl.'" onerror="this.onerror=null; this.src='.DEFAULT_IMAGE_PATH.';"></a>
                                  </div>
                                 </td>
                                  <td>'.$val['company_name'].'</td>
                                  <td>'.$val['product_name'].'</td>
                                  <td>'.$val['product_qty'].'</td>
                                  <td>'.$val['size_text'].'</td>
                                  <td><i class="fa fa-inr"></i> '.number_format($val['actual_product_price'],2).'</td>
                                  <td><i class="fa fa-inr"></i> '.number_format(round($val['product_price'],2),2).'</td>
                                  <td><i class="fa fa-inr"></i> '.number_format(round($grandPrice,2),2).'</td>
                                  <td><i class="fa fa-inr"></i> '.number_format(round($grandPrice,2),2).'</td>
                                  '.$statusDiv.'
                                  <td><textarea tabindex="1" name="remark" id="remark_'.$val['order_prd_id'].'"  onblur="saveRemark(\''.$val['order_prd_id'].'\');" placeholder="Remark" rows="2" class="form-control">'.$val['remark'].'</textarea>
                                  <span class="text-green" id="remarkMsg_'.$val['order_prd_id'].'"></span>
                                  </td>
                                  <td>
                                  '.$show_email_button.'
                                  </td>
                                </tr>';
                              }
                            }

                            $box_tax_html= '';
							if(in_array($val['product_id'],unserialize(SCBOX_PRODUCTID))){
								$box_tax_html = '<td colspan="10"> 
									   <div class="form-inline">
											Tax ID <input class="form-control" type="text" name="tax_id" id="tax_id_'.$val['order_unique_no'].'" value="'.$val['txnid'].'">
									  Payu ID <input  class="form-control" type="text" name="payu_id" id="payu_id_'.$val['order_unique_no'].'" value="'.$val['mihpayid'].'">
									   <input  class="form-control date_to" type="text" name="date" id="date_to11">
									   <button class="btn btn-primary btn-sm" onclick="save_order_data(\''.$val['order_unique_no'].'\');">save</button></div>  </td>';
							}		

                            //-$couponDiscountDiff[$b_id]['OrderSubTotal']
                               $orderDetailhtml = $orderDetailhtml.' <tr> 
                                  <td colspan="15" class="seller-amount-box">                                    
                                    '.$taxCodShipDiv.'                                    
                                    <div><label>Total Amount</label> <span class="price"> <i class="fa fa-inr"></i> '.number_format(round(($couponDiscountDiff[$b_id]['OrderSubTotal']),2),2).'</span></div>
                                  </td>
                                </tr>
                              </tbody> ';
          //}
                        }
                      }
			$box_html = '';
			if(in_array(@$val['product_id'],unserialize(SCBOX_PRODUCTID))){
				$login_user_id = $this->session->userdata('user_id');

				$box_html = '
				<!--<select id="delivery_type_" name="delivery_type">
				<option value="">Select Delivery Type</option>
				<option value="Express">Express</option>
				<option value="Economy">Economy</option>
				</select>
				<input type="text" name="box_weight" id="box_weight" placeholder="Box Weight">
				<a href="javascript:void(0)">
				<button class="btn btn-primary btn-sm btn-quick-close" onclick="get_aws_no(\''.$val['order_unique_no'].'\')" id="btnSubmit">Generate AWB</button>-->
				<!--<button class="btn btn-primary btn-sm btn-quick-close" data-toggle="modal" data-target="#addAwb">Add AWB</button>-->

				<button class="btn btn-primary btn-sm btn-quick-close"  onclick="getPaData(\''.$val['order_display_no'].'\',\''.$val['user_id'].'\')" data-target="#myModal">PA DATA</button></a> <a href="javascript:void(0)" onclick="download_pdf(\''.@$val['order_display_no'].'\')" ><button class="btn btn-primary btn-sm btn-quick-close">Download pdf</button></a>   <a href="'.base_url().'stylistorder/user_view/'.$login_user_id.'/'.@$val['order_unique_no'].'/'.@$val['user_id'].'"></a> <a href="javascript:void(0)" data-orderid="'.@$val['order_unique_no'].'" onclick="get_order_popup(this)"><button class="btn btn-primary btn-sm btn-quick-close">add scbox payment data</button></a>  <a href="'.base_url().'Scbox/scbox_manual/'.@$val['order_unique_no'].'/'.@$val['user_id'].'"><button class="btn btn-primary btn-sm btn-quick-close">Add - Order Specific Data</button></a>';
			}
			$cartMetaData = $this->Manage_orders_model->get_cart_meta_data($cartAwbNumber['id']);
			/* echo "<pre>";
			print_r($cartMetaData);
			echo "</pre>"; */
			$delivery_partner = $this->Manage_orders_model->get_cart_metavalue_bykey($cartAwbNumber['id'],'order_delivery_partner');
			$awb_number = $this->Manage_orders_model->get_cart_metavalue_bykey($cartAwbNumber['id'],'order_awb_no');
			$delpartner = $this->Manage_orders_model->get_del_partner(@$delivery_partner);
          echo  $orderDetailhtml.'  </table> </div>
						  <div class="pull-left">
						  <b>AWB No: </b><span id="awbno_'.$val['order_unique_no'].'">'.@$awb_number.'</span><br>
						  <b>Delivery Partner: </b><span id="delpartner_'.$val['order_unique_no'].'">'.$delpartner['delivery_partner_name'].'</span>
						  </div>

						  <div class="pull-right">
						  
                          <span id="Updatemessage_'.@$val['order_unique_no'].'"></span>
                          '.$dpButton.'  '.$box_html.'
                            <button class="btn btn-primary btn-sm btn-quick-close" onclick="order_close(\''.$order_unique_id.'\')"><i class="fa fa-close"></i> Close</button>
                          </div>
                      </div>
                    </div>
                    </div>';
      }
  }

  function ordercancelMessage($order_id,$user_id,$first_name,$last_last,$product_order_id,$product_id,$order_status,$brand_id){
  /*function ordercancelMessage(){
      $order_id = '31335_1433';
      $user_id = '31335';
      $first_name = 'Rohith ';
      $last_last = 'Chevuru';
      $product_order_id = '4388';
      $product_id = '99992';
      $order_status = '5';     */ 

      if($this->getEmailAddress($user_id)!=''){
        $config['protocol'] = 'smtp';
        $totalProductPrice=0;

      //      $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;
            $config['smtp_host'] = $this->config->item('smtp_host');
            $config['smtp_user'] = $this->config->item('smtp_user');
            $config['smtp_pass'] = $this->config->item('smtp_pass');
            $config['smtp_port'] = $this->config->item('smtp_port');

            $this->email->initialize($config);

            $data['orderUniqueNo'] = $order_id;
            $data['Username'] = $first_name.' '.$last_last;
            /*$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);
            $data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);*/
            $data['order_product_info'] =  $this->Manage_orders_model->su_get_order_info($order_id,$product_order_id,$product_id,$order_status);

            
            $data['order_info'] = $this->Manage_orders_model->su_get_order_price_info_res($order_id,$product_order_id);
          /*  echo '<pre>';print_r($data['order_info']);exit;  */

            $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
            $data['city_name'] = $data['order_info'][0]['city_name'];
            $data['state_name'] = $data['order_info'][0]['state_name'];
            $data['pincode'] = $data['order_info'][0]['pincode'];
            $data['country_name'] = $data['order_info'][0]['country_name'];
            $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
            $data['company_name'] = trim($this->getBrands_info($brand_id));  
            $data['cancel_product_id'] = $product_id;
            $data['order_display_no'] = $data['order_info'][0]['order_display_no'];
            $data['mobile_no'] = $data['order_info'][0]['mobile_no'];
            $brand_email = trim($this->getEmailAddress($brand_id));  


            if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

            //$data['res'] =  $this->Manage_orders_model->su_get_order_info($order_id);
            //$this->load->view('emails/order_successful_user_copy',$data);

            $this->email->from($this->config->item('from_email'), 'StyleCracker');

            if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
		         $email = $this->config->item('sc_test_emaild');
		         $this->email->to($email); 
		    }else{
	            $this->email->to($this->getEmailAddress($user_id)); 
	            $this->email->cc(array('scbox@stylecracker.com')); 
	        }         
            //$this->email->cc(array('customercare@stylecracker.com',$brand_email));    
            //**$this->email->to('sudha@stylecracker.com');
            $this->email->subject('StyleCracker: Order Cancelled');
            //$this->load->view('emails/order_cancel_user_email',$data);            
			$scbox_productids = unserialize(SCBOX_PRODUCTID);
			if(in_array($product_id, $scbox_productids)){
				$this->email->subject('Your SCBox has been cancelled');
				$message = $this->load->view('emails/scboxorder_cancelby_user_email',$data,true);
            }else{
				$this->email->subject('StyleCracker: Order Cancelled');
				if($this->session->userdata('role_id')!=6)
				{
					$message = $this->load->view('emails/order_cancelby_user_email',$data,true);
					$this->email->message($message);					
				}else
				{
					$message = $this->load->view('emails/order_cancel_user_email',$data,true);
					$this->email->message($message);
				}
            }       
            $this->email->message($message);
            $this->email->send();

        }
    }



    function order_Delivered_Message($order_id,$user_id,$first_name,$last_last,$product_order_id,$product_id,$order_status,$brand_id){

     /*function order_Delivered_Message(){
      $order_id = '31335_1433';
      $user_id = '31335';
      $first_name = 'Rohith'; 
      $last_last = 'Chevuru';
      $product_order_id = '4388';
      $product_id = '99992';
      $order_status = '4';*/

      if($this->getEmailAddress($user_id)!=''){
        $config['protocol'] = 'smtp';
        $totalProductPrice=0;

      //      $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['smtp_host'] = $this->config->item('smtp_host');
        $config['smtp_user'] = $this->config->item('smtp_user');
        $config['smtp_pass'] = $this->config->item('smtp_pass');
        $config['smtp_port'] = $this->config->item('smtp_port');

            $this->email->initialize($config);

            $data['orderUniqueNo'] = $order_id;
            $data['first_name'] = $first_name;
            $data['Username'] = $first_name.' '.$last_last;
            /*$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);
            $data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);*/
            $data['order_product_info'] =  $this->Manage_orders_model->su_get_order_info($order_id,$product_order_id,$product_id,$order_status);

            /*echo '<pre>';print_r($data['order_product_info']);*/
            $data['order_info'] = $this->Manage_orders_model->su_get_order_price_info_res($order_id,$product_order_id);
         /*   echo '<pre>';print_r($data['order_info']);exit;  */

            $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
            $data['city_name'] = $data['order_info'][0]['city_name'];
            $data['state_name'] = $data['order_info'][0]['state_name'];
            $data['pincode'] = $data['order_info'][0]['pincode'];
            $data['country_name'] = $data['order_info'][0]['country_name'];
            $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
            $data['company_name'] = trim($this->getBrands_info($brand_id));  
            $data['set_product_id'] = $product_id;
            $data['order_product_id'] = $product_order_id;
            $data['order_display_no'] = $data['order_info'][0]['order_display_no'];
            $data['mobile_no'] = $data['order_info'][0]['mobile_no'];
            $brand_email = trim($this->getEmailAddress($brand_id));  


            if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

            //$data['res'] =  $this->Manage_orders_model->su_get_order_info($order_id);
            //$this->load->view('emails/order_successful_user_copy',$data);

            $this->email->from($this->config->item('from_email'), 'StyleCracker');
            if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
		         $email = $this->config->item('sc_test_emaild');
		         $this->email->to($email); 
		    }else{
	            $this->email->to($this->getEmailAddress($user_id));
	            $this->email->cc(array('scbox@stylecracker.com'));
	        }
            //$this->email->cc(array('customercare@stylecracker.com',$brand_email));

            //**$this->email->to('sudha@stylecracker.com');
            $scbox_productids = unserialize(SCBOX_PRODUCTID);
            if(in_array($product_id, $scbox_productids)){
				$this->email->subject('Your SC Box has been delivered !');
				//$message = $this->load->view('emails/scboxorder_delivered_user_email',$data,true);
				$message = $this->load->view('emails/scboxorder_delivered_user_email_marketing',$data,true);
            }else{
				$this->email->subject('StyleCracker: Order Delivered');
				$message = $this->load->view('emails/order_delivered_user_email',$data,true);
            }
            $this->email->message($message);
            $this->email->send();

        }
    }

  function order_Dispatched_Message($order_id,$user_id,$first_name,$last_last,$product_order_id,$product_id,$order_status,$brand_id,$email_data){

 /* function order_Dispatched_Message(){
      $order_id = '24224_551_552_553';
      $user_id = '24224';
      $first_name = 'Chatur ';
      $last_last = 'Ramalingum';
      $product_order_id = '580';
      $product_id = '11577';
      $order_status = '3';*/

      if($this->getEmailAddress($user_id)!=''){

          $config['protocol'] = 'smtp';
          $totalProductPrice=0;
        //$config['mailpath'] = '/usr/sbin/sendmail';
          $config['charset'] = 'iso-8859-1';
          $config['mailtype'] = 'html';
          $config['wordwrap'] = TRUE;
          $config['smtp_host'] = $this->config->item('smtp_host');
          $config['smtp_user'] = $this->config->item('smtp_user');
          $config['smtp_pass'] = $this->config->item('smtp_pass');
          $config['smtp_port'] = $this->config->item('smtp_port');

            $this->email->initialize($config);

            $data['orderUniqueNo'] = $order_id;
            $data['Username'] = $first_name.' '.$last_last;
            /*$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);
            $data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);*/
            $data['order_product_info'] =  $this->Manage_orders_model->su_get_order_info($order_id,$product_order_id,$product_id,$order_status);

            /*echo '<pre>';print_r($data['order_product_info']);*/
            $data['order_info'] = $this->Manage_orders_model->su_get_order_price_info_res($order_id,$product_order_id);
          /*  echo '<pre>';print_r($data['order_info']);exit;  */

            $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
            $data['city_name'] = $data['order_info'][0]['city_name'];
            $data['state_name'] = $data['order_info'][0]['state_name'];
            $data['pincode'] = $data['order_info'][0]['pincode'];
            $data['country_name'] = $data['order_info'][0]['country_name'];
            $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
            $data['company_name'] = trim($this->getBrands_info($brand_id));  
            $data['set_product_id'] = $product_id;
            $data['order_product_id'] = $product_order_id;
            $data['order_display_no'] = $data['order_info'][0]['order_display_no'];
            $data['mobile_no'] = $data['order_info'][0]['mobile_no'];
            $brand_email = trim($this->getEmailAddress($brand_id)); 

            if(!empty($email_data))
            {
            	$data['awb_tracking_code'] = $email_data['awb_tracking_code'];
				$data['dp_name'] = $email_data['dp_name'];
				$data['tracking_link'] = $email_data['tracking_link'];
            } 

            if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

            //$data['res'] =  $this->Manage_orders_model->su_get_order_info($order_id);
            //$this->load->view('emails/order_successful_user_copy',$data);

            $this->email->from($this->config->item('from_email'), 'StyleCracker');
            if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
		         $email = $this->config->item('sc_test_emaild');
		         $this->email->to($email); 
		    }else{
	            $this->email->to($this->getEmailAddress($user_id));  
	            $this->email->cc(array('scbox@stylecracker.com'));
	        }          
            //$this->email->cc(array('customercare@stylecracker.com',$brand_email));  
          
            //$this->email->to('sudha@stylecracker.com');
            $scbox_productids = unserialize(SCBOX_PRODUCTID);
			if(in_array($product_id, $scbox_productids)){
				$this->email->subject('Your SCBox has been shipped!');
				$message = $this->load->view('emails/scboxorder_dispatched_user_email',$data,true);
            }else{
				$this->email->subject('StyleCracker: Order Dispatched');
				$message = $this->load->view('emails/order_dispatched_user_email',$data,true);
            }
            // $this->email->subject('StyleCracker: Order Dispatched');
            // $message = $this->load->view('emails/order_dispatched_user_email',$data,true);
            $this->email->message($message);
           $this->email->send();

        }
    }

  function order_Confirmed_Message($order_id,$user_id,$first_name,$last_last,$product_order_id,$product_id,$order_status,$brand_id){
 /*function order_Confirmed_Message(){ 
      $order_id = '51202_5481';
      $user_id = '51202';
      $first_name = 'Chatur ';
      $last_last = 'Ramalingum';
      $product_order_id = '8724';
      $product_id = '190481';
      $order_status = '2';
	  $brand_id = '46352';*/

      if($this->getEmailAddress($user_id)!=''){

          $config['protocol'] = 'smtp';
          $totalProductPrice=0;
        //$config['mailpath'] = '/usr/sbin/sendmail';
          $config['charset'] = 'iso-8859-1';
          $config['mailtype'] = 'html';
          $config['wordwrap'] = TRUE;
          $config['smtp_host'] = $this->config->item('smtp_host');
          $config['smtp_user'] = $this->config->item('smtp_user');
          $config['smtp_pass'] = $this->config->item('smtp_pass');
          $config['smtp_port'] = $this->config->item('smtp_port');

            $this->email->initialize($config);

            $data['orderUniqueNo'] = $order_id;
            $data['Username'] = $first_name.' '.$last_last;
            /*$data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);
            $data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);*/
            $data['order_product_info'] =  $this->Manage_orders_model->su_get_order_info($order_id,$product_order_id,$product_id,$order_status);

            /*echo '<pre>';print_r($data['order_product_info']);*/
            $data['order_info'] = $this->Manage_orders_model->su_get_order_price_info_res($order_id,$product_order_id);
          /*  echo '<pre>';print_r($data['order_info']);exit;  */

            $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
            $data['city_name'] = $data['order_info'][0]['city_name'];
            $data['state_name'] = $data['order_info'][0]['state_name'];
            $data['pincode'] = $data['order_info'][0]['pincode'];
            $data['country_name'] = $data['order_info'][0]['country_name'];
            $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
            $data['company_name'] = trim($this->getBrands_info($brand_id));  
            $data['set_product_id'] = $product_id;
            $data['order_product_id'] = $product_order_id;
            $data['order_display_no'] = $data['order_info'][0]['order_display_no'];
            $data['mobile_no'] = $data['order_info'][0]['mobile_no'];
            $brand_email = trim($this->getEmailAddress($brand_id));

            if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

            //$data['res'] =  $this->Manage_orders_model->su_get_order_info($order_id);
            //$this->load->view('emails/order_successful_user_copy',$data);

            $this->email->from($this->config->item('from_email'), 'StyleCracker');
            if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
		         $email = $this->config->item('sc_test_emaild');
		         $this->email->to($email); 
		    }else{
	            $this->email->to($this->getEmailAddress($user_id));
	            $this->email->cc(array('scbox@stylecracker.com'));
	        }
            //$this->email->cc(array('customercare@stylecracker.com',$brand_email));      
            //**$this->email->to('sudha@stylecracker.com');
            
            $this->email->subject('StyleCracker: Order Confirmed');
            //$this->load->view('emails/order_confirmed_user_email',$data);
            // $message = $this->load->view('emails/order_confirmed_user_email',$data,true);
             $scbox_productids = unserialize(SCBOX_PRODUCTID);
            if(in_array($product_id, $scbox_productids))
            {
				$this->email->subject('Your SCBox order is confirmed');
				$message = $this->load->view('emails/scboxorder_confirmed_user_email',$data,true);
            }else
            {
				$this->email->subject('StyleCracker: Order Confirmed');
				$message = $this->load->view('emails/order_confirmed_user_email',$data,true);
            }
            $this->email->message($message);
            $this->email->send();

        }
    }

/*Referal*/
  function getProductSku($productId,$productSize)
  {
    $product_sku = $this->cartnew_model->getProductSku($productId,$productSize);
    return $product_sku;
  }
/*Referal End*/

  function updateRemark()
  {
    if(!$this->input->is_ajax_request())
    {
      exit("No direct script access allowed");
    }else
    {
      $orderPrdId = $this->input->post('id');
      $orderStatus = $this->input->post('remark');
      $result = $this->Manage_orders_model->updateRemark($orderPrdId,$orderStatus);

      if($result)
      {        
        echo true;
      }else
      {        
        echo false;
      }
    }

  }

  function orderPlaceMessage($user_id,$order_id,$first_name,$last_last)
  {   
      if($this->getEmailAddress($user_id)!=''){
        $config['protocol'] = 'smtp';
        $totalProductPrice=0;
        $referral_point = 0;
      $referral_point = $this->cartnew_model->get_refre_amt($order_id);

  //      $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['smtp_host'] = $this->config->item('smtp_host');
        $config['smtp_user'] = $this->config->item('smtp_user');
        $config['smtp_pass'] = $this->config->item('smtp_pass');
        $config['smtp_port'] = $this->config->item('smtp_port');
      
        $this->email->initialize($config);

        $data['orderUniqueNo'] = $order_id;
        $data['Username'] = $first_name.' '.$last_last;
        $data['order_product_info'] = $this->cartnew_model->su_get_order_info($order_id);    
        $data['order_info'] = $this->cartnew_model->su_get_order_price_info_res($order_id);
        $data['shipping_address'] = $data['order_info'][0]['shipping_address'];
        $data['city_name'] = $data['order_info'][0]['city_name'];
        $data['state_name'] = $data['order_info'][0]['state_name'];
        $data['pincode'] = $data['order_info'][0]['pincode'];
        $data['country_name'] = $data['order_info'][0]['country_name'];
        $data['paymentmode'] = $data['order_info'][0]['pay_mode'];
        $data['referral_point'] = $referral_point;
        $data['order_display_no'] = $data['order_info'][0]['order_display_no'];
        $data['mobile_no'] = $data['order_info'][0]['mobile_no'];
        
          if($data['order_info'][0]['state_id'] == '36') { $data['state_name'] =  $data['order_info'][0]['it_state']; }

          $data['res'] =  $this->cartnew_model->su_get_order_info($order_id); 

        //$this->load->view('emails/order_successful_user_copy',$data);

          $this->email->from($this->config->item('from_email'), 'StyleCracker');

          if(ENVIRONMENT=='local' || ENVIRONMENT=='development' || ENVIRONMENT=='testing'){
		         $email = $this->config->item('sc_test_emaild');
		         $this->email->to($email); 
		    }else{
	         	$this->email->to($this->getEmailAddress($user_id));    
	            $this->email->cc('customercare@stylecracker.com'); 
	        }  
          
          //**$this->email->to('sudha@stylecracker.com');                  
          $this->email->subject('StyleCracker: Order Processing');
          //$this->load->view('emails/order_successful_user_copy',$data);
          $message = $this->load->view('emails/order_successful_user_copy',$data,true);
          $this->email->message($message);
          //$this->email->send();

    }
  } 
  
 function change_order_status(){
      $order_id = $this->input->post('order_id');
      $order_status = $this->input->post('order_status');
      $first_name = $this->input->post('first_name');
      $last_name = $this->input->post('last_name');
      $user_id = $this->input->post('user_id');
      
      $this->Manage_orders_model->change_order_status($order_id,$order_status,$first_name,$last_name,$user_id);
  }

  function saveOrderDp()
  {
    //echo '<pre>';print_r($this->input->post('dop'));    
    //echo "saveOrderDp";
    $dop_arr = $this->input->post('dop');
    $result = $this->Manage_orders_model->assign_order_dp($dop_arr);
  }


	function send_notification($notification_message,$data,$product_info){
		
		$content = array("en" => ''.$notification_message.'');
		$deeplink_for = 'order';
		$deeplink_id = $data[0]['order_unique_no']; // $deeplink_id = '42707_3906'; //order_id
		$img_link = $product_info['product_img'];
		$useremail = $data[0]['email_id']; // $useremail = 'rajeshkharatmol94@gmail.com';
		
		if($content!='' && $deeplink_for!='' && $deeplink_id!='' && $useremail!='')
		{
			$fields = array(
				'app_id' => "774c695f-4257-45ad-bbd7-76a22b12117b",
				'filters' => array(array("field" => "tag", "key" => "email", "relation" => "=", "value" => $useremail) ),
				'data' => array($deeplink_for => $deeplink_id),
				'contents' => $content,
				'big_picture'=> $img_link,
			);

			$fields = json_encode($fields);
			//print("\nJSON sent:\n");
			// print($fields);exit;

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
													   'Authorization: Basic MjdkNWEyNjgtNjA1Ni00OGE1LTlhMDgtNTA4MjdlMzQ4NTQ5'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

			$response = curl_exec($ch);
			curl_close($ch);
			$result = json_decode($response);
			return true;
		}
	}
	
	function save_tax_id(){
		$order_id = $this->input->post('order_id');
		$txnid = $this->input->post('txnid');
		$payu_id = $this->input->post('payu_id');
		$paid_price = $this->input->post('paid_price');
		$payment_date = $this->input->post('payment_date');
		$is_paymentdone = $this->input->post('is_paymentdone');
		$data = $this->Manage_orders_model->update_tax_id($order_id,$txnid,$payu_id,$paid_price,$payment_date,$is_paymentdone);
		return true;
	}
	
	function get_order_popup(){
		$order_id = $this->input->post('order_id');
		$data = $this->Manage_orders_model->get_order_info($order_id);
		// echo "<pre>";print_r($data);
		$order_html = '';$order_html2 = '';
		if(!empty($data)){
			$selected1 = '';$selected2 = '';$readonly = '';
			if($data[0]['is_paymentdone'] == '1' || $data[0]['pay_mode'] == '2'){
				$selected1 = 'selected';
				$readonly = 'readonly';
			}else if($data[0]['is_paymentdone'] == '2' || $data[0]['is_paymentdone'] == '0'){
				$selected2 = 'selected';
				$order_html2 = '<div class="form-group">
						<label for="tag_name" class="control-label">Paid Price</label>
						<input class="form-control" type="text" name="paid_price_'.$order_id.'" id="paid_price_'.$order_id.'" value="'.$data[0]['paid_price'].'" '.$readonly.' >
					</div>
					<div class="form-group">
						<label for="tag_name" class="control-label">Payment Date</label>
						<input class="form-control date_to" type="text" name="payment_date_'.$order_id.'" id="payment_date_'.$order_id.'" value="'.$data[0]['payment_date'].'">
					</div>
					<div class="form-group">
						<label for="tag_name" class="control-label">Payment Status</label>
						<select name="is_paymentdone'.$order_id.'" id="is_paymentdone'.$order_id.'" class="form-control input-sm">
						  <option value="0" >Select</option>
						  <option value="1" '.$selected1.' >Done</option>
						  <option value="2" '.$selected2.' >Not Done</option>
						</select>
					</div>
					<button class="btn btn-sm btn-primary" onclick="save_order_data(\''.$order_id.'\');"><i class="fa fa-save"></i> Submit</button>
				</div>';
			}
			$order_html = '<div>
			<div class="form-group">
				<label for="tag_name" class="control-label">Transaction ID</label>
				<input class="form-control" type="text" name="txnid_'.$order_id.'" id="txnid_'.$order_id.'" value="'.$data[0]['txnid'].'" '.$readonly.' >
			</div>
			<div class="form-group">
				<label for="tag_name" class="control-label">PayU ID</label>
				<input class="form-control" type="text" name="payu_id_'.$order_id.'" id="payu_id_'.$order_id.'" value="'.$data[0]['mihpayid'].'" '.$readonly.' >
			</div>';
		}
		echo $order_html.$order_html2;
	}
	
	function download_pdf($order_display_no='SC1500371729'){
		//load mPDF library
		$this->load->library('m_pdf');
		$data = $this->Manage_orders_model->getOrders($search_by=9,$table_search=$order_display_no);
		/* echo $this->db->last_query();
		exit(); */
		$discount = 0;
		$paymod='';
		if($data[0]['coupon_code'] != ''){
			$couponCode 		= $data[0]['coupon_code'];
			$OrderProductTotal 	= $data[0]['OrderProductTotal'];
			$user_email 		= $data[0]['email_id'];
			$coupon_discount 	= $this->Manage_orders_model->check_coupon_exist($couponCode,$user_email,$OrderProductTotal);
			/* echo $this->db->last_query();
			exit(); */
			$discount 			= $coupon_discount['coupon_discount_amount'];
		}
		if($data[0]['pay_mode'] == '1'){
			$paymod = 'COD';
		}else {
			$paymod = 'Online'; 
		}

		
		$scbox_gift_object 	= $this->Scbox_model->get_object('scbox_gift')[0]['object_id'];
		$gift_data 			= $this->Scbox_model->get_gift_data($scbox_gift_object,$order_display_no);
		//echo '<pre>';print_r($gift_data);exit;
    if(!empty($gift_data))
    {
      $order_data['gift']['name'] 		= $gift_data['name'];
      $order_data['gift']['email'] 		= $gift_data['email'];
      $order_data['gift']['mobileno'] 	= $gift_data['mobileno'];
      $order_data['gift']['pincode'] 	= $gift_data['pincode'];
      $order_data['gift']['city'] 		= $gift_data['city'];
      $order_data['gift']['state'] 		= @$this->Manage_orders_model->get_state($gift_data['state']);
      $order_data['gift']['address'] 	= $gift_data['address'];
    }
		

		$order_data['order_no'] 				= $data[0]['order_display_no'];
		$order_data['first_name'] 				= $data[0]['first_name'];
		$order_data['last_name'] 				= $data[0]['last_name'];
		$order_data['mobile_no'] 				= $data[0]['mobile_no'];
		$order_data['last_name'] 				= $data[0]['last_name'];
		$order_data['created_datetime'] 		= $data[0]['created_datetime'];
		$order_data['paymod'] 					= $paymod;
		$order_data['shipping_address'] 		= $data[0]['shipping_address'];
		$order_data['pincode'] 					= $data[0]['pincode'];
		$order_data['city_name'] 				= $data[0]['city_name'];
		$order_data['state_name'] 				= $data[0]['state_name'];
		$order_data['state_id'] 				= $data[0]['state_id'];
		$order_data['discount'] 				= $discount;
		$order_data['scbox_price'] 				= $data[0]['OrderProductTotal'];
		$order_data['order_tax_amount'] 		= round($data[0]['order_tax_amount']);
		$order_data['order_total'] 				= round($data[0]['OrderProductTotal'] + $data[0]['order_tax_amount'] - $discount);
		$order_data['tax_payble_amount'] 		= $data[0]['OrderProductTotal'] - $discount;
		$order_data['order_total_in_words'] 	= $this->getIndianCurrency($order_data['order_total']);
		$order_data['order_tax_amount_in_words'] = $this->getIndianCurrency($order_data['order_tax_amount']);
		// echo "<pre>";print_r($data);
		// echo "<pre>";print_r($order_data);exit;
		
		// $this->load->view('common/invoice',$order_data);
		$html=$this->load->view('common/invoice',$order_data, true);
		$pdfFilePath ="mypdfName-".time()."-download.pdf";
		$pdf = $this->m_pdf->load();
		//generate the PDF!
		$stylesheet = file_get_contents(APPPATH.'third_party/pdf.css'); // external css
		$pdf->WriteHTML($stylesheet,1);
		$pdf->WriteHTML($html,2);
		//offer it to user via browser download! (The PDF won't be saved on your server HDD)
		$pdf->Output($pdfFilePath, "D");
	}
	
	function getIndianCurrency($number){
		
		$decimal = round($number - ($no = floor($number)), 2) * 100;
		$hundred = null;
		$digits_length = strlen($no);
		$i = 0;
		$str = array();
		$words = array(0 => '', 1 => 'one', 2 => 'two',
			3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
			7 => 'seven', 8 => 'eight', 9 => 'nine',
			10 => 'ten', 11 => 'eleven', 12 => 'twelve',
			13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
			16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
			19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
			40 => 'forty', 50 => 'fifty', 60 => 'sixty',
			70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
		$digits = array('', 'hundred','thousand','lakh', 'crore');
		while( $i < $digits_length ) {
			$divider = ($i == 2) ? 10 : 100;
			$number = floor($no % $divider);
			$no = floor($no / $divider);
			$i += $divider == 10 ? 1 : 2;
			if ($number) {
				$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
				$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
				$str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
			} else $str[] = null;
		}
		$Rupees = implode('', array_reverse($str));
		$paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
		return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
	}
	
	function download_stylist_extracted_data($search_by=NULL,$table_search=NULL,$is_download=NULL,$search_by2=NULL){
		$is_download = $this->uri->segment(6);
		
		if($search_by == 6 ){
		  $table_search_encode = base64_decode($table_search);
		}else if($search_by == 8){
		  $table_search_encode = base64_decode($table_search);
		}else if($search_by == 2)
		{
			$cname = explode('-', $table_search);
			$table_search_encode = $cname;
		}else{
		  $table_search_encode = $table_search;
		}
		// $get_users = $this->websiteusers_model->get_scboxusers($search_by,$table_search,'','','',1);
		$get_users = $this->Manage_orders_model->getViewOrdersExcel($search_by,$table_search_encode,'','',$search_by2,'','',1);
		$SCBOX_SKINTONE_LIST = unserialize(SCBOX_SKINTONE_LIST);
		$SCBOX_COLOR_LIST = unserialize(SCBOX_COLOR_LIST);
		$SCBOX_PRINT_LIST = unserialize(SCBOX_PRINT_LIST);
		// echo "<pre>";print_r($get_users);exit;
		if(!empty($get_users)){
			// men data
			$men_body_shape = $this->Pa_model->load_questAns(24);
			$men_style = $this->Pa_model->load_questAns(25);
			$SCBOX_MENTOP_LIST = $this->Pa_model->get_size_data('mentopsize');
			$SCBOX_MENBOTTOM_LIST = $this->Pa_model->get_size_data('menbottomsize');
			$SCBOX_MENFOOT_LIST = $this->Pa_model->get_size_data('menfootsize');
			
			//women data 
			$women_body_shape = $this->Pa_model->load_questAns(1);
			$women_style = $this->Pa_model->load_questAns(2);
			$SCBOX_WOMENTOP_LIST = $this->Pa_model->get_size_data('womentopsize');
			$SCBOX_WOMENBOTTOM_LIST = $this->Pa_model->get_size_data('womenbottomsize');
			$SCBOX_WOMENFOOT_LIST = $this->Pa_model->get_size_data('womenfootsize');
			$SCBOX_BANDSIZE_LIST = $this->Pa_model->get_size_data('womenbandsize');
			$SCBOX_CUPSIZE_LIST = $this->Pa_model->get_size_data('womencupsize');
			// echo "<pre>";print_r($get_users);
			$fieldName = array('User ID','User Name','Gender','SC BOX Amount','Body Shape','Personality','Size (Shirt/T-shirt Size, WAIST SIZE (INCH), Footwear, Lingerie, Cup size)','Color','Prints','Remarks (Any other specifications)','Notes','Profession','Emailid');
			$result_data = array();$i =0;
			foreach($get_users as $val) {
				$style_pref = '';$size_data = '';$color_prints = '';$user_print = '';$user_color = '';$sc_gender = '';
				$user_colors_array = explode(',',@$val['user_colors']['object_meta_value']);
				$user_prints_array = explode(',',@$val['user_prints']['object_meta_value']);
				$gender = @$val['user_gender']['object_meta_value'];
				$result_data[$i][] = @$val['user_id']['user_id'];
				$result_data[$i][] = @$val['user_name']['object_meta_value'];
				if($gender == '1'){ $sc_gender = 'women'; }else{ $sc_gender = 'men'; }
				$result_data[$i][] = $sc_gender;
				$result_data[$i][] = @$val['scbox_price']['object_meta_value'];
				if($gender == '1'){
					
					foreach($women_body_shape as $key=>$val2){
						if(@$val['user_body_shape']['object_meta_value'] == $key){ 
							$result_data[$i][] = @$val2;
						}
					}
					
					/*foreach($women_style as $key=>$val2){
						if(@$val['user_style_p1']['object_meta_value'] == $key || @$val['user_style_p2']['object_meta_value'] == $key || @$val['user_style_p3']['object_meta_value'] == $key){ 
							$style_pref = $style_pref.'  '.@$val2;	
						}	
					}*/

					if($gender==1)
			        {
			            if(@$val['user_style_p1']['object_meta_value']!='' || @$val['user_style_p2']['object_meta_value']!='' || @$val['user_style_p3']['object_meta_value']!=''){ 

			              //if(!empty($val['user_style_p1']['object_meta_value']))
			              if(array_key_exists(@$val['user_style_p1']['object_meta_value'], $women_style))
			              {
			                $style_pref = $women_style[@$val['user_style_p1']['object_meta_value']];
			              }

			               //if(!empty($val['user_style_p2']['object_meta_value']))
			              if(array_key_exists(@$val['user_style_p2']['object_meta_value'], $women_style))
			              {
			                $style_pref = $style_pref.','.$women_style[@$val['user_style_p2']['object_meta_value']];
			              }

			               //if(!empty($val['user_style_p3']['object_meta_value']))
			              if(array_key_exists(@$val['user_style_p3']['object_meta_value'], $women_style))
			              {
			                $style_pref = $style_pref.','.$women_style[@$val['user_style_p3']['object_meta_value']];
			              }
			              // $style_pref = $women_style[@$val['user_style_p1']['object_meta_value']].','.$women_style[@$val['user_style_p2']['object_meta_value']].','.$women_style[@$val['user_style_p3']['object_meta_value']];
			            }
			        }
					$result_data[$i][] = $style_pref;
					
					foreach($SCBOX_WOMENTOP_LIST as $val2){
						if(@$val['user_sizetop']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' Top = '.@$val2['answer'].' , ';
						}	
					}
					
					foreach($SCBOX_WOMENBOTTOM_LIST as $val2){
						if(@$val['user_sizebottom']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' BOTTOM = '.@$val2['answer'].' , ';
						}	
					}
					
					foreach($SCBOX_WOMENFOOT_LIST as $val2){
						if(@$val['user_sizefoot']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' FOOT = '.@$val2['answer'].' , ';
						}	
					}
					
					foreach($SCBOX_BANDSIZE_LIST as $val2){
						if(@$val['user_sizeband']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' BANDSIZE = '.@$val2['answer'].' , ';
						}	
					}
					
					foreach($SCBOX_CUPSIZE_LIST as $val2){
						if(@$val['user_sizecup']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' CUPSIZE = '.@$val2['answer'];
						}	
					}
					$result_data[$i][] = $size_data;
					
				}else{
					
					foreach($men_body_shape as $key=>$val2){
						if(@$val['user_body_shape']['object_meta_value'] == $key){ 
							$result_data[$i][] = @$val2;
						}
					}
					
					/*foreach($men_style as $key=>$val2){
						if(@$val['user_style_p1']['object_meta_value'] == $key || @$val['user_style_p2']['object_meta_value'] == $key || @$val['user_style_p3']['object_meta_value'] == $key){ 
							$style_pref = $style_pref.'  '.@$val2;	
						}
					}*/
					  if($gender==2)
			          {
			            if(@$val['user_style_p1']['object_meta_value']!='' || @$val['user_style_p2']['object_meta_value']!='' || @$val['user_style_p3']['object_meta_value']!=''){ 

			              if(array_key_exists(@$val['user_style_p1']['object_meta_value'], $men_style)) {
			                $style_pref = $men_style[@$val['user_style_p1']['object_meta_value']];
			              }

			              if(array_key_exists(@$val['user_style_p2']['object_meta_value'], $men_style)) {
			                $style_pref = $style_pref.','.$men_style[@$val['user_style_p2']['object_meta_value']];
			              }

			              if(array_key_exists(@$val['user_style_p3']['object_meta_value'], $men_style)) {
			                $style_pref = $style_pref.','.$men_style[@$val['user_style_p3']['object_meta_value']];
			              }
			              //$style_pref = $men_style[@$val['user_style_p1']['object_meta_value']].','.$men_style[@$val['user_style_p2']['object_meta_value']].','.$men_style[@$val['user_style_p3']['object_meta_value']];
			            }
			          }
					$result_data[$i][] = $style_pref;
					
					foreach($SCBOX_MENTOP_LIST as $val2){
						if(@$val['user_sizetop']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' Top = '.@$val2['answer'].' , ';
						}	
					}
					foreach($SCBOX_MENBOTTOM_LIST as $val2){
						if(@$val['user_sizebottom']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' BOTTOM = '.@$val2['answer'].' , ';
						}	
					}
					foreach($SCBOX_MENFOOT_LIST as $val2){
						if(@$val['user_sizefoot']['object_meta_value'] == $val2['id']){
							$size_data = $size_data .' FOOT = '.@$val2['answer'];
						}	
					}
					$result_data[$i][] = $size_data;
					
				}
				
				foreach($SCBOX_COLOR_LIST as $val2){
					if(in_array($val2->id,$user_colors_array)){
						$user_color = $user_color.$val2->name.' , ';
					}	
				}
				$result_data[$i][] = $user_color;
				
				foreach($SCBOX_PRINT_LIST as $val2){
					if(in_array($val2->id,$user_prints_array)){
						$user_print = $user_print.$val2->name.' , ';
					}	
				}
				$result_data[$i][] = $user_print;
				
				if(isset($val['user_manual_data']['object_meta_value']))
				{
					if(@$val['user_manual_data']['object_meta_value'] != ''){
					$user_manual_data = unserialize(@$val['user_manual_data']['object_meta_value']);
					$result_data[$i][] = $user_manual_data['other_specifications'];
					}
				}else
				{
					$result_data[$i][] = '  ';
				}
				
				
				$result_data[$i][] = '  ';
				if(!empty(@$val['user_profession']['object_meta_value']))
				{
					$result_data[$i][] = @$val['user_profession']['object_meta_value'];
				}else
				{
					$result_data[$i][] = ' ';
				}
				
        		$result_data[$i][] = @$val['user_emailid']['object_meta_value'];
				$i++;
			}
			// echo "<pre>";print_r($result_data);exit;
			$this->writeDataintoCSV($fieldName,$result_data,'sc_box_user_data');
		}
	}

	public function export_stylist_data($search_by='',$table_search='',$is_download='',$search_by2='')
	 {
	  $data = array();
	  $is_download = $this->uri->segment(6);

	  if($search_by == 6 ){
	    $table_search_encode = base64_decode($table_search);
	  }else if($search_by == 8){
	    $table_search_encode = base64_decode($table_search);
	  }
	  else if($search_by == 2)
	  {
	   $cname = explode('-', $table_search);
	   $table_search_encode = $cname;
	  }
	  else{
	    $table_search_encode = $table_search;
	  }
	  
	  $data['scontent'] = array();
	  $data['scontent']['search_by']     = $search_by;
	  $data['scontent']['table_search_encode']  = $table_search_encode;
	  $data['scontent']['search_by2']    = $search_by2;
	  
	  $paObject  = $this->Manage_orders_model->getPaObject();
	  if(is_array($paObject) && count($paObject))
	  {
	   foreach($paObject as $obId)
	   {
	    $PaObjectID[] = $obId['object_id'];
	   }
	  }
	  $getUserData = $this->Manage_orders_model->exportStylistData($data['scontent'], $PaObjectID);
	  
	  if(is_array($getUserData) && count($getUserData))
	  {
	   foreach($getUserData as $uKey => $userData)
	   {
	     $metaKey  = $userData['object_meta_key'];
	     $objectId  = $userData['object_id'];
	     
	     if($objectId == $PaObjectID[1])
	     {
	     $metaData[$userData['object_meta_key']]['paBeforePayment']  = $userData['object_meta_value'];
	     $metaData[$userData['object_meta_key']]['first_name']   = $userData['first_name'];
	     $metaData[$userData['object_meta_key']]['last_name']   = $userData['last_name'];
	     $metaData[$userData['object_meta_key']]['email_id']   = $userData['email_id'];
	     $metaData[$userData['object_meta_key']]['mobile_no']   = $userData['mobile_no'];
		 $metaData[$userData['object_meta_key']]['forme_gender']   	= $userData['forme_gender'];
	     }
	     
	     else if($objectId == $PaObjectID[2]){
	     $metaData[$userData['object_meta_key']]['paAfterPayment']  = $userData['object_meta_value'];
	     $metaData[$userData['object_meta_key']]['first_name']   = $userData['first_name'];
	     $metaData[$userData['object_meta_key']]['last_name']   = $userData['last_name'];
	     $metaData[$userData['object_meta_key']]['email_id']   = $userData['email_id'];
	     $metaData[$userData['object_meta_key']]['mobile_no']   = $userData['mobile_no'];
		 $metaData[$userData['object_meta_key']]['forme_gender']   	= $userData['forme_gender'];
	     }
	     
	     else if($objectId == $PaObjectID[3]){
	     $metaData[$userData['object_meta_key']]['categories']   = $userData['object_meta_value'];
	     $metaData[$userData['object_meta_key']]['first_name']   = $userData['first_name'];
	     $metaData[$userData['object_meta_key']]['last_name']   = $userData['last_name'];
	     $metaData[$userData['object_meta_key']]['email_id']   = $userData['email_id'];
	     $metaData[$userData['object_meta_key']]['mobile_no']   = $userData['mobile_no'];
		 $metaData[$userData['object_meta_key']]['forme_gender']   	= $userData['forme_gender'];
	     }
	     else if($objectId == $PaObjectID[0]){
		   $metaData[$userData['object_meta_key']]['gender']   			= $userData['object_meta_value'];
		   $metaData[$userData['object_meta_key']]['first_name']   		= $userData['first_name'];
		   $metaData[$userData['object_meta_key']]['last_name']   		= $userData['last_name'];
		   $metaData[$userData['object_meta_key']]['email_id']   		= $userData['email_id'];
		   $metaData[$userData['object_meta_key']]['mobile_no']   		= $userData['mobile_no'];
		   $metaData[$userData['object_meta_key']]['forme_gender']   	= $userData['forme_gender'];
		   }
	     
	   }
	  }
	  

	  $fieldName = array('Order Unique Id','customer Name','Gender','Email Id','Mobile No.','Categories','Body Shape','Height','Apparel stuff dont like','Apparel Top Size','Apparel top fit like this','Apparel Dress Size','Apparel dress fit like this','Apparel trouser size','Apparel jeans size','Apparel bottom fit like this','Apparel band size','Apparel cup size','Apparel skirt fit like this','Apparel dos and donts', 'Apparel color avoid','Apparel print avoid','Apparel tshirt size','Apparel tshirt fit like this','Apparel shirt size','Apparel shirt fit like this','Apparel trouser fit like this','Apparel jeans fit like this','Apparel print avoid men','Footwear Size','Footwear Type','Footwear Width','Footwear','Footwear Heel Height','Sunglasses','Refelector Type','Belt Type','Jewellery Type','Jewellery Tone','Beauty','Beauty Product','Beauty Shade','Beauty Finish','Bag Type','Bag Want','Bag Size','Bag Color Avoid','Accessories');
	  
	  $result_data = array();$i =0;
	  
	  $colorsName = array('#90140c'=>'maroon','#b8bebe'=>'grey','#be914a'=>'gold','#ff0080'=>'fuschia','#734021'=>'brown','#fec4c3'=>'blush pink','#0b0706'=>'black','#98012e'=>'berry','#ffffff'=>'white','#f57d31'=>'tangerine','#87ceeb'=>'sky blue','#cdd1d1'=>'silver','#d5011a'=>'red','#800080'=>'purple','#bcd2ee'=>'powder blue','#576331'=>'olive','#041531'=>'navy blue','#f7b719'=>'mustard','#8bf5cd'=>'mint','#89aad5'=>'light blue','#bf94e4'=>'lavender','#aeb0ae'=>'gray','#2082ef'=>'bright blue','#68dbd6'=>'aqua','#f5dc05'=>'yellow','#fffefe'=>'white men','#0c4c8a'=>'royal blue','#de443a'=>'red','#bde4f5'=>'pastel blue','#ffbbc7'=>'baby pink','#0a6148'=>'bottle green','#8bf5cd'=>'mint','#89aad5'=>'light blue','#bf94e4'=>'lavender','#aeb0ae'=>'gray','#2082ef'=>'bright blue','#68dbd6'=>'aqua','#f5dc05'=>'yellow','#fffefe'=>'white men','#0c4c8a'=>'royal blue','#de443a'=>'red','#bde4f5'=>'pastel blue','#ffbbc7'=>'baby pink','#0a6148'=>'bottle green');
	  
	  if(is_array($metaData) && count($metaData)>0)
	  { 
	   foreach($metaData as $metaKey => $metaValue)
	   {
	    
		   $color = '';
		   if(!empty($metaValue['gender'])){ 
			 $getGender  = unserialize($metaValue['gender']);
				if($getGender['gender']== 1){ $gender = "Female (Is Gift)"; }
				else if($getGender['gender']== 2){ $gender = "Male (Is Gift)"; }
			
		  }
		   else{
				if($metaValue['forme_gender']== 1){ $gender = "Female (For Me)"; }
				else if($metaValue['forme_gender']== 2){ $gender = "Male (For Me)"; }
		  }
	    
	    if(!empty($metaValue['paBeforePayment'])){
		     $jsonBeforeResult  = json_decode($metaValue['paBeforePayment'], true);
		     $objApparel   		= $jsonBeforeResult[0]['objApparel'];
		    $objFootwear  		= $jsonBeforeResult[1]['objFootwear'];
		    $objAccessories 	= $jsonBeforeResult[2]['objAccessories'];
		    $objJewellery  		= $jsonBeforeResult[3]['objJewellery'];
		    $objBeauty   		= $jsonBeforeResult[4]['objBeauty'];
		    $objBag    			= $jsonBeforeResult[5]['objBag'];
	    }
	    else{
		    $objApparel   		= "";
		    $objFootwear  		= "";
		    $objAccessories  	= "";
		    $objJewellery 		= "";
		    $objBeauty   		= "";
		    $objBag    			= "";
	    }
		
		//echo '<pre>';print_r($objApparel);exit;
	    // JSON After Payment Result
	    if(!empty($metaValue['paAfterPayment'])){
	    		$jsonAfterResult 		= json_decode($metaValue['paAfterPayment'], true);
	    		if(!empty($jsonAfterResult[0]['objApparel']))
	    		{
	    			$objApparel_After  		= $jsonAfterResult[0]['objApparel'];
	    		}	
	    		else{ $objApparel_After  		= ""; }

	    		if(!empty($jsonAfterResult[1]['objFootwear']))
	    		{
			   		 $objFootwear_After  	= $jsonAfterResult[1]['objFootwear'];
				}
				else{ $objFootwear_After  	= "";}

				if(!empty($jsonAfterResult[2]['objAccessories']))
				{
			    $objAccessories_After 	= $jsonAfterResult[2]['objAccessories'];
				}
				else{$objAccessories_After 	= "";}

				if(!empty($jsonAfterResult[3]['objJewellery']))
				{
				    $objJewellery_After  	= $jsonAfterResult[3]['objJewellery'];
				}
				else{ $objJewellery_After  	= "";}

				if(!empty($jsonAfterResult[4]['objBeauty']))
				{
				    $objBeauty_After  		= $jsonAfterResult[4]['objBeauty'];
				}
				else{$objBeauty_After = "";}

				if(!empty($jsonAfterResult[5]['objBag']))
				{
				    $objBag_After   		= $jsonAfterResult[5]['objBag'];
				}
				else{ $objBag_After   		= ""; }
			}
				/*}
	    }*/

	    if(!empty($metaValue['categories']))
	    {
	     $categories  = $metaValue['categories'];
	    }
	    else{
	    	$categories = "";
	    }
	   
	    $result_data[$metaKey][] = $metaKey;
      
      if(!empty($metaValue['first_name'])){ $result_data[$metaKey][] = $metaValue['first_name'].' '.$metaValue['last_name']; }
      else{ $result_data[$metaKey][] =''; }
	  
       if(!empty($gender)){ $result_data[$metaKey][] = $gender; }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($metaValue['email_id'])){ $result_data[$metaKey][] = $metaValue['email_id']; }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($metaValue['mobile_no'])){ $result_data[$metaKey][] = $metaValue['mobile_no']; }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($categories)){$result_data[$metaKey][] = $categories ;}
      else{ $result_data[$metaKey][] =''; } 
      
      if(!empty($objApparel['pa_bodshape'])){ $result_data[$metaKey][] = $objApparel['pa_bodshape']; }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty(@$objApparel_After['apparel_height_feet'])){
      @$result_data[$metaKey][] = $objApparel_After['apparel_height_feet'].', '.@$objApparel_After['apparel_height_inches'];}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_stuff_dont_like'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_stuff_dont_like']); }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_top_size'])){ $result_data[$metaKey][] = $objApparel['apparel_top_size']; }
      else{ $result_data[$metaKey][] =''; }
	  
       if(!empty($objApparel['apparel_top_fit_like_this'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_top_fit_like_this']); }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_dress_size'])){$result_data[$metaKey][] = $objApparel['apparel_dress_size'];}
      else{ $result_data[$metaKey][] =''; }
	  
       if(!empty($objApparel['apparel_dress_fit_like_this'])){$result_data[$metaKey][] = implode(',', $objApparel['apparel_dress_fit_like_this']);}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_trouser_size'])){$result_data[$metaKey][] = $objApparel['apparel_trouser_size']; }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_jeans_size'])){ $result_data[$metaKey][] = $objApparel['apparel_jeans_size']; }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_bottom_fit_like_this'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_bottom_fit_like_this']); }
      else{ $result_data[$metaKey][] =''; }
	  
      if(!empty($objApparel['apparel_band_size'])){ $result_data[$metaKey][] = $objApparel['apparel_band_size']; }
      else{ $result_data[$metaKey][] =''; }
	  
      if(!empty($objApparel['apparel_cup_size'])){ $result_data[$metaKey][] = $objApparel['apparel_cup_size']; }
      else{ $result_data[$metaKey][] =''; }
	  
      
      if(!empty($objApparel['apparel_skirt_fit_like_this'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_skirt_fit_like_this']); }
      else{ $result_data[$metaKey][] =''; }
	  
	  if(!empty($objApparel['apparel_dos_and_donts']))
	  { 
			@$doData = '';
		//$result_data[$metaKey][] = implode(',', $objApparel['apparel_dos_and_donts']);
			 foreach($objApparel['apparel_dos_and_donts'] as $dontsKey => $donts)
				{
					if(!empty($donts[1]))
					{
						@$doData[] = @$donts[0].'=>'. @$donts[1];
					}
				}
			@$result_data[$metaKey][] = implode(', ', @$doData);
	  }
		else{ $result_data[$metaKey][] =''; }
	  
	  
	  
      
      if(!empty($objApparel['apparel_color_avoid']))
	  { 
			//$result_data[$metaKey][] = implode(',', $objApparel['apparel_color_avoid']);
			 foreach($objApparel['apparel_color_avoid'] as $c => $name)
			  {
				  if(!empty($name)){
				  $color= $color.','.@$colorsName[$name];
				  }
			  } 
			  $result_data[$metaKey][] =$color; 
	  }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_print_avoid'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_print_avoid']); }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_tshirt_size'])){ $result_data[$metaKey][] = $objApparel['apparel_tshirt_size']; }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_tshirt_fit_like_this'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_tshirt_fit_like_this']); }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_shirt_size'])){ $result_data[$metaKey][] = $objApparel['apparel_shirt_size']; }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_shirt_fit_like_this'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_shirt_fit_like_this']); }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_trouser_fit_like_this'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_trouser_fit_like_this']); }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_jeans_fit_like_this'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_jeans_fit_like_this']); }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objApparel['apparel_print_avoid_men'])){ $result_data[$metaKey][] = implode(',', $objApparel['apparel_print_avoid_men']); }
      else{ $result_data[$metaKey][] =''; }
      
      
      if(!empty($objFootwear['footwear_size'])){ $result_data[$metaKey][] = $objFootwear['footwear_size']; }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objFootwear['footwear_type'])){ $result_data[$metaKey][] = implode(',',$objFootwear['footwear_type']); }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objFootwear['footwear_width'])){ $result_data[$metaKey][] = $objFootwear['footwear_width']; }
      else{ $result_data[$metaKey][] =''; } 

      if(!empty($objFootwear_After['footwear_heel_type'])){$result_data[$metaKey][] = implode(',',$objFootwear_After['footwear_heel_type']);}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objFootwear_After['footwear_heel_height'])){$result_data[$metaKey][] = $objFootwear_After['footwear_heel_height'];}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objAccessories_After['accessories_sunglasses_type'])){ $result_data[$metaKey][] = implode(',',$objAccessories_After['accessories_sunglasses_type']); }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objAccessories_After['accessories_refelector_type'])){$result_data[$metaKey][] = $objAccessories_After['accessories_refelector_type'];}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objAccessories_After['accessories_belt_type'])){ $result_data[$metaKey][] = implode(',',$objAccessories_After['accessories_belt_type']); }
      else{ $result_data[$metaKey][] =''; }
      
      
      if(!empty($objJewellery['jewellery_type'])){$result_data[$metaKey][] = implode(',',$objJewellery['jewellery_type']);}
      else{ $result_data[$metaKey][] =''; }

      if(!empty($objJewellery_After['jewellery_tone'])){$result_data[$metaKey][] = implode(',',$objJewellery_After['jewellery_tone']);}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objBeauty_After['objBeauty'])){$result_data[$metaKey][] = implode(',',$objBeauty_After['objBeauty']);}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objBeauty['beauty_type'])){$result_data[$metaKey][] = $objBeauty['beauty_type'];}
      else{ $result_data[$metaKey][] =''; }
    
      if(!empty($objBeauty['beauty_shade'])){$result_data[$metaKey][] = $objBeauty['beauty_shade'];}
      else{ $result_data[$metaKey][] =''; }

       if(!empty($objBeauty['beauty_finishing_type'])){$result_data[$metaKey][] = $objBeauty['beauty_finishing_type'];}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objBag['bag_type'])){$result_data[$metaKey][] = implode(',',$objBag['bag_type']);}
      else{ $result_data[$metaKey][] =''; } 
      //echo "<pre>";print_r($jsonBeforeResult);echo"</pre>";      
   
      
      if(!empty($objBag_After['bag_i_want'])){$result_data[$metaKey][] = $objBag_After['bag_i_want'];}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objBag_After['bag_size'])){$result_data[$metaKey][] = $objBag_After['bag_size'];}
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objBag_After['bag_color_avoid']))
	  {
		  //$result_data[$metaKey][] = implode(',',$objBag_After['bag_color_avoid']);
		  foreach($objBag_After['bag_color_avoid'] as $c => $name)
			  {
				  if(!empty($name)){
				  $color= $color.','.@$colorsName[$name];
				  }
			} 
			  $result_data[$metaKey][] =$color; 
		  }
      else{ $result_data[$metaKey][] =''; }
      
      if(!empty($objAccessories['accessories_types'])){ $result_data[$metaKey][] = $objAccessories['accessories_types'];}
      else{ $result_data[$metaKey][] =''; }
	  
	 
	   }
	   
	  }
	  
	  //echo "<pre>";print_r($result_data);echo"</pre>";
	  $this->writeDataintoCSV($fieldName,$result_data,'sc_box_user_data');
	  exit();
	}
	
	
	
public function get_awb_no($order_unique_number ='')
	{
		$order_unique_number  	= $this->input->post('order_unique_number');
		$result 				= $this->Manage_orders_model->getOrderProductInfo($order_unique_number);
		$paymod_data 			= $result[0]['pay_mode'];
		$paymod 				= ($paymod_data == 1)?'C':'P';
		$CollectableAmount 		= ($paymod_data == 1)? round($result[0]['OrderGrandTotal']):' ';
		$userid = explode('_',$order_unique_number);
		$fields = '
			<NewDataSet xmlns="http://schemas.datacontract.org/2004/07/WebX.Entity">
			  <Customer>
				<CUSTCD>CC000100132</CUSTCD>
			  </Customer>
			  <DocketList>
			 <DocketList>
				  <AgentID></AgentID>
				  <AwbNo></AwbNo>
				  <Breath>1</Breath>
				  <CollectableAmount>'.$CollectableAmount.'</CollectableAmount>
				  <CustomerName>'.$result[0]['first_name'].' '.$result[0]['last_name'].'</CustomerName>
				  <Height>1</Height>
				  <IsPudo>N</IsPudo>
				  <ItemName>'.$result[0]['product_name'].'</ItemName>
				  <Length>1</Length>
				  <Mode>'.$paymod.'</Mode>
				  <NoOfPieces>'.$result[0]['product_qty'].'</NoOfPieces>
				  <OrderConformation>Y</OrderConformation>
				  <OrderNo>'.$result[0]['order_id'].'</OrderNo>
				  <ProductCode>'.$result[0]['order_prd_id'].'</ProductCode>
				  <PudoId></PudoId>
				  <RateCalculation>N</RateCalculation>
				  <ShippingAdd1>'.$result[0]['shipping_address'].'</ShippingAdd1>
				  <ShippingAdd2> </ShippingAdd2>
				  <ShippingCity>'.$result[0]['city_name'].'</ShippingCity>
				  <ShippingEmailId>'.$result[0]['email_id'].'</ShippingEmailId>
				  <ShippingMobileNo>'.$result[0]['mobile_no'].'</ShippingMobileNo>
				  <ShippingState>'.$result[0]['state_name'].'</ShippingState>
				  <ShippingTelephoneNo>'.$result[0]['mobile_no'].'</ShippingTelephoneNo>
				  <ShippingZip>'.$result[0]['pincode'].'</ShippingZip>
				  <TotalAmount>'.$CollectableAmount.'</TotalAmount>
				  <TypeOfDelivery>Home Delivery</TypeOfDelivery>
				  <TypeOfService>Economy</TypeOfService>
				  <UOM>Per KG</UOM>
				  <VendorAddress1>Impression House,</VendorAddress1>
				  <VendorAddress2>First Floor, 42A, G. D. Ambekar Marg, Wadala,</VendorAddress2>
				  <VendorName>StyleCracker</VendorName>
				  <VendorPincode>400031</VendorPincode>
				  <VendorTeleNo>8048130591</VendorTeleNo>
				  <Weight>0.30</Weight>
				</DocketList>
			  </DocketList>
			</NewDataSet> ';
			
		$headers = array
		(
			//'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/xml'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'http://dotzot-test.azurewebsites.net/RestService/PushOrderDataService.svc/PushOrderData_PUDO_New' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, $fields );
		$returnData = curl_exec($ch );
		curl_close( $ch );

		$awbData = json_decode($returnData, true);
		$data['awp_no'] 		= $awbData[0]['DockNo'];
		$data['orderNo'] 		= $awbData[0]['OrderNo'];
		$data['reason'] 		= $awbData[0]['Reason'];
		$data['succeed'] 		= $awbData[0]['Succeed'];
		$data['totalFreight'] 	= $awbData[0]['TotalFreight'];
		echo  json_encode($data); 
		
	
		/*$login_user_id = $this->session->userdata('user_id');
		$deliveryProcess 						= array();
		$deliveryProcess['order_unq_no']		= $order_unique_number;
		$deliveryProcess['order_prd_id']		= $result[0]['order_prd_id'];	
		$deliveryProcess['order_no']			= $result[0]['order_id'];
		$deliveryProcess['product_id']			= $result[0]['product_id'];
		$deliveryProcess['brand_id']			= $result[0]['brand_id'];
		$deliveryProcess['dp_id']				= '';
		$deliveryProcess['pickup_address']		= 'Impression House, First Floor, 42A, G. D. Ambekar Marg, Wadala, Mumbai - 400031, Maharashtra, India';
		$deliveryProcess['shipping_address']	= $result[0]['shipping_address'].', '.$result[0]['city_name'].'-'.$result[0]['pincode'].', '.$result[0]['state_name'];
		$deliveryProcess['awp_no']				= $awbData[0]['DockNo'];
		$deliveryProcess['dpCharges']			= $awbData[0]['CODCHARGE'];
		$deliveryProcess['dp_status']			= '';
		$deliveryProcess['created_by']			= $login_user_id;
		$deliveryProcess['modified_by']			= $login_user_id;
		$deliveryProcess['created_datetime']	= date('Y-m-d H:i:s');
		$dpId = $this->Manage_orders_model->addDeliveryProcess($deliveryProcess);
		
		$getAwbNosData = $this->Manage_orders_model->getAwbNosData($dpId, $order_unique_number);
		echo  json_encode($getAwbNosData);*/
		
	}


	
	
	
public function pickerr_awb_no($order_unique_number ='')
	{
		$order_unique_number  	= $this->input->post('order_unique_number')?$this->input->post('order_unique_number'):'24785_1846';
		
		$result 				= $this->Manage_orders_model->getOrderProductInfo($order_unique_number);
		
		$paymod_data 			= $result[0]['pay_mode'];
		$paymod 				= ($paymod_data == 1)?'C':'P';
		$CollectableAmount 		= ($paymod_data == 1)? round($result[0]['OrderGrandTotal']):' ';
		$userid = explode('_',$order_unique_number);
		$params = array(
					"auth_token" 		=> "36af5c05fa1317700ad50ccb3921a8c65544",
					"item_name" 		=> $result[0]['product_name'],  
					"from_name" 		=> "StyleCracker",
					"from_phone_number" => "02240007796",
					"from_address" 		=> "Impression House, G.D. Ambekar Road, Wadala West",  
					"from_pincode" 		=> "400031",
					"pickup_gstin" 		=> "",
					"to_name" 			=> $result[0]['first_name'].' '.$result[0]['last_name'],
					"to_phone_number" 	=> $result[0]['mobile_no'],
					"to_pincode" 		=> $result[0]['pincode'],
					"to_address" 		=> $result[0]['shipping_address'].', '.$result[0]['city_name'].'-'.$result[0]['pincode'].', '.$result[0]['state_name'],  
					"quantity" 			=> $result[0]['product_qty'],  
					"invoice_value" 	=> $result[0]['product_price'],
					"cod_amount" 		=> $CollectableAmount,
					"client_order_id" 	=> $result[0]['order_display_no'],
					"item_breadth" 		=> "",
					"item_length" 		=> "",
					"item_height" 		=> "",
					"item_weight" 		=> "",
					"is_reverse" 		=> "False" 
					
				);
			
		$headers = array
		(
			//'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);

            $json_params = json_encode( $params );
            $ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'http://www.pickrr.com/api/place-order/' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, $json_params );
			$returnData = curl_exec($ch );
			curl_close( $ch );
		
			$awbData = json_decode($returnData, true);
			$data['awp_no'] 			= $awbData['tracking_id'];
			$data['success'] 			= $awbData['success'];
			$data['client_order_id'] 	= $awbData['client_order_id'];
			$data['courier'] 			= $awbData['courier'];
			$data['ip_string'] 			= $awbData['ip_string'];
			$data['routing_code'] 		= $awbData['routing_code'];
			$data['order_pk'] 			= $awbData['order_pk'];
			$data['manifest_link'] 		= $awbData['manifest_link'];
			$data['err'] 				= $awbData['err'];
			echo  json_encode($data);
		/*$login_user_id = $this->session->userdata('user_id');
		$deliveryProcess 						= array();
		$deliveryProcess['order_unq_no']		= $order_unique_number;
		$deliveryProcess['order_prd_id']		= $result[0]['order_prd_id'];	
		$deliveryProcess['order_no']			= $result[0]['order_id'];
		$deliveryProcess['product_id']			= $result[0]['product_id'];
		$deliveryProcess['brand_id']			= $result[0]['brand_id'];
		$deliveryProcess['dp_id']				= '';
		$deliveryProcess['pickup_address']		= 'Impression House, First Floor, 42A, G. D. Ambekar Marg, Wadala, Mumbai - 400031, Maharashtra, India';
		$deliveryProcess['shipping_address']	= $result[0]['shipping_address'].', '.$result[0]['city_name'].'-'.$result[0]['pincode'].', '.$result[0]['state_name'];
		$deliveryProcess['awp_no']				= $awbData['tracking_id'];
		$deliveryProcess['dpCharges']			= ''; //$awbData[0]['CODCHARGE'];
		$deliveryProcess['dp_status']			= '';
		$deliveryProcess['created_by']			= $login_user_id;
		$deliveryProcess['modified_by']			= $login_user_id;
		$deliveryProcess['created_datetime']	= date('Y-m-d H:i:s');
		$dpId = $this->Manage_orders_model->addDeliveryProcess($deliveryProcess);
		$getAwbNosData = $this->Manage_orders_model->getAwbNosData($dpId, $order_unique_number);
		echo  json_encode($getAwbNosData);*/
		
	}
	
	public function addawb()
	{
		$awbNumber 		= trim($this->input->post('awb_num'));
		$delPartner 	= trim($this->input->post('delivery_partner'));
		$cartId 		= trim($this->input->post('cartidforawb'));
		$orderPrdId   = trim($this->input->post('orderprdidforawb'));
		$login_user_id 	= $this->session->userdata('user_id');		
		$cartMeta 						= array();
		$cartMeta[0]['cart_meta_key'] 	= 'order_delivery_partner';
		$cartMeta[0]['cart_meta_value'] = $delPartner;
		$cartMeta[1]['cart_meta_key'] 	= 'order_awb_no';	
		$cartMeta[1]['cart_meta_value'] = $awbNumber;
		
		
		if(is_array($cartMeta) && count($cartMeta)>0)
		{
			foreach($cartMeta as $key=>$cartMetaValue)
			{
				$cartData['cart_id']  			= $cartId;
				$cartData['cart_meta_key']  	= $cartMetaValue['cart_meta_key'];
				$cartData['cart_meta_key']  	= $cartMetaValue['cart_meta_key'];
				$cartData['cart_meta_value']	= $cartMetaValue['cart_meta_value'];
				$cartData['created_by']			= $login_user_id;
				$cartData['modified_by']		= $login_user_id;
				$cartData['created_datetime']	= date('Y-m-d H:i:s');
				
				$cartMetaId = $this->Manage_orders_model->add_cart_meta($cartData);
				if(!empty($cartMetaId))
				{
					$result = $this->Manage_orders_model->updateOrder($orderPrdId,3);
					echo $cartMetaId;
				}else
				{
					echo 'false';
				}
				
			}
		}
		
	}
	
	public function gat_cart_data()
	{
		$cartId = trim($this->input->post('cartId'));
		$orderPrdId = $this->input->post('id');
		$orderStatus = $this->input->post('orderStatus');
		$cartMetaData = $this->Manage_orders_model->get_cart_meta_data($cartId);
		
		// if(!empty($cartMetaData))
		// {
		// 	//$result = $this->Manage_orders_model->updateOrder($orderPrdId,$orderStatus);
		// }	
		
		echo json_encode($cartMetaData);
		/* echo "<pre>";
		print_r($cartMetaData);
		echo "</pre>"; */
	}
	

	
	
	


	public function get_user_data()
	{
		$orderUniqueId 			= $this->input->post('orderUniqueId');
		$orderUserData			= $this->Manage_orders_model->get_order_user_data($orderUniqueId);
		
		$scbox_gift_object = $this->Scbox_model->get_object('scbox_gift')[0]['object_id']; 
		$gift_data = $this->Scbox_model->get_gift_data($scbox_gift_object,$orderUserData['order_display_no']);
		if(!empty($gift_data) && count($gift_data)>0)
		{
			$name	= explode(' ',$gift_data['name']);
			$giftData['order_unique_no']	= $orderUserData['order_unique_no'];
			$giftData['order_display_no']	= $orderUserData['order_display_no'];
			$giftData['first_name']			= $name[0];
			$giftData['last_name']			= $name[1];
			$giftData['mobile_no']			= $gift_data['mobileno'];
			$giftData['pincode']			= $gift_data['pincode'];
			$giftData['shipping_address']	= $gift_data['address'];
			$giftData['city_name']			= $gift_data['city'];
			$giftData['state_name']			= $gift_data['state'];
			$data['orderUserData']	= $giftData;
		}
		else{
			$data['orderUserData']	= $orderUserData;
		}
		echo json_encode($data);
	}
	
	public function update_user_order_info()
	{
		$unique_no			= $this->input->post('awb_order_unique_no');
		$display_no			= $this->input->post('awb_order_display_no');
		$first_name			= $this->input->post('awb_customer_fname');
		$last_name			= $this->input->post('awb_customer_lname');
		$user_mobile		= $this->input->post('awb_customer_mobile');
		$user_pincode		= $this->input->post('awb_customer_pincode');
		$shipping_address	= $this->input->post('awb_customer_address');
		$city				= $this->input->post('awb_customer_city');
		$state				= $this->input->post('awb_customer_state');
		
		$scbox_gift_object = $this->Scbox_model->get_object('scbox_gift')[0]['object_id']; 
		$gift_data = $this->Scbox_model->get_gift_data($scbox_gift_object,$display_no);
		 if(!empty($gift_data) && count($gift_data)>0)
		 {
			$giftData['gender'] 				= $gift_data['gender'];
			$giftData['name'] 					= $first_name.' '.$last_name;
			$giftData['email'] 					= $gift_data['email'];
			$giftData['mobileno'] 				= $user_mobile;
			$giftData['pincode'] 				= $user_pincode;
			$giftData['city'] 					= $city;
			$giftData['state'] 					= $state;
			$giftData['address'] 				= $shipping_address;
			$giftData['profession'] 			= $gift_data['profession'];
			$giftData['gift_msg'] 				= $gift_data['gift_msg'];
			$giftData['stylist_call_receiver']  = $gift_data['stylist_call_receiver'];
			$serializeGiftDetail				= serialize($giftData);
			$arrUpdate['object_meta_value']		= $serializeGiftDetail;
			$update		= $this->Manage_orders_model->update_gift_datail($arrUpdate, $scbox_gift_object, $display_no);
		 }
		else{
			$updateOrderData = array();
			$updateOrderData['first_name']			= $first_name;	
			$updateOrderData['last_name']			= $last_name;
			$updateOrderData['mobile_no']			= $user_mobile;
			$updateOrderData['pincode']				= $user_pincode;
			$updateOrderData['shipping_address']	= $shipping_address;
			$updateOrderData['city_name']			= $city;
			$updateOrderData['state_name']			= $state;
			$update = $this->Manage_orders_model->update_user_order_data($updateOrderData,$unique_no,$display_no);
		}
		if($update == 1) {$data = 'success';}
		else{$data = 'fail';}
		echo json_encode($data);
		
	}
	

	}
?>
