<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scboxusers extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Pa_model');
		//$this->load->model('Scbox_model');		
		//$this->load->library('email');
		//$this->load->library('form_validation');
	}

	public function pa_data_edit(){
		$data = array();
		if($this->input->post()){
			
			$SCBOX_PACKAGE = unserialize(SCBOX_PACKAGE);
			
			$box_data = array();$data = array();
			$data['scbox_package'] = $this->input->post('scbox_package');			
			redirect('websiteusers/scbox');
		}else{
			
			$this->load->view('common/header2');
			$this->load->view('scbox/scbox_pa_edit',$data);
			$this->load->view('common/footer2');
		}		
	}

	 function user_order_pa_save()
	 {
	 	//echo 'user_order_pa_save--';exit;
	      $category_pa_data = array();
	      $attribute_pa_data = array();
	      $attributes = array();
	      $attributes1 = array();
	      $attributes2 = array();
	      $categories = array();
	      $categories1 = '';
	      $categories2 = '';
	      $categories3 = '';	      
	     
	      $order_unq_no = $this->input->post('order_unq_no');
	      $order_unq_no_arr = explode('_', $order_unq_no);
	      //echo '<pre>';print_r($order_unq_no_arr);exit;
	      $user_id = $order_unq_no_arr[0];
	      $orderid = $order_unq_no_arr[1];

	     $cartid =  $this->Pa_model->get_order_cartid($orderid)[0]['id'];

	      $categories1_post = $this->input->post('arrUserCategorySelected');
	      $categories2_post = $this->input->post('arrUserAnswerCategory');
	      $categories3_post = $this->input->post('arrUserAnswerAfterCategory');

	      if(!empty($categories1_post))
	      {
	      	$categories1 = implode(',',$categories1_post);
	      }
	      if(!empty($categories2_post))
	      {
	      	$categories2 = implode(',',$categories2_post);
	  	  }
	  	  if(!empty($categories3_post))
	      {
	      	$categories3 = implode(',',$categories3_post);
	      }

	      //echo '<pre>';print_r($categories1);exit;
	      //$categories = array_merge($categories1,$categories2);
	      $categories = $categories1.','.$categories2;

	      if(!empty($categories1) || !empty($categories2) || !empty($categories3))
	      {
	        $categories = $categories1.','.$categories2.','.$categories3;
	      }

	      //$attributes = @$_COOKIE['arrUserAnswerSelected']; 
	      $attributes1_post = $this->input->post('arrUserAnswerSelected'); 
	      $attributes2_post = $this->input->post('arrUserAnswerAfterSelected');	

	      $attributes1 = implode(',',$attributes1_post);
	      $attributes2 = implode(',',$attributes2_post);

	      if(!empty($attributes1) || !empty($attributes2))
	      {
	        $attributes = $attributes1.','.$attributes2;
	      }
	     // echo '<pre>';print_r($attributes);exit;
	      if(!empty($categories))
	      {
	        $category_array = explode(',', $categories);     
	        $i=0;
	        foreach($category_array as $val)
	        {
	          if($val!='' && $val!='0')
	          {
	            $category_pa_data[$i]['object_id'] = $cartid;
	            $category_pa_data[$i]['category_id'] = $val;
	            $category_pa_data[$i]['object_type'] = 'order';
	            $category_pa_data[$i]['modified_by'] = $user_id;
	          }
	          $i++;
	        }
	      }

	      if(!empty($attributes))
	      {
	           $attributes_array = explode(',', $attributes);
	            $j=0;
	            foreach($attributes_array as $val)
	            {
	              if($val!='' && $val!='0')
	              {
	                $attribute_pa_data[$j]['object_id'] = $cartid;
	                $attribute_pa_data[$j]['attribute_id'] = $val;
	                $attribute_pa_data[$j]['object_type'] = 'order';
	                $attribute_pa_data[$j]['modified_by'] = $user_id;
	              }       
	              
	              $j++;
	            }
	      }

	      //echo '<pre>';print_r($attribute_pa_data);exit;
	     
	       if(!empty($category_pa_data) && !empty($attribute_pa_data))
	      {
	        $result = $this->Pa_model->user_pa_data('order',$category_pa_data,$attribute_pa_data,$cartid);

	        if($result)
	        {
	        	echo 'success';
	        }else
	        {
	        	echo 'error';
	        }
	      }
	     
	  }
	
}