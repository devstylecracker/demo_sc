<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends MY_Controller {
	
	function __construct(){
        parent::__construct();
        
    }  

	public function index()
	{
 

		$this->load->view('common/header');
		$this->load->view('help_view');
		$this->load->view('common/footer');	
	}	
}
?>