<?php
//error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	
	function __construct(){
        parent::__construct();
        $this->load->model('Home_model'); 
        $this->load->model('Login_model'); 
        $this->load->model('Reports_model');      

    }  

	public function index()
	{ 
    if(@$this->session->userdata('role_id')==6){
      redirect('/brandDashboard');
    }else if(@$this->session->userdata('role_id')==8)
    {
      redirect('/AffiliateUsers');
    }
    if(@$this->session->userdata('role_id') == '7')
    {
      redirect('/delivery_process');
    }
		if(!$this->session->userdata('user_id')){ 

      if(@$this->input->cookie('7572ce8a49ff8982ba4d17f9e56b6d1f')!='' && @$this->input->cookie('a6c16340e402e946c64ce53bb41ac24a')!=''){
        
        $email_id = $this->encryptOrDecrypt($this->input->cookie('7572ce8a49ff8982ba4d17f9e56b6d1f'),'');
        $password = $this->encryptOrDecrypt($this->input->cookie('a6c16340e402e946c64ce53bb41ac24a'),'');

        $res = $this->Login_model->login_user(trim($email_id),trim($password));  
         if($res == 1){ 
           if($this->input->cookie('sc_admin_last_url')!=''){
              redirect($this->input->cookie('sc_admin_last_url'));
            }else{
              redirect('/home','refresh');
            }
         }else{
            $data['error'] = "Invalid Username and Password";
            $this->load->view('login',$data); 
         }

      }else{
			 //$this->load->view('login');
       $email_id = $this->input->post('email');
       $password = $this->input->post('password');
        $res = $this->Login_model->login_user(trim($email_id),trim($password));  
        // echo '<pre>';print_r($res);exit;
         if($res == 1){ 
           if($this->input->cookie('sc_admin_last_url')!=''){
              redirect($this->input->cookie('sc_admin_last_url'));
            }else{
              //redirect('/home','refresh');
              redirect('/home');
            }
         }else{
            $data['error'] = "Invalid Username and Password";
            $this->load->view('login',$data); 
         }
      }
		}else{ 

      $date_from = '';  $date_to = '';

       if($this->input->post()){
          $date_from = $this->input->post('date_from')!='' ? $this->input->post('date_from') : 0;
          $date_to = $this->input->post('date_to')!='' ? $this->input->post('date_to') : 0;
       }else{ $date_from = ''; $date_to =''; }
  			$data = array();
  			//$data = $this->Home_model->total_no_products(); 
       // $data['dashboard'] = $this->dashboard($date_from,$date_to);       

      /*  $data['auclick'] = '[{value: '. $data['dashboard']['authCount'].',color: "#1E90FF",highlight: "#1E90FF",label: "Authenticated"},{value: '. $data['dashboard']['unauthCount'].',color: "#00a65a",highlight: "#00a65a",label: "Unauthenticated"}]';

        $data['click'] = '[{value: '.$data['dashboard']['cpcCount'].',color: "#1E90FF",highlight: "#1E90FF",label: "CPC"},{value: '.$data['dashboard']['cpaCount'].',color: "#00a65a",highlight: "#00a65a",label: "CPA"}]';
*/
        //$data['look'] = $this->Home_model->get_look_data(); 
        //$data['look'] = $this->Home_model->get_look_data();
        /*$data['user_info'] = $this->dashboard_user_info($date_from,$date_to); 

        $data['signup'] = '[{value: '. $data['user_info']['website'].',color: "#1E90FF",highlight: "#1E90FF",label: "Website"},{value: '.$data['user_info']['facebook'].',color: "#00a65a",highlight: "#00a65a",label: "Facebook"},{value: '.$data['user_info']['google'].',color: "#f39c12",
                  highlight: "#f39c12",label: "Google"},{value: '.$data['user_info']['mobile'].',color: "#00c0ef",highlight: "#00c0ef",label: "Mobile"},{value: '.$data['user_info']['mobile_facebook'].',color: "#dd4b39",highlight: "#dd4b39",label: "Mobile Facebook"},{value: '.$data['user_info']['mobile_google'].',color: "#605ca8",highlight: "#605ca8",label: "Mobile Google"}]';
      */

  			$this->load->view('common/header');   
  			//$this->load->view('home',$data);
        //$this->load->view('home');
  			$this->load->view('common/footer');
		}
	}

	public function reports(){
		if(!$this->session->userdata('user_id')){
			$this->load->view('login');
		}else{ 

			$data = array(); $reports_data = '';
			//$data = $this->Home_model->total_no_products(); 

      // print_r($data);exit;
      if($this->session->userdata('role_id')==1)
      {
    			$reports_data = $reports_data.' <div class="row">
        <div class="box-header">
              <h3 class="box-title">SignUp Data</h3>               
        </div>          
        </div>';
      } 

    $reports_data = $reports_data.' <div class="row">
      <div class="box-header">
            <h3 class="box-title">Product Data</h3>               
      </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-teal-active color-palette"><i class="fa fa-fw fa-file-powerpoint-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total Product</span>
                  <span class="info-box-number">'.$data['product_data']['product_count'].'</span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-teal color-palette"><i class="fa fa-fw fa-file-powerpoint-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Today</span>
                  <span class="info-box-number">'.$data['product_data']['current_date_product'].'</span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-teal color-palette"><i class="fa fa-fw fa-file-powerpoint-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Current Month</span>
                  <span class="info-box-number">'.$data['product_data']['current_month_product'].'</span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-teal disabled color-palette"><i class="fa fa-fw fa-file-powerpoint-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Last Month</span>
                  <span class="info-box-number">'.$data['product_data']['last_month_product'].'</span>
                </div>
              </div>
            </div>

            <!-- fix for small devices only -->
            <!--div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-file"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Products</span>
                  <span class="info-box-number">'.$data['product_data']['product_count'].'</span>
                </div>
              </div>
            </div-->          
    </div>';


      if($this->session->userdata('role_id')==1 || $this->session->userdata('role_id')==3)
      {
       $reports_data = $reports_data.'<div class="row">
            <div class="box-header">
                  <h3 class="box-title">Look Data</h3>               
            </div>
             <!--     <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Look Count</span>
                        <span class="info-box-number">'.$data['look_data']['total_look_count'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['look_data']['current_look_count'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['look_data']['current_month_look_count'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['look_data']['last_month_look_count'].'</span>
                      </div>
                    </div>
                  </div>

            <div class="box-header">
                  <h3 class="box-title">Look Broadcasted Data</h3>               
            </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-purple-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Look Count</span>
                        <span class="info-box-number">'.$data['look_data']['total_look_broadcast'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-purple color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['look_data']['current_look_broadcast'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-purple color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['look_data']['current_month_look_broadcast'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-purple disabled color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['look_data']['last_month_look_broadcast'].'</span>
                      </div>
                    </div>
                  </div>

            <div class="box-header">
                  <h3 class="box-title">Look Pending Data</h3>               
            </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-orange-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Look Count</span>
                        <span class="info-box-number">'.$data['look_data']['total_look_pending'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-orange color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['look_data']['current_look_pending'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-orange color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['look_data']['current_month_look_pending'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-orange disabled color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['look_data']['last_month_look_pending'].'</span>
                      </div>
                    </div>
                  </div>

            <div class="box-header">
                  <h3 class="box-title">Look Rejected Data</h3>               
            </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Look Count</span>
                        <span class="info-box-number">'.$data['look_data']['total_look_rejected'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['look_data']['current_look_rejected'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['look_data']['current_month_look_rejected'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['look_data']['last_month_look_rejected'].'</span>
                      </div>
                    </div>
                  </div>          
          </div>


          <div class="row">
            <div class="box-header">
                  <h3 class="box-title">Brand Data</h3>               
            </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Brand</span>
                        <span class="info-box-number">'.$data['brand_data']['overall_brands'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['brand_data']['current_date_brand'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['brand_data']['current_month_brand'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['brand_data']['last_month_brand'].'</span>
                      </div>
                    </div>
                  </div>

                <div class="box-header">
                  <h3 class="box-title">Store Data</h3>               
                </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-light-blue-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Store</span>
                        <span class="info-box-number">'.$data['brand_data']['overall_store'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-light-blue-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['brand_data']['current_date_store'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-light-blue-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['brand_data']['current_month_store'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-light-blue-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['brand_data']['last_month_store'].'</span>
                      </div>
                    </div>
                  </div>-->
              </div>';
       }

     $reports_data = $reports_data.'<div class="row">            
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-thumbs-o-up"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Click Count</span>
                  <span class="info-box-number">'.$data['brand_data']['brand_product_click_count'].'</span>
                </div>
              </div>
            </div>
         
          <div class="clearfix visible-sm-block"></div>            
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-rupee"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">CPC</span>
                  <span class="info-box-number">'.$data['brand_data']['brand_cpc'].'</span>
                </div>
              </div>
            </div>
           </div>           
      </div>			
			<br/>';

			echo $reports_data;
		}
	}

/* SignUp Report Function */
  public function signupreports()
  {
      if(!$this->session->userdata('user_id')){
        $this->load->view('login');
      }else{ 

        $data = array(); $reports_data = '';
        $data = $this->Home_model->get_signup_data(); 

          //print_r($data);exit;
          if($this->session->userdata('role_id')==1)
          {
              $reports_data = $reports_data.' <div class="row" id="signupreportdiv">
            <!--<div class="box-header">
                  <h3 class="box-title">SignUp Data</h3>               
            </div>
             <div class="box-header">
                <h3 class="box-title">For Today</h3>  
            </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-users"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Overall Today</span>
                    <span class="info-box-number">'.$data['current_date'].'</span>
                  </div>
                </div>
              </div>           
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-google-plus"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Google Users Today</span>
                    <span class="info-box-number">'.$data['current_date_google'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-facebook"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Facebook Users Today</span>
                    <span class="info-box-number">'.$data['current_date_facebook'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-user"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Website Users Today</span>
                    <span class="info-box-number">'.$data['current_date_website'].'</span>
                  </div>
                </div>
              </div>-->
             
          <div class="box-header">
            <h3 class="box-title">Current Month</h3>  
          </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-users"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Overall Users</span>
                    <span class="info-box-number">'.$data['current_month'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-google-plus"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Google Users</span>
                    <span class="info-box-number">'.$data['current_month_google'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-facebook"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Facebook Users</span>
                    <span class="info-box-number">'.$data['current_month_facebook'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-user"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Website Users</span>
                    <span class="info-box-number">'.$data['current_month_website'].'</span>
                  </div>
                </div>
              </div>  


              <div class="box-header">
            <h3 class="box-title">Last Month</h3>  
          </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-users"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Overall Users</span>
                    <span class="info-box-number">'.$data['last_month'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-google-plus"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Google Users</span>
                    <span class="info-box-number">'.$data['last_month_google'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-facebook"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Facebook Users</span>
                    <span class="info-box-number">'.$data['last_month_facebook'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-user"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Website Users</span>
                    <span class="info-box-number">'.$data['last_month_website'].'</span>
                  </div>
                </div>
              </div> 
      

          <div class="box-header">
            <h3 class="box-title">Overall</h3>  
          </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-users"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Overall Users</span>
                    <span class="info-box-number">'.$data['overall_signup'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-google-plus"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Google Users</span>
                    <span class="info-box-number">'.$data['google'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-facebook"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Facebook Users</span>
                    <span class="info-box-number">'.$data['facebook'].'</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-user"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Website Users</span>
                    <span class="info-box-number">'.$data['website'].'</span>
                  </div>
                </div>
              </div>   
             
              </div>';
         }
         echo $reports_data; 
       }
     }
  /*End of SignUp Report Function */

  /* Product Report Function */
  public function productreports()
  {
      if(!$this->session->userdata('user_id')){
        $this->load->view('login');
      }else{ 

        $data = array(); $reports_data = '';
        $data = $this->Home_model->get_product_data(); 

          //print_r($data);exit;
          if($this->session->userdata('role_id')==1)
          {
               $reports_data = $reports_data.' <div class="row" id="productreportdiv">
              <div class="box-header">
                    <h3 class="box-title">Product Data</h3>               
              </div>                   
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-teal color-palette"><i class="fa fa-fw fa-file-powerpoint-o"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Today</span>
                          <span class="info-box-number">'.$data['current_date_product'].'</span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-teal color-palette"><i class="fa fa-fw fa-file-powerpoint-o"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Current Month</span>
                          <span class="info-box-number">'.$data['current_month_product'].'</span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-teal disabled color-palette"><i class="fa fa-fw fa-file-powerpoint-o"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Last Month</span>
                          <span class="info-box-number">'.$data['last_month_product'].'</span>
                        </div>
                      </div>
                    </div>                
                      
            </div>';
          }

         echo $reports_data; 
       }
     }
  /*End of Product Report Function */

  /* Look Report Function */
  public function lookreports()
  {
      if(!$this->session->userdata('user_id')){
        $this->load->view('login');
      }else{ 

        $data = array(); $reports_data = '';
        $data = $this->Home_model->get_look_data(); 

       // echo "<pre>";print_r($data['look_bucket']);exit;
        $tblbody ='';
        
        if(!empty($data['look_bucket'])) { $i = 0;
            foreach($data['look_bucket'] as $val){             
              $tblbody=$tblbody.'<tr>
                <td>'.$val['look_count'].'</td>
                <td>'.$data['look_bucket_user'][$val['bucket_id']].'</td>
                <td>'.$val['body_shape'].'</td>
                <td>'.$val['style'].'</td>                            
                    </tr>';
                     $i++; } }else{  
                      $tblbody ='<tr>
                        <td colspan="5">Sorry no record found</td>
                      </tr>';
                      } 
            //echo $tblbody;exit;

      if($this->session->userdata('role_id')==1)
      {
       $reports_data = $reports_data.'<div class="row" id="lookreportdiv">
            <div class="box-header">
                  <h3 class="box-title">Look Data</h3>               
            </div>             
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['current_look_count'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['current_month_look_count'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['last_month_look_count'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <!--<span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number"></span>
                      </div>-->
                    </div>
                  </div>

            <div class="box-header">
                  <h3 class="box-title">Look Broadcasted Data</h3>               
            </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-purple-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Look Count</span>
                        <span class="info-box-number">'.$data['total_look_broadcast'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-purple color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['current_look_broadcast'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-purple color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['current_month_look_broadcast'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-purple disabled color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['last_month_look_broadcast'].'</span>
                      </div>
                    </div>
                  </div>

            <div class="box-header">
                  <h3 class="box-title">Look Pending Data</h3>               
            </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-orange-active color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Look Count</span>
                        <span class="info-box-number">'.$data['total_look_pending'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-orange color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['current_look_pending'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-orange color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['current_month_look_pending'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-orange disabled color-palette"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['last_month_look_pending'].'</span>
                      </div>
                    </div>
                  </div>

            <div class="box-header">
                  <h3 class="box-title">Look Rejected Data</h3>               
            </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Look Count</span>
                        <span class="info-box-number">'.$data['total_look_rejected'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['current_look_rejected'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['current_month_look_rejected'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-fw fa-file-image-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['last_month_look_rejected'].'</span>
                      </div>
                    </div>
                  </div>  

      <div class="col-xs-12">
      <table class="table table-bordered table-striped dataTable" id="datatable" style="border:1px;">
        <thead>
          <tr>
            <th>Look Count</th>
            <th>User Count</th>
            <th>Body Shape</th>
            <th>Style</th>           
          </tr>
        </thead>
        <tbody>'.$tblbody.'</tbody></table>
            </div>        
          </div>';          
          }

         echo $reports_data; 
       }
     }
  /*End of Look Report Function */


  /* Brand Report Function */
  public function brandreports()
  {
      if(!$this->session->userdata('user_id')){
        $this->load->view('login');
      }else{ 

        $data = array(); $reports_data = '';
        $data = $this->Home_model->get_brand_data(); 

          //print_r($data);exit;
      if($this->session->userdata('role_id')==1)
      {
       $reports_data = $reports_data.'<div class="row" id="brandreportdiv">
            <div class="box-header">
                  <h3 class="box-title">Brand Data</h3>               
            </div>
                  <!--div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Brand</span>
                        <span class="info-box-number">'.$data['overall_brands'].'</span>
                      </div>
                    </div>
                  </div-->
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['current_date_brand'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['current_month_brand'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['last_month_brand'].'</span>
                      </div>
                    </div>
                  </div>

                <!--<div class="box-header">
                  <h3 class="box-title">Store Data</h3>               
                </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-light-blue-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Store</span>
                        <span class="info-box-number">$data[brand_data][overall_store]</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-light-blue-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">$data[brand_data][current_date_store]</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-light-blue-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">$data[brand_data][current_month_store]</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-light-blue-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">$data[brand_data][last_month_store]</span>
                      </div>
                    </div>
                  </div>-->
              </div>';          
          }

         echo $reports_data; 
       }
     }
  /*End of Brand Report Function */

  /* Click Report Function */
  public function clickreports()
  {
      if(!$this->session->userdata('user_id')){
        $this->load->view('login');
      }else{ 

        $data = array(); $reports_data = '';
       // $data = $this->Home_model->get_brand_data();
        $brand_id = "";
        $data = $this->Home_model->click_report($brand_id); 
        $topfive = $this->Home_model->topfivereports();
        $cpa_track = $this->Home_model->track_cpa();
        //echo "<pre>"; print_r($topfive);exit;
         //echo "<pre>"; print_r($data);exit;

         $tblbody ='';

        if(!empty($topfive)) { $i = 0;
            foreach($topfive as $val)
            {             
              $tblbody=$tblbody.'<tr>
                <td>'.$val['brand_name'].'</td>               
                <td>'.$val['brand_product_click_count'].'</td>
                <td>'.$val['brand_cpc'].'</td>                            
                    </tr>';
                     $i++; } }else{  
                      $tblbody ='<tr>
                        <td colspan="5">Sorry no record found</td>
                      </tr>';
            } 

      if($this->session->userdata('role_id')==1)
      {
       $reports_data = $reports_data.'<div class="row" id="productclickreportdiv">
            <div class="box-header">
                  <h3 class="box-title">Brand Data</h3>               
            </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-teal disabled color-palette"><i class="fa fa-thumbs-o-up"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Product Click</span>
                        <span class="info-box-number">'.$data['brand_product_click_count'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-teal disabled color-palette"><i class="fa fa-rupee"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">CPC</span>
                        <span class="info-box-number">'.$data['brand_cpc'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix visible-sm-block"></div> 
                  <div class="clearfix visible-sm-block"></div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-maroon-active color-palette"><i class="fa fa-thumbs-o-up"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">CPA</span>
                        <span class="info-box-number">'.$cpa_track.'</span>
                      </div>
                    </div>
                  </div>
                   <div class="col-xs-12">
                  <table class="table table-bordered table-striped dataTable" id="datatable" style="border:1px;">
                    <thead>
                      <tr>
                        <th>Brand Name</th>
                        <th>Click Count</th>
                        <th>CPC</th>                          
                      </tr>
                    </thead>
                    <tbody>'.$tblbody.'</tbody></table>
                        </div>        
                      </div>';          
          }

         echo $reports_data; 
       }
     }
  /*End of Brand Report Function */

   /* Top Five Report Function */
  public function topfivereports()
  {
      if(!$this->session->userdata('user_id')){
        $this->load->view('login');
      }else{ 

        $data = array(); $reports_data = '';
       // $data = $this->Home_model->get_brand_data(); 

          //print_r($data);exit;
      if($this->session->userdata('role_id')==1)
      {
       $reports_data = $reports_data.'<div class="row" id="brandreportdiv">
            <div class="box-header">
                  <h3 class="box-title">Brand Data</h3>               
            </div>
                  <!--div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Total Brand</span>
                        <span class="info-box-number">'.$data['overall_brands'].'</span>
                      </div>
                    </div>
                  </div-->
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Today</span>
                        <span class="info-box-number">'.$data['current_date_brand'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Current Month</span>
                        <span class="info-box-number">'.$data['current_month_brand'].'</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-navy-active color-palette"><i class="fa fa-bold"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Last Month</span>
                        <span class="info-box-number">'.$data['last_month_brand'].'</span>
                      </div>
                    </div>
                  </div>            
                  ';          
          }

         echo $reports_data; 
       }
     }
  /*End of Brand Report Function */


  public function dashboard($date_from,$date_to)
  {     
    // $date_from = '';  $date_to = '';
     $data = array();
     $obPrice = 0;
     $obPrdUsedPrice = 0;
     $ubPrice = 0;

     /*if($this->input->post()){
        $date_from = $this->input->post('date_from')!='' ? $this->input->post('date_from') : 0;
        $date_to = $this->input->post('date_to')!='' ? $this->input->post('date_to') : 0;
     }else{
        $date_from =  $this->uri->segment(3);
        $date_to =  $this->uri->segment(4);
     }*/

     $ObBrandCount = count($this->Reports_model->onboardedbrand($date_from,$date_to));
     $ObProductCount = count($this->Reports_model->onboardedproducts($date_from,$date_to));
     $obLookCount = count($this->Reports_model->onboardedproductsUsed($date_from,$date_to));
     $productArray = $this->Reports_model->onboardedproducts($date_from,$date_to);
     $ObProductUsedCount = count($this->Reports_model->onboardedproductsUsed($date_from,$date_to));
     //$ObProductUsedCount = count($this->Reports_model->dashborad_onboarded_brand_product($date_from,$date_to));
     $productUsedArray = $this->Reports_model->onboardedproductsUsed($date_from,$date_to);
     $UnpaidBrandCount = count($this->Reports_model->unpaidbrand($date_from,$date_to));
     $UnpaidBrandPrdCount = count($this->Reports_model->unpaidbrandproducts($date_from,$date_to));
     $UnpaidBrandArray = $this->Reports_model->unpaidbrandproducts($date_from,$date_to);
     $cpcData = $this->Reports_model->userCPCClickData($date_from,$date_to);
     $cpaData = $this->Reports_model->userCPAClickData($date_from,$date_to);
     $AuthenClick = $this->Reports_model->AuthenClickData($date_from,$date_to);
     $UnauthenClick = $this->Reports_model->UnAuthenClickData($date_from,$date_to);

	 // cpc data 
	 $authcpcData = $this->Reports_model->authCPCClickData($date_from,$date_to);
	 $unauthcpcData = $this->Reports_model->unauthCPCClickData($date_from,$date_to);
	 
	  // cpa data
	 $authcpaData = $this->Reports_model->authCPAClickData($date_from,$date_to);
     $unauthcpaData = $this->Reports_model->unauthCPAClickData($date_from,$date_to);


	  
     //echo "<pre>"; print_r($AuthenClick);
     //echo "<br/>";print_r($UnauthenClick);exit;
     foreach($productArray as $val)
     {
        if($val['price']!='')
        {
           $obPrice = $obPrice+$val['price'];
        }     
     }

     foreach($UnpaidBrandArray as $val)
     {
        if($val['price']!='')
        {
           $ubPrice = $ubPrice+$val['price'];
        }     
     }

     foreach($productUsedArray as $val)
     {
        if($val['price']!='')
        {
           $obPrdUsedPrice = $obPrdUsedPrice+$val['price'];
        }     
     }


     $data['ObBrandCount'] = $ObBrandCount;
     $data['ObProductCount'] = $ObProductCount;
     $data['ObProductPrice'] = $this->my_money_format($obPrice);
     $data['ObProductUsedCount'] = $ObProductUsedCount;
     $data['obPrdUsedPrice'] = $this->my_money_format($obPrdUsedPrice);
     $data['UnpaidBrandCount'] = $UnpaidBrandCount;
     $data['UnpaidBrandPrdCount'] = $UnpaidBrandPrdCount;
     $data['UnpaidBrandPrdPrice'] = $this->my_money_format($ubPrice);
     $data['cpcCount'] = $cpcData[0]['count'];
     $data['cpaCount'] = $cpaData[0]['count'];
     $data['cpcvalue'] = $cpcData[0]['value'];
     $data['cpavalue'] = $cpaData[0]['value'];
     $data['authCount'] = $AuthenClick[0]['count'];
     $data['unauthCount'] = $UnauthenClick[0]['count'];
	 
     $totalcpc= $authcpcData[0]['count']+$unauthcpcData[0]['count'];
     $data['totalcpc']=$totalcpc;
     $totalcpcval=$this->my_money_format($authcpcData[0]['value']+$unauthcpcData[0]['value']);
     $data['totalcpcval']= $totalcpcval;



     $totalcpa=$authcpaData[0]['count']+$unauthcpaData[0]['count'];
     $data['totalcpa']=  $totalcpa;

     $totalcpaval=$this->my_money_format($authcpaData[0]['value']+$unauthcpaData[0]['value']);
     $data['totalcpaval']= $totalcpaval;
	 
	// cpc count
	  $data['authcpccount'] = $authcpcData[0]['count'];
      $data['authcpcvalue'] = $this->my_money_format($authcpcData[0]['value']);
	  $data['unauthcpccount'] = $unauthcpcData[0]['count'];
      $data['unauthcpcvalue'] = $this->my_money_format($unauthcpcData[0]['value']);
	
	 
	 // cpa count
	 
	 $data['authcpaCount'] = $authcpaData[0]['count'];
     $data['authcpavalue'] = $this->my_money_format($authcpaData[0]['value']);
	 $data['unauthcpaCount'] = $unauthcpaData[0]['count'];
     $data['unauthcpavalue'] = $this->my_money_format($unauthcpaData[0]['value']);
     return $data;
  }


  public function dashboard_user_info($date_from,$date_to)
  {
    if($this->session->userdata('user_id')){ 

      $date_from = '';  $date_to = '';

       if($this->input->post()){
          $date_from = $this->input->post('date_from');
          $date_to = $this->input->post('date_to');
       }

       $signupdata = $this->Reports_model->get_signup_graphdata($date_from,$date_to);
       
       $facebook =0;
       $google = 0;
       $website = 0;
       $mobile  = 0;
       $mobile_facebook = 0;
       $mobile_google = 0;

       foreach($signupdata as $val)
       {
          if($val['registered_from'] == 'facebook')
          {
             $facebook = $val['count_user'];
          }else if($val['registered_from'] == 'google')
          {
            $google = $val['count_user'];

          }else if($val['registered_from'] == 'website')
          {
            $website = $val['count_user'];

          } else if($val['registered_from'] == 'mobile')
          {
            $mobile = $val['count_user'];

          } else if($val['registered_from'] == 'mobile_facebook')
          {
            $mobile_facebook = $val['count_user'];

          }else if($val['registered_from'] == 'mobile_google')
          {
            $mobile_google = $val['count_user'];
          }  
       }

       /*$website = $this->Reports_model->get_signup_website_info($date_from,$date_to); 
       $facebook = $this->Reports_model->get_signup_facebook_info($date_from,$date_to); 
       $gmail = $this->Reports_model->get_signup_gmail_info($date_from,$date_to); 
       $mobile = $this->Reports_model->get_signup_mobile_info($date_from,$date_to);*/ 
       $data['most_returning_users'] = $this->Reports_model->most_returning_users($date_from,$date_to); 
       $data['most_least_runing'] = $this->Reports_model->most_least_runing($date_from,$date_to); 
       $data['most_sharing'] = $this->Reports_model->most_sharing($date_from,$date_to); 
       $data['most_fav'] = $this->Reports_model->most_fav($date_from,$date_to); 
       $data['most_wardrobe'] = $this->Reports_model->most_wardrobe($date_from,$date_to); 

      /*$data['website'] = $website[0]->count_user;
      $data['facebook'] = $facebook[0]->count_user;
      $data['gmail'] = $gmail[0]->count_user;
      $data['mobile'] = $mobile[0]->count_user;*/

      $data['website'] =  $website;
      $data['facebook'] = $facebook;
      $data['google'] =  $google;
      $data['mobile'] = $mobile;
      $data['mobile_facebook'] = $mobile_facebook;
      $data['mobile_google'] = $mobile_google;
      
      return  $data;
    }else{ 
     
     return '';
    }
  }
}
