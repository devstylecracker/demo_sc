<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ucartreports extends MY_Controller {
	
	function __construct(){
        parent::__construct();
        $this->load->model('Ucartreports_model'); 
       
    }  

	public function index()
	{

      $date_from = $this->input->post('date_from');
      $date_to = $this->input->post('date_to');
      $data['date_from'] = $date_from;
      $data['date_to'] = $date_to;
      if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
      if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }


      $data['product_click_count'] = $this->Ucartreports_model->product_click_count($date_from,$date_to);
      $data['notify_count'] = $this->Ucartreports_model->notify_count($date_from,$date_to);
      $data['cart_count'] = $this->Ucartreports_model->cart_count($date_from,$date_to);
      $data['product_sale_info'] = $this->Ucartreports_model->product_sale_info($date_from,$date_to);
      $data['abendant_cart'] = $this->Ucartreports_model->abendant_cart($date_from,$date_to);
      $data['remove_from_cart'] = $this->Ucartreports_model->remove_from_cart($date_from,$date_to);
      $data['cart_info_platform'] = $this->Ucartreports_model->get_cart_platform_info($date_from,$date_to);

      $platform_count['desktop_window'] = 0;
      $platform_count['web_ios'] = 0;
      $platform_count['mobile_android'] = 0;
      $platform_count['mobile_ios'] = 0;
      $platform_count['web_android'] = 0;
      $platform_count['desktop_mac']=0;
      $platform_count['desktop_linux'] = 0;
      $platform_count['total'] = 0;
      $platform_count['others'] = 0;
      //------Data filtered of platform count
      foreach($data['cart_info_platform'] as $row){
        if(substr($row['platform_info'], 0, 7) == 'Windows'){
          $platform_count['desktop_window'] += $row['count'];
        }else if($row['platform_info'] == 'iOS'){
          $platform_count['web_ios'] = $row['count'];
        }else if($row['platform_info'] == 'mobile_android'){
          $platform_count['mobile_android'] = $row['count'];
        }else if($row['platform_info'] == 'mobile_ios'){
          $platform_count['mobile_ios'] = $row['count'];
        }else if($row['platform_info'] == 'Android'){
          $platform_count['web_android'] = $row['count'];
        }else if($row['platform_info'] == 'Linux'){
          $platform_count['desktop_linux'] = $row['count'];
        }else if($row['platform_info'] == 'Mac OS X'){
          $platform_count['desktop_mac'] = $row['count'];
        }else{
          $platform_count['others'] += $row['count'];
        }
        $platform_count['total'] += $row['count'];
      }
      $data['platform_count'] = $platform_count;
      $data['cart_info_platform'] = '[
      {value: '.$platform_count['mobile_android'].',color: "#990099",highlight: "#990099",label: "App Android"},
      {value: '.$platform_count['mobile_ios'].',color: "#dd4b39",highlight: "#dd4b39",label: "App iOS"},
      {value: '.$platform_count['desktop_window'].',color: "#f39c12",highlight: "#f39c12",label: "Desktop Window"},
      {value: '.$platform_count['desktop_linux'].',color: "#3266CC",highlight: "#3266CC",label: "Desktop Linux"},
      {value: '.$platform_count['desktop_mac'].',color: "#3C8DBC",highlight: "#3C8DBC",label: "Desktop Mac"},
      {value: '.$platform_count['web_ios'].',color: "#00a65a",highlight: "#00a65a",label: "Web iOS"},
      {value: '.$platform_count['web_android'].',color: "#00c0ef",highlight: "#00c0ef",label: "Web Android"},
      {value: '.$platform_count['others'].',color: "#000000",highlight: "#000000",label: "Others"}]';
     //------Data filtered of platform count
     // print_r($platform_count);exit;
     
      //echo $data['remove_from_cart'];exit;
      $data['click'] = '[{value: '.$data['product_click_count'].',color: "#1E90FF",highlight: "#1E90FF",label: "Product Click"},{value: '.$data['notify_count'].',color: "#00a65a",highlight: "#00a65a",label: "Notify me"},{value: '.$data['cart_count'].',color: "#f39c12",highlight: "#f39c12",label: "Cart"}]';

      $data['cart_info'] = '[{value: '.$data['product_sale_info'].',color: "#1E90FF",highlight: "#1E90FF",label: "Product sales"},
      {value: '.$data['abendant_cart'].',color: "#00a65a",highlight: "#00a65a",label: "Abandoned Cart"},
      {value: '.$data['remove_from_cart'].',color: "#f39c12",highlight: "#f39c12",label: "Products removed from cart"}]';

      $data['cart_platform_info'] = '[{value: '.$data['product_sale_info'].',color: "#1E90FF",highlight: "#1E90FF",label: "Product sales"},
      {value: '.$data['abendant_cart'].',color: "#00a65a",highlight: "#00a65a",label: "Abandoned Cart"},
      {value: '.$data['remove_from_cart'].',color: "#1E90FF",highlight: "#1E90FF",label: "Products removed from cart"}]';

      $this->load->view('common/header');
      $this->load->view('reports/Ucartreports',$data);
      $this->load->view('common/footer');
	}

  public function product_click(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'product_click';
    $data['page_title'] = 'Product Click ( via Buy Now)'.$date_from.' - '.$date_to;
    $data['product_click_list'] = $this->Ucartreports_model->product_click_list($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('reports/ucartreports_details',$data);
    $this->load->view('common/footer');
  }


  public function notify_me(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'notify_me';
    $data['page_title'] = 'Notify me'.$date_from.' - '.$date_to;
    $data['product_click_list'] = $this->Ucartreports_model->notify_count_list($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('reports/ucartreports_details',$data);
    $this->load->view('common/footer');
  }

  public function cart_count(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'cart_click';
    $data['page_title'] = 'Notify me'.$date_from.' - '.$date_to;
    $data['product_click_list'] = $this->Ucartreports_model->cart_count_list($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('reports/ucartreports_details',$data);
    $this->load->view('common/footer');
  }

   public function abandoned_cart(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'abendant_cart';
    $data['page_title'] = 'Abandoned Cart '.$date_from.' - '.$date_to;
    $data['product_click_list'] = $this->Ucartreports_model->abendant_cart_list($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('reports/ucartreports_details',$data);
    $this->load->view('common/footer');
  }

     public function product_sales(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'product_sales';
    $data['page_title'] = 'Product Sale '.$date_from.' - '.$date_to;
    $data['product_click_list'] = $this->Ucartreports_model->product_sale_info_list($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('reports/ucartreports_details',$data);
    $this->load->view('common/footer');
  }
public function remove_from_cart(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'remove_from_cart';
    $data['page_title'] = 'remove_from_cart'.$date_from.' - '.$date_to;
    $data['product_click_list'] = $this->Ucartreports_model->remove_from_cart_list_data($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('reports/ucartreports_details',$data);
    $this->load->view('common/footer');
  }

  public function platform_info_details(){
    $data = array();

    $filter = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_from = $this->uri->segment(4) ? $this->uri->segment(4) : '';
    $date_to = $this->uri->segment(5) ? $this->uri->segment(5) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'remove_from_cart';
    $data['page_title'] = $filter.' platform details '.$date_from.' - '.$date_to;//'remove_from_cart'.$date_from.' - '.$date_to;
    $data['product_click_list'] = $this->Ucartreports_model->get_cart_platform_info_data($date_from,$date_to,$filter);

    $this->load->view('common/header');
    $this->load->view('reports/platform_info_details',$data);
    $this->load->view('common/footer');
  }






}
?>