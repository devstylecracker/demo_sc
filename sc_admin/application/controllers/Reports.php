<?php
ini_set('memory_limit', '-1');
/*
*
  This controller is basically for the dashboard report  - User Related All the Dashboard functions 
  are return in this controller
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {
	
	function __construct(){
        parent::__construct(); 
        $this->load->model('reports_model'); 
        $this->load->model('lookcreator_model');
        $this->load->model('websiteusers_model');
        $this->load->model('product_model');    
    }  

	public function user_info()
	{
		if($this->session->userdata('user_id')){ 

      $date_from = '';  $date_to = '';

       if($this->input->post()){
          $date_from = $this->input->post('date_from');
          $date_to = $this->input->post('date_to');
       }


       $signupdata = $this->reports_model->get_signup_graphdata($date_from,$date_to);
       
       $facebook =0;
       $google = 0;
       $website = 0;
       $mobile  = 0;
       $mobile_facebook = 0;
       $mobile_google = 0;

       foreach($signupdata as $val)
       {
          if($val['registered_from'] == 'facebook')
          {
             $facebook = $val['count_user'];
          }else if($val['registered_from'] == 'google')
          {
            $google = $val['count_user'];

          }else if($val['registered_from'] == 'website')
          {
            $website = $val['count_user'];

          } else if($val['registered_from'] == 'mobile')
          {
            $mobile = $val['count_user'];

          } else if($val['registered_from'] == 'mobile_facebook')
          {
            $mobile_facebook = $val['count_user'];

          }else if($val['registered_from'] == 'mobile_google')
          {
            $mobile_google = $val['count_user'];
          }  
       }

      /* $website = $this->reports_model->get_signup_website_info($date_from,$date_to); 
       $facebook = $this->reports_model->get_signup_facebook_info($date_from,$date_to); 
       $gmail = $this->reports_model->get_signup_gmail_info($date_from,$date_to); 
       $mobile = $this->reports_model->get_signup_mobile_info($date_from,$date_to);*/ 
       $data['most_returning_users'] = $this->reports_model->most_returning_users($date_from,$date_to); 
       $data['most_least_runing'] = $this->reports_model->most_least_runing($date_from,$date_to); 
       $data['most_sharing'] = $this->reports_model->most_sharing($date_from,$date_to); 
       $data['most_fav'] = $this->reports_model->most_fav($date_from,$date_to); 
       $data['most_wardrobe'] = $this->reports_model->most_wardrobe($date_from,$date_to);


      $data['signup'] = '[{value: '.$website.',color: "#1E90FF",highlight: "#1E90FF",label: "Website"},{value: '.$facebook.',color: "#00a65a",highlight: "#00a65a",label: "Facebook"},{value: '.$google.',color: "#f39c12",
                highlight: "#f39c12",label: "Gmail"},{value: '.$mobile.',color: "#00c0ef",highlight: "#00c0ef",label: "Mobile"},{value: '.$mobile_facebook.',color: "#dd4b39",highlight: "#dd4b39",label: "Mobile Facebook"},{value: '.$mobile_google.',color: "#605ca8",highlight: "#605ca8",label: "Mobile Google"}]';

      $data['website'] = $website;
      $data['facebook'] = $facebook;
      $data['google'] = $google;
      $data['mobile'] = $mobile;
      $data['mobile_facebook'] = $mobile_facebook;
      $data['mobile_google'] = $mobile_google;

      $this->load->view('common/header');
      $this->load->view('reports/user_info',$data);
      $this->load->view('common/footer');
      
		}else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
		}
	}

  public function get_users_info(){
      if($this->session->userdata('user_id'))
      { 
         
      }
  }  

  

   public function brand_product_info($datefrom,$dateto)
  {   
    if($this->session->userdata('user_id'))
    {  
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }else if($datefrom!='' || $dateto!='')
        {
          $date_from = $datefrom;
           $date_to = $dateto;
        }
        $brand = $this->reports_model->get_brand_onboard($date_from,$date_to);     
        $data['brand'] = $brand;
      $this->load->view('common/header');
      $this->load->view('reports/brand_info',$data);
      $this->load->view('common/footer');
      
    }else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }

  public function brandClickdetails($type,$brandid)
  {
    if($this->session->userdata('user_id'))
    {  
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }

        if($type=='look')
        {
          $brand = $this->reports_model->brand_look_products($brandid,$date_from,$date_to);
          
          $data['type'] = 'Look';
        }elseif($type=='product')
        {
          
          $brand = $this->reports_model->brand_products($brandid,$date_from,$date_to);
           $data['type'] = 'Product';
        }       
       
        $data['user_click'] = $brand;      

      $this->load->view('common/header');
      $this->load->view('reports/brand_products',$data);
      $this->load->view('common/footer');
      
    }else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }

    public function top_5(){
    if($this->session->userdata('user_id')){ 

        $data = array();

        $date_from = '';  $date_to = '';

        if($this->input->post()){
          $date_from = $this->input->post('date_from');
          $date_to = $this->input->post('date_to');
        }

        $data['top_5_product_cpc'] = $this->reports_model->top_5_product_cpc($date_from,$date_to);
        $data['top_5_look_cpc'] = $this->reports_model->top_5_look_cpc($date_from,$date_to);
        $data['top_5_product_cpa'] = $this->reports_model->top_5_product_cpa($date_from,$date_to);
        $data['top_5_brands_cpc'] = $this->reports_model->top_5_brands_cpc($date_from,$date_to);
        $data['top_5_buyers'] = $this->reports_model->top_5_buyers_cpa($date_from,$date_to);
        
        $this->load->view('common/header');
        $this->load->view('reports/top_5',$data);
        $this->load->view('common/footer');

    }
  }


  public function get_all_cpa_users(){
    if($this->session->userdata('user_id')){ 
  
   $data['top_5_buyers'] = $this->reports_model->all_buyers_cpa();
   $data['total_rows'] = count($this->reports_model->all_buyers_cpa());  
    $this->load->view('common/header');
      $this->load->view('reports/alluserscpa_info',$data);
      $this->load->view('common/footer');

  }
  }
  
   public function product_cpa_detail(){
    
   $user_id =  $this->uri->segment(3);
   $data['top_5_buyers'] = $this->reports_model->product_cpa_details($user_id);
   $data['total_rows'] = count($this->reports_model->product_cpa_details($user_id));    
    $this->load->view('common/header');
      $this->load->view('reports/product_cpa_view',$data);
      $this->load->view('common/footer');


  }

  public function top_5details($datefrom,$dateto)
  {
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }else if($datefrom!='' || $dateto!='')
        {
          $date_from = $datefrom;
          $date_to = $dateto;
        }
      
      $offset = $this->uri->segment(6);
      $config['base_url'] = base_url().'reports/top_5details/'.$date_from.'/'.$date_to.'/index/';
      $config['total_rows'] = count($this->reports_model->top_5_product_cpc_all($date_from,$date_to,'',''));
      $config['per_page'] = 20;
      $config['uri_segment'] = 6;
      $config['use_page_numbers'] = TRUE;
      $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
      $config['full_tag_close'] = '</ul></div>';

      $config['first_link'] = '&laquo; First';
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';

      $config['last_link'] = 'Last &raquo;';
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';

      $config['next_link'] = 'Next &rarr;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';

      $config['prev_link'] = '&larr; Previous';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';

      $config['cur_tag_open'] = '<li><a href="">';
      $config['cur_tag_close'] = '</a></li>';

      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      
      $this->pagination->initialize($config);
   
      $data1['top_5_product_cpc'] = $this->reports_model->top_5_product_cpc_all($date_from,$date_to,$offset,$config['per_page']);         
      $data1['total_rows'] =  $config['total_rows'];
      $this->load->view('common/header');
      $this->load->view('reports/top5_details',$data1);
      $this->load->view('common/footer');
      
  }


  public function look_details($datefrom,$dateto)
  {
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }else if($datefrom!='' || $dateto!='')
        {
          $date_from = $this->uri->segment(3);
          $date_to = $this->uri->segment(4);
        }
      
      $offset = $this->uri->segment(6);
      $config['base_url'] = base_url().'reports/look_details/'.$date_from.'/'.$date_to.'/index/';
      $config['total_rows'] = count($this->reports_model->top_5_look_cpc_all($date_from,$date_to,'',''));
      $config['per_page'] = 20;
      $config['uri_segment'] = 6;
      $config['use_page_numbers'] = TRUE;
      $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
      $config['full_tag_close'] = '</ul></div>';

      $config['first_link'] = '&laquo; First';
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';

      $config['last_link'] = 'Last &raquo;';
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';

      $config['next_link'] = 'Next &rarr;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';

      $config['prev_link'] = '&larr; Previous';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';

      $config['cur_tag_open'] = '<li><a href="">';
      $config['cur_tag_close'] = '</a></li>';

      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';

      
      $this->pagination->initialize($config);      

       $data1['top_5_look_cpc'] = $this->reports_model->top_5_look_cpc_all($date_from,$date_to,$offset,$config['per_page']);         
       $data1['total_rows'] = $config['total_rows']; 
      $this->load->view('common/header');
      $this->load->view('reports/look_click_details',$data1);
      $this->load->view('common/footer');
      
  }


  public function top5brandclick_details($datefrom,$dateto)
  {
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }else if($datefrom!='' || $dateto!='')
        {
          $date_from = $this->uri->segment(3);
          $date_to = $this->uri->segment(4);
        }
      
      $offset = $this->uri->segment(6);
      $config['base_url'] = base_url().'reports/top5brandclick_details/'.$date_from.'/'.$date_to.'/index/';
      $config['total_rows'] = count($this->reports_model->top_5_brand_click_all($date_from,$date_to));
      $config['per_page'] = 20;
      $config['uri_segment'] = 6;
      $config['use_page_numbers'] = TRUE;
      $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
      $config['full_tag_close'] = '</ul></div>';

      $config['first_link'] = '&laquo; First';
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';

      $config['last_link'] = 'Last &raquo;';
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';

      $config['next_link'] = 'Next &rarr;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';

      $config['prev_link'] = '&larr; Previous';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';

      $config['cur_tag_open'] = '<li><a href="">';
      $config['cur_tag_close'] = '</a></li>';

      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';

      
      $this->pagination->initialize($config);      

       $data1['top_5_brands_cpc'] = $this->reports_model->top_5_brand_click_all($date_from,$date_to,$offset,$config['per_page']);         
       $data1['total_rows'] = $config['total_rows']; 
      $this->load->view('common/header');
      $this->load->view('reports/brand_click_details',$data1);
      $this->load->view('common/footer');
      
  }

 public function top_5cpa($type,$datefrom,$dateto)
  {   

      if($this->session->userdata('user_id'))
      {
        $data1 = array();
        $date_from = '';  $date_to = '';

           if($this->input->post())
           {
              $date_from = $this->input->post('date_from');
              $date_to = $this->input->post('date_to');

           }
           else if($datefrom!='' || $dateto='')
          {
            $date_from = $datefrom;
            $date_to = $dateto;
          }

          $type = $this->uri->segment(3);

          if($type == 'Authenticated_CPC')
          {
             $data1['top_5_product_cpc'] = $this->reports_model->top_5_product_cpc_details($type,$date_from,$date_to); 
          }
          else if($type == 'Authenticated_CPA')
          {
             $data1['top_5_product_cpc'] = $this->reports_model->top_5_product_cpc_details($type,$date_from,$date_to); 
          }
          else if($type == 'Unauthenticated_CPC')
          {
               $data1['top_5_product_cpc'] = $this->reports_model->top_5_product_cpc_details($type,$date_from,$date_to); 
          }
          else if($type == 'Unauthenticated_CPA')
          {
             $data1['top_5_product_cpc'] = $this->reports_model->top_5_product_cpc_details($type,$date_from,$date_to); 
          }
        $data1['total_count'] = count($data1['top_5_product_cpc']);
        $this->load->view('common/header');
        $this->load->view('reports/top_5detailscpc',$data1);
        $this->load->view('common/footer');   
  
      }
  }


 public function product_cpa_details($datefrom, $dateto)
  {
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');

        }else if($datefrom!='' || $dateto!='')
        {
          $date_from = $datefrom;
           $date_to = $dateto;
        }
      
      $offset = $this->uri->segment(6);
      $config['base_url'] = base_url().'reports/product_cpa_details/'.$date_from.'/'.$date_to.'/index';
      $config['total_rows'] = count($this->reports_model->top_5_product_cpa_all($date_from,$date_to,'',''));
      $config['per_page'] = 20;
      $config['uri_segment'] = 3;
      $config['use_page_numbers'] = TRUE;
      $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
      $config['full_tag_close'] = '</ul></div>';

      $config['first_link'] = '&laquo; First';
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';

      $config['last_link'] = 'Last &raquo;';
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';

      $config['next_link'] = 'Next &rarr;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';

      $config['prev_link'] = '&larr; Previous';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';

      $config['cur_tag_open'] = '<li><a href="">';
      $config['cur_tag_close'] = '</a></li>';

      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';

      
      $this->pagination->initialize($config);     

         $data1['top_5_product_cpa'] = $this->reports_model->top_5_product_cpa_all($date_from,$date_to,$offset,$config['per_page']);         
          $data1['total_rows'] =  $config['total_rows'];

        $this->load->view('common/header');
        $this->load->view('reports/product_cpa_details',$data1);
        $this->load->view('common/footer');
      
  }

  // look_info 
  public function look_info($offset=null)
  {   
    if($this->session->userdata('user_id'))
    {  
      $date_from = '';  $date_to = '';
       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }
          
        /* Pagination Code start*/
        $config['base_url'] = base_url().'reports/look_info/';
        $config['total_rows'] = count($this->reports_model->get_look_info($date_from,$date_to,'',''));
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul></div>';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        
        $this->pagination->initialize($config);
    
    /* Pagination code End*/
      
      
      
      
        $look = $this->reports_model->get_look_info($date_from,$date_to,$offset,$config['per_page']); 
    // echo "<pre>";
    // print_r($look);
    // exit;
        $data['look'] = $look;
         $data['total_rows'] = $config['total_rows'];      

      $this->load->view('common/header');
      $this->load->view('reports/look_info',$data);
      $this->load->view('common/footer');
      
    }else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }

  
  //look_details
  public function look_details2($offset=null)
  {   
    if($this->session->userdata('user_id'))
    {  
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }
          
      /* Pagination Code start*/
        $config['base_url'] = base_url().'reports/look_details2/';
        $config['total_rows'] = count($this->reports_model->get_look_details($date_from,$date_to,'',''));
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul></div>';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        
        $this->pagination->initialize($config);
    
    /* Pagination code End*/


      
        $look = $this->reports_model->get_look_details($date_from,$date_to,'',''); 
    // echo "<pre>";
    // print_r($look);
    // exit;
        $data['look'] = $look;      
        $data['total_rows'] = $config['total_rows']; 
      $this->load->view('common/header');
      $this->load->view('reports/look_details',$data);
      $this->load->view('common/footer');
      
    }else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }
  
  
  public function look_view(){
    if($this->has_rights(52) == 1){
       $id = $this->uri->segment(3);  
       // echo $id;
       // echo "coming";
       // exit;
       $data['look_data'] =  $this->lookcreator_model->get_look_info($id);  
       $data['product_data'] = $this->lookcreator_model->get_looks_product($id); 
       $data['bucket_data'] = $this->reports_model->look_bucket_details($id);
       $data['user_count'] = $this->reports_model->look_user_count($id);
       if($data['look_data'][0]['looks_for'] == 1){
         $data['budget'] = $this->product_model->load_questAns(7);
       }else{
         $data['budget'] = $this->product_model->load_questAns(18);
       }
       $this->load->view('common/header',$data);
             $this->load->view('reports/look_view');
             $this->load->view('common/footer');

    }else{
      $this->load->view('common/header');
            $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }
  
  public function user_details($type,$datefrom,$dateto,$offset=null)
  {
    if(!$this->session->userdata('user_id')){
      $this->load->view('login');
    }else{ 
      $data = array();
      
      if($this->has_rights(15) == 1){
         $date_from = '';  $date_to = '';


         if($this->input->post()){
             $date_from = $this->input->post('date_from');
             $date_to = $this->input->post('date_to');

          }else if($datefrom!='' || $dateto='')
          {
            $date_from = $datefrom;
            $date_to = $dateto;
          }

          $type = $this->uri->segment(3);
        
          $config['base_url'] = base_url().'reports/user_details/'.$type.'/'.$date_from.'/'.$date_to;          
          $config['per_page'] = 20;
          $config['uri_segment'] = 6;
          $config['use_page_numbers'] = TRUE;
   

        if($type == 'website')
        {
          //$users_data = $this->reports_model->get_signup_website_details($date_from,$date_to,$offset,$config['per_page']); 
          $users_data = $this->reports_model->get_signup_graphDetails($type,$date_from,$date_to,$offset,$config['per_page']); 
          $config['total_rows'] = count($users_data['data']);
          $data['type']='Website User';

        }else if($type == 'facebook')
        {
          //$users_data = $this->reports_model->get_signup_facebook_details($date_from,$date_to,$offset,$config['per_page']); 
          $users_data = $this->reports_model->get_signup_graphDetails($type,$date_from,$date_to,$offset,$config['per_page']);
          $config['total_rows'] = count($users_data['data']);
          $data['type']='Facebook User';

        }else if($type == 'google')
        {
          // $users_data =$this->reports_model->get_signup_gmail_details($date_from,$date_to,$offset,$config['per_page']);  
          $users_data = $this->reports_model->get_signup_graphDetails($type,$date_from,$date_to,$offset,$config['per_page']); 
          $config['total_rows'] = count($users_data['data']);
          $data['type']='Google User';

        }else if($type == 'mobile')
        {
          //$users_data = $this->reports_model->get_signup_mobile_details($date_from,$date_to,$offset,$config['per_page']);   
          $users_data = $this->reports_model->get_signup_graphDetails($type,$date_from,$date_to,$offset,$config['per_page']);
          $config['total_rows'] = count($users_data['data']);
          $data['type']='Mobile User';

        }else if($type == 'blog')
        {           
          $users_data['data'] = "";          
          $config['total_rows'] = 0;
          $data['type']='Blog User';

        }else if($type == 'mobile_facebook')
        {           
          $users_data = $this->reports_model->get_signup_graphDetails($type,$date_from,$date_to,$offset,$config['per_page']);        
         
          $config['total_rows'] = count($users_data['data']);
          $data['type']='Mobile Facebook User';

        }else if($type == 'mobile_google')
        {           
          $users_data = $this->reports_model->get_signup_graphDetails($type,$date_from,$date_to,$offset,$config['per_page']);        
          $config['total_rows'] = count($users_data['data']);
          $data['type']='Mobile Google User';
        }

          $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
          $config['full_tag_close'] = '</ul></div>';

          $config['first_link'] = '&laquo; First';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';

          $config['last_link'] = 'Last &raquo;';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';

          $config['next_link'] = 'Next &rarr;';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';

          $config['prev_link'] = '&larr; Previous';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';

         $config['cur_tag_open'] = '<li><a href="" style="background-color:#7CCDEF;">';
         $config['cur_tag_close'] = '</a></li>';


          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $data['users_data'] = $users_data;
          $data['total_rows'] = $config['total_rows'];
          $this->pagination->initialize($config);         

        
        $this->load->view('common/header',$data);
        $this->load->view('reports/dashboard_detail_view');
        $this->load->view('common/footer');
      }else{
        $this->load->view('common/header',$data);
        $this->load->view('not_permission');
        $this->load->view('common/footer');
      }
    } 
}

/* Report Brand Click details  */
  public function brand_details($offset=null,$datefrom,$dateto)
  {
    if(!$this->session->userdata('user_id')){
      $this->load->view('login');
    }else{ 
      $data = array();
      
      if($this->has_rights(15) == 1){

         $date_from = '';  $date_to = '';

         if($this->input->post()){
             $date_from = $this->input->post('date_from');
             $date_to = $this->input->post('date_to');

          }else if($datefrom!='' || $dateto!='')
          {
            $date_from = $datefrom;
            $date_to = $dateto;
          }

          $type = $this->uri->segment(3);
          $brand_id = $this->uri->segment(4);

          $config['base_url'] = base_url().'reports/brand_details/'.$type.'/'.$datefrom.'/'.$dateto;
          
         // $config['per_page'] = 20;
          $config['per_page'] = '';
          $config['uri_segment'] = 4;
          $config['use_page_numbers'] = TRUE;



        if($type == 'Product_Uploaded')
        {
          $brand_data = $this->reports_model->product_used_count_till_today_detail($brand_id,$date_from,$date_to); 
          $config['total_rows'] = count($brand_data['data']);          

        }else if($type == 'Product_Used')
        {
          $brand_data = $this->reports_model->product_look_used_count_till_today_details($brand_id); 
          $config['total_rows'] = count($brand_data['data']);

        }else if($type == 'Product_Uploaded_Today')
        {
           $brand_data =$this->reports_model->product_used_count_details($type,$brand_id,date('Y-m-d'),'');  
           $config['total_rows'] = count($brand_data['data']);

        }else if($type == 'Product_Used_Today')
        {
            $brand_data = $this->reports_model->product_look_used_details($brand_id,date('Y-m-d'),'');   
            $config['total_rows'] = count($brand_data['data']);

        }else if($type == 'Product_CPC')
        {
            $brand_data = $this->reports_model->product_cpc_count_today_details($brand_id,date('Y-m-d'),'');   
            $config['total_rows'] = count($brand_data['data']);

        }else if($type == 'Product_CPA')
        {
            $brand_data = $this->reports_model->product_cpa_count_today_details($brand_id,date('Y-m-d'),'');   
            $config['total_rows'] = count($brand_data['data']);

        }else if($type == 'Product_Approved')
        {
            $brand_data = $this->reports_model->product_approve_details($brand_id);   
            $config['total_rows'] = count($brand_data['data']);

        }else if($type == 'Product_Reject')
        {
            $brand_data = $this->reports_model->product_reject_details($brand_id);   
            $config['total_rows'] = count($brand_data['data']);

        }else if($type == 'Product_Pending')
        {
            $brand_data = $this->reports_model->product_pending_details($brand_id);   
            $config['total_rows'] = count($brand_data['data']);

        }

          $config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
          $config['full_tag_close'] = '</ul></div>';

          $config['first_link'] = '&laquo; First';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';

          $config['last_link'] = 'Last &raquo;';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';

          $config['next_link'] = 'Next &rarr;';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';

          $config['prev_link'] = '&larr; Previous';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';

          $config['cur_tag_open'] = '<li><a href="">';
          $config['cur_tag_close'] = '</a></li>';

          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $data['brand_data'] = $brand_data;
          $data['total_rows'] = $config['total_rows'];
          $this->pagination->initialize($config);         

        
        $this->load->view('common/header',$data);
        $this->load->view('reports/brand_details_view');
        $this->load->view('common/footer');
      }else{
        $this->load->view('common/header',$data);
        $this->load->view('not_permission');
        $this->load->view('common/footer');
      }
    }

  }



  public function onboarded_brand_product_uploaded($datefrom,$dateto)
  {   
    if($this->session->userdata('user_id'))
    {  
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }else if($datefrom!='' && $dateto!=''){
           $date_from = $this->uri->segment(3);
           $date_to =  $this->uri->segment(4);
        }
        $brand = $this->reports_model->brand_product_uploaded($date_from,$date_to);   
        $data['product_uploaded'] = $brand;
        $data['uploaded_count'] = count($data['product_uploaded']);
        //echo $data_count;
        //exit;
      $this->load->view('common/header');
      $this->load->view('reports/onboarded_brand_product',$data);
      $this->load->view('common/footer');
      
    }else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }


  public function onboarded_brand_product_used($datefrom,$dateto)
  {   
    if($this->session->userdata('user_id'))
    {  
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }else if($datefrom!='' && $dateto!=''){
           $date_from = $this->uri->segment(3);
           $date_to =  $this->uri->segment(4);
        }
        $brand = $this->reports_model->dashborad_onboarded_brand_product($date_from,$date_to);   
        //$brand = $this->reports_model->onboardedproductsUsed($date_from,$date_to);  
        $data['brand_product_used'] = $brand;
        $data['brand_product_used_count'] = count($data['brand_product_used']);
        $this->load->view('common/header');
        $this->load->view('reports/brand_product_used',$data);
        $this->load->view('common/footer');      
    }else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }

  public function un_paid_brand_count($datefrom,$dateto)
  {   
    if($this->session->userdata('user_id'))
    {  
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }else if($datefrom!='' && $dateto!=''){
           $date_from = $this->uri->segment(3);
           $date_to =  $this->uri->segment(4);
        }
        $brand = $this->reports_model->dashborad_paid_brand_count($date_from,$date_to);     
        $data['paid_brand_count'] = $brand;
        $data['paid_brand_brand_count'] = count($data['paid_brand_count']);
        $this->load->view('common/header');
        $this->load->view('reports/paid_brand_count',$data);
        $this->load->view('common/footer');      
    }else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }

  public function unpaid_brand_prd_count($datefrom,$dateto)
  {   
    if($this->session->userdata('user_id'))
    {  
      $date_from = '';  $date_to = '';

       if($this->input->post()){
           $date_from = $this->input->post('date_from');
           $date_to = $this->input->post('date_to');
        }else if($datefrom!='' && $dateto!=''){
           $date_from = $this->uri->segment(3);
           $date_to =  $this->uri->segment(4);
        }

        $brand = $this->reports_model->dashborad_brand_prd_count($date_from,$date_to);     
        $data['unpaid_brand'] = $brand;
        $data['unpaid_brand_count'] = count($data['unpaid_brand']);
      $this->load->view('common/header');
      $this->load->view('reports/brand_prd_count',$data);
      $this->load->view('common/footer');
      
    }else{ 
      $this->load->view('common/header',$data);
      $this->load->view('not_permission');
      $this->load->view('common/footer');
    }
  }
  /* End Report Brand Click details */

  /* Authenticate Unathen*/
  public function user_auth_unath_click($type,$datefrom,$dateto)
  { 
       
    if(!$this->session->userdata('user_id')){        

      $this->load->view('login');
    }else{ 
      $data = array();      
      if($this->has_rights(15) == 1){        
        $date_from = '';  $date_to = '';
        
         if($this->input->post()){
             $date_from = $this->input->post('date_from');            
             $date_to = $this->input->post('date_to');
          }else if($datefrom!='' || $dateto='')
          {
            $date_from = $datefrom;
            $date_to = $dateto;
          }

          $type = $this->uri->segment(3);
       

        if($type == 'Authenticated')
        {
          $users_data = $this->reports_model->authCPCClickData_details($type,$date_from,$date_to);        

        }else if($type == 'UnAuthenticated')
        {
           $users_data = $this->reports_model->authCPCClickData_details($type,$date_from,$date_to);
          
        }
        $data['type'] = $type;
        $data['count_product'] = count($users_data);
        $data['top_5_product_cpc'] = $users_data;           
        
        $this->load->view('common/header',$data);
        $this->load->view('reports/user_clickdetail_view');
        $this->load->view('common/footer');
      }else{
        $this->load->view('common/header',$data);
        $this->load->view('not_permission');
        $this->load->view('common/footer');
      }
    } 
  }

}

?>