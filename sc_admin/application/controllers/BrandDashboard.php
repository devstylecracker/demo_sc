<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BrandDashboard extends MY_Controller {
	
	function __construct(){
        parent::__construct();
       $this->load->model('brandDashboard_model');      

    }  

	public function index()
	{
    $data = array();

    $date_from = $this->input->post('date_from');
    $date_to = $this->input->post('date_to');
	$brand_id = $this->session->userdata('user_id');
	
    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['basic_details'] = $this->brandDashboard_model->basic_details($date_from,$date_to);
	$data['brand_info'] =  $this->brandDashboard_model->get_brand_info($brand_id);
    //echo "<pre>"; print_r($data['basic_details']['total_orders_info']);

    $processing = @$data['basic_details']['total_orders_info_pending'] ? $data['basic_details']['total_orders_info_pending'] : 0;
    $confirmed = @$data['basic_details']['total_orders_info_confirmed'] ? $data['basic_details']['total_orders_info_confirmed'] : 0;
    $dispatched = @$data['basic_details']['total_orders_info_dispatched'] ? $data['basic_details']['total_orders_info_dispatched'] : 0;
    $delivered = @$data['basic_details']['total_orders_info_delivered'] ? $data['basic_details']['total_orders_info_delivered'] : 0;
    $cancelled = @$data['basic_details']['total_orders_info_cancelled'] ? $data['basic_details']['total_orders_info_cancelled'] : 0;
    $returned = @$data['basic_details']['total_orders_info_returned'] ? $data['basic_details']['total_orders_info_returned'] : 0;

    $data['processing'] = $processing;
    $data['confirmed'] = $confirmed;
    $data['dispatched'] = $dispatched;
    $data['delivered'] = $delivered;
    $data['cancelled'] = $cancelled;
    $data['returned'] = $returned;

    $data['clicks'] ='[{value: '.$processing.',color: "#f39c12",highlight: "#f39c12",label: "Processing"},{value: '.$confirmed.',color: "#00a65a",highlight: "#00a65a",label: "Confirmed"},{value: '.$dispatched.',color: "#00c0ef",highlight: "#00c0ef",label: "Dispatched"},{value: '.$delivered.',color: "#990099",highlight: "#990099",label: "Delivered"},{value: '.$cancelled.',color: "#dd4b39",highlight: "#dd4b39",label: "Cancelled"},{value: '.$returned.',color: "#3266CC",highlight: "#3266CC",label: "Returned"}]';

		if(@$data['brand_info'][0]['is_form_completed'] == 1 && @$data['brand_info'][0]['is_verify'] == 1){
			$this->load->view('common/header');
			$this->load->view('brandDashboard/brandhome',$data);
			$this->load->view('common/footer');	
		}else if(@$data['brand_info'][0]['is_form_completed'] == 1 && @$data['brand_info'][0]['is_verify'] != 1){
			redirect('brand_onboarding/Store_registration/brand_step6');
		}else if(@$data['brand_info'][0]['step_completed'] == 5){
			redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'');
		}else if($data['brand_info'][0]['step_completed'] > 0 && $data['brand_info'][0]['step_completed'] < 5){
			redirect('brand_onboarding/Store_registration/brand_step'.$data['brand_info'][0]['step_completed'].'');
		}else{
            session_destroy();
            redirect('login');
        }
	}

  public function total_sales(){

    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'total_sales';
    $data['page_title'] = 'List of Total Sales '.$date_from.' - '.$date_to;
    $data['total_sales_details'] = $this->brandDashboard_model->total_sales_details($date_from,$date_to);
    $data['total_rows'] = count($data['total_sales_details']);
    $this->load->view('common/header');
    $this->load->view('brandDashboard/brand_report_details',$data);
    $this->load->view('common/footer');

  }



  public function products(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'products';
    $data['page_title'] = 'List of Products '.$date_from.' - '.$date_to;
    $data['products'] = $this->brandDashboard_model->view_products($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('brandDashboard/brand_report_details',$data);
    $this->load->view('common/footer');
  }

  public function approved_products(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'approved_products';
    $data['page_title'] = 'List of Approved Products '.$date_from.' - '.$date_to;
    $data['products'] = $this->brandDashboard_model->approved_products($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('brandDashboard/brand_report_details',$data);
    $this->load->view('common/footer');
  }

  public function looks(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'looks';
    $data['page_title'] = 'List of Looks '.$date_from.' - '.$date_to;
    $data['products'] = $this->brandDashboard_model->looks_details($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('brandDashboard/brand_report_details',$data);
    $this->load->view('common/footer');
  }

  public function view_products_used(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'view_products_used';
    $data['page_title'] = 'List of Products Used in Look '.$date_from.' - '.$date_to;
    $data['products'] = $this->brandDashboard_model->view_products_used($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('brandDashboard/brand_report_details',$data);
    $this->load->view('common/footer');
  }

  public function view_products_click(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    $data['date_from'] = $date_from;
    $data['date_to'] = $date_to;
    $data['type'] = 'view_products_click';
    $data['page_title'] = 'List of Products Click '.$date_from.' - '.$date_to;
    $data['products'] = $this->brandDashboard_model->view_products_click($date_from,$date_to);

    $this->load->view('common/header');
    $this->load->view('brandDashboard/brand_report_details',$data);
    $this->load->view('common/footer');
  }

  public function top_five(){
    $data = array();

    $date_from = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    $date_to = $this->uri->segment(4) ? $this->uri->segment(4) : '';

    if($date_from!=''){ $date_from = date('Y-m-d H:i:s', strtotime($date_from)); }
    if($date_to!=''){ $date_to = date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 second',str_replace('-', '/', strtotime($date_to)))); }

    //$data['date_from'] = $date_from;
    //$data['date_to'] = $date_to;
    //$data['type'] = 'view_products_click';
    //$data['page_title'] = 'List of Products Click '.$date_from.' - '.$date_to;
    $data['products'] = $this->brandDashboard_model->top_five($date_from,$date_to);


    $this->load->view('common/header');
    $this->load->view('brandDashboard/top_five_details',$data);
    $this->load->view('common/footer');
  }

  public function get_orders_info(){

  }
	
}
