<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Lookcreator_Mobile extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('lookcreator_mobile_model');  
       
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //$this->methods['products_details_get']['limit'] = 100; // 100 requests per hour per user/key
        //$this->methods['products_details_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    public function category_post($id_param = NULL)
    {
       if($this->lookcreator_mobile_model->get_allcategories()){
            $categories = $this->lookcreator_mobile_model->get_allcategories();
            $api_status = "True";
            $api_message = "Done";
            //$data =    json_encode($categories); 
            //print_r($data);          
            $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>$categories], REST_Controller::HTTP_CREATED);
       }else{
        $api_status = "True";
        $api_message = "Not Available";
        $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>''], REST_Controller::HTTP_CREATED);
       }
       
    }

    public function allproducts_post($id_param = NULL)
    {
       if($this->lookcreator_mobile_model->get_allproducts()){
            $products = $this->lookcreator_mobile_model->get_allproducts();
            $api_status = "True";
            $api_message = "Done";
            //$data =    json_encode($products); 
            //print_r($data);          
            $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>$products], REST_Controller::HTTP_CREATED);
       }else{
        $api_status = "True";
        $api_message = "Not Available";
        $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>''], REST_Controller::HTTP_CREATED);
       }
       
    }

    public function variations_post($cat_id = null){
        $cat_id = $this->post('category_id');
        if($cat_id >0){

           if($this->lookcreator_mobile_model->get_subcategories($cat_id)){
                $categories = $this->lookcreator_mobile_model->get_subcategories($cat_id);
                $api_status = "True";
                $api_message = "Done";
                $data =    json_encode($categories);
                $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>$data], REST_Controller::HTTP_CREATED);
            }else{
                $api_status = "True";
                $api_message = "Not Available";
                $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>''], REST_Controller::HTTP_CREATED);
           }

        }else{
                $api_status = "True";
                $api_message = "Please Select the Category";
                $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>''], REST_Controller::HTTP_CREATED);
        }
    }

    public function variations_values_post($variation_id = null){
        $variation_id = $this->post('variation_id');
        if($variation_id >0){

           if($this->lookcreator_mobile_model->get_variation_value($variation_id)){
                $categories = $this->lookcreator_mobile_model->get_variation_value($variation_id);
                $api_status = "True";
                $api_message = "Done";
                $data =    json_encode($categories);
                $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>$data], REST_Controller::HTTP_CREATED);
            }else{
                $api_status = "True";
                $api_message = "Not Available";
                $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>''], REST_Controller::HTTP_CREATED);
           }

        }else{
                $api_status = "True";
                $api_message = "Please Select the Category";
                $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>''], REST_Controller::HTTP_CREATED);
        }
    }

    public function products_post($variation_value_id = null){
        $variation_value_id = $this->post('variation_value_id');
        if($variation_value_id >0){

           if($this->lookcreator_mobile_model->get_products($variation_value_id)){
                $categories = $this->lookcreator_mobile_model->get_products($variation_value_id);
                $api_status = "True";
                $api_message = "Done";
                $data =    json_encode($categories);
                $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>$data], REST_Controller::HTTP_CREATED);
            }else{
                $api_status = "True";
                $api_message = "Not Available";
                $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>''], REST_Controller::HTTP_CREATED);
           }

        }else{
                $api_status = "True";
                $api_message = "Please Select the Category";
                $this->set_response(['status' => $api_status,'message' => $api_message,'data'=>''], REST_Controller::HTTP_CREATED);
        }

    }


   

}