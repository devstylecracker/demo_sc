<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attributes extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Attribute_model');
        $this->load->model('Categories_model');
        $this->load->model('productnew_model');
        $this->load->model('productmanagement_model');

    }

    function index(){
    	
    	if($this->has_rights(1) == 1 && $this->session->userdata('user_id')){
    		$data = array();
    		 $is_true = 0;
    		if($this->input->post()){
                $this->form_validation->set_rules('attribute_name','Attributes Name','required|trim|min_length[2]|max_length[50]');
                $this->form_validation->set_rules('sort','Sort','required|trim|numeric');
                
                if($this->form_validation->run() == FALSE){   
                     $is_true = 1;
                }else{ 
                   
                    $is_true = 0;

                }
                if($is_true == 0){
    			     $this->Attribute_model->save_attribues($_POST);
                     $_POST = array();
                }else{
                    $data['error'] = "Check image format or dimension";
                }
    		}
            //$data['cat_data'] = $this->Attribute_model->get_all_categories();
            $data['cat_data'] = $this->Categories_model->get_cat_data();
            $data['attr_data'] = $this->Attribute_model->get_all_attributes();
			$this->load->view('common/header');
            $this->load->view('attributes',$data);
            $this->load->view('common/footer');

        }else{

            $this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
        }
        
    }

    function attributes_edit($id=null){
        if($this->has_rights(1) == 1 && $this->session->userdata('user_id')){
            $data = array(); $is_true = 0;
            
            if($this->input->post()){

                if($is_true == 0){
                     $this->Attribute_model->edit_attribute($_POST,$id);
                     $_POST = array();
                }else{
                    $data['error'] = "Error : please check data";
                }

            }

            $data['cat_data_data'] = $this->Attribute_model->get_attributes($id);
            //$data['cat_data'] = $this->Attribute_model->get_all_categories();
            $data['attr_data'] = $this->Attribute_model->get_all_attributes();
            $cat = array();
            $cat = explode(',', $data['cat_data_data'][0]['attribute_parent']);
            $data['cat_data'] = $this->productmanagement_model->get_cat_data($cat);
            

            $this->load->view('common/header');
            $this->load->view('attributes',$data);
            $this->load->view('common/footer');
         }else{

            $this->load->view('common/header');
            $this->load->view('not_permission');
            $this->load->view('common/footer');
        }
    }

    function check_product_count(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
           $id = $this->input->post('id');
           echo $this->Attribute_model->get_attributes($id)[0]['count'];
        }
    }

    function delete_category(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
           $id = $this->input->post('id');
           echo $this->Attribute_model->delete_category($id);
        }
    }

    function search_category(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }else{
            $data = array();
            $search_category = @$this->input->post('search_text');
            $data['cat_data'] = $this->Attribute_model->get_filtered_category($search_category);
            
            $this->load->view('attributes_list',$data);
        }
    }

    function migrate_attr_category_relation()
    {
       //$this->Attribute_model->migrate_acr();
    }
}
?>