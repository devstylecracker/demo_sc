<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Enquiry extends MY_Controller {    

    function __construct(){
        parent::__construct();
        $this->load->model('User_enquiry_model');       

    }  
 /*---------Category submodule functionalities----------*/

    public function index($offset = null)
    {   
      
        if(!$this->session->userdata('user_id')){

            $this->load->view('login');                    

        }else{ 		
			/*Open Get and Search All Data*/				  
				$data_post = array();
				if($this->has_rights(44) == 1){	 
					$search_by = $this->input->post('search_by');
					$table_search = $this->input->post('table_search');                 
					$paginationUrl = 'User_Enquiry/index/';
					$uri_segment = 3;
					$config['per_page'] = 20;
					$config['total_rows'] = count($this->User_enquiry_model->get_all_User_enquiry($search_by,$table_search,'',''));                
					$total_rows = $config['total_rows'];                                                    
					$get_users = $this->User_enquiry_model->get_all_User_enquiry($search_by,$table_search,$offset,$config['per_page']);					
					$count = $config['total_rows']; 
					$data_post['User_enquirys'] = $get_users;
					
                
					$this->customPagination($paginationUrl,$count,$uri_segment,$get_users,$total_rows);
					$this->load->view('common/header');
					$this->load->view('User_Enquiry',$data_post);
					$this->load->view('common/footer');
			/*End Get and Search All Data*/		
				}else{
					$this->load->view('common/header');
					$this->load->view('not_permission');
					$this->load->view('common/footer');
				}				
			}        
	}
	
	public function exportExcel($param,$search_by=NULL,$table_search=NULL,$is_download=NULL)
	{	
		$is_download = $this->uri->segment(6); 
		// echo $param.'--------'.$search_by.'--------'.$table_search.'--------'.$is_download;exit;
		if($search_by == 6 )
		{
			$table_search_encode = base64_decode($table_search);
		}else
		{
			$table_search_encode = $table_search;
		}	
		if($search_by == 1 && $table_search_encode == '0'){ $search_by =0; }
        $fieldName = array('ID','Name','Email','Contact No','Message','Subject','Enquiry Date Time');//Brand Payout 
		$get_data = (array)$this->User_enquiry_model->get_all_User_enquiry($search_by,$table_search_encode);
		// echo $this->db->last_query();exit;
        // echo "<pre>";print_r($get_data);exit;
       // echo $param.' '.$is_download;exit;
	   $result_data = array();
	   $i =0;
	   foreach($get_data as $val) {   
            
            $result_data[$i][] = $val->id;            
            $result_data[$i][] = $val->name;
            $result_data[$i][] = $val->email;
            $result_data[$i][] = $val->contact_no;
			$result_data[$i][] = $val->message;
            $result_data[$i][] = $val->subject;
            $result_data[$i][] = $val->enquiry_date_time;
			$i++;
	    }
	   //print_r($result_data);exit;
		if($param == 'downloadExcel' && $is_download == 1)
		{       	
		  //========================Excel Download==============================
			//$excel_result = $this->writeDataintoCSV($fieldName,$get_data,'view_order_payment');
		   $this->writeDataintoCSV($fieldName,$result_data,'view_all_User_enquiry');
		   //========================End Excel Download==========================

		   
		}
              
	}
	
}
		    
/*----------End of Common functions------------------------------------*/	


