<?php
class MY_Model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
       
    }
    
    function get_bucket_id($body_shape,$body_style,$work_style=0,$personal_style=0){
			$query = $this->db->get_where('bucket', array('body_shape' => $body_shape,'style' => $body_style,'work_style'=>$work_style,'personal_style'=>$personal_style));
			$res = $query->result_array();
            if($res)
                return $res[0]['id'];
            else
                return 0;
		}  

    function my_money_format($number) 
   { 
    $negative = '';
    $thousands = '';
    if(strstr($number,"-")) 
    { 
        $number = str_replace("-","",$number); 
        $negative = "-"; 
    } 
    
    $split_number = @explode(".",$number); 
    
    $rupee = @$split_number[0]; 
    $paise = @$split_number[1]; 
    
    if(@strlen($rupee)>3) 
    { 
        $hundreds = substr($rupee,strlen($rupee)-3); 
        $thousands_in_reverse = strrev(substr($rupee,0,strlen($rupee)-3)); 
        for($i=0; $i<(strlen($thousands_in_reverse)); $i=$i+2) 
        { 
            $thousands .= @$thousands_in_reverse[$i].@$thousands_in_reverse[$i+1].","; 
        } 
        $thousands = strrev(trim($thousands,",")); 
        $formatted_rupee = $thousands.",".$hundreds; 
        
    } 
    else 
    { 
        $formatted_rupee = $rupee; 
    } 
    
    if((int)$paise>0) 
    { 
        $formatted_paise = ".".substr($paise,0,2); 
    } else{
        $formatted_paise = ".".substr(0,0,2); 
    }
    
    return $negative.$formatted_rupee.$formatted_paise; 

}  

    function add_sc_options($option_name,$option_value){
        if($option_name!='' && $option_value!=''){
            $user_id = $this->session->userdata('user_id');
            $this->db->delete('sc_options', array('option_name' => $option_name)); 
            $this->db->insert('sc_options',array('option_name' => $option_name,'option_value' => $option_value,'created_by'=>$user_id));
        }
    }

    function get_sc_options($option_name){
        if($option_name!=''){
            $query = $this->db->get_where('sc_options', array('option_name' => $option_name));
            $res = $query->result_array();
            if(!empty($res)){
                return $res[0]['option_value'];
            }else{ return ''; }
        }
    }
    
 } 
?>
