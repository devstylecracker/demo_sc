<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');
//ini_set('session.gc_maxlifetime', 86400);
/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class MY_Controller extends CI_Controller {

	public $data = array();
	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();	
		$this->auto_login();	
		$this->pass_permission();
	}
	
	public function auto_login(){
		if(!$this->session->userdata('user_id')){ 

	      if($this->input->cookie('7572ce8a49ff8982ba4d17f9e56b6d1f') && $this->input->cookie('a6c16340e402e946c64ce53bb41ac24a')){
	        
	        $email_id = $this->encryptOrDecrypt($this->input->cookie('7572ce8a49ff8982ba4d17f9e56b6d1f'),'');
	        $password = $this->encryptOrDecrypt($this->input->cookie('a6c16340e402e946c64ce53bb41ac24a'),'');

	        $res = $this->Login_model->login_user(trim($email_id),trim($password));  
	         if($res == 1){ 
	            redirect('/home','refresh');
	         }else{
	            $data['error'] = "Invalid Username and Password";
	            $this->load->view('login',$data); 
	         }

	      }
			}
	}



	 public function pass_permission()
    {
		$user_rights=$this->userrights_model->pass_permission();
		$users_menu = array();
		
		if(!empty($user_rights))
		{
			foreach($user_rights as $val){
			$users_menu[] = $val->module_id;
			}
			$this->data['users_menu']=$users_menu;
		}
		
	}	
	
	function has_rights($module_id)
	{
		$result = $this->userrights_model->has_rights($module_id);
		return $result;
	}
	
	function user_pass_permission($user_id)
	{
		$result = $this->userrights_model->user_pass_permission($user_id);
		return $result;
	}
	
	function customPagination($paginationUrl,$count,$uri_segment,$get_users,$total_rows)
	{
		//echo $paginationUrl;exit;

		$config['base_url'] = base_url().$paginationUrl;		
		$config['total_rows'] = $count;
		$config['per_page'] = 1000;
		$config['uri_segment'] = $uri_segment;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<div class="box-tools"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';

		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		
		$this->pagination->initialize($config);	
		$this->data['total_rows'] = $total_rows;
	}

	function encryptOrDecrypt($mprhase, $crypt) {
	     $MASTERKEY = "STYLECRACKERAPI";
	     $td = mcrypt_module_open('tripledes', '', 'ecb', '');
	     $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	     mcrypt_generic_init($td, $MASTERKEY, $iv);
	     if ($crypt == 'encrypt')
	     {
	         $return_value = base64_encode(mcrypt_generic($td, $mprhase));
	     }
	     else
	     {
	         $return_value = mdecrypt_generic($td, base64_decode($mprhase));
	     }
	     mcrypt_generic_deinit($td);
	     mcrypt_module_close($td);
	     return $return_value;
	} 
	
	function compress_image($source_url, $destination_url, $quality) {
		$info = getimagesize($source_url);
	 
		if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
		elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
		elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
	 
		//save file
		imagejpeg($image, $destination_url, $quality);
	 
		//return destination file
		return $destination_url;
	}


	function my_money_format($number) 
   { 
	    $negative = '';
	    $thousands = '';
	    if(strstr($number,"-")) 
	    { 
	        $number = str_replace("-","",$number); 
	        $negative = "-"; 
	    } 
	    
	    $split_number = @explode(".",$number); 
	    
	    $rupee = @$split_number[0]; 
	    $paise = @$split_number[1]; 
	    
	    if(@strlen($rupee)>3) 
	    { 
	        $hundreds = substr($rupee,strlen($rupee)-3); 
	        $thousands_in_reverse = strrev(substr($rupee,0,strlen($rupee)-3)); 
	        for($i=0; $i<(strlen($thousands_in_reverse)); $i=$i+2) 
	        { 
	            $thousands .= @$thousands_in_reverse[$i].@$thousands_in_reverse[$i+1].","; 
	        } 
	        $thousands = strrev(trim($thousands,",")); 
	        $formatted_rupee = $thousands.",".$hundreds; 
	        
	    } 
	    else 
	    { 
	        $formatted_rupee = $rupee; 
	    } 
	    
	    if((int)$paise>0) 
	    { 
	        $formatted_paise = ".".substr($paise,0,2); 
	    } else{
	        $formatted_paise = ".".substr(0,0,2); 
	    }
	    
	    return $negative.$formatted_rupee.$formatted_paise; 

	}  

	/* this function is used to create table and write that table into excel */
    function writeDataintoCSV($fieldname,$db_result,$fname) {
    	//echo "<pre>";print_r($db_result);exit;
        //place where the excel file is created
        $date = date('Y-m-d-H-i-s');
    	$filename = $fname."_".$date.".xls";       
        $myFile = "./upload/".$filename;
        $this->load->library('parser');
 
        //load required data from database        
        $data['fields'] =  $fieldname;
        $data['data'] = $db_result;
 
        //pass retrieved data into template and return as a string
        //$stringData = $this->parser->parse('user_details_csv', $data, true);
        $stringData = $this->parser->parse('excel_detail_csv', $data, true);        
 
        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
 
        fclose($fh);
        //download excel file
        $this->downloadExcel($filename);
    }
 
	/* download created excel file */
    function downloadExcel($filename) {
        //$myFile = "./upload/testexcel1.xls";
        $myFile = "./upload/".$filename;
        header("Content-Length: " . filesize($myFile));
        header('Content-Type: application/vnd.ms-excel');
         //header('Content-Type: application/msexcel');
        
        //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        
        // header('Content-Disposition: attachment; filename=testexcel.xls');
        header('Content-Disposition: attachment; filename='.$filename); 
        //exec('unoconv -f xls -o output.xls input.ods');	
        readfile($myFile);
    }
}
