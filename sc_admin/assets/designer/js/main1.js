if($('html').hasClass('touch')){
	//add fast click
}
/*$("#scteam").owlCarousel({
      items : 3,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,2],
      itemsTablet:[768,2],
      itemsMobile:[599,1],
      navigation:true,
      pagination:false
 
  });*/
  /*$("#founders").owlCarousel({
      items : 2,
      itemsDesktop : [1199,2],
      itemsDesktopSmall : [979,2],
      itemsTablet:[768,2],
      itemsMobile:[599,1],
      navigation:true,
      pagination:false
 
  });*/
$('#about').on('click',function(){
	$('#compNav li a').removeClass('active');
	$(this).find('a').addClass('active');
	$('#teamSlider').removeClass('leftPage')
});
$('#team').on('click',function(){
	$('#compNav li a').removeClass('active');
	$(this).find('a').addClass('active');
	$('#teamSlider').addClass('leftPage')
});
/*===slider===*/
$(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
//=======Scroll section========
var aChildren = $('#nav li').children();
var aArray = (function(){
	var tempArray = []
	for (var i=0; i < aChildren.length; i++) {    
        var aChild = aChildren[i];
        var ahref = $(aChild).attr('href');
        tempArray.push(ahref);
    }
    return tempArray; 
})();
//fix the navigation
$(window).scroll(function(){
	if($(window).scrollTop()>20){
		$('#navHeader').addClass('fixedNav');
	}else{
		$('#navHeader').removeClass('fixedNav');
	}
});
//highlight navigation
var offset = (function(){
	if(window.innerWidth<900){
		return 40;
	}else{
		return 120;
	}
})()
$(window).scroll(function(){
    try{
        var windowPos = $(window).scrollTop()+offset; // get the offset of the window from the top of page
        var windowHeight = $(window).height(); // get the height of the window
        var docHeight = $(document).height();

        for (var i=0; i < aArray.length; i++) {
            var theID = aArray[i];
            var divPos = $(theID).offset().top; // get the offset of the div from the top of page
            var divHeight = $(theID).height(); // get the height of the div in question
            if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                $("a[href='" + theID + "']").addClass("nav-active");
            } else {
                $("a[href='" + theID + "']").removeClass("nav-active");
            }
        }

        if(windowPos + windowHeight == docHeight) {
            if (!$("nav li:last-child a").hasClass("nav-active")) {
                var navActiveCurrent = $(".nav-active").attr("href");
                $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                $("nav li:last-child a").addClass("nav-active");
            }
        }
    }
    catch(error){
        
    }
});
var offset = (function(){
	if(window.innerWidth<900){
		return 40;
	}else{
		return 120;
	}
})()
$('#nav a').click(function(e){
	var elementClicked = $(this).attr("href");
	var destination = $(elementClicked).offset().top - offset;
	/*if($.browser.opera)
	{
			$("html:not(:animated)").animate({ scrollTop: destination}, 1000 );
	}
	else
	{
		
	}*/
	if(window.innerWidth < 900){
		$('#homeWrapper').on('webkitTransitionEnd oTransitionEnd msTransitionEnd transitionend',function(e){
			console.log('complete');
			setTimeout(function(){
				$("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination}, 1000 );
				$('#homeWrapper').off('webkitTransitionEnd oTransitionEnd msTransitionEnd transitionend')
			},300)
		})
		$('#homeWrapper').removeClass('slide');
		$('#sendryNav').removeClass('slide');	
	}else{
		$("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination}, 1000 );
	} 
	
	return false;
});



//slide the registration page
$('#login-reg-nav li').on('click',function(){
	var li = $(this);
	var otherli =  $('#login-reg-nav li');
	if(!li.hasClass('activeTab')){
		otherli.removeClass('activeTab');
		li.addClass('activeTab');
	}
	if(li.index() === 1){
		$('#regWrapper').addClass('leftPage');
	}else{
		$('#regWrapper').removeClass('leftPage');
	}
});

$("#loginBtn").on('click',function(){
	$('#login-register').addClass('show');
});
$("#secLoginBtn").on('click',function(){
	$('#login-register').addClass('show');
});
$("#registerBtn,.registerBtn").on('click',function(){
	$('#login-register').addClass('show');
});
$('#loginClose').on('click',function(){
	$('#login-register').removeClass('show');
});

/*Contact Section*/
$('#contactBtn').on('click',function(){
	$('#contactNav a').removeClass('active');
	$(this).addClass('active');
	$('#contactSlider').removeClass('leftPage');
});
$('#careerBtn').on('click',function(){
	$('#contactNav a').removeClass('active');
	$(this).addClass('active');
	$('#contactSlider').addClass('leftPage');
});

$('#menuBtn').on('click',function(){
	var wapper = $('#homeWrapper');
	var nav = $('#sendryNav');
	if(wapper.hasClass('slide')){
		wapper.removeClass('slide');
		nav.removeClass('slide');
		//$('body').removeClass('flowHidden');
	}else{
		wapper.addClass('slide');
		nav.addClass('slide');
		//$('body').addClass('flowHidden');
	}
});

$('#nav li').on('click',function(){
	/*$('#homeWrapper').removeClass('slide');
	$('#sendryNav').removeClass('slide');*/
});

/*===Question===*/
var qnLen = (function(el){
	return el.length;
})($('.question-container'));

$('#prevQn').click(function(){
	var prevBtn = $(this);
	var prevQnId = Number(prevBtn.attr('data-prev'));
	var nxtbtn = $('#nextQn');
	var nxtQnId = Number(nxtbtn.attr('data-next'));
	if(prevQnId > 0){
            $('.question-wrapper').css('left','-'+((prevQnId-1)*100)+'%');
            prevBtn.attr('data-prev',(prevQnId-1));
            nxtbtn.attr('data-next',(nxtQnId-1));
            $('.qn-count').text(prevQnId+"/"+qnLen);
	}
	
});
$('#nextQn').click(function(){
	var nxtbtn = $(this);
	var nxtQnId = Number(nxtbtn.attr('data-next'));
	var prevBtn = $('#prevQn');
	var prevQnId = Number(prevBtn.attr('data-prev'));
	if(nxtQnId < qnLen){
            $('.question-wrapper').css('left','-'+(nxtQnId*100)+'%');
            nxtbtn.attr('data-next',(nxtQnId+1));
            prevBtn.attr('data-prev',(prevQnId+1));
            $('.qn-count').text((nxtQnId+1)+"/"+qnLen);	
	}
	
});

/*==Wardrobe==*/
/*
* debouncedresize: special jQuery event that happens once after a window resize
*
* latest version and complete README available on Github:
* https://github.com/louisremi/jquery-smartresize/blob/master/jquery.debouncedresize.js
*
* Copyright 2011 @louis_remi
* Licensed under the MIT license.
*/
var $event = $.event,
$special,
resizeTimeout;

$special = $event.special.debouncedresize = {
	setup: function() {
		$( this ).on( "resize", $special.handler );
	},
	teardown: function() {
		$( this ).off( "resize", $special.handler );
	},
	handler: function( event, execAsap ) {
		// Save the context
		var context = this,
			args = arguments,
			dispatch = function() {
				// set correct event type
				event.type = "debouncedresize";
				$event.dispatch.apply( context, args );
			};

		if ( resizeTimeout ) {
			clearTimeout( resizeTimeout );
		}

		execAsap ?
			dispatch() :
			resizeTimeout = setTimeout( dispatch, $special.threshold );
	},
	threshold: 250
};

// ======================= imagesLoaded Plugin ===============================
// https://github.com/desandro/imagesloaded

// $('#my-container').imagesLoaded(myFunction)
// execute a callback when all images have loaded.
// needed because .load() doesn't work on cached images

// callback function gets image collection as argument
//  this is the container

// original: MIT license. Paul Irish. 2010.
// contributors: Oren Solomianik, David DeSandro, Yiannis Chatzikonstantinou

// blank image data-uri bypasses webkit log warning (thx doug jones)
var BLANK = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';

$.fn.imagesLoaded = function( callback ) {
	var $this = this,
		deferred = $.isFunction($.Deferred) ? $.Deferred() : 0,
		hasNotify = $.isFunction(deferred.notify),
		$images = $this.find('img').add( $this.filter('img') ),
		loaded = [],
		proper = [],
		broken = [];

	// Register deferred callbacks
	if ($.isPlainObject(callback)) {
		$.each(callback, function (key, value) {
			if (key === 'callback') {
				callback = value;
			} else if (deferred) {
				deferred[key](value);
			}
		});
	}

	function doneLoading() {
		var $proper = $(proper),
			$broken = $(broken);

		if ( deferred ) {
			if ( broken.length ) {
				deferred.reject( $images, $proper, $broken );
			} else {
				deferred.resolve( $images );
			}
		}

		if ( $.isFunction( callback ) ) {
			callback.call( $this, $images, $proper, $broken );
		}
	}

	function imgLoaded( img, isBroken ) {
		// don't proceed if BLANK image, or image is already loaded
		if ( img.src === BLANK || $.inArray( img, loaded ) !== -1 ) {
			return;
		}

		// store element in loaded images array
		loaded.push( img );

		// keep track of broken and properly loaded images
		if ( isBroken ) {
			broken.push( img );
		} else {
			proper.push( img );
		}

		// cache image and its state for future calls
		$.data( img, 'imagesLoaded', { isBroken: isBroken, src: img.src } );

		// trigger deferred progress method if present
		if ( hasNotify ) {
			deferred.notifyWith( $(img), [ isBroken, $images, $(proper), $(broken) ] );
		}

		// call doneLoading and clean listeners if all images are loaded
		if ( $images.length === loaded.length ){
			setTimeout( doneLoading );
			$images.unbind( '.imagesLoaded' );
		}
	}

	// if no images, trigger immediately
	if ( !$images.length ) {
		doneLoading();
	} else {
		$images.bind( 'load.imagesLoaded error.imagesLoaded', function( event ){
			// trigger imgLoaded
			imgLoaded( event.target, event.type === 'error' );
		}).each( function( i, el ) {
			var src = el.src;

			// find out if this image has been already checked for status
			// if it was, and src has not changed, call imgLoaded on it
			var cached = $.data( el, 'imagesLoaded' );
			if ( cached && cached.src === src ) {
				imgLoaded( el, cached.isBroken );
				return;
			}

			// if complete is true and browser supports natural sizes, try
			// to check for image status manually
			if ( el.complete && el.naturalWidth !== undefined ) {
				imgLoaded( el, el.naturalWidth === 0 || el.naturalHeight === 0 );
				return;
			}

			// cached images don't fire load sometimes, so we reset src, but only when
			// dealing with IE, or image is complete (loaded) and failed manual check
			// webkit hack from http://groups.google.com/group/jquery-dev/browse_thread/thread/eee6ab7b2da50e1f
			if ( el.readyState || el.complete ) {
				el.src = BLANK;
				el.src = src;
			}
		});
	}

	return deferred ? deferred.promise( $this ) : $this;
};

var Grid = (function() {

		// list of items
	var $grid = $( '#og-grid' ),
		// the items
		$items = $grid.children( 'li' ),
		// current expanded item's index
		current = -1,
		// position (top) of the expanded item
		// used to know if the preview will expand in a different row
		previewPos = -1,
		// extra amount of pixels to scroll the window
		scrollExtra = 0,
		// extra margin when expanded (between preview overlay and the next items)
		marginExpanded = 10,
		$window = $( window ), winsize,
		$body = $( 'html, body' ),
		// transitionend events
		transEndEventNames = {
			'WebkitTransition' : 'webkitTransitionEnd',
			'MozTransition' : 'transitionend',
			'OTransition' : 'oTransitionEnd',
			'msTransition' : 'MSTransitionEnd',
			'transition' : 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		// support for csstransitions
		support = Modernizr.csstransitions,
		// default settings
		settings = {
			minHeight : 500,
			speed : 350,
			easing : 'ease'
		};

	function init( config ) {
		
		// the settings..
		settings = $.extend( true, {}, settings, config );

		// preload all images
		$grid.imagesLoaded( function() {

			// save item´s size and offset
			saveItemInfo( true );
			// get window´s size
			getWinSize();
			// initialize some events
			initEvents();

		} );

	}

	// add more items to the grid.
	// the new items need to appended to the grid.
	// after that call Grid.addItems(theItems);
	function addItems( $newitems ) {

		$items = $items.add( $newitems );

		$newitems.each( function() {
			var $item = $( this );
			$item.data( {
				offsetTop : $item.offset().top,
				height : $item.height()
			} );
		} );

		initItemsEvents( $newitems );

	}

	// saves the item´s offset top and height (if saveheight is true)
	function saveItemInfo( saveheight ) {
		$items.each( function() {
			var $item = $( this );
			$item.data( 'offsetTop', $item.offset().top );
			if( saveheight ) {
				$item.data( 'height', $item.height() );
			}
		} );
	}

	function initEvents() {
		
		// when clicking an item, show the preview with the item´s info and large image.
		// close the item if already expanded.
		// also close if clicking on the item´s cross
		initItemsEvents( $items );
		
		// on window resize get the window´s size again
		// reset some values..
		$window.on( 'debouncedresize', function() {
			
			scrollExtra = 0;
			previewPos = -1;
			// save item´s offset
			saveItemInfo();
			getWinSize();
			var preview = $.data( this, 'preview' );
			if( typeof preview != 'undefined' ) {
				//hidePreview();
			}

		} );

	}

	function initItemsEvents( $items ) {
		$items.on( 'click', 'span.og-close', function() {
			hidePreview();
			return false;
		} ).children( 'a' ).on( 'click', function(e) {

			var $item = $( this ).parent();
			// check if item already opened
			
			current === $item.index() ? hidePreview() : showPreview( $item );
			return false;

		} );
	}

	function getWinSize() {
		winsize = { width : $window.width(), height : $window.height() };
	}

	function showPreview( $item ) {
			//debugger ;
		var preview = $.data( this, 'preview' ),
			// item´s offset top
			position = $item.data( 'offsetTop' );

		scrollExtra = 0;

		// if a preview exists and previewPos is different (different row) from item´s top then close it
		if( typeof preview != 'undefined' ) {

			// not in the same row
			if( previewPos !== position ) {
				// if position > previewPos then we need to take te current preview´s height in consideration when scrolling the window
				if( position > previewPos ) {
					scrollExtra = preview.height;
				}
				hidePreview();
			}
			// same row
			else {
				preview.update( $item );
				return false;
			}
			
		}

		// update previewPos
		previewPos = position;
		// initialize new preview for the clicked item
		preview = $.data( this, 'preview', new Preview( $item ) );
		// expand preview overlay
		preview.open();

	}

	function hidePreview() {
		current = -1;
		var preview = $.data( this, 'preview' );
		preview.close();
		$.removeData( this, 'preview' );
	}

	// the preview obj / overlay
	function Preview( $item ) {
		this.$item = $item;
		this.expandedIdx = this.$item.index();
		this.create();
		this.update();
	}

	Preview.prototype = {
		create : function() {
			// create Preview structure:
			this.$title = $( '<h3></h3>' );
			this.$description = $( '<p></p>' );
			this.$href = $( '<a href="javascript:void(0)" class="pull-to-chat">Pull To Chat</a>' );
                        this.$upsection = $( '<div></div>' );
			this.$details = $( '<div class="og-details"></div>' ).append( this.$title, this.$description, this.$href , this.$upsection);
			this.$loading = $( '<div class="og-loading"></div>' );
			this.$fullimage = $( '<div class="og-fullimg"></div>' ).append( this.$loading );
			this.$closePreview = $( '<span class="og-close"></span>' );
			this.$previewInner = $( '<div class="og-expander-inner"></div>' ).append( this.$closePreview, this.$fullimage, this.$details );
			this.$previewEl = $( '<div class="og-expander"></div>' ).append( this.$previewInner );
			// append preview element to the item
			this.$item.append( this.getEl() );
			// set the transitions for the preview and the item
			if( support ) {
				this.setTransition();
			}
		},
		update : function( $item ) {

			if( $item ) {
				this.$item = $item;
			}
			
			// if already expanded remove class "og-expanded" from current item and add it to new item
			if( current !== -1 ) {
				var $currentItem = $items.eq( current );
				$currentItem.removeClass( 'og-expanded' );
				this.$item.addClass( 'og-expanded' );
				// position the preview correctly
				this.positionPreview();
			}

			// update current value
			current = this.$item.index();

			// update preview´s content
			var $itemEl = this.$item.children( 'a' ),
				eldata = {
					href : $itemEl.attr( 'href' ),
					largesrc : $itemEl.data( 'largesrc' ),
					title : $itemEl.data( 'title' ),
					description : $itemEl.data( 'description' ),
                                        id : $itemEl.data('id'),
                                        user : $itemEl.data('user')
				};

			this.$title.html( eldata.title );
			this.$description.html( eldata.description );
			this.$href.attr( 'href', eldata.href ).attr('data-id',eldata.id).attr('data-user',eldata.user);
                        this.$upsection.attr( 'id', 'responsesection'+eldata.id );
                        
			var self = this;
			
			// remove the current image in the preview
			if( typeof self.$largeImg != 'undefined' ) {
				self.$largeImg.remove();
			}

			// preload large image and add it to the preview
			// for smaller screens we don´t display the large image (the media query will hide the fullimage wrapper)
			if( self.$fullimage.is( ':visible' ) ) {
				this.$loading.show();
				$( '<img/>' ).load( function() {
					var $img = $( this );
					if( $img.attr( 'src' ) === self.$item.children('a').data( 'largesrc' ) ) {
						self.$loading.hide();
						self.$fullimage.find( 'img' ).remove();
						self.$largeImg = $img.fadeIn( 350 );
						self.$fullimage.append( self.$largeImg );
					}
				} ).attr( 'src', eldata.largesrc );	
			}

		},
		open : function() {

			setTimeout( $.proxy( function() {	
				// set the height for the preview and the item
				this.setHeights();
				// scroll to position the preview in the right place
				this.positionPreview();
			}, this ), 25 );

		},
		close : function() {

			var self = this,
				onEndFn = function() {
					if( support ) {
						$( this ).off( transEndEventName );
					}
					self.$item.removeClass( 'og-expanded' );
					self.$previewEl.remove();
				};

			setTimeout( $.proxy( function() {

				if( typeof this.$largeImg !== 'undefined' ) {
					this.$largeImg.fadeOut( 'fast' );
				}
				this.$previewEl.css( 'height', 0 );
				// the current expanded item (might be different from this.$item)
				var $expandedItem = $items.eq( this.expandedIdx );
				$expandedItem.css( 'height', $expandedItem.data( 'height' ) ).on( transEndEventName, onEndFn );

				if( !support ) {
					onEndFn.call();
				}

			}, this ), 25 );
			
			return false;

		},
		calcHeight : function() {

			var heightPreview = winsize.height - this.$item.data( 'height' ) - marginExpanded,
				itemHeight = winsize.height;

			if( heightPreview < settings.minHeight ) {
				heightPreview = settings.minHeight;
				itemHeight = settings.minHeight + this.$item.data( 'height' ) + marginExpanded;
			}

			this.height = heightPreview;
			this.itemHeight = itemHeight;

		},
		setHeights : function() {

			var self = this,
				onEndFn = function() {
					if( support ) {
						self.$item.off( transEndEventName );
					}
					self.$item.addClass( 'og-expanded' );
				};

			this.calcHeight();
			this.$previewEl.css( 'height', this.height );
			this.$item.css( 'height', this.itemHeight ).on( transEndEventName, onEndFn );

			if( !support ) {
				onEndFn.call();
			}

		},
		positionPreview : function() {

			// scroll page
			// case 1 : preview height + item height fits in window´s height
			// case 2 : preview height + item height does not fit in window´s height and preview height is smaller than window´s height
			// case 3 : preview height + item height does not fit in window´s height and preview height is bigger than window´s height
			var position = this.$item.data( 'offsetTop' ),
				previewOffsetT = this.$previewEl.offset().top - scrollExtra,
				scrollVal = this.height + this.$item.data( 'height' ) + marginExpanded <= winsize.height ? position : this.height < winsize.height ? previewOffsetT - ( winsize.height - this.height ) : previewOffsetT;
			
			$body.animate( { scrollTop : scrollVal }, settings.speed );

		},
		setTransition  : function() {
			this.$previewEl.css( 'transition', 'height ' + settings.speed + 'ms ' + settings.easing );
			this.$item.css( 'transition', 'height ' + settings.speed + 'ms ' + settings.easing );
		},
		getEl : function() {
			return this.$previewEl;
		}
	}

	return { 
		init : init,
		addItems : addItems
	};

})();
Grid.init();

$('.chatBtn').on('click',function(){
	var wapper = $('#homeWrapper');
	var chatWapper = $('#chat-wrapper');
        var secNav = $('#sendryNav');
	if(wapper.hasClass('slide-left')){
		chatWapper.addClass('dspNone');
		wapper.removeClass('slide-left');
                secNav.removeClass('slide-left');
		//$('body').removeClass('flowHidden');
	}else{
		chatWapper.removeClass('dspNone');
		wapper.addClass('slide-left');
                secNav.addClass('slide-left');
		chatWith("stylist");
		//$('body').addClass('flowHidden');
	}
});

$('#chat-wrapper').on('click','#chat-slide',function(){
	var wapper = $('#homeWrapper');
	var chatWapper = $('#chat-wrapper');
        var secNav = $('#sendryNav');
	chatWapper.addClass('dspNone');
	wapper.removeClass('slide-left');
        secNav.removeClass('slide-left');

});

//function test(){
//    alert('hi');
//}
$('#og-grid').on('click','.pull-to-chat',function(){
    wardrobe = $(this).attr('data-id');
    user = $(this).attr('data-user');
    if(user == 'admin'){
        alert('Please use attachment symbol on chatbox, to pull Wardrobe!');
    }else{
        var chatbox = $('#chatbox_stylist');
        var chatboxtextarea = $('#chatbox_stylist .chatboxtextarea');
        var chatboxcontent = $("#chatbox_stylist .chatboxcontent");
        chat_sess_id = chatbox.attr('chat_sess_id');
        if($.trim(chat_sess_id) == ""){
            alert('Please start a conversation with Text!');
        }else{
            $.ajax({
               url: webconfig.base+'wardrobe/insertPullToChatWardrobesChatImagesMeta', 
               type: 'POST',
               data: {wardrobe_id:wardrobe,chat_session_id:chat_sess_id},
               beforeSend:function(){
                   $('#responsesection'+wardrobe).html('<img alt="Loader" src="'+webconfig.base+'assets/front/images/vloader.gif">');
               },
               success:function(data){
                   try {
                        response = $.parseJSON(data);
                        if(response.Status == 200){
                            to_id = chatbox.attr('assoc_u_id');
                            to = "stylist";
                            message = response.chat_message;
                            
                            chatboxtextarea.val('');
                            chatboxtextarea.focus();
                            chatboxtextarea.css('height','44px');
                            if (message != '') {
                                    $.post(webconfig.base+"chat/sendchat", {to: "stylist",to_id:to_id,sess_id:chat_sess_id, message: message, chat_images_meta_id:response.chat_images_meta_id} , function(data){
                                        $('#responsesection'+wardrobe).html('');
                                            if(data == '0'){
                                                //mail need to be scheduled
                                                chatboxcontent.append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+username+':&nbsp;&nbsp;</span><span class="chatboxmessagecontent">We are offline now, We will get back to you.</span></div>');
                                                chatboxcontent.scrollTop(chatboxcontent[0].scrollHeight);
                                            }else{
                                                data_arr = data.split('-');
                                                chatbox.attr('assoc_u_id',data_arr[1]);
                                                chatbox.attr('chat_sess_id',data_arr[2]);
                                                chatboxcontent.append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+username+':&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+message+'</span></div>');
                                                chatboxcontent.scrollTop(chatboxcontent[0].scrollHeight);
                                                $('.attach_img').removeClass('disabled');
                                            }
                                    });
                            }
                            chatHeartbeatTime = minChatHeartbeat;
                            chatHeartbeatCount = 1;
                            var adjustedHeight = chatboxtextarea.clientHeight;
                            var maxHeight = 94;

                            if (maxHeight > adjustedHeight) {
                                    adjustedHeight = Math.max(chatboxtextarea.scrollHeight, adjustedHeight);
                                    if (maxHeight)
                                            adjustedHeight = Math.min(maxHeight, adjustedHeight);
                                    if (adjustedHeight > chatboxtextarea.clientHeight)
                                            $(chatboxtextarea).css('height',adjustedHeight+8 +'px');
                            } else {
                                    $(chatboxtextarea).css('overflow','auto');
                            }
                            chatWith('stylist');
                        }else{
                            $('#responsesection'+wardrobe).html(response);
                        }
                    } catch(error) {
                        if(error.name == "SyntaxError"){
                            $('#responsesection'+wardrobe).html(data);
                        }else{
                            $('#responsesection'+wardrobe).html('Something went wrong please, TRY AGAIN!');
                        }
                    }
               }
            });
        }
    }
});

$(document).ready(function(){
    $('.tab-btn-1').on('click', function(){
        $('#tab1').hide();
        $('#tab2').show('left');
		$('.tab-btn-1').css('background','#FF0');
		$('.tab-btn-2').css('background','#2c2c2c');
        return false;
    });

    $('.tab-btn-2').on('click', function(){
        $('#tab2').hide();
        $('#tab1').show('left');
		$('.tab-btn-2').css('background','#FF0');
		$('.tab-btn-1').css('background','#2c2c2c');
        return false;
    });
	 $('.media .tabBtnWrp li a span').on('click', function(){
		 $('.media .tabBtnWrp li a').children("span").css('background-color','#FFF');
		 $(this).css('background-color','#FF0');
		
	 });
    $('.media .tabBtnWrp li a').on('click', function(){

		 
            var btn = $(this).attr('data-btn');
            console.log(btn);
            $('.sectionWrp .tabWrp').hide();
            $('.tabWrp.'+btn+'_sec').show('left');
            return false

    });

    $('.media .tabBtnWrp li:eq(0) a').click();
});

