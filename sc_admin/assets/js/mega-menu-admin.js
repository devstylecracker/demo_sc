var getUrl = window.location;
var local_baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+ "/" + getUrl.pathname.split('/')[2];
var baseUrl =getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+ "/";
var sc_siteUrl =getUrl .protocol + "//" + getUrl.host + "/" ;
var strMenuItemSlug_url = "";
var intColumnLimit = 0;
function msg(m) { console.log(m); }

  $(function() {
   /* $( "#sortable1, #sortable2" ).sortable({
      connectWith: ".connectedSortable"
    }).disableSelection();
*/
    $('.box-body').find('a').click(function(){ return false; });
  });

function InitMegaMenu(){
	// menu-item-type ---------------------------------------
	$('.menu-item-type select').change(function(){
		MenuItemType($(this).val());
	});
	$('.menu-item-type select').change();

	// menu-category-type ---------------------------------------
	$('.menu-category-type select').change(function(){
		MenuCategoryType($(this).val());
	});
	// menu-item-selected ---------------------------------------
	//function in side container

	// column-number-selected -------------------------------
	$('.column-number-selected select').change(function(){
		ColumnNumberSelected($(this).val());
	});

}

//## Init Function -------------------------------------------
InitMegaMenu();

//## Variable Declaration ------------------------------------
var strMenuItemType, strMenuItemSelected, strMenuItemSlug, strCustomLabelText, strCustomStyleClass, strCustomURL;
var intColumnNumberSelected, intCategoryType, intMenuID = 0;

//## Param Setting --------------------------------------------
var intInsertElementLimit = 11;
var intInsertElementLimitForLast = 5;

//## Array to Save --------------------------------------------
var arrMenuData = [];


//## Param Defining --------------------------------------------
function MenuItemType(e){
	strMenuItemType = e;
}

function MenuCategoryType(e) {
	intCategoryType = e;
}

function CategoryItemSelected(){
	strMenuItemSelected = $('#variations option:selected').attr('label').trim();
	intMenuID = $('#variations option:selected').val();
	strMenuItemSlug = $('#variations option:selected').attr('slug').trim();
}

function ColumnNumberSelected(e){
	intColumnNumberSelected = e;
}

function InitCustomParam() {
	strCustomLabelText = $('.custom-label-text').val();
	strCustomStyleClass = $('.custom-style-class').val();
	strCustomURL = $('.custom-url-text').val();
}

//## Add Option To Table Function ---------------------------------
function AddOptionToTable(strmtype) {
	InitCustomParam();

	var strAddToContainer = $('.megamenu-list').find('.col-'+(intColumnNumberSelected));
	var intColumnLimit = $(strAddToContainer).find('li').length;

	if(intColumnLimit<intInsertElementLimit && intColumnNumberSelected<=4) {
		AddListElement(intCategoryType,strMenuItemSelected,strAddToContainer,strmtype);

	} else if(intColumnLimit<intInsertElementLimitForLast && intColumnNumberSelected>4){
		AddListElement(intCategoryType,strMenuItemSelected,strAddToContainer,strmtype);

	} else {
		alert('Limit is exceeded for this column');
	}

}

function AddListElement(intCatType,strLabel,strContainer,strmtype) {
	var strULContainer = $(strContainer).find('ul').last();
	strMenuItemSlug_url = get_category_slug_url(strMenuItemSlug);
	if(intCatType==1) {
		$('<ul type="'+intCatType+'" label="'+strLabel+'" id="'+intMenuID+'" slug="'+strMenuItemSlug_url+'"><li type="'+intCatType+'" id="'+intMenuID+'" slug="'+strMenuItemSlug+'"  url="'+strCustomURL+'" class="title"><a href="'+sc_siteUrl+'products'+strMenuItemSlug_url+'" >'+strLabel+'</a><i onclick="RemoveListElementUl(this)" class="fa fa-close btn-delete"></i></li></ul>').appendTo(strContainer);
		
		// $('<ul type="'+intCatType+'" label="'+strLabel+'" id="'+intMenuID+'" slug="'+strMenuItemSlug+'"><li type="'+intCatType+'" id="'+intMenuID+'" slug="'+strMenuItemSlug+'"  url="'+strCustomURL+'" class="title"><a href="'+sc_siteUrl+'all-products/'+strmtype+'/variation/'+strMenuItemSlug+'" >'+strLabel+'</a><i onclick="RemoveListElementUl(this)" class="fa fa-close btn-delete"></i></li></ul>').appendTo(strContainer);
	}else if(intCatType==2) {
		$('<li type="'+intCatType+'" id="'+intMenuID+'" slug="'+strMenuItemSlug+'"  url="'+strCustomURL+'"><a href="'+sc_siteUrl+'products'+strMenuItemSlug_url+'" >'+strLabel+'</a><i onclick="RemoveListElement(this)" class="fa fa-close btn-delete"></i></li>').appendTo(strULContainer);
		
		//$('<li type="'+intCatType+'" id="'+intMenuID+'" slug="'+strMenuItemSlug_url+'"  url="'+strCustomURL+'"><a href="'+sc_siteUrl+'all-products/'+strmtype+'/variations/'+strMenuItemSlug+'" >'+strLabel+'</a><i onclick="RemoveListElement(this)" class="fa fa-close btn-delete"></i></li>').appendTo(strULContainer);
	}


	/*$( ".megamenu-list ul" ).sortable({
		connectWith: ".connectedColumns"
	}).disableSelection();*/

}


function get_category_slug_url(strMenuItemSlug){
	var category_call_url =  baseUrl+"/webui/megamenu/get_category_slug_url/"+strMenuItemSlug;
	var jqXHR = $.ajax({
			url: category_call_url,
			//url: baseUrl+'/webui/Megamenu',
			type: 'get',
			async: false,
			success: function(response, status) {
				if(status == 'success'){
				}
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
	});
	return jqXHR.responseText;
}

function RemoveListElementUl(e) {
	$(e).closest('ul').remove();
}

function RemoveListElement(e) {
	$(e).closest('li').remove();
}

//## Publish Menu function -----------------------------------
var objMenuData = {};
function PublishMenu(menutype) {
	$("#previewing a > img").unwrap();
	var img_href = $('#imgurl').val();
	if(img_href.trim()!=''){
		$('#img_link').attr('href',img_href);
	}else{
		$('#img_link').attr('href','javascript:void(0)');
	}

      //$("#previewing").wrap("<a href='"+img_href+"' id='img_link' target='_blank' >");
		objMenuData = {};
		$('.megamenu-list ul').each(function () {
			id = this.id;
			type = $(this).attr('type');
			slug = $(this).attr('slug');
			label = $(this).attr('label');
			var array = [];
			$('li', this).each(function () {
				array.push([$(this).attr('type'),$(this).attr('id'),$(this).find('a').text(),$(this).attr('slug')]);
			});
			objMenuData[id,type,slug,label] = {
				id: id,
				type: type,
				label: label,
				slug:slug,
				data: array
			};
		});

		/*msg(objMenuData);
		msg($('.megamenu-list'));*/
		saveMegamenuData(menutype);
}

function saveMegamenuData(menutype)
{
	var megamenuhtml = $('.megamenu-list');
	//console.log('megamenuhtml==='+megamenuhtml[0].innerHTML);
	//$('#message').html(megamenuhtml[0].innerHTML);
	var mmhtml = megamenuhtml[0].innerHTML;
	var objMenuData1 = JSON.stringify(objMenuData);
	$.ajax({
			url: baseUrl+'/webui/Megamenu/save_megamenu_data',
			//url: baseUrl+'/webui/Megamenu',
			type: 'post',
			data: { 'type' : 'megamenu','megamenu_array': objMenuData1 ,'megamenu_html' : mmhtml ,'menutype' : menutype },
			success: function(data, status) {
				if(status == 'success'){
					$('#message').html('Successfully Saved !!!');
				}
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
	});
}


function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

 $(function() {
  $('#megamenu_form').submit(function(e) {
        e.preventDefault();
    $.ajaxFileUpload({
          url       :'megamenu',
          fileElementId :'file',
          dataType    : 'json',
          success : function (data, status)
          {
            if(data.status == 'success'){
             // location.reload();
            }else{
              alert('error');
            }
          },error: function(xhr) {
            console.log(xhr);
          }
        });
  });

   $('#megamenu_men_form').submit(function(e) {
        e.preventDefault();
    $.ajaxFileUpload({
          url       :'men',
          fileElementId :'file',
          dataType    : 'json',
          success : function (data, status)
          {
            if(data.status == 'success'){
             // location.reload();
            }else{
              alert('error');
            }
          },error: function(xhr) {
            console.log(xhr);
          }
        });
  });

});
