var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
$( document ).ready(function() { 

	$( "#brand" ).change(function() {
    var brand_id = $("#brand").val();
        /*$('#products').pqSelect();*/
	    if(brand_id!='' && brand_id!=0 )
	    {	
			if(brand_id!='' && brand_id!=0)
	    	{
				get_store_products(brand_id,'');
				$('#stylecracker_discount').prop('checked', false); // Unchecks it	    	 
		    }else
		    {	    	
		    	$('#stylecracker_discount').prop('checked', true); // Checks it			
		    }
	    }   

  	});
  	 
	$("#products").pqSelect({
                   bootstrap: { on: true },
                   multiplePlaceholder: 'Select Products',
                   checkbox: true, //adds checkbox to options
                }).on("change", function(evt) {
                    var val = $(this).val();
                   // console.log(val);
    });

});


/* Get Brand Products */
	function get_store_products(id,sel_id)
	{	 
	  //$('#products').find('option').remove();  
	   $(".page-overlay").show();
       $(".page-overlay .loader").css('display','inline-block');
  
	  $.ajax({
	    type:"POST",
	    url:baseUrl+"/CouponSettings/get_store_products",
	    data :{ brand_id:id,sel_id:sel_id},
	    //datatype : 'html',
	    success: function(data){

	    	$(".page-overlay").hide();
      		$(".page-overlay .loader").hide();

	       $('#products option').remove();
	      data = $.parseJSON(data);
	      
	      sel_id_array = sel_id; 
	     
	      $.each(data, function(key, value) {
	      //$('#products').append($("<option></option>").attr("value",value.id) .text(value.id+'  '+value.name));  

	      if(jQuery.inArray(value.id,sel_id_array) !== -1){       
	      		$('#products').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
	  		}else{
	  			$('#products').append('<option value="'+value.id+'">'+value.name+'</option>');
	  		}
	      });

	       $("#products").pqSelect({  bootstrap: { on: true },
	                         multiplePlaceholder: 'Select Products',
	                      checkbox: true });
	       $('#products').pqSelect('refreshData');  
	    }

	  });
	}
