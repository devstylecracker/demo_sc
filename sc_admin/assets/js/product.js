$( document ).ready(function() {
	$('.datetimepicker').datetimepicker();
  //$("select:not([multiple])").addClass("select-box");
  //$(".select-box").SumoSelect();

  //$("select[multiple=''].multi-select-box").SumoSelect({selectAll: true });

    $( "#category" ).change(function() {
		var cat_id = $("#category").val();
		get_sub_cat(cat_id,'');
	});

	$( "#sub_category" ).change(function() {
		var cat_id = $("#sub_category").val();
		get_variation_type(cat_id,'');
	});

 //$('input[type=file]').bootstrapFileInput();


/*	$("#product_tags").pqSelect({
                       bootstrap: { on: true },
                       multiplePlaceholder: 'Select Tags',
                    checkbox: true, //adds checkbox to options
                }).on("change", function(evt) {
                    var val = $(this).val();
                   // console.log(val);
    });*/


/*
    $("#product_avail").pqSelect({
                    multiplePlaceholder: 'Select Product Availability',
                    checkbox: true //adds checkbox to options
                }).on("change", function(evt) {
                    var val = $(this).val();
                   // console.log(val);
    });
*/

	/*$("#brand").pqSelect({
                       bootstrap: { on: true },
                       multiplePlaceholder: 'Select Tags',
                    checkbox: true, //adds checkbox to options
                }).on("change", function(evt) {
                    var val = $(this).val();
                   // console.log(val);
    });

	$("#product_tags").pqSelect({
                       bootstrap: { on: true },
                       multiplePlaceholder: 'Select Tags',
                         maxDisplay: 1200,
                    checkbox: true, //adds checkbox to options
                }).on("change", function(evt) {
                    var val = $(this).val();
                   // console.log(val);
    });
    $("#product_avail").pqSelect({
                    multiplePlaceholder: 'Select Product Availability',
                    checkbox: true //adds checkbox to options
                }).on("change", function(evt) {
                    var val = $(this).val();
                   // console.log(val);
    });*/

    $("#approved").click(function(){
    	console.log('sasa');
    	var product_id = $('#product_id').val();
    	product_approve(product_id);
    });

    $("#reject").click(function(){
    	var product_id = $('#product_id').val();
    	var reject_reason = $('#reject_reason').val();
    	product_reject(product_id,reject_reason);
    });
});


/* Get Sub category */
function get_sub_cat(id,sel_id){
	var getUrl = window.location;
  	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	$.ajax({
			url: baseUrl+'/product_new/get_sub_cat/',			
			type: 'post',
			data: { 'type' : 'get_sub_cat','cat_id': id,'sel_id' : sel_id },
			success: function(data, status) {
				if(status == 'success'){
					$('#sub_category').html(data);
				}
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
	});
}

/* Get Variation Type */
function get_variation_type(id,sel_id){
	var getUrl = window.location;
  	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	$.ajax({
			url: baseUrl+'/product_new/get_var_value_type',			
			type: 'post',
			data: { 'type' : 'get_var_value_type','sub_cat_id': id ,'sel_id' : sel_id },
			success: function(data, status) {
				if(status == 'success'){
					$('#variation_type').html(data);
				}
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
	});
}

/*Product Apprroval*/
function product_approve(product_id){
	var getUrl = window.location;
  	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	var conf = confirm('Are you sure you want to approve');
	if(conf){
		$.ajax({
			url: baseUrl+'/product_new/product_approve',			
			type: 'post',
			data: { 'type' : 'product_approve','product_id':  product_id },
			success: function(data, status) {
				if(status == 'success'){
					 alert('Approved successfully');
					 var getUrl = window.location;
  					 var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
              		  window.location = baseUrl+'/product_management/';              		 
				} else
	            {
	              	 alert('OOPS!something went wrong');
	            }
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
		});
	}
}

/* Reject Reason */
function product_reject(product_id,reason){
	var getUrl = window.location;
  	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	if(!$('#product_reject_reason').is(":visible")){
		$('#product_reject_reason').show();
	}else{
		var reject_reason = $('#product_reject_reason').val();
		if(reject_reason.trim() == ''){
			alert("Please Enter Product Reject Reason");

		}else{
			var conf = confirm('Are you sure you want to Reject the Product');
			if(conf){
				var getUrl = window.location;
  					 var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
				$.ajax({
					url: baseUrl+'/product_new/product_reject',					
					type: 'post',
					data: { 'type' : 'product_reject','product_id':  product_id, 'reason' :reject_reason },
					success: function(data, status) {
						if(status == 'success'){
							 alert('Rejected successfully');
		              		   var getUrl = window.location;
  					 	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
              		 	 window.location = baseUrl+'/product_management/';      	              		  
						} else
			            {
			              	 alert('OOPS!something went wrong');
			            }
					},
					error: function(xhr, desc, err) {
						console.log(err);
					}
				});
			}

		}

	}

}
/*Extra image delete*/
function delete_extra_img(id){
  if (confirm("Are you sure?")) {
  		var getUrl = window.location;
  		var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    	$.ajax({
			url: baseUrl+'/product_new/delete_extra_img',			
			type: 'post',
			data: { 'type' : 'delete_extra_img','image_id':  id },
			success: function(data, status) {
				    location.reload();
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
		});


  }
   return false;
}

function addnew_row(id){
	var new_id = parseInt($('#total_rows').val())+1;
	var rowhtml = '<div class="row" id="row'+new_id+'"><div class="col-md-3"><input type="text" name="size'+new_id+'" id="size'+new_id+'" placeholder="Size" class="form-control"></div><div class="col-md-3"><input type="text" name="qty'+new_id+'" id="qty'+new_id+'" placeholder="Qty" class="form-control"></div><div class="col-md-3"><input type="text" name="product_sku'+new_id+'" id="product_sku'+new_id+'" placeholder="Product SKU" class="form-control"></div><div class="col-md-3"><span class="btn btn-primary btn-xs btn-delete" onclick="remove_row('+new_id+');"><i class="fa fa-minus"></i></span></div></div>';
	$('#total_rows').val(new_id);
	$('#product_inventory').append(rowhtml);
}

function remove_row(id){
	$('#row'+id).remove();
}