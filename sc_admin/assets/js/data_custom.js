$( document ).ready(function() {
  // page image-upload
 /*   $("#image-upload .cat-label" ).click(function() {
      $(".cat-label").removeClass( "active" );
      $(this).addClass( "active" );
      $(".cat-label .fa").removeClass( "fa-dot-circle-o" );
      $(this).children(".fa").addClass("fa-dot-circle-o");

    });

  $("#image-upload .cat-trends" ).click(function() {
    $(".cat-options" ).css( "display","none" );
    $("#cat-trends" ).css("display","block" );

  });

  $("#image-upload .cat-fashion-shows" ).click(function() {
    $(".cat-options" ).css( "display","none" );
    $("#cat-fashion-shows" ).css("display","block" );
  });

  $("#image-upload .cat-lookbooks" ).click(function() {
    $(".cat-options" ).css( "display","none" );
    $("#cat-lookbooks" ).css("display","block" );
  });

  $("#image-upload .cat-references" ).click(function() {
    $(".cat-options" ).css( "display","none" );
    $("#cat-references" ).css("display","block" );
  });
  $("#image-upload .cat-products" ).click(function() {
    $(".cat-options" ).css( "display","none" );
    $("#cat-products" ).css("display","block" );
  });


  $('#image-upload input[type=radio][name=ref-style]').change(function() {
    if (this.value == 'Celeb Style') {
      $(".ref-personality-content" ).css("display","none" );
    }
    else if (this.value == 'Personality') {
      $(".ref-personality-content" ).css("display","block" );
    }
  });
  /// page image-upload

  // image catalog
$("#image-catalog .cat-options" ).css( "display","none" );
  $('#image-catalog select.image-categories').change(function() {
    if (this.value == 'trends') {
      $(".cat-options" ).css( "display","none" );
      $("#cat-trends" ).css("display","block" );
    }
    else if (this.value == 'fashion-shows') {
      $(".cat-options" ).css( "display","none" );
      $("#cat-fashion-shows" ).css("display","block" );
    }
    else if (this.value == 'lookbooks') {
      $(".cat-options" ).css( "display","none" );
      $("#cat-lookbooks" ).css("display","block" );
    }
    else if (this.value == 'references') {
      $(".cat-options" ).css( "display","none" );
      $("#cat-references" ).css("display","block" );
    }
    else if (this.value == 'products') {
      $(".cat-options" ).css( "display","none" );
      $("#cat-products" ).css("display","block" );
    }
    else {
      $(".cat-options" ).css( "display","none" );
    }
  });

  $("#image-catalog .ref-style-personality" ).css("display","none" );
  $('#image-catalog select.ref-style').change(function() {
    if (this.value == 'celeb-style') {
      $(".ref-style-personality" ).css("display","none" );
    }
    else if (this.value == 'personality') {
      $(".ref-style-personality" ).css("display","block" );
    }
    else
    {  $(".ref-style-personality" ).css("display","none" );}
  });


// datatables
//  $("#example1").dataTable();
  /*$('#tbl-view-contacts').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
    });*/



});
