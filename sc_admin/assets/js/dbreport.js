$( document ).ready(function() {

  	$("#signupclick").click(function(){
    	//var product_id = $('#product_id').val();
    	if($('#signupreportdiv').length > 0)
    	{ 
    	  $('#signupreportsdata').html(''); 

    	}else{ 
    		signupreports();
    	}    
    });

	$("#productclick").click(function(){
		
		if($('#productreportdiv').length > 0)
		{ 
		  $('#productreportsdata').html(''); 

		}else{ 
			productreports();
		} 		
    	
    });

    $("#lookclick").click(function(){
		
		if($('#lookreportdiv').length > 0)
		{ 
		  $('#lookreportsdata').html(''); 

		}else{ 
			lookreports();
		} 	
    	
    });

     $("#brandclick").click(function(){
		
		if($('#brandreportdiv').length > 0)
		{ 
		  $('#brandreportsdata').html(''); 

		}else{ 
			brandreports();
		} 	
    	
    });

      $("#click").click(function(){		
		if($('#productclickreportdiv').length > 0)
		{ 
		  $('#productclickreportsdata').html(''); 

		}else{ 
			productclickreports();
		} 	
    	
    });
   
});


/* Get Sign Up Report */
function signupreports(){	
	var getUrl = window.location;
  	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	$.ajax({
			url: baseUrl+'/home/signupreports/',			
			type: 'post',
			data: { 'type' : 'signupreports' },
			success: function(data, status) {
				if(status == 'success'){
					$('#signupreportsdata').html(data);
				}
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
	});
}

/* Get Product Report */
function productreports(){	
	var getUrl = window.location;
  	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	$.ajax({
			url: baseUrl+'/home/productreports/',			
			type: 'post',
			data: { 'type' : 'productreports' },
			success: function(data, status) {
				if(status == 'success'){
					$('#productreportsdata').html(data);
				}
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
	});
}

/* Get Look Report */
function lookreports(){	
	var getUrl = window.location;
  	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	$.ajax({
			url: baseUrl+'/home/lookreports/',			
			type: 'post',
			data: { 'type' : 'lookreports' },
			success: function(data, status) {
				if(status == 'success'){
					$('#lookreportsdata').html(data);
				}
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
	});
}

/* Get Brand Report */
function brandreports(){	
	var getUrl = window.location;
  	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	$.ajax({
			url: baseUrl+'/home/brandreports/',			
			type: 'post',
			data: { 'type' : 'brandreports' },
			success: function(data, status) {
				if(status == 'success'){
					$('#brandreportsdata').html(data);
				}
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
	});
}


/* Get Product Click Report */
function productclickreports(){	
	var getUrl = window.location;
  	var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	$.ajax({
			url: baseUrl+'/home/clickreports/',			
			type: 'post',
			data: { 'type' : 'clickreports' },
			success: function(data, status) {
				if(status == 'success'){
					$('#productclickreportsdata').html(data);
				}
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
	});
}