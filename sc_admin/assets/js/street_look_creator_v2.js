	$(document).ready(function(e){
		
		$("body").addClass('sidebar-collapse'); 
			$(".step-content").hide();
                        $(".canvas-template-wrp").show();
                        $(".steps li").removeClass("active");
                        $(this).addClass("active");

		$(".steps .step1").click(function() {
			$(".step-content").hide();
			$(".canvas-template-wrp").show();
			$(".steps li").removeClass("active");
			$(this).addClass("active");
		});
		$(".steps .step2").click(function() {
			$(".step-content").hide();
			$(".canvas-products-wrp").show();
			$(".steps li").removeClass("active");
			$(this).addClass("active");
		});
		
		$(".canvas-template-wrp li").click(function() {			
			$(".canvas-template-wrp li").removeClass("active");
			$(this).addClass("active");
		});

			$("#cat").change(function(){
			var cat_id = $("#cat").val();
			$('#category_specific_attr').html('');
			$.ajax({
			url: 'lookcreator_v2/get_attributes',
			type: 'post',
			data: { 'cat_id': cat_id },
			success: function(data, status) { 	
				$('#category_specific_attr').append(data);
				$('#category_specific_attr').chosen();
			}
			});
			
			
		});
	
		$("#gender").change(function(){
			var gender = $("#gender").val(); 
			$('#cat').html('');
			$.ajax({
			url: 'lookcreator_v2/get_child_cat',
			type: 'post',
			data: { 'gender': gender },
			success: function(data, status) { 	
				$('#cat').append(data);
				$('#cat').trigger("chosen:updated");
			}
			});
			
			
		});
	
		
		$('.show-loading').hide();
		$('#sc_variations_value_type').hide();
		$('#sc_variations_type').hide();

		   
		$( "#getVariationVal" ).scroll(function() { alert('say');
			console.log('say');
		});	 
		LookCreatorDOMInit();
		LookCreatorInit();	

		/*$("#brand_combo").pqSelect({
                       bootstrap: { on: true },
                       multiplePlaceholder: 'Select Brands',
                       width: 200,
                    checkbox: true, //adds checkbox to options
                 }).on("change", function(evt) {
                    var val = $(this).val();
                   // console.log(val);
    	});*/

	});
	/*
 * LOOK CREATOR
 * Start Date: 18-Jun-2015
 * Author: Arun Arya
 * Version: v1.0
 * */

function msg(m) { console.log(m);}
//##VARIBALES (DOM) ====================================================
var intCurrentCategoryL2 = 0;
var strCurrentColor, strCurrentBrand, strCurrentPrice;

//## Initialize function for DOM function ==============================
function LookCreatorDOMInit() {
	
	//## SUB CATEGORY EVENT --------------------------------------------
$("#sub_cat").change(function() {	
		$('#getVariationVal').html('');	
		
		var sub_cat_id = $("#sub_cat").val();
		$.ajax({
			url: 'lookcreator/get_sub_cat',
			type: 'post',
			data: { 'type' : 'get_variation','sub_cat_id': sub_cat_id },
			success: function(data, status) { 	
				$('#getVariation').html(data);
			
				$('#getVariation li').on('click touch', function(e){
					localStorage.setItem("sub_cat_value", $(this).attr('id'));
					$('#getVariation li').removeClass('active');
					$(this).addClass('active');

					getvariationvalue_data($(this).attr('id'));
				    //getvariationtypes_data($(this).attr('id'));

					//ProductSubCatClick($(this).attr('id'));
					intCurrentCategoryL2 = $(this).attr('id');
					$('#sc_variations_value_type').html('');
					$('#sc_variations_type').html('');
					$('#sc_variations_value_type').show();
					$('#sc_variations_type').show();
					

				});
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
		});	
	});
	
	//## PICK COLOR FUNCTION ===========================================
	/* $('#colorSelector').ColorPicker({
		color: '#0000ff',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#colorSelector div').css('backgroundColor', '#' + hex);
			SetCanvasBgColor(hex);
		}
	});*/
	
	//## PICK BG IMAGE  ================================================
	$('.bg-container li').on('click', function(e){
		msg($(this).find('img').attr('src'));
		AddCanvasBgImage($(this).find('img').attr('src'));
	});
	
	
		
	//## BACK BUTTON EVENT ---------------------------------------------
	$('.backBtn').on('click touch', function(e){
		$('#getVariation #'+intCurrentCategoryL2).trigger('click');
		$('.search-container').hide();
	});
	
	//## COLOR COMBOBOX EVENT ------------------------------------------
	$('#cmb-color').on('change',  function(e){
		strCurrentColor = $(this).val();
	});
	
	//## PRICE COMBOBOX EVENT ------------------------------------------
	$('#cmb-price').on('change',  function(e){
		strCurrentPrice = $(this).val();
	});
	
	//## BRANDS COMBOBOX EVENT -----------------------------------------
	$('#cmb-brand').on('change',  function(e){
		strCurrentBrand = $(this).val();
	});
	
	//## DELETE BUTTON EVENT -------------------------------------------
	$('.delete-btn').on('click touch',  function(e){
		DeleteProduct();
	});
	
	//## DELETE ALL BUTTON EVENT ---------------------------------------
	$('.reset-btn').on('click touch',  function(e){
		ResetAll();
	});
	
	//## FLIP BUTTON EVENT ---------------------------------------------
	$('.flip-btn').on('click touch',  function(e){
		FlipObject();
	});
	
	//## FL0P BUTTON EVENT ---------------------------------------------
	$('.flop-btn').on('click touch',  function(e){
		FlopObject();
	});
	
	//## CLONE BUTTON EVENT --------------------------------------------
	$('.clone-btn').on('click touch',  function(e){
		CloneObject();
	});
	
	//## ADD TEXT BUTTON EVENT -----------------------------------------
	$('.add-text-btn').on('click touch',  function(e){
		AddText();
	});
	
	//## PUBLISH IMAGE EVENT -------------------------------------------
	$('.publish-btn').on('click touch',  function(e){
		PublishImage();
	});
	
	//## DELETE BG IMAGE EVENT -----------------------------------------
	$('.delete-bg-btn').on('click touch',  function(e){
		DeleteCanvasBgImage();
	});
	
	//## GET LOOK BTN EVENT --------------------------------------------
	$('.get-look-btn').on('click touch',  function(e){
		SaveLook();
	});
	
	//## SET LOOK BTN EVENT --------------------------------------------
	$('.set-look-btn').on('click touch',  function(e){
		ShowLook();
	});
	
	$('#color_combo').on('change', function(e){
		var vari_value = localStorage.getItem("sub_cat_value");
		//ProductSubCatClick(vari_value);
	});

	$('#brand_combo').on('change', function(e){
		var vari_value = localStorage.getItem("sub_cat_value");
		//ProductSubCatClick(vari_value);
	});	

	$('#tag_name').on('keyup', function(e){
		var vari_value = localStorage.getItem("sub_cat_value");
		//ProductSubCatClick(vari_value);
	});	

	$('#sc_variations_value_type').on('change', function(e){ 
		localStorage.setItem("_variations_type", $(this).attr('id'));
		var sub_cat_value = localStorage.getItem("sub_cat_value");
		var vari_value = localStorage.getItem("sub_cat_value");

		getvariationtypes_data(sub_cat_value,$(this).val());
		//ProductSubCatClick(vari_value);
	});	

	$('#sc_variations_type').on('change', function(e){ 
		var vari_value = localStorage.getItem("sub_cat_value");
		//ProductSubCatClick(vari_value);
	});	
	
	$("#show_products").on('click', function(e){ 
		var vari_value = localStorage.getItem("sub_cat_value"); 
		$('#getVariationVal').html('');
		ProductSubCatClick(vari_value);
	});
	
	
	
	
}

//## Product Categories Click ==========================================
function ProductCategoriesClick(id) {
	 $.ajax({
		   url: 'lookcreator/get_variation_type',
		   type: 'post',
		   data: { 'type' : 'get_variation_type','id': id },
		   success: function(data, status) {
				$('#getVariationVal').html(data);
				$('#getVariationVal li:not(".btn-load-more") img').on('click touch', function(e){
					var page = $(this).attr("data-page");
					localStorage.setItem("variation_value", $(this).attr('id'));
					//ProductSubCatClick($(this).attr('id'),page);
				});				
		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('ProductCategoriesClick func..');
}


function getvariationtypes_data(id,variations_type){
	$('#lookoffset').remove();
	$('#getVariationVal').html('');
	$.ajax({
		   url: 'lookcreator/get_variation_type',
		   type: 'post',
		   data: { 'type' : 'get_variation_type','id': id,'variations_type':variations_type },
		   success: function(data, status) { 
				
				$('#sc_variations_type').html(data);
				//$( '#sc_variations_type').pqSelect( "destroy" );
				/*$("#sc_variations_type").pqSelect({
		                    bootstrap: { on: true },
		                    multiplePlaceholder: 'Select Variations',
		                    checkbox: true, //adds checkbox to options
				        }).on("change", function(evt) {
				        	var val = $(this).val();
				                   // console.log(val);
				    	});		*/

		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('ProductCategoriesClick func..');
}

function getvariationvalue_data(id){ 
	$('#lookoffset').remove();
	$('#getVariationVal').html('');
	$.ajax({
		   url: 'lookcreator/sc_variations_value_type',
		   type: 'post',
		   data: { 'type' : 'sc_variations_value_type','id': id },
		   success: function(data, status) {
		
				$('#sc_variations_value_type').html(data);
				/* $("#sc_variations_value_type").pqSelect({
		                    bootstrap: { on: true },
		                    multiplePlaceholder: 'Select Variations Type',
		                    checkbox: true, //adds checkbox to options
				        }).on("change", function(evt) {
				        	var val = $(this).val();
				                   // console.log(val);
				    	});				*/	
		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('ProductCategoriesClick func..');
}

//## Product SubCat Click ==============================================
/*function ProductSubCatClick(id){ 
	var color = '';
	var brand = $('#brand_combo').val();
	var tags = $('#tag_name').val();
	var variations_type = $('#sc_variations_type').val(); 
	var sc_variations_value_type = $('#sc_variations_value_type').val();
	if($('#lookoffset').length > 0) { var lookoffset = $('#lookoffset').val(); } else { var lookoffset = '0'; }
	
		$(".page-overlay").show();
    	$(".page-overlay .loader").css('display','inline-block');
	$.ajax({
		   url: 'lookcreator/get_product',
		   type: 'post',
		   data: { 'type' : 'get_product','id': id,'color':color,'brand':brand,'tags':tags,'variations_type':variations_type,'sc_variations_value_type':sc_variations_value_type,'lookoffset':lookoffset },
		   success: function(data, status) { 
		   		$(".page-overlay").hide();
    			$(".page-overlay .loader").hide();
				$('#load_more').parent().remove();
    			if(lookoffset == 0){
    				$('#getVariationVal').html(data);
    			}else{
    				$('#getVariationVal').append(data);
    			}
				
				lookoffset = parseInt(lookoffset) + 1;	
				$('#lookoffset').val(lookoffset);				
				
				jQuery("img.lazy").lazy({
        			 beforeLoad: function(element) {
					        element.removeClass("lazy");
					    },
					    afterLoad: function(element) { 
					        element.removeClass("loading").addClass("loaded"); 
					    },
					    onError: function(element) {
					        console.log("image loading error: " + element.attr("src"));
					    },
					    onFinishedAll: function() {
					        console.log("finished loading all images");
					    },
    			});

				$('#getVariationVal li:not(".btn-load-more") img').on('click touch', function(e){ 
					AddProduct($(this).attr('id'), $(this).attr('data-img-link'));	
					e.stopPropagation();
				});	
				$('#load_more').click(function() {		
			 		var vari_value = localStorage.getItem("sub_cat_value"); 
					ProductSubCatClick(vari_value);
				});

				/*$('#getVariationVal li').on('click touch', function(e){ 
					AddProduct($(this).find('img').attr('id'), $(this).find('img').attr('src'));	
					e.stopPropagation();
				});	*/

		   /*},
		   error: function(xhr, desc, err) {
				   console.log(err);
		   }
	 });
	
	 msg('ProductSubCatClick func..');
	 $('.search-container').show();
}*/

function ProductSubCatClick(id){ 
	
	var brand = $('#brand_combo').val();
	var cat = $('#cat').val();
	var attributes_chosen = $('#attributes').val(); 
	var category_specific_attr = $('#category_specific_attr').val();
	if($('#lookoffset').length > 0) { var lookoffset = $('#lookoffset').val(); } else { var lookoffset = '0'; }
	/*if(brand!='' ){*/
	$(".page-overlay").show();
    $(".page-overlay .loader").css('display','inline-block');
    if(cat > 0){
	$.ajax({
		   url: 'lookcreator_v2/get_product_v2',
		   type: 'post',
		   data: { 'type' : 'get_product','brand':brand,'cat':cat,'attributes_chosen':attributes_chosen,'category_specific_attr':category_specific_attr,'lookoffset':lookoffset },
		   success: function(data, status) { 

		   		$(".page-overlay").hide();
    			$(".page-overlay .loader").hide();
    			$('#load_more').parent().remove();
    			//if((lookoffset == 0 || data == '<li class="cat">No record found </li>') && (id!=1)){
    			if((id=='0')){
    				$('#getVariationVal').html(data);
    			}else{
    				$('#getVariationVal').append(data);
    			}
				
				lookoffset = parseInt(lookoffset) + 1;	
				$('#lookoffset').val(lookoffset);		
				
				//alert("loading.....");
				
				// if(data!="")
				// {
					$(".page-overlay").hide();	
					$(".page-overlay .loader").hide();
					//$('#getVariationVal').html(data);			
				// }
				
				
				jQuery("img.lazy").lazy({
        			 beforeLoad: function(element) {
					        element.removeClass("lazy");
					    },
					    afterLoad: function(element) { 
					        element.removeClass("loading").addClass("loaded"); 
					    },
					    onError: function(element) {
					        console.log("image loading error: " + element.attr("src"));
					    },
					    onFinishedAll: function() {
					    		
					        console.log("finished loading all images");
					    },
    			});

				$('#getVariationVal li:not(".btn-load-more") img').on('mousedown touch', function(e){
					AddProduct($(this).attr('id'), $(this).attr('data-img-link'));	
					e.stopPropagation();
				});	
				$('#load_more').click(function() {		
			 		 
					ProductSubCatClick(1);
				});
				/*$('#getVariationVal li').on('click touch', function(e){ 
					AddProduct($(this).find('img').attr('id'), $(this).find('img').attr('src'));	
					e.stopPropagation();
				});	*/

		   },
		   error: function(xhr, desc, err) {
				   console.log(err);
		   }
	 });
	}else{
		alert('Please Select Category');
		$(".page-overlay").hide();
    	$(".page-overlay .loader").css('display','none');
	}	
	/*}else{
		alert('Please Select Brand');
	}*/
	 msg('ProductSubCatClick func..');
	 $('.search-container').show();
}



//## Product Click =====================================================
function ProductClick(id, src){
	msg('ProductClick func..'+id);
	//AddProduct(id, src);

}

//##VARIBALES (Canvas) =================================================
var objBaseFbCanvas;
var objPickedForFbCanvas; 
var isObjOnStageArea = false;
var isObjAddedOnStageArea = false;
var intObjCtr = 0;

//##INITIALIZE FUNCTION ================================================
function LookCreatorInit(){
	
}

//##DETECTING STAGE AREA OF CANVAS =====================================
/*

$(document).on('mousemove',function(e){
	var objCanvas = $('.canvas-container');
	var intLeftCanvas = objCanvas.offset().left;
	var intTopCanvas = objCanvas.offset().top;
	var intWDCanvas = intLeftCanvas+objCanvas.width();
	var intHTCanvas = intTopCanvas+objCanvas.height();
	
	if(DetectXPos()==true && DetectYPos()==true) {
		//msg("INSIDE");
		isObjAddedOnStageArea = true;
	}
	
	function DetectXPos(){
		if(e.pageX > intLeftCanvas && e.pageX < intWDCanvas) { return true;	}
	}
	function DetectYPos(){
		if(e.pageY > intTopCanvas && e.pageY < intHTCanvas) { return true;	}
	}		
});*/


/*
$(document).on('mouseup',function(e){
	if(isObjOnStageArea==true && isObjAddedOnStageArea==false){
		//AddObject(objPickedForFbCanvas);
		//isObjAddedOnStageArea=true;
	}
});*/
//##OBJECT EVENTS ======================================================
function AllObjectEvents(){
	//##Object on selected 
    objBaseFbCanvas.on("object:selected", function(options) {
		options.target.bringToFront();
		
		// Only for Text objects
		if(options.target.get('type')=='i-text'){
			objTextSelected = options.target;
			ShowTextEditPanel();
		} else {
			HideTextEditPanel();
		}
		
	});
	//##Object on Rotating 
    objBaseFbCanvas.on("object:rotating", function(options) {
		//msg("Rotating");
	});
	//##Object on Scaling 
    objBaseFbCanvas.on("object:scaling", function(options) {
		//msg("Scaling");
	});
	//##Object on Moving 
    objBaseFbCanvas.on("object:moving", function(options) {
		//msg("Moving");
		var el = options.target;	
		//##
		if(el.left<=5) { el.left = 5; }
		if(el.top<=5) { el.top=5; }
		//##
		intLeftObjLimit = objBaseFbCanvas.width - el.getBoundingRectWidth() / 2;
		intTopObjLimit = objBaseFbCanvas.height - el.getBoundingRectHeight() / 2;
		if(el.left>=intLeftObjLimit) { el.left = intLeftObjLimit; }		
		if(el.top>=intTopObjLimit) { el.top = intTopObjLimit; }
	});
	
	//##Object on Mouse Up 
    objBaseFbCanvas.on("mouse:up", function(options) {
		msg("Mouse Up");
		if(!objBaseFbCanvas.getActiveObject()){
			HideTextEditPanel();
		}
	});
	
	//##Object on Mouse Down 
    objBaseFbCanvas.on("mouse:down", function(options) {
		//msg("Mouse Down");
	});
	
}

//##BASE CANVAS OBJECT EVENTS ==========================================
function BaseObjEvents() {

	objBaseFbCanvas.on("mouse:up", function(e){
		//msg(e);
	});

	
	//##Disable multisection objects -----------------------------------
	objBaseFbCanvas.selection = false;
}
//##ADD LOGO TO CANVAS =================================================
function AddLogo() {
	fabric.Image.fromURL('assets/images/sc-logo.jpg', function(oImg) {
		oImg.set({
			hasControls:false,
			left:405,
			top:5,
			lockMovementX: 400,
			lockMovementY: 80,
			hasBorders:false
		});
		objBaseFbCanvas.add(oImg);
	});	
}

//##ADD OBJECTS TO CANVAS ==============================================
function AddProduct(id, src){
	// Direct reference of image path
	if(id){
	$('#recommended_products').append('<li id=recommended_'+id+'><img src="'+src+'"><a class="btn-prod-remove" href="javascript:void(0);" onclick="delete_img('+id+')"><i class="fa fa-times-circle-o"></i></a></li>');
	}
ShowList();
	
}


function delete_img(id){
	$('#recommended_'+id).remove();
}


function ShowList () {
setTimeout(function(){

$('#recommended_products li').each(function(e){
console.log($(this).attr('id'));

});

console.log("----------------------");
 },5000);

}

function save_street_look(){
	var street_canvas_img_id = $('#street_canvas_img_id').val();
	var look_name = $('#look_name').val();
	var look_desc = $('#look_desc').val();
	var look_tags = $('#look_tags').val();
	var look_type = $('input[name=look_type]:checked').val();
	var look_products = [];
	$('#recommended_products li').each(function(e){
		look_products.push($(this).attr('id'));
	});
	if(look_products.length<=0){
		alert("Please select atleast one recommended product.");
		$('#error_text').html('Please select atleast one recommended product.');
	}else if(street_canvas_img_id.trim() == ''){
		alert('Please select the Street Style look image');
		$('#error_text').html('Please select the Street Style look image');
	}else if(look_name.trim() == ''){
		alert('Please select the look name');
		$('#error_text').html('Please select the look name');
	}else if(look_desc.trim() == ''){
		alert('Please enter look description');
		$('#error_text').html('Please enter look description');
	}else if(look_type.trim() == ''){
		alert('Please enter look type');
		$('#error_text').html('Please enter look type');
	}else if(look_tags.trim() == ''){
		alert('Please enter look tag');
		$('#error_text').html('Please enter look tag');
	}
	else{
	
		 $.ajax({
                   url: 'Lookcreator/save_look',
                   type: 'post',
                   data: { 'type' : 'save_streeet_look','street_canvas_img_id': street_canvas_img_id,'look_name':look_name,'look_desc':look_desc,'look_products':look_products,'look_type':look_type,'look_tags':look_tags },
                   success: function(data, status) {
					//location.reload();
					alert("Done");
					window.location.href="lookcreator/manage_look";
                   },
                   error: function(xhr, desc, err) {
                   }
                 });	
	}

}

//## DELETE OBJECT =====================================================
window.onkeydown = onKeyDownHandler;

function onKeyDownHandler() {
	//msg(e.keyCode);
  /* switch (e.keyCode) {
	
      case 46: // delete
         var activeObject = canvas.getActiveObject();
         if (!activeObject) canvas.remove(activeObject);
            
         return;
   }*/
};

//## DELETE OBJECT =====================================================
function DeleteProduct(){
	if(objBaseFbCanvas.getActiveObject()) {
		objBaseFbCanvas.remove(objBaseFbCanvas.getActiveObject());
	}
}

//## RESET THE BASE ====================================================
function ResetAll(){
	//objBaseFbCanvas.clear().renderAll();
	objBaseFbCanvas.forEachObject(function(obj){
		if(obj.id){ obj.remove(); }
	});
	objBaseFbCanvas.setBackgroundColor('#fff');
	DeleteCanvasBgImage();
	objBaseFbCanvas.renderAll();
	msg("Reset all");
}

//## FLIP OBJECT ======================================================= 
function FlipObject(){
	var object = objBaseFbCanvas.getActiveObject();
	if(objBaseFbCanvas.getActiveObject()) {
		if(!object.get('flipX')) {
			object.set('flipX', true);
		} else {
			object.set('flipX', false);
		}
		objBaseFbCanvas.renderAll();
	}
}

//## FLOP OBJECT ======================================================= 
function FlopObject(){
	var object = objBaseFbCanvas.getActiveObject();
	if(objBaseFbCanvas.getActiveObject()) {
		if(!object.get('flipY')) {
			object.set('flipY', true);
		} else {
			object.set('flipY', false);
		}
		objBaseFbCanvas.renderAll();
	}
}

//## CLONE OBJECT ======================================================
function CloneObject() {
	var obj = objBaseFbCanvas.getActiveObject();
	var intLeft = obj.get('left')+20;
	var intTop = obj.get('top')+20;
	
	// Discard object from canvas stage
	objBaseFbCanvas.discardActiveObject();
	objBaseFbCanvas.renderAll(); 
	
	// Creating a clone and setActive
	var clone = fabric.util.object.clone(obj);
	clone.set({left: intLeft,top: intTop});
	objBaseFbCanvas.add(clone); 
	objBaseFbCanvas.setActiveObject(clone);
}


//## ADD TEXT OBJECT ===================================================
var objTextSelected;
function AddText() {
	intObjCtr++;
	var text = new fabric.IText('I love StyleCracker...', { 
		id: 'txt_'+intObjCtr,
		left: 100,
		top: 100
	});
	objBaseFbCanvas.add(text);
	
	//## Setting the properties to Canvas Object 
	SetPropertiesToNewObject(text);	
}

//## Show Text Edit Panel
function ShowTextEditPanel() {
	$('.text-properties').fadeIn()
}

//## Hide Text Edit Panel
function HideTextEditPanel() {
	$('.text-properties').fadeOut()
}

//## FONT SIZE =========================================================
/*$('.txt-size').on('change', function(e){
	if(objBaseFbCanvas.getActiveObject()) {
		objTextSelected.setFontSize($(this).val());
		objBaseFbCanvas.renderAll(); 
	}
});*/

//## FONT FAMILY =======================================================
$('.cmb-font').on('change', function(e){
	if(objBaseFbCanvas.getActiveObject()) {
		objTextSelected.setFontFamily($(this).val());
		objBaseFbCanvas.renderAll(); 
	}
});

//## FONT COLOR ========================================================
$('.cmb-color').on('change', function(e){
	if(objBaseFbCanvas.getActiveObject()) {
		objTextSelected.setColor($(this).val());
		objBaseFbCanvas.renderAll(); 
	}
});

//## TEXT SHADOW =======================================================
$('.cmb-shadow').on('change', function(e){
	if(objBaseFbCanvas.getActiveObject()) {
		if($(this).val()=='Show'){
			objTextSelected.setShadow('2px 2px 2px rgba(0,0,0,0.5)');
		} else {
			objTextSelected.setShadow('none');
		}
		
		
		objBaseFbCanvas.renderAll(); 
	}
});

//## BG COLOR  =========================================================
function SetCanvasBgColor(color){
	objBaseFbCanvas.setBackgroundColor('#'+color);
	objBaseFbCanvas.renderAll(); 
}

//## BG IMAGE  =========================================================
function AddCanvasBgImage (src){
	objBaseFbCanvas.setBackgroundImage(src, objBaseFbCanvas.renderAll.bind(objBaseFbCanvas), {
	backgroundImageOpacity: 0.5,
	backgroundImageStretch: true
	});
	objBaseFbCanvas.renderAll(); 
}

function DeleteCanvasBgImage(){
	objBaseFbCanvas.setBackgroundImage('', objBaseFbCanvas.renderAll.bind(objBaseFbCanvas), { 
		backgroundImageOpacity: 0.5, 
		backgroundImageStretch: false 
	});
	objBaseFbCanvas.renderAll();
}
//## CONVERT CANVAS TO IMAGE ===========================================
function PublishImage(){
	msg("publish img");	
	// Passing the image data to php -----------------------------------	
	var dataURL = objBaseFbCanvas.toDataURL('image/png'); 
	var ajax = new XMLHttpRequest();
	ajax.open("POST",'upload_img.php',false);
	ajax.setRequestHeader('Content-Type', 'application/upload');
	ajax.send(dataURL);
}

//## APPLY TO ALL OBJECTS WHILE ADDING ON CANVAS =======================
function SetPropertiesToNewObject(obj) {
	obj.set({
			borderColor: 'red',
			cornerColor: 'green',
			lockUniScaling: true,
			cornerSize: 12,
			hoverCursor: 'pointer',
			minScaleLimit: .1,
			strokeLineCap: 'round',
			transparentCorners: false,
			intersecting: true,
			rotatingPointOffset:30,
		});
	
	objBaseFbCanvas.setActiveObject(obj);
	
	//## Resize object propotionally while object is bigger than stage--
	ResizeObjPropotionally(obj);
	
}

//## PROPOTIONAL RESIZE OF THE OBJECT ON CANVAS ========================
function ResizeObjPropotionally(obj){
	var baseHT = objBaseFbCanvas.getHeight();
	var baseWD = objBaseFbCanvas.getWidth();
	var objTH = obj.getHeight();
	var objWD = obj.getWidth();
	if(objTH > baseHT || objWD > baseWD){
		var resizeObj = calculateAspectRatioFit(obj.getWidth(), obj.getHeight(), baseWD, baseHT);
		obj.set({
			width: resizeObj.width,
			height: resizeObj.height
		});
		objBaseFbCanvas.renderAll();
	}
}

// ## RATION WISE SCALING ==============================================
function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return { width: srcWidth*ratio, height: srcHeight*ratio };
 }






// ## GET ALL OBJECT DETAILS ===========================================
function SaveLook(){
	var storedLook = JSON.stringify(objBaseFbCanvas.toJSON());
	var tagsForLook = $('.tag-txt').val();
}

var storedLook = '{"objects":[{"type":"image","originX":"left","originY":"top","left":405,"top":5,"width":125,"height":66,"fill":"rgb(0,0,0)","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"round","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":1,"scaleY":1,"angle":0,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","src":"http://localhost/site/images/sc-logo.jpg","filters":[],"crossOrigin":"","alignX":"none","alignY":"none","meetOrSlice":"meet"},{"type":"i-text","originX":"left","originY":"top","left":24.89,"top":398.94,"width":258,"height":52.43,"fill":"Green","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"round","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":1,"scaleY":1,"angle":343.4,"flipX":false,"flipY":false,"opacity":1,"shadow":{"color":"rgba(0,0,0,0.5)","blur":2,"offsetX":2,"offsetY":2},"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","text":"My first look...","fontSize":40,"fontWeight":"normal","fontFamily":"Georgia","fontStyle":"","lineHeight":1.16,"textDecoration":"","textAlign":"left","textBackgroundColor":"","styles":{"0":{"1":{},"2":{},"3":{},"4":{},"5":{},"6":{},"7":{},"8":{},"9":{},"10":{},"11":{},"12":{},"13":{},"14":{},"15":{},"16":{},"17":{},"18":{},"19":{},"20":{},"21":{},"22":{},"23":{},"24":{},"25":{},"26":{},"27":{}}}},{"type":"image","originX":"left","originY":"top","left":346.94,"top":103.05,"width":208.79,"height":530,"fill":"rgb(0,0,0)","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"round","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":0.7,"scaleY":0.7,"angle":12.94,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","src":"http://localhost/site/assets/products/ForeverNew_HANNAH-ZIP-CROP-LEG-SKINNY-JEAN_Rs4000.png","filters":[],"crossOrigin":"","alignX":"none","alignY":"none","meetOrSlice":"meet"},{"type":"i-text","originX":"left","originY":"top","left":22.25,"top":258.93,"width":40,"height":52.43,"fill":"Red","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"round","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":2.13,"scaleY":2.13,"angle":343.13,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","text":"hii","fontSize":40,"fontWeight":"normal","fontFamily":"Arial","fontStyle":"","lineHeight":1.16,"textDecoration":"","textAlign":"left","textBackgroundColor":"","styles":{"0":{"1":{},"2":{},"3":{}}}},{"type":"image","originX":"left","originY":"top","left":86.32,"top":110.97,"width":280,"height":389,"fill":"rgb(0,0,0)","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"round","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":0.55,"scaleY":0.55,"angle":338.16,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","src":"http://localhost/site/assets/products/topshop-cross-back-halter-crop-top-rs-622.png","filters":[],"crossOrigin":"","alignX":"none","alignY":"none","meetOrSlice":"meet"},{"type":"image","originX":"left","originY":"top","left":125.86,"top":431.91,"width":284,"height":200,"fill":"rgb(0,0,0)","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"round","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":0.47,"scaleY":0.47,"angle":339.88,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","src":"http://localhost/site/assets/products/Forever21_ChainStrapCrossbody_1299.png","filters":[],"crossOrigin":"","alignX":"none","alignY":"none","meetOrSlice":"meet"}],"background":"#26eb29","backgroundImage":{"type":"image","originX":"left","originY":"top","left":0,"top":0,"width":1300,"height":1300,"fill":"rgb(0,0,0)","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"butt","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":1,"scaleY":1,"angle":0,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","src":"http://localhost/site/assets/backgrounds/bg-2.jpg","filters":[],"crossOrigin":"","alignX":"none","alignY":"none","meetOrSlice":"meet"}}';


// ## SHOWS THE LOOK BY JSON STORAGE ===================================
function ShowLook(){
	objBaseFbCanvas.loadFromJSON(storedLook);
	objBaseFbCanvas.renderAll();
	setTimeout(function(){ SetPropertiesToObjects(); },100);
}

function SetPropertiesToObjects(){
	for(var i=0; i<objBaseFbCanvas.getObjects().length; i++){
		objBaseFbCanvas.item(i).set({
			borderColor: 'red',
			cornerColor: 'green',
			lockUniScaling: true,
			cornerSize: 12,
			hoverCursor: 'pointer',
			minScaleLimit: .1,
			strokeLineCap: 'round',
			transparentCorners: false,
			intersecting: true,
			rotatingPointOffset:30,
		});
		if(objBaseFbCanvas.item(i).get('width')==125 && objBaseFbCanvas.item(i).get('height')==66){
			objBaseFbCanvas.item(i).set({
				hasControls:false,
				left:405,
				top:5,
				lockMovementX: 400,
				lockMovementY: 80,
				hasBorders:false
			});
		}
	}
}



