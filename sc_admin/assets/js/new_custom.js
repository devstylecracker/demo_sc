$(document).ready(function() {

  //Initialize Select2 Elements
$(".select2").select2({  placeholder: "Select Tags",   allowClear: true});
	
	//Initialize datepicker Elements
	$("#date_from").datepicker({ format: "yyyy-mm-dd", endDate: '+500d', autoclose: true});
	$("#date_to").datepicker({ format: "yyyy-mm-dd", endDate: '+500d', autoclose: true });

  //image popup gallery
  $('.popup-gallery').magnificPopup({
    delegate: 'a', // child items selector, by clicking on it popup will open
    type: 'image',
      gallery:{
        enabled:true
      }
  });

});
