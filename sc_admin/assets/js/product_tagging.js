baseUrl='https://www.stylecracker.com/sc_admin';
$(document).ready(function(e){
	ProductTaggingDOMInit();


});

function msg(m) { console.log(m);}
//##VARIBALES (DOM) ====================================================
var intbrand = 0;
var arrProductID = [];

//## Initialize function for DOM function ==============================
function ProductTaggingDOMInit() {

	$("#show_products").on('click', function(e){ 		
		ProductList();		
	});

	//##

	$('.product-selectAll').on('click', function(e){
		ProductSelectAll();
	});

	$('.product-deselectAll').on('click', function(e){
		ProductDelselectAll();
	});

	$('.product-publish').on('click', function(e){
		GetProductID();
		ProductDelselectAll();//call this function on server side success else don't call
	});

	$('.tag-selectAll').on('click', function(e){
		TagSelectAll();
	});

	$('.tag-deselectAll').on('click', function(e){
		TagDelselectAll();
	});

	$("#store").on('change', function(e){ 	
		$('#myTags').tagit("destroy");
		$('#myTags').html('');
		$('#myTags').tagit();
		InitTags();	
		PageList();	

	});

	$('#myTags').tagit();

	$('#saveTags').on('click', function(e){		
		SaveProductTag();
	});

}

function msg(m) { console.log(m); }

function ProductList()
{
	msg("product list..");	
	var store = $('#store').val();
	var page = $('#page').val();
	var pendingprd = $('#showPendingPrd').attr('checked');
	//alert($('#showPendingPrd').attr('checked'));
	if($('#showPendingPrd').attr('checked'))
	{
		pendingprd ='true';
	}else
	{
		pendingprd ='false';
	}
	//alert(pendingprd);
	
 	if(store!='')
 	{
		$.ajax({
			   url: 'Products_tagging/getproduct/'+store+'/'+page,
			   type: 'post',
			   data: { 'type' : 'product', pendingprd : '' },
			   success: function(data, status) {
					$('#product_list').html(data);	
					ProductSelectDeselect();				

			   },
			   error: function(xhr, desc, err) {
					console.log(err);
			   }
		});
	}else
	{
		alert('Please select the store');
	}
	msg('ProductList func..');
	
}

function ProductTagList()
{	
	msg("product tag list..");
	$('#myTags').tagit("destroy");
				$('#myTags').html('');
				$('#myTags').tagit();
				 InitTags();
	
	$.ajax({
		   url: 'Products_tagging/getproductTags/',
		   type: 'post',
		   data: { 'type' : 'tags', 'product_ids' : JSON.stringify(arrProductID) },
		   success: function(data, status) {
		   		//alert(data);
				//$('#myTags').val(data);

				$('#myTags').tagit("destroy");
				$('#myTags').append(data);
				$('#myTags').tagit();
				InitTags();				
				 
		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('ProductTagList func..');	
}

//## Product Select/Deselect function ===============================
function ProductSelectDeselect(){
	$('#product_list li').on('click',function(e){
		if(!$(this).hasClass('selected')){
			$(this).addClass('selected');
		} else {
			$(this).removeClass('selected');
		}

		GetProductID();
	});

}

function ProductSelectAll(){
	$('#product_list li').each(function(e){
		$(this).addClass('selected');
	});

	GetProductID();
}


function ProductDelselectAll() {
	$('#product_list li').each(function(e){
		$(this).removeClass('selected');
	});

	GetProductID();
}


function GetProductID() {	
	arrProductID = [];
	$('#product_list li').each(function(e){
		if($(this).hasClass('selected')){

			$(this).find('img').attr('id');
			arrProductID.push($(this).find('img').attr('id'));
		}
	});
	msg(arrProductID);
	ProductTagList();
}

//## Tag Select/Deselect function ===============================
function TagSelectDeselect(){
	console.log('product_tag_list');
	$('#myTags li').on('click',function(e){

		if(!$(this).hasClass('selected')){
			$(this).addClass('selected');
		} else {
			$(this).removeClass('selected');
		}

		GetTagID();
	});
}

function TagSelectAll(){
	$('#myTags li').each(function(e){
		$(this).addClass('selected');
	});

	GetTagID();
}

function TagDelselectAll() {
	$('#myTags li').each(function(e){
		$(this).removeClass('selected');
	});

	GetTagID();
}

var arrTagID = [];
function GetTagID() {
	arrTagID = [];
	$('#myTags li').each(function(e){
		if($(this).hasClass('selected')){
			$(this).attr('id');
			arrTagID.push($(this).attr('id'));
		}
	});		

	msg(arrTagID);

}


function PageList()
{
	msg("page list..");	
	var store = $('#store').val();
	//var page = $('#page').val(); 	
	$.ajax({
		   url: 'Products_tagging/getpage/'+store,
		   type: 'post',
		   data: { 'type' : 'page' },
		   success: function(data, status) {		   
				$('#page').html(data);	
				$('#product_list').html('');				

		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('PageList func..');
	
}


function SaveProductTag()
{	
	msg("Save product tag..");
	var arrtags = $('#myTags').tagit('assignedTags');
	//alert(arrtags);

	if(arrProductID=='')
	{
		alert(" Please select the product(s) to add the tags(s).");
		return false;
	}

	if(arrtags=='')
	{
		alert(" Add Product Tag(s) ");
		return false;
	}
	
	$.ajax({
		   url: 'Products_tagging/saveproductTags/',
		   type: 'post',
		   data: { 'type' : 'savetags','tags' : $('#myTags').tagit('assignedTags'), 'products' : arrProductID },
		   success: function(data, status) {

		   	if(status == 'success')
			{
				$('#product_tag_list').html(data);
				alert("Product tag(s) are added sucessfully.");
				$('#myTags').tagit("destroy");
				$('#myTags').html('');
				$('#myTags').tagit();

			}else
			{
				alert('OOPS!something went wrong');
			}
				
		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('SaveProductTag func..');	
}


function publishProduct()
{
	msg("Publish product..");	

	var arrtags = $('#myTags').tagit('assignedTags');
	//alert(arrtags);

	if(arrProductID=='')
	{
		alert(" Please select the product(s) to Publish. ");
		return false;
	}

	if(arrProductID!='' && arrtags!='')
	{
		SaveProductTag();
	}else
	{
		alert(" Add product tag(s) and apply ");
		return false;
	}

	$.ajax({
		   url: 'Products_tagging/publishproduct/',
		   type: 'post',
		   data: { 'type' : 'publish', 'products' : arrProductID },
		   success: function(data, status) {
							
				if(status == 'success')
				{
					alert("Product Published Successfully");
					$('#myTags').tagit("destroy");
					$('#myTags').html('');
					$('#myTags').tagit();
					//var getUrl = window.location;
  					//var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
              		window.location = baseUrl+'/Products_tagging';  
				}else
				{
					 alert('OOPS!something went wrong');
				}			
				
		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('publishProduct func..');	
}