$(document).ready(function(e){
		var select_gender=0;
		$(".pa_gender").click(function(){
			var select_gender = $(this).val();
			if(select_gender == 1){
				$("#female_pa").removeClass("hide");
				$("#male_pa").addClass("hide");
			}else if(select_gender == 2){
				$("#female_pa").addClass("hide");
				$("#male_pa").removeClass("hide");
			}
		});

	});
	
	
function get_pa_answers(){
	var gender = $("#pa_gender").val(); 
	$.ajax({
		url: sc_backend_url+'Pa/get_question_ans',
		type: 'post',
		data: { 'gender': gender },
		success: function(data, status) { 	
		$('#pa_data').append(data);
		}
	});

}

function insert_pa(){
	var segment = document.getElementById("pa_popup_button").getAttribute("data-attr");
	// alert(segment);
	var pa_body_shape = new Array();
	var id = '';
	if (document.getElementById('pa_gender1').checked) {
		var select_gender = document.getElementById('pa_gender1').value;
		var id = '_f';
	}else if (document.getElementById('pa_gender2').checked){
		var select_gender = document.getElementById('pa_gender2').value;
		var id = '_m';
	}else{
		var select_gender = document.getElementById('pa_gender3').value;
	}
	var pa_body_shape = $('#pa_body_shape'+id).val();
	var pa_age = $('#pa_age'+id).val();
	var pa_budget = $('#pa_budget'+id).val();
	var pa_pref1 = $('#pa_pref1'+id).val();
	var pa_pref2 = $('#pa_pref2'+id).val();
	var pa_pref3 = $('#pa_pref3'+id).val();
	if(select_gender == '3'){
		var pa_body_shape = new Array("2","3","6","1","5","4","4697","4696","4699","4698","4695");
		var pa_age = new Array("4684");
		var pa_budget = new Array("4689");
		var pa_pref1 = new Array("14","13","15","1140","9","10","4702","4705","4700","4701","4704","4703");
		var pa_pref2 = new Array("14","13","15","1140","9","10","4702","4705","4700","4701","4704","4703");
		var pa_pref3 = new Array("14","13","15","1140","9","10","4702","4705","4700","4701","4704","4703");
	}
	
	if((pa_body_shape != null && pa_age != null && pa_budget != null && pa_pref1 != null && pa_pref2 != null && pa_pref3 != null) || select_gender == '3'){
		
		$.ajax({
		url: sc_backend_url+'Pa/get_answer_html',
		type: 'post',
		data: { 'pa_body_shape': pa_body_shape,'pa_age': pa_age,'pa_budget': pa_budget,'pa_pref1': pa_pref1,'pa_pref2': pa_pref2,'pa_pref3': pa_pref3,'gender': select_gender },
		success: function(data, status) { 	
			if(segment == 'add-collection' || segment == 'edit-collection' || segment == 'broadcast-event' || segment == 'broadcast-collection'){
				$('#pa_html').html('');
				$('#pa_html').append(data);
				$("#pa_body_shape").val(pa_body_shape);
				$("#pa_age").val(pa_age);
				$("#pa_budget").val(pa_budget);
				$("#pa_pref1").val(pa_pref1);
				$("#pa_pref2").val(pa_pref2);
				$("#pa_pref3").val(pa_pref3);
				$("#gender").val(select_gender);
			}else{
				alert('something went wrong please try again');
			}
			$( "#pa-popup").modal('hide');
		}
		});
		
	}else{
		alert('Please select compulsory fields');
	}
	
}