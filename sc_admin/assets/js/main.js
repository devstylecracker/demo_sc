// JavaScript Document
$("#sliderImgs").cycle({ 
    fx:     'scrollLeft',
    timeout: 3000, 
    next:   '#nextBtn', 
    prev:   '#prevBtn',
	pager:  '#sliderNav',
	pagerAnchorBuilder: function(idx, slide) { 
      				  return '<a href="#"></a>'; 
    			}  
});

$(".productListHolder li").live("mouseover mouseout",function(e){
		$elem  = $(this);
		if(e.type ==="mouseover")
		{
			$elem.find(".artDetails").stop().animate({bottom:0+'px'},{duration:300})
		}
		else if(e.type === "mouseout")
		{
			$elem.find(".artDetails").stop().animate({bottom:-33+'px'},{duration:300})
		}
		
	})
	
var loginMenuAdjust = {
		init:function(elemObj){
			$elem = elemObj.element;
			this.adjust($elem);
			},
		adjust:function(elem){
			$elem = elem;
			$parentElem = $elem.parent("li");
			var parWithd = $parentElem.width()*0.5;
			var elemWidth = $elem.width()*0.5;
			var left = parWithd - elemWidth;
			$elem.css({"left":left+'px'});
			}
	}
	
	loginMenuAdjust.init({element:$(".userMenu")});