	$(document).ready(function(e){
		
		$("body").addClass('sidebar-collapse');
		$(".steps .step1").click(function() {
			$(".step-content").hide();
			$(".canvas-template-wrp").show();
			$(".steps li").removeClass("active");
			$(this).addClass("active");
		});
		$(".steps .step2").click(function() {
			$(".step-content").hide();
			$(".canvas-products-wrp").show();

			$('.canvas-data').show();
			
			$(".steps li").removeClass("active");
			$(this).addClass("active");
		});
		$(".steps .step3").click(function() {
			$(".step-content").hide();
			$(".canvas-effects-wrp").show();

			$('.canvas-data').hide();
			$(".steps li").removeClass("active");
			$(this).addClass("active");
		});

		$(".steps .step4").click(function() {
			$(".step-content").hide();
			$(".canvas-effects-wrp-movable").show();

			$('.canvas-data').hide();
			$(".steps li").removeClass("active");
			$(this).addClass("active");
		});
		
		$(".canvas-template-wrp li").click(function() {			
			$(".canvas-template-wrp li").removeClass("active");
			$(this).addClass("active");
		});
		
		$('.show-loading').hide();
		$('#sc_variations_value_type').hide();
		$('#sc_variations_type').hide();
				   
		$( "#getVariationVal" ).scroll(function() { 
			console.log('say');
		});	 
		LookCreatorDOMInit();
		LookCreatorInit();	
		$("#brand_combo").pqSelect({
                       bootstrap: { on: true },
                       multiplePlaceholder: 'Select Brands',
                    checkbox: true, //adds checkbox to options
                 }).on("change", function(evt) {
                    var val = $(this).val();
                   // console.log(val);
    	});
	});

/*
 * LOOK CREATOR
 * Start Date: 18-Jun-2015
 * Author: Arun Arya
 * Version: v1.0
 * */

function msg(m) { console.log(m);}
//##VARIBALES (DOM) ====================================================
var intCurrentCategoryL2 = 0;
var strCurrentColor, strCurrentBrand, strCurrentPrice;

//## Initialize function for DOM function ==============================
function LookCreatorDOMInit() {
	
	//## SUB CATEGORY EVENT --------------------------------------------
	$("#sub_cat").change(function() {	
		$('#getVariationVal').html('');	
		
		var sub_cat_id = $("#sub_cat").val();
		$.ajax({
			url: 'lookcreator/get_sub_cat',
			type: 'post',
			data: { 'type' : 'get_variation','sub_cat_id': sub_cat_id },
			success: function(data, status) { 	
				$('#getVariation').html(data);
			
				$('#getVariation li').on('click touch', function(e){
					localStorage.setItem("sub_cat_value", $(this).attr('id'));
					$('#getVariation li').removeClass('active');
					$(this).addClass('active');

					getvariationvalue_data($(this).attr('id'));
				    //getvariationtypes_data($(this).attr('id'));

					//ProductSubCatClick($(this).attr('id'));
					intCurrentCategoryL2 = $(this).attr('id');
					$('#sc_variations_value_type').html('');
					$('#sc_variations_type').html('');
					$('#sc_variations_value_type').show();
					$('#sc_variations_type').show();
					

				});
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
		});	
	});
	
	//## PICK COLOR FUNCTION ===========================================
	$("#colorSelector").colorpicker().on('changeColor.colorpicker', function(event){
		SetCanvasBgColor(event.color.toHex());
	});
	 /*$('#colorSelector').colorpicker({
		color: '#0000ff',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#colorSelector div').css('backgroundColor', '#' + hex);
			SetCanvasBgColor(hex);
		}
	});*/
	
	//## PICK BG IMAGE  ================================================
	$('.bg-container li').on('click', function(e){
		msg($(this).find('img').attr('src'));
		AddCanvasBgImage($(this).find('img').attr('src'));
	});
			
	//## Movable Stickers
	$('.bg-container-movable li').on('click', function(e){		
			msg($(this).find('img').attr('src'));		
			AddCanvasBgImageMovable($(this).find('img').attr('src'));		
		});
		
	//## BACK BUTTON EVENT ---------------------------------------------
	$('.backBtn').on('click touch', function(e){
		$('#getVariation #'+intCurrentCategoryL2).trigger('click');
		$('.search-container').hide();
	});
	
	//## COLOR COMBOBOX EVENT ------------------------------------------
	$('#cmb-color').on('change',  function(e){
		strCurrentColor = $(this).val();
	});
	
	//## PRICE COMBOBOX EVENT ------------------------------------------
	$('#cmb-price').on('change',  function(e){
		strCurrentPrice = $(this).val();
	});
	
	//## BRANDS COMBOBOX EVENT -----------------------------------------
	$('#cmb-brand').on('change',  function(e){
		strCurrentBrand = $(this).val();
	});
	
	//## DELETE BUTTON EVENT -------------------------------------------
	$('.delete-btn').on('click touch',  function(e){
		DeleteProduct();
	});
	
	//## DELETE ALL BUTTON EVENT ---------------------------------------
	$('.reset-btn').on('click touch',  function(e){
		ResetAll();
	});
	
	//## FLIP BUTTON EVENT ---------------------------------------------
	$('.flip-btn').on('click touch',  function(e){
		FlipObject();
	});
	
	//## FL0P BUTTON EVENT ---------------------------------------------
	$('.flop-btn').on('click touch',  function(e){
		FlopObject();
	});
	
	//## CLONE BUTTON EVENT --------------------------------------------
	$('.clone-btn').on('click touch',  function(e){
		CloneObject();
	});
	
	//## ADD TEXT BUTTON EVENT -----------------------------------------
	$('.add-text-btn').on('click touch',  function(e){
		AddText();
	});
	
	//## PUBLISH IMAGE EVENT -------------------------------------------
	$('.publish-btn').on('click touch',  function(e){		
		PublishLook();
	});
	
	//## DELETE BG IMAGE EVENT -----------------------------------------
	$('.delete-bg-btn').on('click touch',  function(e){
		DeleteCanvasBgImage();
	});
	
	//## GET LOOK BTN EVENT --------------------------------------------
	$('.get-look-btn').on('click touch',  function(e){
		SaveLook();
	});
	
	//## SET LOOK BTN EVENT --------------------------------------------
	$('.set-look-btn').on('click touch',  function(e){
		ShowLook();
	});
	
	//## FONT FAMILY ---------------------------------------------------
	$('.cmb-font').on('change', function(e){
		msg(objBaseFbCanvas.getActiveObject());
		if(objBaseFbCanvas.getActiveObject()) {
			objTextSelected.setFontFamily($(this).val());
			objBaseFbCanvas.renderAll(); 
		}
	});

	//## FONT COLOR ----------------------------------------------------
	$('.cmb-color').on('change', function(e){
		if(objBaseFbCanvas.getActiveObject()) {
			objTextSelected.setColor($(this).val());
			objBaseFbCanvas.renderAll(); 
		}
	});

	//## TEXT SHADOW ---------------------------------------------------
	$('.cmb-shadow').on('change', function(e){
		if(objBaseFbCanvas.getActiveObject()) {
			if($(this).val()=='Show'){
				objTextSelected.setShadow('2px 2px 2px rgba(0,0,0,0.5)');
			} else {
				objTextSelected.setShadow('none');
			}
			objBaseFbCanvas.renderAll(); 
		}
	});	
	
	
	$('#color_combo').on('change', function(e){
		var vari_value = localStorage.getItem("sub_cat_value");
		//ProductSubCatClick(vari_value);
	});

	$('#brand_combo').on('change', function(e){
		var vari_value = localStorage.getItem("sub_cat_value");
		//ProductSubCatClick(vari_value);
	});	

	$('#tag_name').on('keyup', function(e){
		var vari_value = localStorage.getItem("sub_cat_value");
		//ProductSubCatClick(vari_value);
	});	

	$('#sc_variations_value_type').on('change', function(e){ 
		localStorage.setItem("_variations_type", $(this).attr('id'));
		var sub_cat_value = localStorage.getItem("sub_cat_value");
		var vari_value = localStorage.getItem("sub_cat_value");

		getvariationtypes_data(sub_cat_value,$(this).val());
		//ProductSubCatClick(vari_value);
	});	

	$('#sc_variations_type').on('change', function(e){ 
		var vari_value = localStorage.getItem("sub_cat_value");
		//ProductSubCatClick(vari_value);
	});	
	
	$("#show_products").on('click', function(e){ 
		var vari_value = localStorage.getItem("sub_cat_value"); 
		$('#getVariationVal').html('');
		ProductSubCatClick(vari_value);
	});
	
	
}

function getvariationtypes_data(id,variations_type){
	$('#lookoffset').remove();
	$('#getVariationVal').html('');
	$.ajax({
		   url: 'lookcreator/get_variation_type',
		   type: 'post',
		   data: { 'type' : 'get_variation_type','id': id,'variations_type':variations_type },
		   success: function(data, status) { 
				
				$('#sc_variations_type').html(data);
				//$( '#sc_variations_type').pqSelect( "destroy" );
				/*$("#sc_variations_type").pqSelect({
		                    bootstrap: { on: true },
		                    multiplePlaceholder: 'Select Variations',
		                    checkbox: true, //adds checkbox to options
				        }).on("change", function(evt) {
				        	var val = $(this).val();
				                   // console.log(val);
				    	});		*/

		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('ProductCategoriesClick func..');
}


function getvariationvalue_data(id){ 
	$('#lookoffset').remove();
	$('#getVariationVal').html('');
	$.ajax({
		   url: 'lookcreator/sc_variations_value_type',
		   type: 'post',
		   data: { 'type' : 'sc_variations_value_type','id': id },
		   success: function(data, status) {
		
				$('#sc_variations_value_type').html(data);
				/* $("#sc_variations_value_type").pqSelect({
		                    bootstrap: { on: true },
		                    multiplePlaceholder: 'Select Variations Type',
		                    checkbox: true, //adds checkbox to options
				        }).on("change", function(evt) {
				        	var val = $(this).val();
				                   // console.log(val);
				    	});				*/	
		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('ProductCategoriesClick func..');
}



//## Product Categories Click ==========================================
function ProductCategoriesClick(id) {
	$('#lookoffset').remove();
	$('#getVariationVal').html('');
	 $.ajax({
		   url: 'lookcreator/get_variation_type',
		   type: 'post',
		   data: { 'type' : 'get_variation_type','id': id },
		   success: function(data, status) {
				$('#getVariationVal').html(data);
				$('#getVariationVal li:not(".btn-load-more") img').on('mousedown touch', function(e){
					var page = $(this).attr("data-page");
					localStorage.setItem("variation_value", $(this).attr('id'));
					//ProductSubCatClick($(this).attr('id'),page);
				});				
		   },
		   error: function(xhr, desc, err) {
				console.log(err);
		   }
	});
	msg('ProductCategoriesClick func..');
}


//## Product SubCat Click ==============================================
function ProductSubCatClick(id){ 
	var color = '';
	var brand = $('#brand_combo').val();
	var tags = $('#tag_name').val();
	var variations_type = $('#sc_variations_type').val(); 
	var sc_variations_value_type = $('#sc_variations_value_type').val();
	if($('#lookoffset').length > 0) { var lookoffset = $('#lookoffset').val(); } else { var lookoffset = '0'; }
	/*if(brand!='' ){*/
	$(".page-overlay").show();
    $(".page-overlay .loader").css('display','inline-block');

	$.ajax({
		   url: 'lookcreator/get_product',
		   type: 'post',
		   data: { 'type' : 'get_product','id': id,'color':color,'brand':brand,'tags':tags,'variations_type':variations_type,'sc_variations_value_type':sc_variations_value_type,'lookoffset':lookoffset },
		   success: function(data, status) { 

		   		$(".page-overlay").hide();
    			$(".page-overlay .loader").hide();
    			$('#load_more').parent().remove();
    			if(lookoffset == 0){
    				$('#getVariationVal').html(data);
    			}else{
    				$('#getVariationVal').append(data);
    			}
				
				lookoffset = parseInt(lookoffset) + 1;	
				$('#lookoffset').val(lookoffset);		
				
				//alert("loading.....");
				
				// if(data!="")
				// {
					$(".page-overlay").hide();	
					$(".page-overlay .loader").hide();
					//$('#getVariationVal').html(data);			
				// }
				
				
				jQuery("img.lazy").lazy({
        			 beforeLoad: function(element) {
					        element.removeClass("lazy");
					    },
					    afterLoad: function(element) { 
					        element.removeClass("loading").addClass("loaded"); 
					    },
					    onError: function(element) {
					        console.log("image loading error: " + element.attr("src"));
					    },
					    onFinishedAll: function() {
					    		
					        console.log("finished loading all images");
					    },
    			});

				$('#getVariationVal li:not(".btn-load-more") img').on('mousedown touch', function(e){
					AddProduct($(this).attr('id'), $(this).attr('data-img-link'));	
					e.stopPropagation();
				});	
				$('#load_more').click(function() {		
			 		var vari_value = localStorage.getItem("sub_cat_value"); 
					ProductSubCatClick(vari_value);
				});
				/*$('#getVariationVal li').on('click touch', function(e){ 
					AddProduct($(this).find('img').attr('id'), $(this).find('img').attr('src'));	
					e.stopPropagation();
				});	*/

		   },
		   error: function(xhr, desc, err) {
				   console.log(err);
		   }
	 });
	/*}else{
		alert('Please Select Brand');
	}*/
	 msg('ProductSubCatClick func..');
	 $('.search-container').show();
}


//## Product Click =====================================================
function ProductClick(id, src){
	//msg('ProductClick func..'+id);
	//AddProduct(id, src);

}

//##VARIBALES (Canvas) =================================================
var objBaseFbCanvas;
var objPickedForFbCanvas; 
var isObjOnStageArea = false;
var isObjAddedOnStageArea = false;
var intObjCtr = 0;

//##INITIALIZE FUNCTION ================================================
function LookCreatorInit(){
	msg('Look Creator Init func...');
	
	//## Canvas functions ==============================================
	objBaseFbCanvas = new fabric.Canvas('canvas');  
	objBaseFbCanvas.setBackgroundColor('#fff');	
	objBaseFbCanvas.renderAll();
	
	AddLogo();
	BaseObjEvents(); //All base canvas object event
	AllObjectEvents(); //All object event function
}

//##DETECTING STAGE AREA OF CANVAS =====================================
/*

$(document).on('mousemove',function(e){
	var objCanvas = $('.canvas-container');
	var intLeftCanvas = objCanvas.offset().left;
	var intTopCanvas = objCanvas.offset().top;
	var intWDCanvas = intLeftCanvas+objCanvas.width();
	var intHTCanvas = intTopCanvas+objCanvas.height();
	
	if(DetectXPos()==true && DetectYPos()==true) {
		//msg("INSIDE");
		isObjAddedOnStageArea = true;
	}
	
	function DetectXPos(){
		if(e.pageX > intLeftCanvas && e.pageX < intWDCanvas) { return true;	}
	}
	function DetectYPos(){
		if(e.pageY > intTopCanvas && e.pageY < intHTCanvas) { return true;	}
	}		
});*/


/*
$(document).on('mouseup',function(e){
	if(isObjOnStageArea==true && isObjAddedOnStageArea==false){
		//AddObject(objPickedForFbCanvas);
		//isObjAddedOnStageArea=true;
	}
});*/
//##OBJECT EVENTS ======================================================
function AllObjectEvents(){
	//##Object on selected 
    objBaseFbCanvas.on("object:selected", function(options) {
		options.target.bringToFront();
		
		// Only for Text objects
		if(options.target.get('type')=='i-text'){
			objTextSelected = options.target;
			ShowTextEditPanel();
		} else {
			HideTextEditPanel();
		}
		
	});
	//##Object on Rotating 
    objBaseFbCanvas.on("object:rotating", function(options) {
		//msg("Rotating");
	});
	//##Object on Scaling 
    objBaseFbCanvas.on("object:scaling", function(options) {
		//msg("Scaling");
	});
	//##Object on Moving 
    objBaseFbCanvas.on("object:moving", function(options) {
		//msg("Moving");
		var el = options.target;	
		//##
		if(el.left<=5) { el.left = 5; }
		if(el.top<=5) { el.top=5; }
		//##
		intLeftObjLimit = objBaseFbCanvas.width - el.getBoundingRectWidth() / 2;
		intTopObjLimit = objBaseFbCanvas.height - el.getBoundingRectHeight() / 2;
		if(el.left>=intLeftObjLimit) { el.left = intLeftObjLimit; }		
		if(el.top>=intTopObjLimit) { el.top = intTopObjLimit; }
	});
	
	//##Object on Mouse Up 
    objBaseFbCanvas.on("mouse:up", function(options) {
		msg("Mouse Up");
		if(!objBaseFbCanvas.getActiveObject()){
			HideTextEditPanel();
		}
	});
	
	//##Object on Mouse Down 
    objBaseFbCanvas.on("mouse:down", function(options) {
		//msg("Mouse Down");
	});
	
}

//##BASE CANVAS OBJECT EVENTS ==========================================
function BaseObjEvents() {

	objBaseFbCanvas.on("mouse:up", function(e){
		//msg(e);
	});

	
	//##Disable multisection objects -----------------------------------
	objBaseFbCanvas.selection = false;
}
//##ADD LOGO TO CANVAS =================================================
function AddLogo() {
	fabric.Image.fromURL('assets/images/sc-logo.png', function(oImg) {
		oImg.set({
			hasControls:false,
			left:400,
			top:5,
			lockMovementX: 400,
			lockMovementY: 80,
			hasBorders:false
		});
		objBaseFbCanvas.add(oImg);
	});	
}

//##ADD OBJECTS TO CANVAS ==============================================
function AddProduct(id, src){
	// Direct reference of image path
	intObjCtr++;
	fabric.Image.fromURL(src, function(oImg) {
		oImg.set({ 
			id: 'img_'+intObjCtr+'-'+id,
			left:5,
			top:5
		});
		objBaseFbCanvas.add(oImg);
	  
		//## Setting the properties to Canvas Object -------------------
		SetPropertiesToNewObject(oImg);	
	});
	
	// Reference from DOM (working don't delete) 
	/*
	var imgElement = document.getElementById(id);
	var imgInstance = new fabric.Image(imgElement, {
		name: 'obj_'+id,
		left: 100,
		top: 100,
		opacity: 1
	});
	objBaseFbCanvas.add(imgInstance);
	
	//## Setting the properties to Canvas Object -----------------------
	SetPropertiesToNewObject(imgInstance);
	 */

	  /* Added by saylee for extra product */

	 if(id){
		$('#recommended_products').append('<li id=recommended_'+id+'><img src="'+src+'"><a class="btn-prod-remove" href="javascript:void(0);" onclick="delete_img('+id+')"><i class="fa fa-times-circle-o"></i></a></li>');
	}
		ShowList();
}

//## DELETE OBJECT =====================================================
window.onkeydown = onKeyDownHandler;

function onKeyDownHandler() {
	//msg(e.keyCode);
  /* switch (e.keyCode) {
	
      case 46: // delete
         var activeObject = canvas.getActiveObject();
         if (!activeObject) canvas.remove(activeObject);
            
         return;
   }*/
};

//## DELETE OBJECT =====================================================
function DeleteProduct(){
	if(objBaseFbCanvas.getActiveObject()) {
		objBaseFbCanvas.remove(objBaseFbCanvas.getActiveObject());
	}
}

//## RESET THE BASE ====================================================
function ResetAll(){
	//objBaseFbCanvas.clear().renderAll();
	objBaseFbCanvas.forEachObject(function(obj){
		if(obj.id){ obj.remove(); }
	});
	objBaseFbCanvas.setBackgroundColor('#fff');
	DeleteCanvasBgImage();
	objBaseFbCanvas.renderAll();
	msg("Reset all");
}

//## FLIP OBJECT ======================================================= 
function FlipObject(){
	var object = objBaseFbCanvas.getActiveObject();
	if(objBaseFbCanvas.getActiveObject()) {
		if(!object.get('flipX')) {
			object.set('flipX', true);
		} else {
			object.set('flipX', false);
		}
		objBaseFbCanvas.renderAll();
	}
}

//## FLOP OBJECT ======================================================= 
function FlopObject(){
	var object = objBaseFbCanvas.getActiveObject();
	if(objBaseFbCanvas.getActiveObject()) {
		if(!object.get('flipY')) {
			object.set('flipY', true);
		} else {
			object.set('flipY', false);
		}
		objBaseFbCanvas.renderAll();
	}
}

//## CLONE OBJECT ======================================================
function CloneObject() {
	var obj = objBaseFbCanvas.getActiveObject();
	var intLeft = obj.get('left')+20;
	var intTop = obj.get('top')+20;
	
	// Discard object from canvas stage
	objBaseFbCanvas.discardActiveObject();
	objBaseFbCanvas.renderAll(); 
	
	// Creating a clone and setActive
	var clone = fabric.util.object.clone(obj);
	clone.set({left: intLeft,top: intTop});
	objBaseFbCanvas.add(clone); 
	objBaseFbCanvas.setActiveObject(clone);
}


//## ADD TEXT OBJECT ===================================================
var objTextSelected;
function AddText() {
	intObjCtr++;
	var text = new fabric.IText('I love StyleCracker...', { 
		id: 'txt_'+intObjCtr,
		left: 100,
		top: 100
	});
	objBaseFbCanvas.add(text);
	
	//## Setting the properties to Canvas Object 
	SetPropertiesToNewObject(text);	
}

//## Show Text Edit Panel
function ShowTextEditPanel() {
	$('.text-properties').fadeIn()
}

//## Hide Text Edit Panel
function HideTextEditPanel() {
	$('.text-properties').fadeOut()
}

//## FONT SIZE =========================================================
/*$('.txt-size').on('change', function(e){
	if(objBaseFbCanvas.getActiveObject()) {
		objTextSelected.setFontSize($(this).val());
		objBaseFbCanvas.renderAll(); 
	}
});*/



//## BG COLOR  =========================================================
function SetCanvasBgColor(color){
	objBaseFbCanvas.setBackgroundColor(color);
	objBaseFbCanvas.renderAll(); 
}

//## BG IMAGE  =========================================================
function AddCanvasBgImage (src){
	objBaseFbCanvas.setBackgroundImage(src, objBaseFbCanvas.renderAll.bind(objBaseFbCanvas), {
	backgroundImageOpacity: 0.5,
	backgroundImageStretch: true
	});
	objBaseFbCanvas.renderAll(); 
}

//## Movable Stickers		
			
	function AddCanvasBgImageMovable (src){		
			intObjCtr++;		
		fabric.Image.fromURL(src, function(oImg) {		
			oImg.set({ 		
				id: 'movableimg_'+intObjCtr,		
				left:5,		
				top:5		
			});		
			objBaseFbCanvas.add(oImg);		
		  		
			//## Setting the properties to Canvas Object -------------------		
			SetPropertiesToNewObject(oImg);			
		});		
	}
	
function DeleteCanvasBgImage(){
	objBaseFbCanvas.setBackgroundImage('', objBaseFbCanvas.renderAll.bind(objBaseFbCanvas), { 
		backgroundImageOpacity: 0.5, 
		backgroundImageStretch: false 
	});
	objBaseFbCanvas.renderAll();
}
//## CONVERT CANVAS TO IMAGE ===========================================
function PublishLook(){
	msg("publish img");	
	var look_name = $('#look_name').val();
	var look_desc = $('#look_desc').val();
	var look_tags = $('#look_tags').val();
	//var product_id = GetAllProductID();

	var product_id = [];
	$('#recommended_products li').each(function(e){
		var proid = $(this).attr('id').replace('recommended_','');
		product_id.push(proid);
	});

	if(look_name.trim() == ''){
			alert("Please enter look name");
			$('#error_text').html('Please enter look name');
		}else if(look_desc.trim() == ''){
			alert("Please enter look Description");
			$('#error_text').html('Please enter look Description');
		}else if(product_id.length < 1){
			alert("Please add the product");
			$('#error_text').html('Please add the product');	
		}else if(look_tags.trim() == ''){
			alert("Please add the look tags");
			$('#error_text').html('Please add the look tags');	
		}else{
				// Passing the image data to php -----------------------------------	
					//alert(objBaseFbCanvas); alert(objBaseFbCanvas.toDataURL('image/png'));
					var dataURL = objBaseFbCanvas.toDataURL('image/png');
				/*	var ajax = new XMLHttpRequest();
					ajax.open("POST",'Lookcreator/save_stylist_look',false);
					ajax.setRequestHeader('Content-Type', 'application/upload');
					ajax.send(dataURL);*/
				$(".page-overlay").show();
   		 		$(".page-overlay .loader").css('display','inline-block');
				
					$.ajax({
			url: 'Lookcreator/save_stylist_look',
			type: 'post',
			data: { 'img': dataURL,'look_name' : look_name,'look_desc':look_desc,'product_id':product_id,'look_tags':look_tags},
			success: function(data, status) { 
				    $(".page-overlay").hide();
    				$(".page-overlay .loader").hide(); 	
				alert("Done");	
				$('#error_text').html('Done');	
				window.location.href="lookcreator/manage_look";
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
		});	
		}
	
	
}

//## APPLY TO ALL OBJECTS WHILE ADDING ON CANVAS =======================
function SetPropertiesToNewObject(obj) {
	obj.set({
			borderColor: 'red',
			cornerColor: 'green',
			lockUniScaling: true,
			cornerSize: 12,
			hoverCursor: 'pointer',
			minScaleLimit: .1,
			strokeLineCap: 'round',
			transparentCorners: false,
			intersecting: true,
			rotatingPointOffset:30,
		});
	
	objBaseFbCanvas.setActiveObject(obj);
	
	//## Resize object propotionally while object is bigger than stage--
	ResizeObjPropotionally(obj);
	
}

//## PROPOTIONAL RESIZE OF THE OBJECT ON CANVAS ========================
function ResizeObjPropotionally(obj){
	var baseHT = objBaseFbCanvas.getHeight();
	var baseWD = objBaseFbCanvas.getWidth();
	var objTH = obj.getHeight();
	var objWD = obj.getWidth();
	if(objTH > baseHT || objWD > baseWD){
		var resizeObj = calculateAspectRatioFit(obj.getWidth(), obj.getHeight(), baseWD, baseHT);
		obj.set({
			width: resizeObj.width,
			height: resizeObj.height
		});
		objBaseFbCanvas.renderAll();
	}
}

// ## RATION WISE SCALING ==============================================
function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return { width: srcWidth*ratio, height: srcHeight*ratio };
 }






// ## GET ALL OBJECT DETAILS ===========================================
function SaveLook(){
	var storedLook = JSON.stringify(objBaseFbCanvas.toJSON());
	var tagsForLook = $('.tag-txt').val();
	msg(storedLook);
}

var storedLook = '{"objects":[{"type":"image","originX":"left","originY":"top","left":405,"top":5,"width":125,"height":66,"fill":"rgb(0,0,0)","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"butt","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":1,"scaleY":1,"angle":0,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","src":"http://localhost/stylecracker/assets/images/sc-logo.jpg","filters":[],"crossOrigin":"","alignX":"none","alignY":"none","meetOrSlice":"meet"},{"type":"image","originX":"left","originY":"top","left":164.8,"top":26.25,"width":530,"height":397.5,"fill":"rgb(0,0,0)","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"round","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":0.61,"scaleY":0.61,"angle":32.87,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","src":"http://localhost/stylecracker/assets/products/32-flower-wallpaper.preview.jpg","filters":[],"crossOrigin":"","alignX":"none","alignY":"none","meetOrSlice":"meet"}],"background":"#fff"}';


// ## SHOWS THE LOOK BY JSON STORAGE ===================================
function ShowLook(){
	objBaseFbCanvas.loadFromJSON(storedLook);
	objBaseFbCanvas.renderAll();
	setTimeout(function(){ SetPropertiesToObjects(); },100);
}

function SetPropertiesToObjects(){
	for(var i=0; i<objBaseFbCanvas.getObjects().length; i++){
		objBaseFbCanvas.item(i).set({
			borderColor: 'red',
			cornerColor: 'green',
			lockUniScaling: true,
			cornerSize: 12,
			hoverCursor: 'pointer',
			minScaleLimit: .1,
			strokeLineCap: 'round',
			transparentCorners: false,
			intersecting: true,
			rotatingPointOffset:30,
		});
		if(objBaseFbCanvas.item(i).get('width')==125 && objBaseFbCanvas.item(i).get('height')==66){
			objBaseFbCanvas.item(i).set({
				hasControls:false,
				left:405,
				top:5,
				lockMovementX: 400,
				lockMovementY: 80,
				hasBorders:false
			});
		}
	}
}


// ## SHOWS ALL OBJECT IDS =============================================
function GetAllProductID(){
	var arrProductStored = [];
	for(var i=0; i<objBaseFbCanvas.getObjects().length; i++){
		if(objBaseFbCanvas.item(i).get('width')!=125 && objBaseFbCanvas.item(i).get('height')!=66){
			var str = objBaseFbCanvas.item(i).get('id');
			var splitFrom = str.indexOf('-')+1;
			arrProductStored.push(str.substring(splitFrom, str.length));
		}
	}
	return arrProductStored;
}

/* Added by saylee for extra products */

function ShowList () {
setTimeout(function(){

$('#recommended_products li').each(function(e){
console.log($(this).attr('id'));

});

console.log("----------------------");
 },5000);

}
function delete_img(id){
	$('#recommended_'+id).remove();
}