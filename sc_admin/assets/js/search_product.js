	$(document).ready(function(e){	
	$("#gender_n").change(function(){
		var gender = $("#gender_n").val(); 
		$('#child_cat').html('');
		$.ajax({
		url: sc_backend_url+'Collection/get_child_cat',
		type: 'post',
		data: { 'gender': gender },
		success: function(data, status) { 	
			$('#child_cat').append(data);
			$('#child_cat').trigger("chosen:updated");
		}
		});
		
		
	});
	
	$("#search_product").on('click', function(e){ 
		$(".page-overlay").show();
		$(".page-overlay.loader").css('display','inline-block');
		Search_product(0,'search');
	});
	
	$('.show-loading').hide();






});

function msg(m) { console.log(m);}

function Search_product(load_more_cat=0,source=''){ 
	var brand = $('#brand_id').val();
	var cat = $('#child_cat').val();
	var popup_tags = $('#popup_tags').val();
	var cart_id = $('#cart_id').val();
	//alert(popup_tags);
	if(source=='search'){ var offset = '0'; }else { var offset = $('#new_offset').val(); }
	$(".page-overlay").show();
    $(".page-overlay .loader").css('display','inline-block');
    if(cat > 0){
	
			/* start loader */
				 //$('.loaders').html('<img src="https://i.imgur.com/HvS8yF1.gif">');
				 $(".loader").show();
			/* end loader */

	$.ajax({
		   // url: sc_backend_url+'Collection/get_product_new',
		   url: sc_frontend_url+'Category/get_popup_products',
		   type: 'post',
		   data: { 'type' : 'get_product','brand':brand,'cat':cat,'offset':offset,'popup_tags':popup_tags,'cart_id':cart_id  },
		 success: function(data, status) {
			 //$('.loaders').css("display", "none");  //<--- hide loader
			 //alert(data);
			 $(".loader").fadeOut("slow");			 
				if((offset=='0')){
    				$('#product_list').html(data);
    			}else{
    				$('#product_list').append(data);
    			}
		   		//$(".page-overlay").hide();
    			//$(".page-overlay .loader").hide();
				if(offset >0){var load_more = offset-1;$("#load_more_"+load_more).hide();
				}
    			$('#load_more2').parent().remove();
				
				document.getElementById("new_offset").value = parseInt(offset)+1;
				
				$('#getVariationVal li:not(".btn-load-more") img').on('mousedown touch', function(e){
					AddProduct($(this).attr('id'), $(this).attr('data-img-link'));	
					e.stopPropagation();
				});	

		   },
		   error: function(xhr, desc, err) {
				   console.log(err);
		   }
	 });
	}else{
		alert('Please Select Category');
		$(".page-overlay").hide();
    	$(".page-overlay .loader").css('display','none');
	}	
	 $('.search-container').show();
}

function show_product(product_id,cart_id){
	strSKUSelected ="";
	$(".products-list .active").removeClass("active");
	$('#product_img'+product_id).addClass('active');
	$(".loadproduct").show();
	$.ajax({
		url: sc_backend_url+'Collection/get_single_product',
		type: 'post',
		data: { type:'recom','product_id': product_id,'cart_id': cart_id  },
		success: function(data, status) { 
			$(".loadproduct").fadeOut("slow");
			$('#single_product_desc').html(data);
			setTimeout(function(){
				InitTableEvents();
				msg("initiated..................");
			},1000)
			
		}
	});
	
}

function show_product_details(product_id){
	strSKUSelected ="";
	var cart_id = $('#cart_id').val();
	$(".products-list .active").removeClass("active");
	$('#product_img'+product_id).addClass('active');
	$(".loadproduct").show();
	$.ajax({
		url: sc_backend_url+'Collection/get_single_product',
		type: 'post',
		data: { type:'manual','product_id': product_id,'cart_id': cart_id },
		success: function(data, status) { 
			$(".loadproduct").fadeOut("slow");
			$('#single_product').html(data);
			$('#single_product_desc').html(data);
			setTimeout(function(){
				InitTableEvents();
				msg("initiated..................");
			},1000)
			
		}
	});

	
}

var strSKUSelected;
function InitTableEvents(){
	// to select SKU ids========================================
	$("table.sku-table tr").on('click touch',function(){
		$("table.sku-table tr").removeClass('active');
		strSKUSelected = $(this).find('td').eq(0).text();
		if (!$(this).addClass('active')) {
			$(this).addClass('active');			
		}
	});
}

function more_product(){
	Search_product();
}

var intCurrentProductAdded = 0;
var isProdAddClicked = true;
function add_in_list(product_id,image_name){
	
	// if(strSKUSelected.length==0){
	// 	alert("Please select sku of selected product."); 
	// } else {

		intCurrentProductAdded = product_id;
		$('.loader').show();
		if(product_id!='' && isProdAddClicked == true)
		{
			isProdAddClicked = false;
			msg('1. isProdAddClicked: '+isProdAddClicked);
			$('#product_img'+product_id).addClass('selected');
			$.ajax({
				url: sc_backend_url+'Collection/add_in_list',
				type: 'post',
				data: { 'product_id': product_id,'image_name': image_name,'sku_id':strSKUSelected },
				success: function(data, status) { 	
					$('#product_list_b').append(data);
					$('#product_list_curation').append(data);	
					isProdAddClicked = true;
					$('.loader').hide();
					msg('2. isProdAddClicked: '+isProdAddClicked);	
				}
			});

		}else
		{
			console.log('Select Product');
		}
	//}

	setTimeout(function(){
			var isCurate = CurationCost();
			if(isCurate == false){
				alert('You have exceeded your curation cost limit.');

				//$("#product_list_curation li").last().remove();
				ReCheckCurationLimit();
			}
	},500);	
	
}

function delete_product_from_list(id){
	$('#list_'+id).remove();
	setTimeout(function(){ ReCheckCurationLimit();
		CurationCost(); },100);
}


function save_product_data(){
	var segment = document.getElementById("popup_button").getAttribute("data-attr");
	//alert('segment--'+segment);
	/*var strURL = window.location;
	strURL = strURL.toString();
	strURL = strURL.split("/");
	controller = strURL.pop();
	alert(strURL);*/
	var inventory_check = '';
	var user_id = $('#user_id').val();
	var collection_products = [];
	var type = $('#popup_page_type').val();
	type='scbox-order-products';
	var arrNewProductAdded = [];
	//====================
	$('#product_list_curation li').each(function(){
		if($(this).hasClass('new_item')){
			arrNewProductAdded.push($(this).find('img').attr('id'));
		}
	});



	/*$('#product_list_b li').each(function(e){
		collection_products.push([$(this).attr('id'),$(this).attr('sku_id')]);
		//collection_products.push($(this).attr('sku_id'));
	});*/

	$('#product_list_curation li').each(function(e){
		collection_products.push([$(this).attr('id'),$(this).attr('sku_id')]);
		//collection_products.push($(this).attr('sku_id'));
	});
	console.log('collection_products==='+collection_products);
	console.log(collection_products.length);
	console.log('type=='+type);

		
	if(collection_products.length > 0){
		
		if(type=='stylist-abundant-cart')
		{
			$.ajax({
			url: sc_backend_url+controller+'/get_product_list',
			type: 'post',
			data: { 'segment':segment,'collection_products': collection_products,'user_id':user_id},
			success: function(data, status) { 

				$(".loader").fadeOut("slow");	
				if(segment == 'add-collection'){
					$('#final_list').html('');
					$('#final_list').append(data);
					$("#product_array").val(collection_products);
				}else if(segment == 'edit-collection'){
					$('#final_list').append(data);
					$("#product_array").val(collection_products);
				}else if(segment == 'stylist-abundant-cart')
				{
					$('#StylistAddedProduct').append(data);
					console.log(collection_products);
				}
				$( "#products-popup").modal( 'hide' );
			}
			});
		}else if(type=='scbox-order-products')
		{ 
				var intTotalProductCost = 0;
				$("#product_list_curation li").each(function(e,value){
					intTotalProductCost = intTotalProductCost+Number($(this).find('span.price').text());
				});
				$('.curated_amount').text(intTotalProductCost);
				 var intMaxBoxLimitAmount = Number($('#max_box_curation_limit').val());
				 var intTotal = intMaxBoxLimitAmount-intTotalProductCost;
				if (intTotal<0) {
					alert('Either remove products or increase the box limit to continue.');
				}else
				{

				 	if(inventory_check=='' ){
				 		$.ajax({
								url: sc_backend_url+controller+'/check_product_inventory',
								type: 'post',
								data: { 'segment':type,'new_products':arrNewProductAdded },
								success: function(data, status) {
										
									data_arr = JSON.parse(data);									
									inventory_check = data_arr.nonstock_products;
									if(inventory_check)
									{
										alert('Currently product '+inventory_check+' are out of stock');
										$.each( arrNewProductAdded, function( key, value ) {
										  //alert( key + ": " + value );
										  delete_product_from_list(value);
										});										
									}
									inventory_check = 'ok';
									arrNewProductAdded = data_arr.stock_products.split(",");
									}
								});
					 	}{

					 	}
					 	
					 	if(arrNewProductAdded!='')
					 	{	
							var selorder = $('#selected_orderid').val();
							if(confirm('Are you sure you want this order id - '+selorder)){ 
								$(".loader").show();

									$.ajax({
									url: sc_backend_url+controller+'/get_product_list',
									type: 'post',
									data: { 'segment':type,'collection_products': collection_products,'user_id':user_id, 'selorderid':selorder, 'new_products':arrNewProductAdded },
									success: function(data, status) {

										$(".loader").fadeOut("slow");
										$('#product_list_b').empty();
										$( "#products-popup").modal( 'hide' );	
										//location.reload();
										if(status=='success')
										{
											if(confirm('Products have been added to your box - '+selorder+'.  Are you sure you want to exit.')){ 
												self.close();
											}
										}							
																
										
										}
									});
							}
						}else
						{
							alert('Add new product for curation');
						}
				}
			
		}
			
		
	}else{
		alert('please select product first');
	}
	
}

function remove_product(id){
	$('#final_list_'+id).remove();
}

function change_list(product_id){
	var product_ids = $("#product_ids").val();
	// alert(product_ids);
	if(product_ids.indexOf(","+product_id)>0){
		// alert("1");
		var product_ids = product_ids.replace(","+product_id, "");
	}else if(product_ids.indexOf(product_id+",") == 0){
		// alert("2");
		var product_ids = product_ids.replace(product_id+",", "");
	}else if((product_ids.indexOf(product_id)) == 0){
		// alert("3");
		var product_ids = product_ids.replace(product_id, "");
	}else {
		// alert("4");
	}
	// alert(product_ids);
	document.getElementById("product_ids").value = product_ids;
}

function selectOrder(orderid)
{	
	$('#selected_orderid').val(orderid);	
	$('#popup_page_type').val('scbox-order-products');	
	ShowSelectedproducts(orderid,'0');
	//$('#product_list_curation').empty();
}

function delete_orderprd(orderid,productid)
  { 
    var result = confirm("Are you sure you want to remove this product from the order?");
    //var selorder = $('#selected_orderid').val();
    //alert('selorder=='+orderid+'=productid='+productid);
    if(result && orderid!='' && productid!='')
    {
        $.ajax({
                url:sc_backend_url+"Stylist/remove_order_product",
                type: 'POST',
                data: {'selected_orderid':orderid, 'orderprdid' : productid},
                success: function(response)
                {    
                   if(response=='1')
                  {
                  	delete_product_from_list(productid);
                    setTimeout(function() {$('#productDetail').modal('hide');}, 1000);
          			//dispalyOrderDetail(<?php echo $this->uri->segment(3); ?>);
                  }
                },
                error: function(xhr)
                {

                }
              });
    }
   
  }

function getProductRecommendation(cartid,orderid)
  {
  	var type = $('#popup_page_type').val();	
  	$('#selected_orderid').val(orderid);

  	ShowSelectedproducts(orderid,'0');
  	if(cartid!='')
  	{
	  	$.ajax({			   
			   url: sc_frontend_url+'Category/get_order_recommendation',
			   type: 'post',
			   data: { 'type' : type,'cartid':cartid},
			   dataType: "json",
			   success: function(data, status) {
				   var categoryProductList 				= data.category_product_list;
				   var userCategorySelection 			= data.user_category_selection;
				   var userAttributeSelection 			= data.user_attribute_selection;
				   var userFirstlevelcategorySelection 	= data.user_firstlevelcategory_selection;
				   var categoryProductListData 			= data.category_product_list_data;
				   var numofproducts 					= data.numofproducts;
				  
				 $('.loaders').css("display", "none"); 
						var html = '';	var mrp_prodprice = '';
						html += '<div class="col-sm-10" id="categories-adjust">'; 
						html += '<input id="popup_page_type" name="popup_page_type" type="hidden" value="scbox-order-recom-products">';
						html += '<input id="numofproducts" name="numofproducts" type="hidden" value="'+numofproducts+'">';
						$.each(categoryProductListData, function(i, item) {	
							html += '<div class="col">';    
							html += '<div style="font-weight: 700;text-transform: uppercase;">'+item.cat_name+'</div>';    
							html += '<div class="box box-primary box-body-products" style="height: 420px;overflow: scroll;">';
							html += '<div class="products-list-wrp">';
							html += '<div class="loaders"></div>';
							html += '<ul id="recom_product_list" class="products-list product-list-in-box product-list-inline hover search-results text-center">';
								if(item.cat_data !=null){
									var prod_list = item.cat_data.product_list; 
									$.each(prod_list, function(j, itemData){
									var productZoomImage 		= sc_backend_url+'assets/products/'+itemData.product_img_name;
									var productExtraImagePath 	= sc_backend_url+'imgpop/getimg/'+itemData.product_id;
									
									//alert(itemData.mrp+'___________'+itemData.discount_price);
									//alert(productExtraImagePath);
									 html += '<li class="item" cat-name="'+item.cat_name+'" cat-id="'+item.cat_id+'">';
									 html += '<div id="product_img'+itemData.product_id+'" class="product-img" onclick="show_product('+itemData.product_id+','+data.cart_id+');" data-toggle="tooltip" data-placement="top" data-html="true" data-original-title="" title="">';
									 html += '<img data-img-link="'+itemData.product_img+'" class="lazy" id="'+itemData.product_id+'" src="'+itemData.product_img+'" width="80px" height="100px" onerror="this.onerror=null; this.src=\'https://i.imgur.com/Qu4zXlp.jpg\'";">';
									 html += '</div>';
									 html += '<div class="desc" style="height: 85px;text-align: left;">';
									 html += '<div>CTC: <i class="fa fa-inr"></i>'+itemData.discount_price+'</div>';
									 if(itemData.mrp!='')
									 {
									 	mrp_prodprice = itemData.mrp;
									 }else
									 {
									 	mrp_prodprice = itemData.discount_price
									 }
									 
									 //html += '<div>MRP: <i class="fa fa-inr"></i>'+(itemData.mrp!='')?itemData.mrp:itemData.discount_price+'</div>';
									 html += '<div>MRP: <i class="fa fa-inr"></i>'+mrp_prodprice+'</div>';									 
									 html += '<div>Seller:'+itemData.brand_name+'</div>';
									 html += '<div>Brand:'+itemData.product_store_name+'</div>'; 
									 html += '<div class="text-center">';
									 html += '<button id="product_check'+itemData.product_id+'" onclick="curation_product_add('+itemData.product_id+',\''+itemData.product_img+'\',\''+itemData.sku_number+'\','+item.cat_id+')" type="button" class="btn btn-box-tool">ADD</button>';
									 html += '<button class="btn btn-box-tool" style="left: 0 !important;" onclick="get_product_extraimage('+itemData.product_id+',\''+productZoomImage+'\')" >VIEW</button>';
									 html += '</div>';
									 html += '</div>'; 
									 html += '</li>';
									});
								};
							html += '</ul>';
							html += '</div>';
							html += '</div>';
							html += '</div>'; 
						});	
						html += '</div>';
					//$('.XHRLoadData').html(html);
	    			$('#recommended').html(html);
			   },
			   error: function(xhr, desc, err) {
					   console.log(err);
			   }
		 });
  	}else
  	{
  		alert('Cart Id missing');
  	}
  }
	
	
	
	
  function curation_product_add(product_id,image_name,strSKUSelected,cat_id){	
  		var numofproducts = $('#numofproducts').val();
		$('#product_img'+product_id).addClass('selected');
		var intCtr = $("#product_list_curation li").length;
		var isSameCategoryExist = false;
		msg("intCtr: "+intCtr);
		if(intCtr==0){
			PickProduct(product_id,image_name,strSKUSelected,cat_id);
			msg("PickProduct 1");
		} else {
		$('#product_list_curation > li').each(function(){
			msg($(this).attr('selected_cat_id')+" :::"+cat_id)
			if($(this).attr('selected_cat_id')==cat_id) {
				isSameCategoryExist = true;
			}
		});

		//Add product
		msg('numofproducts: '+$('#numofproducts').val() +"  ::  "+$('#numofproducts').attr('value')+" :isSameCategoryExist: "+isSameCategoryExist);

		if(numofproducts!=""){
			if(!isSameCategoryExist){
				PickProduct(product_id,image_name,strSKUSelected,cat_id);
				msg("PickProduct 3");
			}else{
				alert('You have already add product from this category. Please remove selected product to add this product.');
			}
		} else{
			PickProduct(product_id,image_name,strSKUSelected,cat_id);
			msg("PickProduct 2");			
		}		
	}
	// Check for cost curation limit
	setTimeout(function(){ 
			var isCurate = CurationCost();
			if(isCurate == false){
				alert('You have exceeded your curation cost limit.');
				//$("#product_list_curation li").last().remove();
				ReCheckCurationLimit();
			}
	},500);
}

function PickProduct(product_id,image_name,strSKUSelected,cat_id){
	msg("AD PRODUCT")
	$.ajax({
		url: sc_backend_url+'Collection/add_in_list',
		type: 'post',
		data: { 'product_id': product_id,'image_name': image_name,'sku_id':strSKUSelected,'cat_id':cat_id },
		success: function(data, status) { 	
			$('#product_list_curation').append(data);
			
		}
	});	
}


function save_product_curation_data(){
	var segment = document.getElementById("popup_button").getAttribute("data-attr");
	var intProductCtr = $("#product_list_curation li").length;
	var numofproducts = $('#numofproducts').val();
	/*
	//var numofproducts = $('#numofproducts').val();
	var intProductCtr = 0;
	var arrProductSelected = [];
	var arrProductSelectedCombined = [];
	var isProductUnique = false;
	$('#product_list_curation li').each(function(e){
		var intSelectedProdID = $(this).attr('id');
		intSelectedProdID = intSelectedProdID.substring(5,intSelectedProdID.length);
		//msg('intSelectedProdID: '+intSelectedProdID);
		var intTotalCategories = $('#categories-adjust > .col li');
		$('#categories-adjust > .col li').each(function(e){
			var intProdID = $(this).find('img');
			intProdID = intProdID.attr('id');
			msg(intProdID+" ::::: "+intSelectedProdID);
			if(intProdID == intSelectedProdID){
				msg('intProdID matched: '+intProdID)
				arrProductSelected.push(intProdID);
				intProductCtr++;
			}

		}); 

	});

	arrProductSelectedCombined = arrProductSelected.filter(function(item, index, inputArray) {
        return inputArray.indexOf(item) == index;
    });
	
	arrProductSelectedCombined.length == arrProductSelected.length?isProductUnique=true:isProductUnique=false;
	*/
	var user_id = $('#user_id').val();
	var curation_products = [];
	var type = $('#popup_page_type').val();	
	$('#product_list_curation li').each(function(e){
		curation_products.push([$(this).attr('id'),$(this).attr('sku_id')]);		
	});

	
	msg('intProductCtr: '+intProductCtr+'  ::'+numofproducts+'::');
	 //numofproducts ==""?intProductCtr=numofproducts:null;
	
	
	if(intProductCtr!=''){
	//if(intProductCtr == numofproducts){

		if(type=='scbox-order-recom-products')
		{ 
			var selorder = $('#selected_orderid').val();
			if(confirm('Are you sure you want this order id - '+selorder)){ 
				$(".loader").show();
			$.ajax({
			url: sc_backend_url+controller+'/get_product_list',
			type: 'post',
			data: { 'segment':type,'collection_products': curation_products,'user_id':user_id, 'selorderid':selorder },
			success: function(data, status) {
				$(".loader").fadeOut("slow");
				$('#product_list_curation').empty();
				$( "#products-popup1").modal( 'hide' );
				location.reload();
			}
			});
		}	
		}
	}else{
		if(numofproducts==undefined || numofproducts==null){
			alert('Please select unique products, for this order.');
		} else {
			//alert('Please select '+numofproducts+' unique products, for this order.');
		}
		
	}
	
}

function get_product_extraimage(product_id,imagepath){
	
	window.open(imagepath);
	// if(product_id!='')
	// {		
	// 	$.ajax({
	// 		url: sc_backend_url+'Collection/get_productImages',
	// 		type: 'post',
	// 		data: { 'product_id': product_id },
	// 		success: function(data, status) { 	
	// 			//$('#product_list_b').append(data);
				
	// 		}
	// 	});
	// }else
	// {
	// 	console.log('Select Product');
	// }	
}


function InitCurationCost(){
	CurationCost();
}

function CurationCost(){
	msg("in curation cost function");
	msg($("#product_list_curation li"));
	var intTotalProductCost = 0;
	$("#product_list_curation li").each(function(e,value){
		msg(e);
		msg($(this));
		msg($(this).find('span.price').text());
		intTotalProductCost = intTotalProductCost+Number($(this).find('span.price').text());
	});
	msg("intTotalProductCost: "+intTotalProductCost);
	return CheckCurationLimit(intTotalProductCost);
}

function CheckCurationLimit(value){
	 $('.curated_amount').text(value);
	 var intMaxBoxLimitAmount = Number($('#max_box_curation_limit').val());
	 var intTotal = intMaxBoxLimitAmount-value;
	 msg("value: "+ value+"  intMaxBoxLimitAmount: "+intMaxBoxLimitAmount+"  intTotal: "+intTotal);
	 $('.balance_amount').text(intTotal);
	 if(intTotal>-1){
	 	return true;
	 } else {
	 	return false;
	 }
}

function ReCheckCurationLimit(){
	var intTotalProductCost = 0;
	$("#product_list_curation li").each(function(e,value){
		intTotalProductCost = intTotalProductCost+Number($(this).find('span.price').text());
	});
	$('.curated_amount').text(intTotalProductCost);

	$('#overideCurationAmt').val(intTotalProductCost);

	 var intMaxBoxLimitAmount = Number($('#max_box_curation_limit').val());
	 var intTotal = intMaxBoxLimitAmount-intTotalProductCost;
	 msg("intTotalProductCost: "+ intTotalProductCost+"  intMaxBoxLimitAmount: "+intMaxBoxLimitAmount+"  intTotal: "+intTotal);
	 $('.balance_amount_1').text(Math.abs(intTotal));
	 $('.balance_amount').text(intTotal);
}

function overideCurationAmt()
{
	var pwd = $('#overideCurationPassword').val();
	var overideAmt = $('#overideCurationAmt').val();
	var type = $('#popup_page_type').val();
	var cart_id = $('#cart_id').val();
	var max_curation_limit_set = $('#max_curation_limit_set').val();

	if(pwd!='' && overideAmt!='')
	{
		if(overideAmt>max_curation_limit_set)
		{			

			if(pwd=='Style321')
			{
				if(confirm('Are you sure you want to override curation amount.')){ 
					//$(".loader").show();
				$.ajax({
				url: sc_backend_url+'Product_curation/overideCurationAmt',
				type: 'post',
				data: { 'segment':type,'password': pwd, 'overideAmt':overideAmt, 'cart_id':cart_id },
				success: function(data, status){
					if(status=='success')
					{
						$('#max_box_curation_limit').val(overideAmt);
						var curatedAmt = parseInt($('.curated_amount').text());
						var balanceAmt = overideAmt-curatedAmt;
						$('.balance_amount').text(balanceAmt);
						alert('Curation amount Updated');
						$( "#curationModal").modal( 'hide' );
					}else
					{
						console.log('fail');
					}			
					
					}				
				});
				}			
			}else
			{
				alert('Enter Correct Password');
			}
		}else
		{
			alert('Override curation amount should be greater than existing maximum curation amount');
		}
	}else
	{
		alert('Enter Password and Amount');
	}	
}

