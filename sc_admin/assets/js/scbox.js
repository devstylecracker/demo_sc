function msg(m) { console.log(m); }

function validateForm(){
	var scbox_package = $('input[name=scbox_package]:checked', '#scbox-form').val();
	var box_price = $('#box_price').val();
	var scb_gender = $('#scb_gender').val();
	var email_exists = $('#email_exists').val();
	
	/* tab 1 validation */
	//email
	if(email_exists == '1'){
		$("#un_email").removeClass('hide');
		return false;
	}else{
		$("#un_email").addClass('hide');
	}
	
	// scbox_package validation 
	if(scbox_package > 0){
		if(scbox_package == 4){
			if(box_price < 0 || box_price == ''){
				$('#scbox_package-error2').removeClass('hide');
				$('#scbox_package-error').addClass('hide');
				return false;
			}	
		}
	}else {
		$('#scbox_package-error').removeClass('hide');
		return false;
	}
	$('#scbox_package-error').addClass('hide');
	$('#scbox_package-error2').addClass('hide');
	//gender 
	if(scb_gender != 1 && scb_gender != 2){
		$('#scb_gender-error').removeClass('hide');
		return false;
	}
	$('#scb_gender-error').addClass('hide');
	
	/* tab 1 validation */
	
	/* tab 2 validation */
	var user_body_shape = 0;
	var user_style_p1 = 0;
	var user_style_p2 = 0;
	var user_style_p3 = 0;
	var user_skintone = 0;
	var user_colors = 0;
	var user_prints = 0;
	var user_sizetop = 0;
	var user_sizebottom = 0;
	var user_sizefoot = 0;
	var user_sizeband = 0;
	var user_sizecup = 0;
	
	if(scb_gender == 1){
		user_body_shape = $('#women_pa').find("#user_body_shape").val();
		user_style_p1 = $('#women_pa').find("#user_style_p1").val();
		user_style_p2 = $('#women_pa').find("#user_style_p2").val();
		user_style_p3 = $('#women_pa').find("#user_style_p3").val();
		user_skintone = $('#women_pa').find("#user_skintone").val();
		user_colors = $('#women_pa').find("#user_colors").val();
		user_prints = $('#women_pa').find("#user_prints").val();
		user_sizetop = $('#women_pa').find("#user_sizetop").val();
		user_sizebottom = $('#women_pa').find("#user_sizebottom").val();
		user_sizefoot = $('#women_pa').find("#user_sizefoot").val();
		user_sizeband = $('#women_pa').find("#user_sizeband").val();
		user_sizecup = $('#women_pa').find("#user_sizecup").val();
		
		if(user_body_shape == 0 || user_style_p1 == 0 || user_style_p2 == 0 || user_style_p3 == 0 || user_skintone == 0 || user_colors == 0 || user_prints == 0 || user_sizetop == 0 || user_sizebottom == 0 || user_sizefoot == 0 || user_sizeband == 0 || user_sizecup == 0){
			$('#pa-error').removeClass('hide');
			return false;
		}
		
	}else if(scb_gender == 2){
		user_body_shape = $('#men_pa').find("#user_body_shape").val();
		user_style_p1 = $('#men_pa').find("#user_style_p1").val();
		user_style_p2 = $('#men_pa').find("#user_style_p2").val();
		user_style_p3 = $('#men_pa').find("#user_style_p3").val();
		user_skintone = $('#men_pa').find("#user_skintone").val();
		user_colors = $('#men_pa').find("#user_colors").val();
		user_prints = $('#men_pa').find("#user_prints").val();
		user_sizetop = $('#men_pa').find("#user_sizetop").val();
		user_sizebottom = $('#men_pa').find("#user_sizebottom").val();
		user_sizefoot = $('#men_pa').find("#user_sizefoot").val();
		
		if(user_body_shape == 0 || user_style_p1 == 0 || user_style_p2 == 0 || user_style_p3 == 0 || user_skintone == 0 || user_colors == 0 || user_prints == 0 || user_sizetop == 0 || user_sizebottom == 0 || user_sizefoot == 0){
			$('#pa-error').removeClass('hide');
			return false;
		}
		
	}
	$('#pa-error').addClass('hide');
	/* tab 2 validation */
	
	/* tab 3 validation */
	
	var user_profession = $('#user_profession').val();
	var user_occassion = $('#user_occassion').val();
	var user_occassion_text = $('#user_occassion_text').val();
	var user_wardrobe = $('#user_wardrobe').val();
	var user_willtoexpmnt = $('#user_willtoexpmnt').val();
	var scbox_brand = $('#scbox_brand').val();
	return true;
	/* tab 3 validation */
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function validateEmail(sEmail) {
  var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
  if (filter.test(sEmail)) {   
  return true;
  }else {   
    return false;
  }
}

function get_pa_attr(){
	var scb_gender = $('#scb_gender').val();
	if(scb_gender == 1){
		$('#women_pa').removeClass('hide');
		$('#men_pa').addClass('hide');
	}else if(scb_gender == 2){
		$('#men_pa').removeClass('hide');
		$('#women_pa').addClass('hide');
	}
}

function add_checked(){
	$("#scbox_package_4").prop( 'checked', true );
}