$( document ).ready(function() {

    $("#approved").click(function(){
    	var product_id = $('#product_id').val();
    	product_approve(product_id);
    });

    $("#reject").click(function(){
    	var product_id = $('#product_id').val();
    	var reject_reason = $('#reject_reason').val();
    	product_reject(product_id,reject_reason);
    });
});
 

/*Product Apprroval*/
function product_approve(product_id){
	var conf = confirm('Are you sure you want to approve');
	if(conf){
		$.ajax({
			url: baseUrl+'/product_management/product_approve',			
			type: 'post',
			data: { 'type' : 'product_approve','product_id':  product_id },
			success: function(data, status) {
				if(status == 'success'){
					 alert('Approved successfully');
              		  window.location = baseUrl+'/product_management/';              		 
				} else
	            {
	              	 alert('OOPS!something went wrong');
	            }
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
		});
	}
}

/* Reject Reason */
function product_reject(product_id,reason){

	if(!$('#product_reject_reason').is(":visible")){
		$('#product_reject_reason').show();
	}else{
		var reject_reason = $('#product_reject_reason').val();
		if(reject_reason.trim() == ''){
			alert("Please Enter Product Reject Reason");

		}else{
			var conf = confirm('Are you sure you want to Reject the Product');
			if(conf){
			
				$.ajax({
					url: baseUrl+'/product_management/product_reject',					
					type: 'post',
					data: { 'type' : 'product_reject','product_id':  product_id, 'reason' :reject_reason },
					success: function(data, status) {
						if(status == 'success'){
							 alert('Rejected successfully');
		              		
              		 	 window.location = baseUrl+'/product_management/';      	              		  
						} else
			            {
			              	 alert('OOPS!something went wrong');
			            }
					},
					error: function(xhr, desc, err) {
						console.log(err);
					}
				});
			}

		}

	}

}
/*Extra image delete*/
function delete_extra_img(id){
  if (confirm("Are you sure?")) {
  		
    	$.ajax({
			url: baseUrl+'/product_management/delete_extra_img',			
			type: 'post',
			data: { 'type' : 'delete_extra_img','image_id':  id },
			success: function(data, status) {
				    location.reload();
			},
			error: function(xhr, desc, err) {
				console.log(err);
			}
		});


  }
   return false;
}

function addnew_row(id){
	var new_id = parseInt($('#total_rows').val())+1;
	var rowhtml = '<div class="row" id="row'+new_id+'"><div class="col-md-3"><input type="text" name="size'+new_id+'" id="size'+new_id+'" placeholder="Size" class="form-control"></div><div class="col-md-3"><input type="text" name="qty'+new_id+'" id="qty'+new_id+'" placeholder="Qty" class="form-control"></div><div class="col-md-3"><input type="text" name="product_sku'+new_id+'" id="product_sku'+new_id+'" placeholder="Product SKU" class="form-control"></div><div class="col-md-3"><span class="btn btn-primary btn-xs btn-delete" onclick="remove_row('+new_id+');"><i class="fa fa-minus"></i></span></div></div>';
	$('#total_rows').val(new_id);
	$('#product_inventory').append(rowhtml);
}

function remove_row(id){
	$('#row'+id).remove();
}

function quick_edit(id){
	var product_name = $('#product_name'+id).val();
	var product_slug = $('#product_slug'+id).val();
	var price = $('#price'+id).val();
	var product_status = $('#product_status'+id).val();
	var category = $('#category'+id).val();
	var product_tags = $('#product_tags'+id).val();
	var compare_price = $('#compare_price'+id).val();

	$.ajax({
			url: baseUrl+'product_management/quick_edit',			
			type: 'post',
			data: { 'product_name':  product_name,'product_slug':product_slug,'price':price,'product_status':product_status,'id':id,'category':category,'product_tags':product_tags,'compare_price':compare_price },
			success: function(data, status) {
				    alert('Updated Successfully');
				     $('.btn-quick-close').trigger('click');
				     $('#product_name_'+id).html(product_name);
				     $('#price_'+id).html(price);
			},
			error: function(xhr, desc, err) {
				
			}
		});
}